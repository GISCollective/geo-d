{
  "layers": {
    "aerodrome_label": {
      "filter": [
        "==", "aeroway", "aerodrome"
      ],
      "fields": {
        "name": "name",
        "iata": "iata",
        "ele": "ele",
        "icao": "icao",
        "name_de": ["name:de", "name", "name:en"],
        "name_en": ["name:en", "name"],
        "ele_ft": {
          "from": "ele",
          "transform": "toFeet"
        },
        "class": {
          "id": "id",
          "from": [ "aerodrome", "aerodrome:type" ],
          "default": "other",
          "enum": [ "international", "public", "regional", "military", "private", "other" ]
        }
      }
    },

    "aeroway": {
      "filter": [
        "has", "aeroway"
      ],
      "fields": {
        "id": "id",
        "ref": "ref",
        "class": {
          "from": ["aeroway", "area:aeroway"],
          "enum": [ "aerodrome", "heliport", "runway", "helipad", "taxiway", "apron" ]
        }
      }
    },

    "boundary": {
      "filter": [
        "==", "boundary", "administrative"
      ],
      "fields": {
        "id": "id",
        "admin_level": "admin_level",
        "maritime": "maritime",
        "disputed": "disputed"
      }
    },

    "building": {
      "filter": [
        "has", "building"
      ],
      "fields": {
        "id": "id",
        "render_min_height": "height",
        "render_height": "height"
      }
    },

    "housenumber": {
      "filter": [
        "has", "addr:housenumber"
      ],
      "fields": {
        "id": "id",
        "housenumber": "addr:housenumber"
      }
    },

    "landcover": {
      "filter": [
        "any",
          [ "has", "landuse" ],
          [ "has", "natural" ],
          [ "has", "surface" ],
          [ "has", "landcover" ]
      ],
      "fields": {
        "id": "id",
        "class": {
          "from": [ "landuse", "surface", "natural", "amnety", "leisure", "tourism", "landcover" ],
          "enum": [ "farmland", "ice", "wood", "grass", "wetland", "sand" ]
        },
        "subclass": {
        "id": "id",
          "from": [ "landuse", "surface", "natural", "amnety", "leisure", "tourism", "landcover" ],
          "enum": [ "allotments", "farm", "farmland", "orchard", "plant_nursery", "vineyard", "grass", "grassland",
              "glacier", "meadow", "forest", "village_green", "recreation_ground", "park", "garden", "wetland", "grassland",
              "bog", "swamp", "wet_meadow", "marsh", "reedbed", "saltern", "tidalflat", "saltmarsh", "mangrove", "beach", "sand" ]
        }
      }
    },

    "landuse": {
      "filter": [ "has", "landuse" ],
      "fields": {
        "id": "id",
        "class": {
          "from": [ "amenity", "tourism", "landuse", "leisure" ],
          "enum": [ "school", "university", "kindergarten", "college", "library", "hospital", "railway", "cemetery", "military",
              "residential", "commercial", "industrial", "retail", "stadium", "pitch", "playground", "theme_park", "bus_station", "zoo" ]
        }
      }
    },

    "mountain_peak": {
      "filter": [ "==", "natural", "peak" ],
      "fields": {
        "id": "id",
        "name_de": ["name:de", "name", "name:en"],
        "name_en": ["name:en", "name"],
        "name": "name",
        "ele": "ele",
        "ele_ft": {
          "from": "ele",
          "transform": "toFeet"
        },
        "rank": "rank"
      }
    },

    "park": {
      "filter": [ "any",
        [ "==", "boundary", "national_park" ],
        [ "==", "leisure", "nature_reserve" ]
      ],
      "fields": {
        "id": "id",
        "class": {
          "from": ["boundary", "leisure"],
          "enum": ["national_park", "nature_reserve"]
        }
      }
    },

    "place": {
      "filter": [ "has", "place"],
      "fields": {
        "id": "id",
        "name_de": ["name:de", "name", "name:en"],
        "name_en": ["name:en", "name"],
        "name": "name",
        "capital": {
          "from": "admin_level",
          "enum": ["2", "4"]
        },
        "iso_a2": ["country_code_iso3166_1_alpha_2", "iso_a2"],
        "class": {
          "from": "palce",
          "enum": [ "continent", "country", "state", "city", "town", "village", "hamlet", "suburb", "neighbourhood", "isolated_dwelling" ]
        },
        "rank": "rank"
      }
    },

    "poi": {
      "filter": [ "has", "amenity"],
      "fields": {
        "id": "id",
        "name_de": ["name:de", "name", "name:en"],
        "name_en": ["name:en", "name"],
        "name": "name",
        "agg_stop": {
          "from": "uic_ref",
          "enum": [ "1" ]
        },
        "class": {
          "from": ["amenity", "leisure", "landuse", "railway", "station", "sport", "tourism", "information", "religion", "shop"]
        }
      }
    },

    "transportation": {
      "filter": [ "any",
        [ "has", "highway" ],
        [ "has", "railway" ]
      ],
      "fields": {
        "layer": "layer",
        "service": {
          "from": "service",
          "enum": [ "spur", "yard", "siding", "crossover", "driveway", "alley", "parking_aisle" ]
        },
        "level": "level",
        "brunnel": [ "bridge", "tunnel", "ford" ],
        "ramp": "ramp",
        "oneway": {
          "from": "oneway",
          "enum": ["-1", "0", "1"]
        },
        "class": {
          "from": [ "highway", "railway", "aerialway", "route" ],
          "enum": [ "motorway", "trunk", "primary", "secondary", "tertiary", "minor", "service", "track", "path",
            "raceway", "rail", "transit", "cable_car", "ferry" ]
        },
        "subclass": {
          "from": ["railway", "highway", "public_transport"],
          "enum": [ "rail", "narrow_gauge", "preserved", "funicular", "subway", "light_rail",
              "monorail", "tram", "pedestrian", "path", "footway", "cycleway", "steps", "bridleway",
              "corridor", "platform" ]
        }
      }
    },

    "transportation_name": {
      "filter": [ "has", "highway" ],
      "fields": {
        "id": "id",
        "layer": "layer",
        "name": {
          "from": ["name", "ref"]
        },
        "name_de": {
          "from": ["name:de", "name", "name:en"]
        },
        "name_en": {
          "from": ["name:en", "name"]
        },
        "ref": "ref",
        "indoor": {
          "from": "indoor",
          "enum": ["1"]
        },
        "ref_length": {
          "from": "ref",
          "transform": "textLength"
        },
        "class": {
          "from": [ "highway", "railway", "aerialway", "route" ],
          "enum": [ "motorway", "trunk", "primary", "secondary", "tertiary", "minor", "service",
            "track", "path", "raceway", "rail", "transit" ]
        },
        "subclass": {
          "from": "highway",
          "enum": [ "pedestrian", "path", "footway", "cycleway", "steps", "bridleway", "corridor", "platform" ]
        },
        "network": {
          "from": "network",
          "default": "road",
          "enum": [ "us-interstate", "us-highway", "us-state", "ca-transcanada", "gb-motorway", "gb-trunk", "road" ]
        }
      }
    },

    "water": {
      "filter": [ "any",
        [ "has", "waterway" ],
        [ "==", "class", "ocean" ]
      ],
      "fields": {
        "id": "id",
        "class": {
          "from": "waterway",
          "enum": ["ocean", "lake", "river"]
        }
      }
    },

    "waterway": {
      "filter": [ "has", "waterway" ],
      "fields": {
        "id": "id",
        "name_de": ["name:de", "name", "name:en"],
        "name_en": ["name:en", "name"],
        "name": "name",
        "brunnel": {
          "from": "brunnel",
          "enum": ["bridge", "tunnel"]
        },
        "class": {
          "from": "waterway",
          "enum": [ "stream", "river", "canal", "drain", "ditch" ]
        }
      }
    }
  }
}
