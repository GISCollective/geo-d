module app;

import vibe.http.server;
import vibe.http.router;
import vibe.core.core;
import vibe.data.json;

import std.functional;
import std.zlib;
import std.conv;
import std.string;
import std.file;
import std.path;
import std.stdio;
import std.range;
import std.functional;
import std.exception;

import geo.osm.file;
import geo.osm.format;
import geo.osm.conv;
import geo.osm.storage.memory;
import geo.osm.storage.base;
import geo.xyz;
import geo.conv;
import geo.vector;
import geo.mbtiles;
import geo.feature;
import geo.geometries;
import geo.osm.storage.file;
import geo.osm.fileIndex;

CachedMBTiles tiles;
//IVtTiles tiles;

////
void save(string layerName, const Feature feature, uint z, uint x, uint y) {
  VectorFeature vectorFeature;

  if(feature.geometry.peek!Point !is null) {
    if(z < 14) {
      return;
    }

    auto point = feature.geometry.get!Point;
    vectorFeature = point.toVectorFeature(x, y, z);
  }

  if(feature.geometry.peek!LineString !is null) {
    auto line = feature.geometry.get!LineString;
    try {
      vectorFeature = line.toVectorFeature(x, y, z, 4096u, 3u);
    } catch(Exception e) {
      // writeln("can't process line feature on tile ", x, " ", y, " ", z);
      return;
    }
  }

  if(feature.geometry.peek!Polygon !is null) {
    auto polygon = feature.geometry.get!Polygon;

    try {
      vectorFeature = polygon.toVectorFeature(x, y, z, 4096u, 3u);
    } catch(Exception e) {
      // writeln("can't process polygon `", polygon ,"` feature on tile ", x, " ", y, " ", z);
      return;
    }
  }

  if(vectorFeature.geometry.commands.length == 0) {
    return;
  }

  ///
  /// set feature on tile
  const ymax = 1 << z;
  const translated_y = ymax - y - 1;

  auto tile = tiles.getVT(z,x,translated_y);

  long layerIndex = -1;

  foreach(i, layer; tile.layers) {
    if(layer.name == layerName) {
      layerIndex = i;
    }
  }

  if(layerIndex == -1) {
    layerIndex = tile.layers.length;
    tile.layers ~= Layer();
    tile.layers[layerIndex].name = layerName;
    tile.layers[layerIndex].keys = [""];
  }

  tile.layers[layerIndex].features ~= vectorFeature;

  foreach(string key, Json value; feature.properties) {
    if(value.type == Json.Type.bigInt || value.type == Json.Type.int_) {
      tile.layers[layerIndex].addProperty(key, value.to!long);
    } else if(value.type == Json.Type.float_) {
      tile.layers[layerIndex].addProperty(key, value.to!string.to!double);
    } else {
      tile.layers[layerIndex].addProperty(key, value.to!string);
    }
  }

  tiles.setVT(z,x,translated_y, tile);
}

///
void onMatch(const string layer, const Feature feature) @trusted {
  auto layerFile = buildPath("output", layer ~ ".txt");
  string tileFile;

  foreach(z; 10..15) {
    auto indexes = feature.geometry.tileIndexes(z);

    if(indexes.length == 0) {
      continue;
    }

    ///enforce(indexes.length > 0, "`" ~ feature.toJson.to!string ~ "` has no tile index.");

    foreach(pair; indexes) {
      save(layer, feature, z, pair[0], pair[1]);

      // if(pair[0] == 8798 && pair[1] == 5370 && z == 14) {
      //   tileFile = buildPath("output/debug", pair[0].to!string ~ "-" ~ pair[1].to!string ~ ".txt");
      // }
    }
  }

  // if(feature.properties.length > 1) {
  //  append(layerFile, feature.toJson.to!string ~ "\n");

  //   if(tileFile != "") {
  //    append(tileFile, feature.toJson.to!string ~ "\n");
  //   }
  // }
}

int main(string[] args) {
  string source = args[1].absolutePath;

  if(!source.exists) {
    writeln("The source file `", source, "` does not exist.");
    return 1;
  }

  if("output".exists) {
    rmdirRecurse("output");
  }

  auto tilesPath = buildPath("output", "tiles");

  mkdir("output");
  mkdir(buildPath("output", "debug"));
  mkdir(tilesPath);

  writeln("Indexing ", source, " ...");
  auto indexFileData = new FileRange(source);
  auto index = new OsmFileIndex();
  index.indexFileRange(indexFileData);
  writeln("Done indexing.");

  writeln("Parsing ", source, " ...");
  auto data = new FileRange(source);
  auto blocks = new OsmPbfBlockRange!FileRange(data);

  auto nodeStorage = new FileOsmNodeStorage(index, new FileRange(source));
  auto wayStorage = new FileOsmWayStorage(index, new FileRange(source));
  auto storage = new OsmStorage(nodeStorage, wayStorage);

  auto osmImport = readText("layers.json").parseJsonString.deserializeJson!OsmImport;
  osmImport.onMatch = (&onMatch).toDelegate;

  if(std.file.exists("output.mbtiles")) {
    std.file.remove("output.mbtiles");
  }

  // scope fileTiles = new PBFFileVtTiles(tilesPath);
  // tiles = fileTiles;

  scope localTiles = new CachedMBTiles("output.mbtiles");
  tiles = localTiles;

  tiles.name = "Some Tiles";
  tiles.format = "pbf";
  tiles.bounds = "12.26,51.849,14.699,52.994";
  tiles.minzoom = 12;
  tiles.maxzoom = 14;
  tiles.attribution = `<a href="http://www.openstreetmap.org/about/" target="_blank">&copy; OpenStreetMap contributors</a>`;
  tiles.description = `some test tiles`;
  tiles.json = `{"vector_layers":[{"maxzoom":14,"fields":{"class":"String"},"minzoom":0,"id":"water","description":""},{"maxzoom":14,"fields":{"name:mt":"String","name:pt":"String","name:az":"String","name:ka":"String","name:rm":"String","name:ko":"String","name:kn":"String","name:ar":"String","name:cs":"String","name_de":"String","name:ro":"String","name:it":"String","name_int":"String","name:ru":"String","name:pl":"String","name:ca":"String","name:lv":"String","name:bg":"String","name:cy":"String","name:fi":"String","name:he":"String","name:da":"String","name:de":"String","name:tr":"String","name:fr":"String","name:mk":"String","name:nonlatin":"String","name:fy":"String","name:be":"String","name:zh":"String","name:sr":"String","name:sl":"String","name:nl":"String","name:ja":"String","name:lt":"String","name:no":"String","name:kk":"String","name:ko_rm":"String","name:ja_rm":"String","name:br":"String","name:bs":"String","name:lb":"String","name:la":"String","name:sk":"String","name:uk":"String","name:hy":"String","name:sv":"String","name_en":"String","name:hu":"String","name:hr":"String","class":"String","name:sq":"String","name:el":"String","name:ga":"String","name:en":"String","name":"String","name:gd":"String","name:ja_kana":"String","name:is":"String","name:th":"String","name:latin":"String","name:sr-Latn":"String","name:et":"String","name:es":"String"},"minzoom":0,"id":"waterway","description":""},{"maxzoom":14,"fields":{"class":"String","subclass":"String"},"minzoom":0,"id":"landcover","description":""},{"maxzoom":14,"fields":{"class":"String"},"minzoom":0,"id":"landuse","description":""},{"maxzoom":14,"fields":{"name:mt":"String","name:pt":"String","name:az":"String","name:ka":"String","name:rm":"String","name:ko":"String","name:kn":"String","name:ar":"String","name:cs":"String","rank":"Number","name_de":"String","name:ro":"String","name:it":"String","name_int":"String","name:lv":"String","name:pl":"String","name:de":"String","name:ca":"String","name:bg":"String","name:cy":"String","name:fi":"String","name:he":"String","name:da":"String","ele":"Number","name:tr":"String","name:fr":"String","name:mk":"String","name:nonlatin":"String","name:fy":"String","name:be":"String","name:zh":"String","name:sl":"String","name:nl":"String","name:ja":"String","name:lt":"String","name:no":"String","name:kk":"String","name:ko_rm":"String","name:ja_rm":"String","name:br":"String","name:bs":"String","name:lb":"String","name:la":"String","name:sk":"String","name:uk":"String","name:hy":"String","name:ru":"String","name:sv":"String","name_en":"String","name:hu":"String","name:hr":"String","name:sr":"String","name:sq":"String","name:el":"String","name:ga":"String","name:en":"String","name":"String","name:gd":"String","ele_ft":"Number","name:ja_kana":"String","name:is":"String","osm_id":"Number","name:th":"String","name:latin":"String","name:sr-Latn":"String","name:et":"String","name:es":"String"},"minzoom":0,"id":"mountain_peak","description":""},{"maxzoom":14,"fields":{"class":"String"},"minzoom":0,"id":"park","description":""},{"maxzoom":14,"fields":{"admin_level":"Number","disputed":"Number","maritime":"Number"},"minzoom":0,"id":"boundary","description":""},{"maxzoom":14,"fields":{"class":"String"},"minzoom":0,"id":"aeroway","description":""},{"maxzoom":14,"fields":{"brunnel":"String","ramp":"Number","class":"String","service":"String","oneway":"Number"},"minzoom":0,"id":"transportation","description":""},{"maxzoom":14,"fields":{"render_min_height":"Number","render_height":"Number"},"minzoom":0,"id":"building","description":""},{"maxzoom":14,"fields":{"name:mt":"String","name:pt":"String","name:az":"String","name:ka":"String","name:rm":"String","name:ko":"String","name:kn":"String","name:ar":"String","name:cs":"String","name_de":"String","name:ro":"String","name:it":"String","name_int":"String","name:ru":"String","name:pl":"String","name:ca":"String","name:lv":"String","name:bg":"String","name:cy":"String","name:fi":"String","name:he":"String","name:da":"String","name:de":"String","name:tr":"String","name:fr":"String","name:mk":"String","name:nonlatin":"String","name:fy":"String","name:be":"String","name:zh":"String","name:sr":"String","name:sl":"String","name:nl":"String","name:ja":"String","name:lt":"String","name:no":"String","name:kk":"String","name:ko_rm":"String","name:ja_rm":"String","name:br":"String","name:bs":"String","name:lb":"String","name:la":"String","name:sk":"String","name:uk":"String","name:hy":"String","name:sv":"String","name_en":"String","name:hu":"String","name:hr":"String","class":"String","name:sq":"String","name:el":"String","name:ga":"String","name:en":"String","name":"String","name:gd":"String","name:ja_kana":"String","name:is":"String","name:th":"String","name:latin":"String","name:sr-Latn":"String","name:et":"String","name:es":"String"},"minzoom":0,"id":"water_name","description":""},{"maxzoom":14,"fields":{"name:mt":"String","name:pt":"String","name:az":"String","name:ka":"String","name:rm":"String","name:ko":"String","name:kn":"String","name:ar":"String","name:cs":"String","name_de":"String","name:ro":"String","name:it":"String","name_int":"String","name:ru":"String","name:pl":"String","name:ca":"String","name:lv":"String","name:bg":"String","name:cy":"String","name:fi":"String","name:he":"String","name:da":"String","name:de":"String","name:tr":"String","name:fr":"String","name:mk":"String","name:nonlatin":"String","name:fy":"String","name:be":"String","name:zh":"String","name:sr":"String","name:sl":"String","name:nl":"String","name:ja":"String","name:lt":"String","name:no":"String","name:kk":"String","name:ko_rm":"String","name:ja_rm":"String","name:br":"String","name:bs":"String","name:lb":"String","name:la":"String","name:sk":"String","name:uk":"String","name:hy":"String","name:sv":"String","name_en":"String","name:hu":"String","name:hr":"String","class":"String","name:sq":"String","network":"String","name:el":"String","name:ga":"String","name:en":"String","name":"String","name:gd":"String","ref":"String","name:ja_kana":"String","ref_length":"Number","name:is":"String","name:th":"String","name:latin":"String","name:sr-Latn":"String","name:et":"String","name:es":"String"},"minzoom":0,"id":"transportation_name","description":""},{"maxzoom":14,"fields":{"name:mt":"String","name:pt":"String","name:az":"String","name:ka":"String","name:rm":"String","name:ko":"String","name:kn":"String","name:ar":"String","name:cs":"String","rank":"Number","name_de":"String","name:ro":"String","name:it":"String","name_int":"String","name:ru":"String","name:pl":"String","name:ca":"String","name:lv":"String","name:bg":"String","name:cy":"String","name:hr":"String","name:fi":"String","name:he":"String","name:da":"String","name:de":"String","name:tr":"String","name:fr":"String","name:mk":"String","name:nonlatin":"String","name:fy":"String","name:be":"String","name:zh":"String","name:sr":"String","name:sl":"String","name:nl":"String","name:ja":"String","name:ko_rm":"String","name:no":"String","name:kk":"String","capital":"Number","name:ja_rm":"String","name:br":"String","name:bs":"String","name:lb":"String","name:la":"String","name:sk":"String","name:uk":"String","name:hy":"String","name:sv":"String","name_en":"String","name:hu":"String","name:lt":"String","class":"String","name:sq":"String","name:el":"String","name:ga":"String","name:en":"String","name":"String","name:gd":"String","name:ja_kana":"String","name:is":"String","name:th":"String","name:latin":"String","name:sr-Latn":"String","name:et":"String","name:es":"String"},"minzoom":0,"id":"place","description":""},{"maxzoom":14,"fields":{"housenumber":"String"},"minzoom":0,"id":"housenumber","description":""},{"maxzoom":14,"fields":{"name:mt":"String","name:pt":"String","name:az":"String","name:ka":"String","name:rm":"String","name:ko":"String","name:kn":"String","name:ar":"String","name:cs":"String","rank":"Number","name_de":"String","name:ro":"String","name:it":"String","name_int":"String","name:ru":"String","name:pl":"String","name:ca":"String","name:lv":"String","name:bg":"String","name:cy":"String","name:fi":"String","name:he":"String","name:da":"String","subclass":"String","name:de":"String","name:tr":"String","name:fr":"String","name:mk":"String","name:nonlatin":"String","name:fy":"String","name:be":"String","name:zh":"String","name:sr":"String","name:sl":"String","name:nl":"String","name:ja":"String","name:lt":"String","name:no":"String","name:kk":"String","name:ko_rm":"String","name:ja_rm":"String","name:br":"String","name:bs":"String","name:lb":"String","name:la":"String","name:sk":"String","name:uk":"String","name:hy":"String","name:sv":"String","name_en":"String","name:hu":"String","name:hr":"String","class":"String","name:sq":"String","name:el":"String","name:ga":"String","name:en":"String","name":"String","name:gd":"String","name:ja_kana":"String","name:is":"String","name:th":"String","name:latin":"String","name:sr-Latn":"String","name:et":"String","name:es":"String"},"minzoom":0,"id":"poi","description":""}]}`;

  foreach(index, block; blocks) {
    if(block.header.type == "OSMHeader") {
      continue;
    }

    auto decompressor = new UnCompress();
    ubyte[] uncompressedData = cast(ubyte[]) decompressor.uncompress(block.blob.zlib_data);

    auto primitiveBlock = readPrimitiveBlock(uncompressedData);

    auto ways = primitiveBlock.ways(nodeStorage).array;
    auto relations = refRelations(&primitiveBlock, storage);

    osmImport.add(ways);
    osmImport.add(relations);

    if(tiles.cacheSize > 1000) {
      tiles.flush;
      return 0;
    }

    //writeln(index, " ways:", ways.length, " relations:", relations.length, " cached tiles:", tiles.cacheSize);
    writeln(blocks.stats.toString);
  }

  tiles.flush;

  writeln("\n\nBlocks processing stats:");
  writeln(blocks.stats.toString);

  writeln("\n\nLayer property update stats:");
  osmImport.printStats;

  writeln("\n\nTiles processing stats:");
  writeln(tiles.stringStats);

  return 0;
}

/// dmd:  processed_blocks:747 processed_bytes:54_436_428 bytes_per_second:207_772 blocks_per_second:2
/// ldc2: processed_blocks:747 processed_bytes:54_436_428 bytes_per_second:380_674 blocks_per_second:5
