module app;

import std.path;
import std.file;
import std.path;
import std.stdio : writeln;
import std.zlib;
import std.range;
import std.algorithm;
import core.memory;

import geo.osm.format;
import geo.osm.storage.memory;
import geo.osm.storage.mongo;
import geo.osm.storage.base;
import geo.osm.file;
import geo.osm.conv;
import geo.feature;
import geo.algorithm;
import geo.osm.fileIndex;
import geo.osm.storage.file;

import vibe.db.mongo.mongo;
import vibe.data.json;
import vibe.core.core;
import vibe.core.log;

bool isPostalCode(const ref RefRelationFeature feature) {
  string boundary;

  if("boundary" in feature.properties) {
    boundary = feature.properties["boundary"].to!string;
  }

  return boundary == "postal_code";
}

int main(string[] args) {
  GC.disable;
  string source = args[1].absolutePath;

  if(!source.exists) {
    writeln("The source file `", source, "` does not exist.");
    return 1;
  }

  if(!"output".exists) {
    mkdir("output");
  }

  try {
    if(source.isDir) {
      auto dFiles = dirEntries(source,"*.{pbf}", SpanMode.depth);
      foreach (file; dFiles) {
        extractPostalCodes(file.name);
      }
    } else {
      extractPostalCodes(source);
    }
  } catch(Exception e) {
    logError(e.toString);
  }

  return 0;
}

void extractPostalCodes(string source) {
  immutable destination = buildPath("output", source.baseName ~ "-postalcodes.json");

  if(destination.exists) {
    destination.remove;
  }

  destination.write("[");
  scope(exit) destination.append("]");

  writeln("Indexing ", source, " ...");

  auto indexFileData = new FileRange(source);
  auto index = new OsmFileIndex();
  index.indexFileRange(indexFileData);

  writeln("Parsing ", source, " ...");
  auto data = new FileRange(source);
  auto blocks = new OsmPbfBlockRange!FileRange(data);

  auto nodeStorage = new FileOsmNodeStorage(index, new FileRange(source));
  auto wayStorage = new FileOsmWayStorage(index, new FileRange(source));
  auto storage = new OsmStorage(nodeStorage, wayStorage);

  PrimitiveBlock primitiveBlock;

  string glue;


  const(Feature)[] postalCodes;

  foreach(index, block; blocks) {
    if(block.header.type == "OSMHeader") {
      continue;
    }

    auto decompressor = new UnCompress();
    ubyte[] uncompressedData = cast(ubyte[]) decompressor.uncompress(block.blob.zlib_data);

    primitiveBlock = readPrimitiveBlock(uncompressedData);

    foreach(a; refRelations(&primitiveBlock, storage)) {
      if(isPostalCode(a)) {
        postalCodes ~= (cast() a).toFeature;
      }
    }

    foreach(item; postalCodes) {
      auto rings = createRings(item);

      foreach(ring; rings) {
        destination.append(glue);
        destination.append(ring.serializeToJson.toPrettyString);
        glue = ",\n";
      }
    }

    if(postalCodes.length > 0 || index % 100 == 0) {
      GC.collect;
      GC.minimize;

      writeln(index, " postalCodes:", postalCodes.length);
      postalCodes.length = 0;
    }

    writeln(blocks.stats.toString);
  }

  writeln(blocks.stats.toString);
}