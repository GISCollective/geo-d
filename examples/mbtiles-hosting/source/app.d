module app;

import vibe.http.server;
import vibe.http.router;
import vibe.core.core;
import vibe.data.json;

import std.functional;
import std.zlib;
import std.conv;
import std.string;
import std.file;
import std.path;

import geo.mbtiles;
import geo.style;
import geo.svg;
import geo.vector;

MBTilesReader tiles;

void main() {

  auto router = new URLRouter;
  router.get("/:z/:x/:y", &handleRequest);

  auto settings = new HTTPServerSettings;
  settings.port = 8080;

  ///scope localTiles = new MBTilesReader("/home/gedaiu/Documents/maps/2017-07-03_germany_berlin.mbtiles");
  scope localTiles = new MBTiles("../osm-import/output.mbtiles");
  tiles = localTiles;

  listenHTTP(settings, router);
  runApplication();
}

void handleRequest(HTTPServerRequest req, HTTPServerResponse res) {
  string rawY = req.params["y"];

  if(rawY.canFind(".")) {
    auto pieces = rawY.split(".");
    req.params["y"] = pieces[0];
    req.params["format"] = pieces[1];
  } else {
    req.params["format"] = "pbf";
  }

  int z = req.params["z"].to!int;
  int x = req.params["x"].to!int;
  int y = req.params["y"].to!int;
  string format = req.params["format"];

  auto decompressor = new UnCompress();
  ubyte[] data = tiles.get(z, x, y);

  if(data.length == 0) {
    res.statusCode = 404;
    res.writeBody("not found");
    return;
  }

  res.headers["Access-Control-Allow-Origin"] = "*";

  auto bodyWriter = res.bodyWriter();

  if(format == "svg") {
    res.headers["content-type"] = "image/svg+xml";
    ubyte[] uncompressedData = cast(ubyte[]) decompressor.uncompress(data);
    auto style = readText("../../test/style.json").parseMapboxStyleString;
    auto renderer = new TileRenderer(style);
    auto svg = renderer.render(uncompressedData.readTile);

    bodyWriter.write(svg);
    return;
  }

  if(format == "json") {
    res.headers["content-type"] = "text/json";
    ubyte[] uncompressedData = cast(ubyte[]) decompressor.uncompress(data);
    auto tile = uncompressedData.readTile;

    bodyWriter.write(tile.toProtoBuf.readTile.serializeToJson.toPrettyString);
    return;
  }

  if(format == "pbf") {
    res.headers["content-type"] = "application/x-protobuf";
    //res.headers["content-encoding"] = "gzip";

    import std.array;
    ubyte[] uncompressedData = cast(ubyte[]) decompressor.uncompress(data);
    bodyWriter.write(uncompressedData);
  }
}
