/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module geo.geometries;

import std.variant;
import std.conv;
import std.math;
import std.algorithm;
import std.exception;
import std.array;
import std.traits;
import std.range.primitives;

import vibe.data.json;
import vibe.data.bson;

import geo.json;
import geo.epsg;
import geo.algorithm : doIntersect, onSegment, onSameLine;

version(unittest) {
  import fluent.asserts;

  static this() {
    import fluentasserts.core.operations.arrayEqual;
    import fluentasserts.core.operations.registry;

    Registry.instance.register("*[][][]", "*[][][]", "equal", &arrayEqual);
  }
}

enum Direction : byte {
  Inside = 0,
  Left = 1,
  Right = 2,
  Top = 4,
  Bottom = 8
}

template IsSomePoint(T) {
   static if(is(T == PositionT!double) || is(T == EPSG3857) || is(T == EPSG4326) || is(T == double[])) {
     enum IsSomePoint = true;
   } else {
     enum IsSomePoint = false;
   }
}

long toBorderIndex(byte dir) {
  switch(dir) {
    case Direction.Left: return 0;
    case Direction.Top: return 1;
    case Direction.Right: return 2;
    case Direction.Bottom: return 3;
    default: return -1;
  }
}

/// The bounding box field in the OSM header. BBOX, as used in the OSM
/// header. Units are always in nanodegrees -- they do not obey
/// granularity rules.
struct BBox(T) {
  ///
  T left;

  ///
  T right;

  ///
  T top;

  ///
  T bottom;

  alias PointType = Unqual!(T)[];

  ///
  this(const T left, const T right, const T top, const T bottom) @nogc nothrow {
    if(left > right) {
      this.left = right;
      this.right = left;
    } else {
      this.left = left;
      this.right = right;
    }

    if(top < bottom) {
      this.top = bottom;
      this.bottom = top;
    } else {
      this.top = top;
      this.bottom = bottom;
    }
  }

  ///
  this(ref const T[][] pointList) inout @nogc nothrow {
    T l, r, t, b;

    if(pointList.length > 0) {
      l = pointList[0][0];
      r = pointList[0][0];
      t = pointList[0][1];
      b = pointList[0][1];
    }

    foreach (point; pointList) {
      if(point[0] < l) {
        l = point[0];
      }

      if(point[0] > r) {
        r = point[0];
      }

      if(point[1] > t) {
        t = point[1];
      }

      if(point[1] < b) {
        b = point[1];
      }
    }

    this.left = l;
    this.right = r;
    this.top = t;
    this.bottom = b;
  }

  static if(is(T == long)) {
    ///
    this(ref const EPSG3857[] pointList) @nogc nothrow {
      _this!(".x", ".y", "")(pointList);
    }
  }

  ///
  void _this(string lon, string lat, string conv, U)(ref const U list) {
    T l, r, t, b;

    if(list.length > 0) {
      mixin("l = list[0]" ~ lon ~ conv ~ ";");
      mixin("r = list[0]" ~ lon ~  conv ~";");
      mixin("t = list[0]" ~ lat ~  conv ~";");
      mixin("b = list[0]" ~ lat ~  conv ~";");
    }

    foreach (point; list) {
      mixin("auto _lon = point" ~ lon ~ conv ~";");
      mixin("auto _lat = point" ~ lat ~ conv ~";");

      if(_lon < l) {
        l = _lon;
      }

      if(_lon > r) {
        r = _lon;
      }

      if(_lat > t) {
        t = _lat;
      }

      if(_lat < b) {
        b = _lat;
      }
    }

    this.left = l;
    this.right = r;
    this.top = t;
    this.bottom = b;
  }

  ///
  this(ref const T[2][] pointList) inout {
    T[][] tmp = pointList.to!(T[][]);
    this(tmp);
  }

  ///
  this(ref const PositionT!T[] pointList) @nogc nothrow {
    _this!(".lon", ".lat", "")(pointList);
  }

  static if(is(T == double)) {
    this(ref const Bson pointList) {
      _this!("[0]", "[1]", ".to!double")(pointList);
    }

    this(ref EPSG4326[] pointList) @nogc nothrow {
      _this!(".longitude", ".latitude", "")(pointList);
    }

    this(const EPSG4326[] pointList) @nogc nothrow {
      _this!(".longitude", ".latitude", "")(pointList);
    }

    ///
    this(GeoJsonGeometry geometry) {
      Position[] points;

      if(geometry.type == "Point") {
        points = [ geometry.to!Point.coordinates ];
      }

      if(geometry.type == "MultiPoint") {
        points = geometry.to!MultiPoint.coordinates;
      }

      if(geometry.type == "LineString") {
        points = geometry.to!LineString.coordinates;
      }

      if(geometry.type == "MultiLineString") {
        points = geometry.to!MultiLineString.coordinates.joiner.array;
      }

      if(geometry.type == "Polygon") {
        points = geometry.to!Polygon.coordinates[0];
      }

      if(geometry.type == "MultiPolygon") {
        points = geometry.to!MultiPolygon
          .coordinates.map!(a => a[0]).joiner.array;
      }

      this(points);
    }
  }

  T dx() inout {
    return abs(right - left);
  }

  T dy() inout {
    return abs(bottom - top);
  }

  T[] center() inout {
    return [ left + dx, top + dy ];
  }

  T area() inout {
    if(dx == 0 || dy == 0) {
      return 0;
    }

    if(dx > T.max / dy) {
      return T.max;
    }

    if(dy > T.max / dx) {
      return T.max;
    }

    return dx * dy;
  }

  PointType point(T lon, T lat) inout nothrow {
    return [lon, lat];
  }

  ///
  auto lineTop() inout {
    return [point(left, top), point(right, top)];
  }

  ///
  auto lineBottom() inout {
    return [point(left, bottom), point(right, bottom)];
  }

  ///
  auto lineRight() inout {
    return [point(right, bottom), point(right, top)];
  }

  ///
  auto lineLeft() inout {
    return [point(left, bottom), point(left, top)];
  }

  ///
  auto allLines() inout {
    return [ lineLeft, lineTop, lineRight, lineBottom];
  }

  bool contains(T lon, T lat) inout {
    immutable isInsideHorizontally = lon >= left && lon <= right || lon >= right && lon <= left;
    immutable isInsideVertically = lat >= bottom && lat <= top || lat >= top && lat <= bottom;

    return isInsideHorizontally && isInsideVertically;
  }

  static if(!is(PointType == T[])) {
    ///
    bool contains(const T[] point) inout {
      return this.contains(point[0], point[1]);
    }
  }

  ///
  bool contains(U)(const PositionT!U position) inout {
    immutable isInsideHorizontally = position.lon > left && position.lon < right || position.lon > right && position.lon < left;
    immutable isInsideVertically = position.lat > bottom && position.lat < top || position.lat > top && position.lat < bottom;

    return this.contains(position.lon, position.lat);
  }

  ///
  bool contains(PointType point) inout {
    return this.contains(point[0], point[1]);
  }

  ///
  T[][] line(ubyte direction) inout {
    auto result = allLines(direction);

    if(result.length == 0) {
      return [];
    }

    return result[0];
  }

  ///
  T[][][] allLines(ubyte direction) inout {
    PointType[][] result;

    if(direction & Direction.Left) {
      result ~= lineLeft;
    }

    if(direction & Direction.Right) {
      result ~= lineRight;
    }

    if(direction & Direction.Bottom) {
      result ~= lineBottom;
    }

    if(direction & Direction.Top) {
      result ~= lineTop;
    }

    return cast(T[][][]) result;
  }

  ///
  ubyte relativeDirection(U)(const U position) inout {
    ubyte code;

    if(position[0] < left) code |= Direction.Left;
    else if (position[0] > right) code |= Direction.Right;

    if(position[1] > top) code |= Direction.Top;
    else if(position[1] < bottom) code |= Direction.Bottom;

    return code;
  }
}

/// Create a bbox from a geojson point
unittest {
  auto geometry = GeoJsonGeometry.fromJson(`{"type": "Point", "coordinates": [1,2]}`.parseJsonString);

  auto bbox = BBox!double(geometry);

  bbox.left.should.equal(1);
  bbox.right.should.equal(1);
  bbox.top.should.equal(2);
  bbox.bottom.should.equal(2);
}

/// Create a bbox from a geojson LineString
unittest {
  auto geometry = GeoJsonGeometry.fromJson(`{
  "type": "LineString",
  "coordinates": [[1.0, 1.0], [1.0, 2.0], [2.0, 3.0]]
  }`.parseJsonString);

  auto bbox = BBox!double(geometry);

  bbox.left.should.equal(1);
  bbox.right.should.equal(2);
  bbox.top.should.equal(3);
  bbox.bottom.should.equal(1);
}

/// Create a bbox from a geojson MultiPoint
unittest {
  auto geometry = GeoJsonGeometry.fromJson(`{
  "type": "MultiPoint",
  "coordinates": [[1.0, 1.0], [1.0, 2.0], [2.0, 3.0]]
  }`.parseJsonString);

  auto bbox = BBox!double(geometry);

  bbox.left.should.equal(1);
  bbox.right.should.equal(2);
  bbox.top.should.equal(3);
  bbox.bottom.should.equal(1);
}

/// Create a bbox from a geojson MultiLine
unittest {
  auto geometry = `{
    "type": "MultiLineString",
    "coordinates": [[[10, 10], [20, 20], [10, 40]], [[4, 4], [3, 3], [4, 2], [3, 1]]]
  }`.parseJsonString.deserializeJson!GeoJsonGeometry;

  auto bbox = BBox!double(geometry);

  bbox.left.should.equal(3);
  bbox.right.should.equal(20);
  bbox.top.should.equal(40);
  bbox.bottom.should.equal(1);
}

/// Create a bbox from a geojson Polygon
unittest {
  auto geometry = `{
    "type": "Polygon",
    "coordinates": [[[30, 10], [40, 40], [20, 40], [10, 20], [30, 10]]]
  }`.parseJsonString.deserializeJson!GeoJsonGeometry;

  auto bbox = BBox!double(geometry);

  bbox.left.should.equal(10);
  bbox.right.should.equal(40);
  bbox.top.should.equal(40);
  bbox.bottom.should.equal(10);
}

/// Create a bbox from a geojson MultiPolygon
unittest {
  auto geometry = `{
    "type": "MultiPolygon",
    "coordinates": [
      [[[30, 10], [40, 40], [20, 40], [10, 20], [30, 10]]],
      [[[3, 1], [4, 4], [2, 4], [1, 2], [3, 1]]]
    ]
  }`.parseJsonString.deserializeJson!GeoJsonGeometry;

  auto bbox = BBox!double(geometry);

  bbox.left.should.equal(1);
  bbox.right.should.equal(40);
  bbox.top.should.equal(40);
  bbox.bottom.should.equal(1);
}

/// Create a bbox from a geojson Polygon
unittest {
  auto geometry = `{
    "type": "Polygon",
    "coordinates": [[[30, 10], [40, 40], [20, 40], [10, 20], [30, 10]]]
  }`.parseJsonString.deserializeJson!GeoJsonGeometry;

  auto bbox = BBox!double(geometry);

  bbox.left.should.equal(10);
  bbox.right.should.equal(40);
  bbox.top.should.equal(40);
  bbox.bottom.should.equal(10);
}


///
size_t countPoints(T)(T lineString) if(is(T == LineString) || is(T == MultiPoint)) {
  return lineString.coordinates.length;
}

///
size_t countPoints(T)(T polygon) if(is(T == Polygon) || is(T == MultiLineString)) {
  size_t sum;

  foreach (ring; polygon.coordinates) {
    sum += ring.length;
  }

  return sum;
}

size_t countPoints(MultiPolygon multiPolygon) {
  size_t sum;

  foreach (polygon; multiPolygon.coordinates) {
    foreach (ring; polygon) {
      sum += ring.length;
    }
  }

  return sum;
}

/// counts the line points
unittest {
  auto geometry = `{
    "type": "LineString",
    "coordinates": [[30, 10], [40, 40], [20, 40], [10, 20], [30, 10]]
  }`.parseJsonString.deserializeJson!GeoJsonGeometry.to!LineString;

  geometry.countPoints.should.equal(5);
}

/// counts the multi line points
unittest {
  auto geometry = `{
    "type": "MultiLineString",
    "coordinates": [[[30, 10], [40, 40], [20, 40], [10, 20], [30, 10]]]
  }`.parseJsonString.deserializeJson!GeoJsonGeometry.to!MultiLineString;

  geometry.countPoints.should.equal(5);
}

/// counts the poygon points
unittest {
  auto geometry = `{
    "type": "Polygon",
    "coordinates": [[[30, 10], [40, 40], [20, 40], [10, 20], [30, 10]]]
  }`.parseJsonString.deserializeJson!GeoJsonGeometry.to!Polygon;

  geometry.countPoints.should.equal(5);
}

/// counts the poygon points
unittest {
  auto geometry = `{
    "type": "MultiPolygon",
    "coordinates": [
      [[[30, 10], [40, 40], [20, 40], [10, 20], [30, 10]]],
      [[[3, 1], [4, 4], [2, 4], [1, 2], [3, 1]]]
    ]
  }`.parseJsonString.deserializeJson!GeoJsonGeometry.to!MultiPolygon;

  geometry.countPoints.should.equal(10);
}
///
int borderIndex(T, U)(BBox!T box, U x, U y) {
  auto borders = box.allLines;
  auto p = [x, y];

  foreach(i; 0..4) {
    if(onSameLine(borders[i][0], p, borders[i][1])) {
      return i;
    }
  }

  return -1;
}

///
bool fullyInside(T, U)(BBox!T box, U[] points) {
  if(points.length == 0) {
    return false;
  }

  bool allOutside = true;
  ubyte code;
  auto top = box.lineTop;
  top[1][1] = 90;
  auto prevPoint = points[0];
  size_t intersections;

  foreach(ref point; points) {
    auto dir = box.relativeDirection(point);
    code |= dir;

    if(box.contains(point[0], point[1])) {
      allOutside = false;
      break;
    }

    if(top.doIntersect([[prevPoint[0], prevPoint[1]], [point[0], point[1]]])) {
      intersections++;
    }

    prevPoint = point;
  }

  return code == 15 && allOutside && intersections % 2;
}

/// it can check if the bbox is fully inside a polygon
unittest {
  auto bbox = BBox!double(0, 10, 0, 10);
  auto outside = BBox!double(-1, 11, -1, 11).getRingCoords;
  auto crossingV = BBox!double(3, 10, -1, 11).getRingCoords;
  auto crossingH = BBox!double(-1, 11, 3, 10).getRingCoords;
  auto near = BBox!double(11, 20, -1, 11).getRingCoords;
  auto inside = BBox!double(1, 9, 1, 9).getRingCoords;
  double[][] surrounding = [[-1, -1], [-1, 15], [15, 15], [15, -1], [16, -1], [16, 16], [-2, 16], [-2, -2], [4, -2], [4, -1], [-1, -1]];

  bbox.fullyInside(outside).should.equal(true);
  bbox.fullyInside(near).should.equal(false);
  bbox.fullyInside(inside).should.equal(false);
  bbox.fullyInside(crossingV).should.equal(false);
  bbox.fullyInside(crossingH).should.equal(false);
  bbox.fullyInside(surrounding).should.equal(false);
}


///
bool fullyOutside(T, U)(ref const BBox!T box, U[] points) {
  bool allOutside = true;
  ubyte code;

  if(points.length == 0) {
    return false;
  }

  auto allLines = box.allLines;
  auto prevPoint = points[0];

  foreach(ref point; points) {
    auto dir = box.relativeDirection(point);
    code |= dir;

    if(box.contains(point[0], point[1])) {
      allOutside = false;
      break;
    }

    foreach(ref line; allLines) {
      if(line.doIntersect([[prevPoint[0], prevPoint[1]], [point[0], point[1]]])) {
        allOutside = false;
        break;
      }
    }

    prevPoint = point;
  }


  return code != 15 && allOutside;
}

/// it can check if the bbox is fully outside a polygon
unittest {
  auto bbox = BBox!double(0, 10, 0, 10);
  auto outside = BBox!double(-1, 11, -1, 11).getRingCoords;
  auto crossingV = BBox!double(3, 10, -1, 11).getRingCoords;
  auto crossingH = BBox!double(-1, 11, 3, 10).getRingCoords;
  auto near = BBox!double(11, 20, -1, 11).getRingCoords;
  auto inside = BBox!double(1, 9, 1, 9).getRingCoords;

  bbox.fullyOutside(outside).should.equal(false);
  bbox.fullyOutside(near).should.equal(true);
  bbox.fullyOutside(outside).should.equal(false);
  bbox.fullyOutside(crossingV).should.equal(false);
  bbox.fullyOutside(crossingH).should.equal(false);
}


///
auto getRingCoords(T)(BBox!T box) pure nothrow {
  return [[box.left, box.top],
      [box.right, box.top],
      [box.right, box.bottom],
      [box.left, box.bottom],
      [box.left, box.top]];
}

///
auto getRingCoordsAsPosition(U: BBox!T, T)(U box) pure nothrow {
  return [box.point(box.left, box.top),
          box.point(box.right, box.top),
          box.point(box.right, box.bottom),
          box.point(box.left, box.bottom),
          box.point(box.left, box.top)];
}

///
alias GeometryVariant = VariantN!(
    maxSize!(Point, MultiPoint, LineString, MultiLineString, Polygon, MultiPolygon, GeometryCollection),
    Point, MultiPoint, LineString, MultiLineString, Polygon, MultiPolygon, GeometryCollection);

///
alias BasicGeometryVariant = VariantN!(
    maxSize!(Point, MultiPoint, LineString, MultiLineString, Polygon, MultiPolygon),
    Point, MultiPoint, LineString, MultiLineString, Polygon, MultiPolygon);

/// A pair of two coordinates and altitude
alias Position = PositionT!double;


struct PositionT(T) {
  alias This = PositionT!T;
  T lon;
  T lat;
  T alt;
  T m;

  this(T lon, T lat, T alt, T m) {
    this.lon = lon;
    this.lat = lat;
    this.alt = alt;
    this.m = m;
  }

  this(T lon, T lat, T alt) {
    this.lon = lon;
    this.lat = lat;
    this.alt = alt;
  }

  this(T lon, T lat) {
    this.lon = lon;
    this.lat = lat;
  }

  this(T lon) {
    this.lon = lon;
  }

  this(T[] values) {
    this.lon = values[0];
    this.lat = values[1];

    if(values.length >=3) {
      this.alt = values[2];
    }

    if(values.length >=4) {
      this.m = values[3];
    }
  }

  private static bool isInitValue(const T value) pure @safe nothrow @nogc {
    static if(is(T == float) || is(T == double) || is(T == real)) {
      return isNaN(value);
    } else {
      return value == T.init;
    }
  }

  T opIndex(size_t index) inout nothrow @safe pure @nogc {
    switch(index) {
      case 0: return lon;
      case 1: return lat;
      case 2: return alt;
      case 3: return m;
      default:
        assert(false, "You can't get the element. Only 0,1,2 and 3 is allowed.");
    }
  }

  T[] toList() inout {
    T[] result;
    result.length = 2 + (isInitValue(alt) ? 0 : 1) + (isInitValue(m) ? 0 : 1) ;
    result[0] = lon;
    result[1] = lat;

    if(!isInitValue(alt)) {
      result[2] = alt;
    }

    if(!isInitValue(m)) {
      result[3] = m;
    }

    return result;
  }

  alias toArray = toList;

  string toString() {
    string strAlt;
    string strM;

    if(!isInitValue(alt)) {
      strAlt = ", " ~ alt.to!string;
    }

    if(!isInitValue(m)) {
      strM = ", " ~ m.to!string;
    }

    return "[" ~ lon.to!string ~ ", " ~ lat.to!string ~ strAlt ~ strM ~ "]";
  }

  Json toJson() const @safe {
    auto result = Json.emptyArray;
    result.appendArrayElement(Json(lon));
    result.appendArrayElement(Json(lat));

    if(!isInitValue(alt)) {
      result.appendArrayElement(Json(alt));
    }

    if(!isInitValue(m)) {
      result.appendArrayElement(Json(m));
    }

    return result;
  }

  static This fromJson(Json src) @safe {
    enforce(src.type == Json.Type.array, "The Position must be an array");

    This position;

    static if(is(T == real)) {
      position.lon = src[0].to!double.to!T;
      position.lat = src[1].to!double.to!T;

      if(src.length > 2) {
        position.alt = src[2].to!double.to!T;
      }

      if(src.length > 3) {
        position.m = src[3].to!double.to!T;
      }
    } else {
      position.lon = src[0].to!T;
      position.lat = src[1].to!T;

      if(src.length > 2) {
        position.alt = src[2].to!T;
      }

      if(src.length > 3) {
        position.m = src[3].to!T;
      }
    }

    return position;
  }

  static This fromRange(T)(T range) if(isInputRange!T && is(ElementType!T == double)) {
    This position;

    position.lon = range.front;
    range.popFront;

    position.lat = range.front;
    range.popFront;

    if(!range.empty) {
      position.alt = range.front;
      range.popFront;
    }

    if(!range.empty) {
      position.m = range.front;
      range.popFront;
    }

    return position;
  }

  static This fromArray(T)(const T[] list) {
    This position;

    position.lon = list[0];
    position.lat = list[1];

    if(list.length >= 3) {
      position.alt = list[2];
    }

    if(list.length >= 4) {
      position.alt = list[3];
    }

    return position;
  }

  bool opEquals (const(This) lhs, const(This) rhs) inout nothrow {
    bool lonEqual = isInitValue(lhs.lon) && isInitValue(rhs.lon) || lhs.lon == rhs.lon;
    bool latEqual = isInitValue(lhs.lat) && isInitValue(rhs.lat) || lhs.lat == rhs.lat;
    bool altEqual = isInitValue(lhs.alt) && isInitValue(rhs.alt) || lhs.alt == rhs.alt;
    bool mEqual = isInitValue(lhs.m) && isInitValue(rhs.m) || lhs.m == rhs.m;

    return lonEqual && latEqual && altEqual;
  }

  int opCmp(ref const PositionT!T lhs) {
    if (lon == lhs.lon && lat == lhs.lat) {
      return 0;
    }

    auto sum = lon * lon + lat * lat;
    auto lhsSum = lhs.lon * lhs.lon + lhs.lat * lhs.lat;

    return (sum < lhsSum) ? -1 : 1;
  }

  bool opEquals (const(This) rhs) inout nothrow {
    return opEquals(this, rhs);
  }

  void opIndexAssign(in T value, size_t index) {
    switch(index) {
      case 0: lon = value; break;
      case 1: lat = value; break;
      case 2: alt = value; break;
      case 3: m = value; break;
      default:
        assert(false, "You can't set the element. Only 0,1,2 and 3 are allowed.");
    }
  }
}

///
PositionT!T[] toPositions(T)(const T[][] list) if(isBasicType!T) {
  PositionT!T[] result;

  foreach(item; list) {
    result ~= PositionT!T.fromArray(item);
  }

  return result;
}


/// ditto
PositionT!T[][] toPositions(T)(const T[][][] list) if(isBasicType!T) {
  PositionT!T[][] result;

  foreach(item; list) {
    result ~= item.toPositions;
  }

  return result;
}

/// ditto
PositionT!T[][][] toPositions(T)(const T[][][][] list) if(isBasicType!T) {
  PositionT!T[][][] result;

  foreach(item; list) {
    result ~= item.toPositions;
  }

  return result;
}

/// ditto
T[][] toList(T)(const PositionT!T[] list) if(isBasicType!T) {
  T[][] result;

  foreach(item; list) {
    result ~= item.toList;
  }

  return result;
}

/// ditto
T[][][] toList(T)(const PositionT!T[][] list) if(isBasicType!T) {
  T[][][] result;

  foreach(item; list) {
    result ~= item.toList;
  }

  return result;
}

/// ditto
T[][][][] toList(T)(const PositionT!T[][][] list) if(isBasicType!T) {
  T[][][][] result;

  foreach(item; list) {
    result ~= item.toList;
  }

  return result;
}

double[][] toList(const EPSG4326[] list) {
  double[][] result;

  foreach(item; list) {
    result ~= [item[0], item[1]];
  }

  return result;
}

double[][][] toList(const EPSG4326[][] list) {
  double[][][] result;

  foreach(item; list) {
    result ~= item.toList;
  }

  return result;
}

double[][][][] toList(const EPSG4326[][][] list) {
  double[][][][] result;

  foreach(item; list) {
    result ~= item.toList;
  }

  return result;
}

///
struct Point {

  /// Point coordinates are in x, y order (easting, northing for projected
  /// coordinates, longitude, and latitude for geographic coordinates)
  Position coordinates;

  ///
  Json toJson() const @safe {
    Json result = Json.emptyObject;

    result["type"] = "Point";
    result["coordinates"] = coordinates.serializeToJson;

    return result;
  }

  ///
  static Point fromJson(Json src) @safe {
    Point result;

    if(src.type == Json.Type.null_ || src.type == Json.Type.undefined) {
      return result;
    }

    enforce(src.type == Json.Type.object, "GeoJson Point must be an Json object.");
    enforce(src["type"] == "Point", "GeoJson Point must have the `Point` type.");

    result.coordinates = src["coordinates"].deserializeJson!(typeof(Point.coordinates));

    return result;
  }
}

///
struct MultiPoint {
  ///
  Position[] coordinates;

  ///
  Json toJson() const @safe {
    Json result = Json.emptyObject;

    result["type"] = "MultiPoint";
    result["coordinates"] = coordinates.serializeToJson;

    return result;
  }

  ///
  static MultiPoint fromJson(Json src) @safe {
    MultiPoint result;

    if(src.type == Json.Type.null_ || src.type == Json.Type.undefined) {
      return result;
    }

    enforce(src.type == Json.Type.object, "GeoJson MultiPoint must be an Json object.");
    enforce(src["type"] == "MultiPoint", "GeoJson MultiPoint must have the `MultiPoint` type.");

    result.coordinates = src["coordinates"].deserializeJson!(typeof(MultiPoint.coordinates));

    return result;
  }
}

///
struct LineString {

  ///
  Position[] coordinates;

  ///
  Json toJson() const @safe {
    Json result = Json.emptyObject;

    result["type"] = "LineString";
    result["coordinates"] = coordinates.serializeToJson;

    return result;
  }

  ///
  static LineString fromJson(Json src) @safe {
    LineString result;

    if(src.type == Json.Type.null_ || src.type == Json.Type.undefined) {
      return result;
    }

    enforce(src.type == Json.Type.object, "GeoJson LineString must be an Json object.");
    enforce(src["type"] == "LineString", "GeoJson LineString must have the `LineString` type.");

    result.coordinates = src["coordinates"].deserializeJson!(typeof(LineString.coordinates));

    return result;
  }
}

///
struct MultiLineString {

  ///
  Position[][] coordinates;

  ///
  Json toJson() const @safe {
    if(coordinates.length == 0) {
      return Json();
    }

    Json result = Json.emptyObject;

    result["type"] = "MultiLineString";
    result["coordinates"] = coordinates.serializeToJson;

    return result;
  }

  ///
  static MultiLineString fromJson(Json src) @safe {
    MultiLineString result;

    if(src.type == Json.Type.null_ || src.type == Json.Type.undefined) {
      return result;
    }

    enforce(src.type == Json.Type.object, "GeoJson MultiLineString must be an Json object.");
    enforce(src["type"] == "MultiLineString", "GeoJson MultiLineString must have the `MultiLineString` type.");

    result.coordinates = src["coordinates"].deserializeJson!(typeof(MultiLineString.coordinates));

    return result;
  }
}

///
struct Polygon {
  this(Position[][] coordinates) pure @safe {
    this.coordinates = coordinates;
  }

  this(double[][][] coordinates) pure @safe {
    this.coordinates.reserve(coordinates.length);

    foreach (ring; coordinates) {
      Position[] list = [];

      foreach (point; ring) {
        list ~= Position(point);
      }

      this.coordinates ~= list;
    }
  }

  ///
  Position[][] coordinates;

  ///
  string type = "Polygon";

  ///
  Json toJson() const @safe {
    Json result = Json.emptyObject;

    result["type"] = "Polygon";
    result["coordinates"] = coordinates.serializeToJson;

    return result;
  }

  ///
  static Polygon fromJson(Json src) @safe {
    Polygon result;

    if(src.type == Json.Type.null_ || src.type == Json.Type.undefined) {
      return result;
    }

    enforce(src.type == Json.Type.object, "GeoJson Polygon must be an Json object.");
    enforce(src["type"] == "Polygon", "GeoJson Polygon must have the `Polygon` type.");

    result.coordinates = src["coordinates"].deserializeJson!(typeof(Polygon.coordinates));

    return result;
  }
}

///
struct MultiPolygon {

  ///
  Position[][][] coordinates;
}

///
struct GeometryCollection {
  ///
  BasicGeometryVariant[] geometries;
}
