/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module geo.vector;

/// https://github.com/mapbox/vector-tile-spec/tree/master/2.1

import std.conv;
import std.range;
import std.algorithm;
import std.array;
import std.variant;
import std.bitmanip;
import std.utf;
import std.string;
import std.traits;
import std.exception;

import vibe.data.json;

import geo.proto;
import geo.conv;
import geo.json;

version(unittest) {
  import fluent.asserts;
}

///
enum CommandId {
    ///
    MoveTo = 1,
    ///
    LineTo = 2,
    ///
    ClosePath = 7
}

///
struct CommandInteger {
  CommandId id;
  uint count;

  ///
  uint serialize() {
    if( id == CommandId.ClosePath ) {
      return (id & 0x7) | (1 << 3);
    }

    return (id & 0x7) | (count << 3);
  }

  ///
  static CommandInteger deserialize(uint value) {
    CommandInteger result;
    result.id = (value & 0x7).to!CommandId;
    result.count = value >> 3;

    if( result.id == CommandId.ClosePath ) {
      result.count = 0;
    }

    return result;
  }
}

/// CommandId Serialize
unittest {
  CommandInteger(CommandId.MoveTo, 1)
    .serialize
    .should.equal(9);

  CommandInteger(CommandId.MoveTo, 120)
    .serialize
    .should.equal(961);

  CommandInteger(CommandId.LineTo, 1)
    .serialize
    .should.equal(10);

  CommandInteger(CommandId.LineTo, 3)
    .serialize
    .should.equal(26);

  CommandInteger(CommandId.ClosePath, 1)
    .serialize
    .should.equal(15);
}

/// CommandId Deserialize
unittest {
  auto value = CommandInteger.deserialize(9);
  value.id.should.equal(CommandId.MoveTo);
  value.count.should.equal(1);

  value = CommandInteger.deserialize(961);
  value.id.should.equal(CommandId.MoveTo);
  value.count.should.equal(120);

  value = CommandInteger.deserialize(10);
  value.id.should.equal(CommandId.LineTo);
  value.count.should.equal(1);

  value = CommandInteger.deserialize(26);
  value.id.should.equal(CommandId.LineTo);
  value.count.should.equal(3);

  value = CommandInteger.deserialize(15);
  value.id.should.equal(CommandId.ClosePath);
  value.count.should.equal(0);
}

alias serializeParameter = toZigZag;
alias deserializeParameter = fromZigZag;

///
struct Command {
  ///
  CommandInteger commandInteger;

  ///
  int[] parameters;

  ///
  InputRange!uint serialize() {
    InputRange!uint result = [commandInteger.serialize]
      .chain(parameters.map!(a => a.serializeParameter))
      .inputRangeObject;

    return result;
  }

  ///
  static Command deserialize(uint[] data) {
    auto result = Command(CommandInteger.deserialize(data[0]));
    result.parameters = data[1..$].map!(a => deserializeParameter(a)).array;

    return result;
  }
}

/// An example encoding of a point located at (25,17)
unittest {
  Command(CommandInteger(CommandId.MoveTo, 1), [25, 17])
    .serialize
    .array
    .should.equal([ 9, 50, 34 ]);
}

/// An example decoding of a point located at (25,17)
unittest {
  auto command = Command.deserialize([ 9, 50, 34 ]);

  command.commandInteger.id.should.equal(CommandId.MoveTo);
  command.commandInteger.count.should.equal(1);
  command.parameters.should.equal([25, 17]);
}

///
struct Geometry {
  ///
  Command[] commands;

  ///
  InputRange!uint serialize() {
    InputRange!uint result = commands.map!(a => a.serialize).joiner.inputRangeObject;

    return result;
  }

  ///
  size_t totalPoints() const pure {
    size_t sum;

    foreach(ref command; commands) {
      if(command.commandInteger.id == CommandId.MoveTo || command.commandInteger.id == CommandId.LineTo) {
        sum += command.parameters.length / 2;
      }
    }

    return sum;
  }

  ///
  static Geometry deserialize(uint[] data) {
    Geometry result;

    size_t pos;
    while(pos < data.length) {
      auto commandInteger = CommandInteger.deserialize(data[pos]);
      int[] parameters;
      pos++;

      if(commandInteger.count > 0) {
        parameters = data[pos .. pos + commandInteger.count * 2].map!(a => deserializeParameter(a)).array;
      }
      pos += parameters.length;

      result.commands ~= Command(commandInteger, parameters);
    }

    return result;
  }

  /// create a point geometry from a pair of numbers
  static Geometry fromPoint(const int[] coordinates) pure {
    return Geometry([ Command(CommandInteger(CommandId.MoveTo, 1), coordinates.dup) ]);
  }

  /// create a line geometry from a list of coordinates
  static Geometry fromLine(const int[][] coordinates) pure {
    enforce(coordinates.length > 1, "There should be at least 2 points");

    uint size = cast(uint) coordinates.length - 1U;

    int[] relativeCoordinates;

    foreach(i; 1..coordinates.length) {
      auto prev = coordinates[i-1];
      auto curr = coordinates[i];

      relativeCoordinates ~= [ curr[0] - prev[0], curr[1] - prev[1] ];
    }

    return Geometry([
      Command(CommandInteger(CommandId.MoveTo, 1), coordinates[0].dup),
      Command(CommandInteger(CommandId.LineTo, size), relativeCoordinates)
    ]);
  }

  /// create a polygon geometry from a list of coordinates
  static Geometry fromPolygon(const int[][] coordinates) pure {
    enforce(coordinates.length > 2, "There should be at least 3 points");

    int extra = coordinates[0] == coordinates[coordinates.length - 1] ? 1 : 0;

    uint size = cast(uint) coordinates.length - 1U - extra;

    int[] relativeCoordinates;

    foreach(i; 1..coordinates.length - extra) {
      auto prev = coordinates[i-1];
      auto curr = coordinates[i];

      relativeCoordinates ~= [ curr[0] - prev[0], curr[1] - prev[1] ];
    }

    return Geometry([
      Command(CommandInteger(CommandId.MoveTo, 1), coordinates[0].dup),
      Command(CommandInteger(CommandId.LineTo, size), relativeCoordinates),
      Command(CommandInteger(CommandId.ClosePath, 0))
    ]);
  }
}

/// An example encoding of a point located at (25,17)
unittest {
  Geometry([ Command(CommandInteger(CommandId.MoveTo, 1), [25, 17]) ])
    .serialize
    .array
    .should.equal([ 9, 50, 34 ]);
}

/// An example of counting points for a point located at (25,17)
unittest {
  Geometry([ Command(CommandInteger(CommandId.MoveTo, 1), [25, 17]) ])
    .totalPoints.should.equal(1);
}

/// An example of creating a point located at (25,17)
unittest {
  Geometry.fromPoint([25, 17])
    .serialize
    .array
    .should.equal([ 9, 50, 34 ]);
}

/// An example decoding of a point located at (25,17)
unittest {
  auto geometry = Geometry.deserialize([ 9, 50, 34 ]);

  geometry.commands.length.should.equal(1);
  geometry.commands[0].commandInteger.id = CommandId.MoveTo;
  geometry.commands[0].commandInteger.count = 1;
  geometry.commands[0].parameters = [25, 17];
}

/// An example encoding of two points located at (5,7) (3,2)
unittest {
  Geometry([
    Command(CommandInteger(CommandId.MoveTo, 2), [5, 7, -2, -5])
  ])
  .serialize
  .array
  .should.equal([ 17, 10, 14, 3, 9 ]);
}

/// An example of counting two points located at (5,7) (3,2)
unittest {
  Geometry([ Command(CommandInteger(CommandId.MoveTo, 1), [5, 7, -2, -5]) ])
    .totalPoints.should.equal(2);
}


/// An example decoding of a point located at (25,17)
unittest {
  auto geometry = Geometry.deserialize([ 17, 10, 14, 3, 9 ]);

  geometry.commands.length.should.equal(1);
  geometry.commands[0].commandInteger.id = CommandId.MoveTo;
  geometry.commands[0].commandInteger.count = 2;
  geometry.commands[0].parameters = [5, 7, -2, -5];
}

/// An example encoding of a line with the points (2,2) (2,10) (10,10)
unittest {
  Geometry([
    Command(CommandInteger(CommandId.MoveTo, 1), [2, 2]),
    Command(CommandInteger(CommandId.LineTo, 2), [0, 8, 8, 0])
  ])
  .serialize
  .array
  .should.equal([ 9, 4, 4, 18, 0, 16, 16, 0 ]);
}

/// Counting points of a line with the points (2,2) (2,10) (10,10)
unittest {
  Geometry([
    Command(CommandInteger(CommandId.MoveTo, 1), [2, 2]),
    Command(CommandInteger(CommandId.LineTo, 2), [0, 8, 8, 0])
  ])
  .totalPoints.should.equal(3);
}

/// An example encoding of a line with the points (2,2) (2,10) (10,10)
unittest {
  Geometry.fromLine([[2, 2], [2, 10], [10, 10]])
    .serialize
    .array
    .should.equal([ 9, 4, 4, 18, 0, 16, 16, 0 ]);
}

/// An example decoding of a line with the points (2,2) (2,10) (10,10)
unittest {
  auto geometry = Geometry.deserialize([ 9, 4, 4, 18, 0, 16, 16, 0 ]);

  geometry.commands.length.should.equal(2);
  geometry.commands[0].commandInteger.id = CommandId.MoveTo;
  geometry.commands[0].commandInteger.count = 1;
  geometry.commands[0].parameters = [2, 2];

  geometry.commands[1].commandInteger.id = CommandId.LineTo;
  geometry.commands[1].commandInteger.count = 2;
  geometry.commands[1].parameters = [0, 8, 8, 0];
}

/// An example of decoding a large geometry
unittest
{
  uint[] data = [9, 1372, 4564, 234, 238, 96, 528, 172, 52, 2, 872, 122, 672, 51, 12, 15, 146, 17, 90, 186, 13,
    228, 573, 292, 215, 104, 289, 158, 13, 1, 67, 86, 203, 159, 132, 165, 34, 77, 33, 61, 37, 41, 109, 47, 381,
    63, 403, 215, 381, 25, 301, 135, 107, 87, 171, 163, 35, 47, 57, 103, 238, 237, 15, 9, 177, 252, 138, 340,
    320, 246, 116, 52, 16, 676, 68, 908, 124, 316, 62, 212, 3, 34, 13, 0, 13, 227, 6, 323, 75, 917, 125, 303,
    7, 375, 39, 341, 167, 69, 59, 215, 205, 15, 9, 1220, 368, 98, 5, 10, 12, 20, 136, 24, 846, 116, 128, 10,
    10, 3, 6, 9, 1, 11, 9, 7, 109, 23, 895, 119, 101, 7, 15];

  auto geometry = Geometry.deserialize(data).serialize.array.should.equal(data);
}

/// An example encoding of a polygon feature that has the points: (3,6) (8,12) (20,34) (3,6)
unittest {
  Geometry([
    Command(CommandInteger(CommandId.MoveTo, 1), [3, 6]),
    Command(CommandInteger(CommandId.LineTo, 2), [5, 6, 12, 22]),
    Command(CommandInteger(CommandId.ClosePath, 1))
  ])
  .serialize
  .array
  .should.equal([ 9, 6, 12, 18, 10, 12, 24, 44, 15 ]);
}

/// Counting points of a polygon feature that has the points: (3,6) (8,12) (20,34) (3,6)
unittest {
  Geometry([
    Command(CommandInteger(CommandId.MoveTo, 1), [3, 6]),
    Command(CommandInteger(CommandId.LineTo, 2), [5, 6, 12, 22]),
    Command(CommandInteger(CommandId.ClosePath, 1))
  ])
  .totalPoints.should.equal(3);
}

/// An example encoding of a polygon feature that has the points: (3,6) (8,12) (20,34) (3,6)
unittest {
  Geometry.fromPolygon([[3,6], [8,12], [20,34]])
  .serialize
  .array
  .should.equal([ 9, 6, 12, 18, 10, 12, 24, 44, 15 ]);
}

/// An example encoding of a polygon feature that has the points: (3,6) (8,12) (20,34) (3,6)
unittest {
  Geometry.fromPolygon([[3,6], [8,12], [20,34], [3,6]])
  .serialize
  .array
  .should.equal([ 9, 6, 12, 18, 10, 12, 24, 44, 15 ]);
}

/// An example encoding of a Multi Polygon
unittest {
  Geometry([
    Command(CommandInteger(CommandId.MoveTo, 1), [0, 0]),
    Command(CommandInteger(CommandId.LineTo, 3), [10, 0, 0, 10, -10, 0]),
    Command(CommandInteger(CommandId.ClosePath)),

    Command(CommandInteger(CommandId.MoveTo, 1), [11, 1]),
    Command(CommandInteger(CommandId.LineTo, 3), [9, 0, 0, 9, -9, 0]),
    Command(CommandInteger(CommandId.ClosePath)),

    Command(CommandInteger(CommandId.MoveTo, 1), [2, -7]),
    Command(CommandInteger(CommandId.LineTo, 3), [0, 4, 4, 0, 0, -4]),
    Command(CommandInteger(CommandId.ClosePath))
  ])
  .serialize
  .array
  .should.equal([ 9, 0, 0, 26, 20, 0, 0, 20, 19, 0, 15, 9, 22, 2, 26, 18, 0,
    0, 18, 17, 0, 15, 9, 4, 13, 26, 0, 8, 8, 0, 0, 7, 15 ]);
}

/// An example decoding a Multi Polygon
unittest {
  auto geometry = Geometry.deserialize([ 9, 0, 0, 26, 20, 0, 0, 20, 19, 0, 15, 9, 22, 2, 26, 18, 0,
    0, 18, 17, 0, 15, 9, 4, 13, 26, 0, 8, 8, 0, 0, 7, 15 ]);

  auto expected = Geometry([
    Command(CommandInteger(CommandId.MoveTo, 1), [0, 0]),
    Command(CommandInteger(CommandId.LineTo, 3), [10, 0, 0, 10, -10, 0]),
    Command(CommandInteger(CommandId.ClosePath)),

    Command(CommandInteger(CommandId.MoveTo, 1), [11, 1]),
    Command(CommandInteger(CommandId.LineTo, 3), [9, 0, 0, 9, -9, 0]),
    Command(CommandInteger(CommandId.ClosePath)),

    Command(CommandInteger(CommandId.MoveTo, 1), [2, -7]),
    Command(CommandInteger(CommandId.LineTo, 3), [0, 4, 4, 0, 0, -4]),
    Command(CommandInteger(CommandId.ClosePath))
  ]);

  geometry.should.equal(expected);
}

///
string toString(const VectorFeature.GeomType type) pure @safe {

  if(type == VectorFeature.GeomType.point) {
    return "Point";
  }

  if(type == VectorFeature.GeomType.lineString) {
    return "LineString";
  }

  if(type == VectorFeature.GeomType.polygon) {
    return "Polygon";
  }

  return "Unknown";
}

///
size_t rawLength(Geometry geometry) {
  size_t size;

  foreach (a; geometry.serialize) {
    size += Varint(a).length;
  }

  return size;
}

/// It returns the protobuf size
unittest {
  auto geometry = Geometry([
    Command(CommandInteger(CommandId.MoveTo, 1), [0, 0]),
    Command(CommandInteger(CommandId.LineTo, 3), [10, 0, 0, 10, -10, 0]),
    Command(CommandInteger(CommandId.ClosePath)),

    Command(CommandInteger(CommandId.MoveTo, 1), [11, 1]),
    Command(CommandInteger(CommandId.LineTo, 3), [9, 0, 0, 9, -9, 0]),
    Command(CommandInteger(CommandId.ClosePath)),

    Command(CommandInteger(CommandId.MoveTo, 1), [2, -7]),
    Command(CommandInteger(CommandId.LineTo, 3), [0, 4, 4, 0, 0, -4]),
    Command(CommandInteger(CommandId.ClosePath))
  ]);

  geometry.rawLength.should.equal(33);
}

size_t toProtoBuf(Geometry geometry, ubyte[] data) {
  size_t size;

  foreach (command; geometry.serialize) {
    size += Varint(command).write(data[size .. $]);
  }

  return size;
}

///
struct VectorFeature {
  /// GeomType is described in section 4.3.4 of the specification
  enum GeomType : ulong {
    ///
    unknown = 0,
    ///
    point = 1,
    ///
    lineString = 2,
    ///
    polygon = 3
  }

  /// optional feature id
  ulong id;

  /// Tags of this feature are encoded as repeated pairs of
  /// integers.
  ulong[] tags;

  /// The type of geometry stored in this feature.
  GeomType type;

  /// Contains a list of commands and parameters (vertices).
  Geometry geometry;

  /// Get the number of points stored in the feature
  size_t totalPoints() pure const {
    if(type == GeomType.point) {
      return 1;
    }

    return geometry.totalPoints;
  }
}

size_t rawLength(ulong[] tags) {
  size_t size;

  foreach(tag; tags) {
    size += Varint(tag).length;
  }

  return size;
}

///
size_t rawLength(VectorFeature feature) {
  size_t size = ProtoKey(1, WireType.varint).length + Varint(feature.id).length;

  size_t tagsSize = feature.tags.rawLength;
  size += ProtoKey(2, WireType.fixedLength).length + Varint(tagsSize).length + tagsSize;

  size += ProtoKey(3, WireType.varint).length + Varint(feature.type).length;

  auto geometrySize = feature.geometry.rawLength;
  size += ProtoKey(4, WireType.fixedLength).length + Varint(geometrySize).length + geometrySize;

  return size;
}

// it can estimate the raw length of the proto buf data
unittest {
  auto feature = VectorFeature(1, [1,2,3], VectorFeature.GeomType.point, Geometry([
    Command(CommandInteger(CommandId.MoveTo, 1), [0, 0]),
    Command(CommandInteger(CommandId.LineTo, 3), [10, 0, 0, 10, -10, 0]),
    Command(CommandInteger(CommandId.ClosePath)),

    Command(CommandInteger(CommandId.MoveTo, 1), [11, 1]),
    Command(CommandInteger(CommandId.LineTo, 3), [9, 0, 0, 9, -9, 0]),
    Command(CommandInteger(CommandId.ClosePath)),

    Command(CommandInteger(CommandId.MoveTo, 1), [2, -7]),
    Command(CommandInteger(CommandId.LineTo, 3), [0, 4, 4, 0, 0, -4]),
    Command(CommandInteger(CommandId.ClosePath))
  ]));

  feature.rawLength.should.equal(feature.toProtoBuf.array.length);
}

size_t toProtoBuf(VectorFeature feature, ubyte[] data) {
  size_t size;

  // id
  size += ProtoKey(1, WireType.varint).write(data[size .. $]);
  size += Varint(feature.id).write(data[size .. $]);

  // tags
  size_t tagsSize = feature.tags.rawLength;
  size += ProtoKey(2, WireType.fixedLength).write(data[size .. $]);
  size += Varint(tagsSize).write(data[size .. $]);
  foreach(tag; feature.tags) {
    size += Varint(tag).write(data[size .. $]);
  }

  // type
  size += ProtoKey(3, WireType.varint).write(data[size .. $]);
  size += Varint(feature.type).write(data[size .. $]);

  // geometry
  size += ProtoKey(4, WireType.fixedLength).write(data[size .. $]);
  size += Varint(feature.geometry.rawLength).write(data[size .. $]);
  size += feature.geometry.toProtoBuf(data[size .. $]);

  return size;
}

/// the toProtoBuf range is the same as the buffer
unittest {
  auto feature = VectorFeature(1, [1,2,3], VectorFeature.GeomType.point, Geometry([
    Command(CommandInteger(CommandId.MoveTo, 1), [0, 0]),
    Command(CommandInteger(CommandId.LineTo, 3), [10, 0, 0, 10, -10, 0]),
    Command(CommandInteger(CommandId.ClosePath)),

    Command(CommandInteger(CommandId.MoveTo, 1), [11, 1]),
    Command(CommandInteger(CommandId.LineTo, 3), [9, 0, 0, 9, -9, 0]),
    Command(CommandInteger(CommandId.ClosePath)),

    Command(CommandInteger(CommandId.MoveTo, 1), [2, -7]),
    Command(CommandInteger(CommandId.LineTo, 3), [0, 4, 4, 0, 0, -4]),
    Command(CommandInteger(CommandId.ClosePath))
  ]));

  ubyte[] data;
  data.length = 1024;

  auto size = feature.toProtoBuf(data);

  feature.toProtoBuf.array.should.equal(data[0..size]);
}

/// Convert a feature to proto buf
InputRange!(immutable(ubyte)) toProtoBuf(VectorFeature feature) {
  auto id = ProtoKey(1, WireType.varint).value.raw ~ Varint(feature.id).raw;
  auto tags = feature.tags.map!(a => Varint(a).raw).joiner.array;
  auto type = ProtoKey(3, WireType.varint).value.raw ~ Varint(feature.type).raw;
  auto geometry = feature.geometry.serialize.map!(a => Varint(a).raw).joiner.array;

  InputRange!(immutable(ubyte)) result = chain(
    id,
    ProtoKey(2, WireType.fixedLength).value.raw, Varint(tags.length).raw, tags,
    type,
    ProtoKey(4, WireType.fixedLength).value.raw, Varint(geometry.length).raw, geometry)
      .inputRangeObject;

  return result;
}

/// Convert a protobuf to Feature and back
unittest {
  auto data = cast(ubyte[]) [8, 1, 18, 2, 0, 0, 24, 3, 34, 186, 1, 9, 220, 10, 212, 35, 234, 1, 238, 1, 96, 144, 4, 172,
  1, 52, 2, 232, 6, 122, 160, 5, 51, 12, 15, 146, 1, 17, 90, 186, 1, 13, 228, 1, 189, 4, 164, 2, 215, 1, 104, 161, 2,
  158, 1, 13, 1, 67, 86, 203, 1, 159, 1, 132, 1, 165, 1, 34, 77, 33, 61, 37, 41, 109, 47, 253, 2, 63, 147, 3, 215, 1,
  253, 2, 25, 173, 2, 135, 1, 107, 87, 171, 1, 163, 1, 35, 47, 57, 103, 238, 1, 237, 1, 15, 9, 177, 1, 252, 1, 138, 1,
  212, 2, 192, 2, 246, 1, 116, 52, 16, 164, 5, 68, 140, 7, 124, 188, 2, 62, 212, 1, 3, 34, 13, 0, 13, 227, 1, 6, 195, 2,
  75, 149, 7, 125, 175, 2, 7, 247, 2, 39, 213, 2, 167, 1, 69, 59, 215, 1, 205, 1, 15, 9, 196, 9, 240, 2, 98, 5, 10, 12,
  20, 136, 1, 24, 206, 6, 116, 128, 1, 10, 10, 3, 6, 9, 1, 11, 9, 7, 109, 23, 255, 6, 119, 101, 7, 15];

  auto feature = data.readFeature;
  feature.toProtoBuf.array.should.equal(data);
}

/// Convert a VectorFeature to protobuf
unittest {
  auto feature = VectorFeature(1, [1,2,3], VectorFeature.GeomType.point, Geometry([
    Command(CommandInteger(CommandId.MoveTo, 1), [0, 0]),
    Command(CommandInteger(CommandId.LineTo, 3), [10, 0, 0, 10, -10, 0]),
    Command(CommandInteger(CommandId.ClosePath)),

    Command(CommandInteger(CommandId.MoveTo, 1), [11, 1]),
    Command(CommandInteger(CommandId.LineTo, 3), [9, 0, 0, 9, -9, 0]),
    Command(CommandInteger(CommandId.ClosePath)),

    Command(CommandInteger(CommandId.MoveTo, 1), [2, -7]),
    Command(CommandInteger(CommandId.LineTo, 3), [0, 4, 4, 0, 0, -4]),
    Command(CommandInteger(CommandId.ClosePath))
  ]));

  feature.toProtoBuf.array.should.equal(cast(ubyte[])
    [8, 1, 18, 3, 1, 2, 3, 24, 1, 34, 33, 9, 0, 0, 26, 20, 0, 0, 20, 19, 0, 15, 9, 22, 2, 26,
     18, 0, 0, 18, 17, 0, 15, 9, 4, 13, 26, 0, 8, 8, 0, 0, 7, 15]);
}

/// Convert a large Vector Feature to protobuf
unittest {
  uint[] data = [9, 1372, 4564, 234, 238, 96, 528, 172, 52, 2, 872, 122, 672, 51, 12, 15, 146, 17, 90, 186, 13,
    228, 573, 292, 215, 104, 289, 158, 13, 1, 67, 86, 203, 159, 132, 165, 34, 77, 33, 61, 37, 41, 109, 47, 381,
    63, 403, 215, 381, 25, 301, 135, 107, 87, 171, 163, 35, 47, 57, 103, 238, 237, 15, 9, 177, 252, 138, 340,
    320, 246, 116, 52, 16, 676, 68, 908, 124, 316, 62, 212, 3, 34, 13, 0, 13, 227, 6, 323, 75, 917, 125, 303,
    7, 375, 39, 341, 167, 69, 59, 215, 205, 15, 9, 1220, 368, 98, 5, 10, 12, 20, 136, 24, 846, 116, 128, 10,
    10, 3, 6, 9, 1, 11, 9, 7, 109, 23, 895, 119, 101, 7, 15];

   auto feature = VectorFeature(1, [1,2,3], VectorFeature.GeomType.point, Geometry.deserialize(data));

  feature.toProtoBuf.array.should.equal(cast(ubyte[])
    [8, 1, 18, 3, 1, 2, 3, 24, 1, 34, 186, 1, 9, 220, 10, 212, 35, 234, 1, 238, 1, 96, 144, 4,
    172, 1, 52, 2, 232, 6, 122, 160, 5, 51, 12, 15, 146, 1, 17, 90, 186, 1, 13, 228, 1, 189, 4,
    164, 2, 215, 1, 104, 161, 2, 158, 1, 13, 1, 67, 86, 203, 1, 159, 1, 132, 1, 165, 1, 34, 77,
    33, 61, 37, 41, 109, 47, 253, 2, 63, 147, 3, 215, 1, 253, 2, 25, 173, 2, 135, 1, 107, 87,
    171, 1, 163, 1, 35, 47, 57, 103, 238, 1, 237, 1, 15, 9, 177, 1, 252, 1, 138, 1, 212, 2,
    192, 2, 246, 1, 116, 52, 16, 164, 5, 68, 140, 7, 124, 188, 2, 62, 212, 1, 3, 34, 13, 0, 13,
    227, 1, 6, 195, 2, 75, 149, 7, 125, 175, 2, 7, 247, 2, 39, 213, 2, 167, 1, 69, 59, 215, 1, 205, 1,
    15, 9, 196, 9, 240, 2, 98, 5, 10, 12, 20, 136, 1, 24, 206, 6, 116, 128, 1, 10, 10, 3, 6, 9, 1, 11, 9,
    7, 109, 23, 255, 6, 119, 101, 7, 15]);

  feature.toProtoBuf.readFeature.geometry.serialize.array.should.equal(data);
}

/// Convert protobuf to a feature
VectorFeature readFeature(T)(T data) if(isInputRange!T && is(Unqual!(ElementType!T) == ubyte)) {
  scope range = VarintInputRange(data);
  return readFeature(range);
}

/// ditto
VectorFeature readFeature(VarintInputRange range) {
  VectorFeature feature;

  while(!range.empty) {
    mixin(genReadKey);

    if(key.index == 1) {
      feature.id = value.to!ulong;
    }

    if(key.index == 2 && value.to!ulong > 0) {
      auto rawData = range.readRaw(value.to!ulong);
      ulong[] buffer;
      buffer.length = value.to!ulong;

      scope r = VarintInputRange(rawData);
      size_t decodedLength;
      foreach(index, value; r) {
        buffer[index] = value.to!ulong;
        decodedLength++;
      }

      feature.tags = buffer[0..decodedLength];
    }

    if(key.index == 3) {
      feature.type = cast(VectorFeature.GeomType) value.to!ulong;
    }

    if(key.index == 4 && value.to!ulong > 0) {
      auto rawData = range.readRaw(value.to!ulong);

      uint[] buffer;
      buffer.length = value.to!ulong;

      scope r = VarintInputRange(rawData);
      size_t decodedLength;
      foreach(index, value; r) {
        buffer[index] = value.to!uint;
        decodedLength++;
      }

      feature.geometry = Geometry.deserialize(buffer[0..decodedLength]);
    }
  }

  return feature;
}

/// Convert a protobuf to Feature
unittest {
  auto feature = readFeature(cast(ubyte[]) [8, 1, 16, 3, 1, 2, 3, 24, 1, 32, 33, 9, 0, 0, 26, 20, 0, 0, 20, 19, 0, 15, 9, 22,
    2, 26, 18, 0, 0, 18, 17, 0, 15, 9, 4, 13, 26, 0, 8, 8, 0, 0, 7, 15]);

  auto geometry = Geometry([
    Command(CommandInteger(CommandId.MoveTo, 1), [0, 0]),
    Command(CommandInteger(CommandId.LineTo, 3), [10, 0, 0, 10, -10, 0]),
    Command(CommandInteger(CommandId.ClosePath)),

    Command(CommandInteger(CommandId.MoveTo, 1), [11, 1]),
    Command(CommandInteger(CommandId.LineTo, 3), [9, 0, 0, 9, -9, 0]),
    Command(CommandInteger(CommandId.ClosePath)),

    Command(CommandInteger(CommandId.MoveTo, 1), [2, -7]),
    Command(CommandInteger(CommandId.LineTo, 3), [0, 4, 4, 0, 0, -4]),
    Command(CommandInteger(CommandId.ClosePath))
  ]);

  feature.id.should.equal(1);
  feature.tags.should.equal(cast(ulong[]) [1, 2, 3]);
  feature.type.should.equal(VectorFeature.GeomType.point);
  feature.geometry.should.equal(geometry);
}

///
struct Layer {
  /// Any compliant implementation must first read the version
  /// number encoded in this message and choose the correct
  /// implementation for this version number before proceeding to
  /// decode other parts of this message.
  uint version_ = 2;

  /// The layer name
  string name;

  ///
  VectorFeature[] features;

  ///
  string[] keys;

  ///
  Variant[] values;

  ///
  uint extent = 4096;

  ///
  Json toJson() @trusted const {
    Json value = Json.emptyObject;

    value["version"] = this.version_;
    value["name"] = this.name;
    value["extent"] = this.extent;
    value["features"] = features.serializeToJson;
    value["keys"] = keys.serializeToJson;
    value["values"] = values.map!(a => a.toJson).filter!(a => a.type != Json.Type.undefined).array.serializeToJson;

    return value;
  }

  ///
  static Layer fromJson(Json src) @safe {
    assert(false, "not implemented");
  }
}

///
size_t rawLength(Layer layer) {
  size_t size;

  // version
  size += ProtoKey(15, WireType.varint).length;
  size += Varint(layer.version_).length;

  // name
  size += stringToProtoBufLength(1, layer.name);

  // features
  foreach(feature; layer.features) {
    auto protoFeatureLength = feature.rawLength;
    size += ProtoKey(2, WireType.fixedLength).length;
    size += Varint(protoFeatureLength).length + protoFeatureLength;
  }

  // keys
  foreach(key; layer.keys) {
    size += stringToProtoBufLength(3, key);
  }

  // values
  foreach(value; layer.values) {
    auto protoValueLength = value.layerValueToProtoBufLength;
    size += ProtoKey(4, WireType.fixedLength).length + Varint(protoValueLength).length + protoValueLength;
  }

  // extent
  size += ProtoKey(5, WireType.varint).length + Varint(layer.extent).length;

  return size;
}

/// can get the length of the serialized data
unittest {
  Layer layer;
  layer.version_ = 3;
  layer.name = "test layer";
  layer.extent = 512;
  uint[] geometry = [ 9, 2410, 3080 ];

  layer.addFeature(VectorFeature.GeomType.point, Geometry.deserialize(geometry));
  layer.addProperty("hello", "world");
  layer.addProperty("h", "world");
  layer.addProperty("count", 1.23);

  layer.addFeature(VectorFeature.GeomType.point, Geometry.deserialize(geometry));
  layer.addProperty("hello", "again");
  layer.addProperty("count", 2);

  layer.rawLength.should.equal(layer.toProtoBuf.array.length);
}

/// Layer to Json
unittest {
  Layer layer;
  layer.version_ = 3;
  layer.name = "test layer";
  layer.extent = 512;
  uint[] geometry = [ 9, 2410, 3080 ];

  layer.addFeature(VectorFeature.GeomType.point, Geometry.deserialize(geometry));
  layer.addProperty("hello", "world");
  layer.addProperty("h", "world");
  layer.addProperty("count", 1.23);

  layer.addFeature(VectorFeature.GeomType.point, Geometry.deserialize(geometry));
  layer.addProperty("hello", "again");
  layer.addProperty("count", 2);

  layer.serializeToJson.should.equal(`{
  "keys": [ "hello", "h", "count" ],
  "name": "test layer",
  "version": 3,
  "features": [ {
      "type": 1,
      "tags": [ 0, 0, 1, 0, 2, 1 ],
      "geometry": {
        "commands": [ {
            "commandInteger": {
              "count": 1,
              "id": 1
            },
            "parameters": [
              1205,
              1540
            ]}]},
      "id": 0 },{
      "type": 1,
      "tags": [ 0, 2, 2, 3 ],
      "geometry": {
        "commands": [
          { "commandInteger": {
              "count": 1,
              "id": 1
            }, "parameters": [ 1205, 1540 ]
          }]},
      "id": 0}],
  "extent": 512,
  "values": [ "world", 1.23, "again" ]}`.parseJsonString);
}

/// Converts a layer value to protobuf
immutable(ubyte)[] layerValueToProtoBuf(Variant value) {
  if(value.peek!string !is null) {
    return layerValueToProtoBuf(*value.peek!string);
  }

  if(value.peek!long !is null) {
    return layerValueToProtoBuf(*value.peek!long);
  }

  if(value.peek!ulong !is null) {
    return layerValueToProtoBuf(*value.peek!ulong);
  }

  if(value.peek!uint !is null) {
    return layerValueToProtoBuf(value.get!ulong);
  }

  if(value.peek!int !is null) {
    return layerValueToProtoBuf(value.get!long);
  }

  if(value.peek!bool !is null) {
    return layerValueToProtoBuf(*value.peek!bool);
  }

  if(value.peek!float !is null) {
    return layerValueToProtoBuf(*value.peek!float);
  }

  if(value.peek!double !is null) {
    return layerValueToProtoBuf(*value.peek!double);
  }

  return [];
}

size_t layerValueToProtoBuf(Variant value, ubyte[] data) {
  if(value.peek!string !is null) {
    return layerValueToProtoBuf(*value.peek!string, data);
  }

  if(value.peek!long !is null) {
    return layerValueToProtoBuf(*value.peek!long, data);
  }

  if(value.peek!ulong !is null) {
    return layerValueToProtoBuf(*value.peek!ulong, data);
  }

  if(value.peek!uint !is null) {
    return layerValueToProtoBuf(value.get!ulong, data);
  }

  if(value.peek!int !is null) {
    return layerValueToProtoBuf(value.get!long, data);
  }

  if(value.peek!bool !is null) {
    return layerValueToProtoBuf(*value.peek!bool, data);
  }

  if(value.peek!float !is null) {
    return layerValueToProtoBuf(*value.peek!float, data);
  }

  if(value.peek!double !is null) {
    return layerValueToProtoBuf(*value.peek!double, data);
  }

  return 0;
}

/// Convert variant to protobuf
unittest {
  Variant(1L).layerValueToProtoBuf.should.equal(cast(ubyte[]) [0x20, 0x01]);
  Variant(1).layerValueToProtoBuf.should.equal(cast(ubyte[]) [0x20, 0x01]);
  Variant(1U).layerValueToProtoBuf.should.equal(cast(ubyte[]) [0x28, 0x01]);
  Variant(1UL).layerValueToProtoBuf.should.equal(cast(ubyte[]) [0x28, 0x01]);

  Variant(true).layerValueToProtoBuf.should.equal(cast(ubyte[]) [0x38, 0x01]);
  Variant(false).layerValueToProtoBuf.should.equal(cast(ubyte[]) [0x38, 0x00]);

  Variant(1.5f).layerValueToProtoBuf.should.equal(cast(ubyte[]) [0x15, 0, 0, 0xC0, 0x3F]);
  Variant(1.5).layerValueToProtoBuf.should.equal(cast(ubyte[]) [0x19, 0, 0, 0, 0, 0, 0, 0xF8, 0x3F]);

  Variant("testing").layerValueToProtoBuf.should.equal(cast(ubyte[]) [0x0A, 0x07, 0x74, 0x65, 0x73, 0x74, 0x69, 0x6e, 0x67]);
}

/// ditto
immutable(ubyte)[] layerValueToProtoBuf(string value) {
  return stringToProtoBuf(1, value).array;
}

/// ditto
size_t layerValueToProtoBuf(string value, ubyte[] data) {
  return stringToProtoBuf(1, value, data);
}

/// ditto
immutable(ubyte)[] layerValueToProtoBuf(float value) {
  return ProtoKey(2, WireType.fixed32).value.raw ~ nativeToLittleEndian(value).idup;
}

/// ditto
size_t layerValueToProtoBuf(float value, ubyte[] data) {
  size_t start;
  start += ProtoKey(2, WireType.fixed32).write(data[start..$]);

  auto v = nativeToLittleEndian(value);
  data[start..start+v.length] = v[0..$];

  return start + v.length;
}

/// ditto
immutable(ubyte)[] layerValueToProtoBuf(double value) {
  return ProtoKey(3, WireType.fixed64).value.raw ~ nativeToLittleEndian(value).idup;
}

/// ditto
size_t layerValueToProtoBuf(double value, ubyte[] data) {
  size_t start;
  start += ProtoKey(3, WireType.fixed64).write(data[start..$]);

  auto v = nativeToLittleEndian(value);
  data[start..start+v.length] = v[0..$];

  return start + v.length;
}

/// ditto
immutable(ubyte)[] layerValueToProtoBuf(long value) {
  return ProtoKey(4, WireType.varint).value.raw ~ Varint(value).raw;
}

/// ditto
size_t layerValueToProtoBuf(long value, ubyte[] data) {
  size_t start;
  start += ProtoKey(4, WireType.varint).write(data[start..$]);
  start += Varint(value).write(data[start..$]);

  return start;
}

/// ditto
immutable(ubyte)[] layerValueToProtoBuf(ulong value) {
  return ProtoKey(5, WireType.varint).value.raw ~ Varint(value).raw;
}

/// ditto
size_t layerValueToProtoBuf(ulong value, ubyte[] data) {
  size_t start;
  start += ProtoKey(5, WireType.varint).write(data[start..$]);
  start += Varint(value).write(data[start..$]);

  return start;
}

/// ditto
immutable(ubyte)[] layerValueToProtoBuf(bool value) {
  return ProtoKey(7, WireType.varint).value.raw ~ Varint(value ? 1 : 0).raw;
}

/// ditto
size_t layerValueToProtoBuf(bool value, ubyte[] data) {
  size_t start;
  start += ProtoKey(7, WireType.varint).write(data[start..$]);
  start += Varint(value ? 1 : 0).write(data[start..$]);

  return start;
}

///
size_t layerValueToProtoBufLength(Variant value) {
  if(value.peek!string !is null) {
    return layerValueToProtoBufLength(*value.peek!string);
  }

  if(value.peek!long !is null) {
    return layerValueToProtoBufLength(*value.peek!long);
  }

  if(value.peek!ulong !is null) {
    return layerValueToProtoBufLength(*value.peek!ulong);
  }

  if(value.peek!uint !is null) {
    return layerValueToProtoBufLength(value.get!ulong);
  }

  if(value.peek!int !is null) {
    return layerValueToProtoBufLength(value.get!long);
  }

  if(value.peek!bool !is null) {
    return 2;
  }

  if(value.peek!float !is null) {
    return layerValueToProtoBufLength(*value.peek!float);
  }

  if(value.peek!double !is null) {
    return layerValueToProtoBufLength(*value.peek!double);
  }

  return 0;
}

/// ditto
size_t layerValueToProtoBufLength(string value) {
  return stringToProtoBufLength(1, value);
}

/// ditto
size_t layerValueToProtoBufLength(float value) {
  return ProtoKey(2, WireType.fixed32).length + nativeToLittleEndian(value).length;
}

/// ditto
size_t layerValueToProtoBufLength(double value) {
  return ProtoKey(3, WireType.fixed64).length + nativeToLittleEndian(value).length;
}

/// ditto
size_t layerValueToProtoBufLength(long value) {
  return ProtoKey(4, WireType.varint).length + Varint(value).length;
}

/// ditto
size_t layerValueToProtoBufLength(ulong value) {
  return ProtoKey(5, WireType.varint).length + Varint(value).length;
}

/// Convert variant to protobuf
unittest {
  Variant(1L).layerValueToProtoBufLength.should.equal(2);
  Variant(1).layerValueToProtoBufLength.should.equal(2);
  Variant(1U).layerValueToProtoBufLength.should.equal(2);
  Variant(1UL).layerValueToProtoBufLength.should.equal(2);

  Variant(true).layerValueToProtoBufLength.should.equal(2);
  Variant(false).layerValueToProtoBufLength.should.equal(2);

  Variant(1.5f).layerValueToProtoBufLength.should.equal(5);
  Variant(1.5).layerValueToProtoBufLength.should.equal(9);

  Variant("testing").layerValueToProtoBufLength.should.equal(9);
}

/// Convert a protobuf value to variant
Variant readLayerValue(T)(T data) if(isInputRange!T && is(Unqual!(ElementType!T) == ubyte)) {
  scope range = VarintInputRange(data);
  return readLayerValue(range);
}

/// ditto
Variant readLayerValue(VarintInputRange data) {
  auto key = ProtoKey(data.moveFront.raw);

  if(key.index == 2) {
    return Variant(littleEndianToNative!(float)(data.readRaw!4));
  }

  if(key.index == 3) {
    return Variant(littleEndianToNative!(double)(data.readRaw!8));
  }

  auto value = data.moveFront;

  if(key.index == 1) {
    return Variant(data.readRaw(value.to!ulong).idup.assumeUTF);
  }

  if(key.index == 4) {
    return Variant(value.to!long);
  }

  if(key.index == 5) {
    return Variant(value.to!ulong);
  }

  if(key.index == 6) {
    return Variant(value.to!long);
  }

  if(key.index == 7) {
    return Variant(value.to!size_t ? true : false);
  }

  return Variant();
}

/// Convert protobuf value to variant
unittest {
  (cast(ubyte[]) [0x20, 0x01]).readLayerValue.get!long.should.equal(1L);
  (cast(ubyte[]) [0x28, 0x01]).readLayerValue.get!ulong.should.equal(1U);

  (cast(ubyte[]) [0x38, 0x01]).readLayerValue.get!bool.should.equal(true);
  (cast(ubyte[]) [0x38, 0x00]).readLayerValue.get!bool.should.equal(false);

  (cast(ubyte[]) [0x15, 0, 0, 0xC0, 0x3F]).readLayerValue.get!float.should.equal(1.5f);
  (cast(ubyte[]) [0x19, 0, 0, 0, 0, 0, 0, 0xF8, 0x3F]).readLayerValue.get!double.should.equal(1.5);

  (cast(ubyte[]) [0x0A, 0x07, 0x74, 0x65, 0x73, 0x74, 0x69, 0x6e, 0x67]).readLayerValue.get!string.should.equal("testing");
}

///
void addFeature(ref Layer layer, VectorFeature.GeomType type, Geometry geometry) {
  VectorFeature feature;
  feature.type = type;
  feature.geometry = geometry;

  layer.features ~= feature;
}

/// Adding features to a layer
unittest {
  Layer layer;
  uint[] geometry = [ 9, 2410, 3080 ];

  layer.addFeature(VectorFeature.GeomType.point, Geometry.deserialize(geometry));
  layer.addProperty("hello", "world");
  layer.addProperty("h", "world");
  layer.addProperty("count", 1.23);

  layer.addFeature(VectorFeature.GeomType.point, Geometry.deserialize(geometry));
  layer.addProperty("hello", "again");
  layer.addProperty("count", 2);

  layer.features.length.should.equal(2);
  layer.features[0].type.should.equal(VectorFeature.GeomType.point);
  layer.features[0].geometry.serialize.array.should.equal(geometry);
  layer.features[0].tags.should.equal(cast(ulong[])[0, 0, 1, 0, 2, 1]);

  layer.features[1].type.should.equal(VectorFeature.GeomType.point);
  layer.features[1].geometry.serialize.array.should.equal(geometry);
  layer.features[1].tags.should.equal(cast(ulong[])[0, 2, 2, 3]);
}

/// Add a new property to the last feature in the layer
void addProperty(T)(ref Layer layer, string key, T value) {
  long featureIndex = layer.features.length - 1;
  long keyIndex = layer.keys.countUntil(key);
  long valueIndex = layer.values.countUntil(value);

  if(keyIndex == -1) {
    keyIndex = layer.keys.length;
    layer.keys ~= key;
  }

  if(valueIndex == -1) {
    valueIndex = layer.values.length;
    layer.values ~= Variant(value);
  }

  layer.features[featureIndex].tags ~= [ keyIndex, valueIndex ];
}

/// Convert a layer to protobuf
InputRange!(immutable(ubyte)) toProtoBuf(Layer layer) {
    immutable version_ = ProtoKey(15, WireType.varint).value.raw ~ Varint(layer.version_).raw;
    immutable name = stringToProtoBuf(1, layer.name).array;

    ubyte[] features;
    foreach(feature; layer.features) {
      auto protoFeature = feature.toProtoBuf.array;
      features ~= ProtoKey(2, WireType.fixedLength).value.raw ~ Varint(protoFeature.length).raw ~ protoFeature;
    }

    ubyte[] keys;
    foreach(key; layer.keys) {
      keys ~= stringToProtoBuf(3, key).array;
    }

    ubyte[] values;
    foreach(value; layer.values) {
      auto protoValue = layerValueToProtoBuf(value).array;
      values ~= ProtoKey(4, WireType.fixedLength).value.raw ~ Varint(protoValue.length).raw ~ protoValue;
    }

    auto extent = ProtoKey(5, WireType.varint).value.raw ~ Varint(layer.extent).raw;

    return chain(version_,
      name,
      cast(immutable) features,
      cast(immutable) keys,
      cast(immutable) values,
      extent).inputRangeObject;
}

/// Create a layer from protobuf data
Layer readLayer(T)(T data) if(isInputRange!T && is(Unqual!(ElementType!T) == ubyte)) {
  scope range = VarintInputRange(data);
  return readLayer(range);
}

/// ditto
Layer readLayer(VarintInputRange range) {
  Layer layer;

  while(!range.empty) {
    auto key = ProtoKey(range.front);
    range.popFront;

    auto value = range.front;
    range.popFront;

    if(key.index == 15) {
      layer.version_ = value.to!uint;
    }

    if(key.index == 1) {
      layer.name = range.readRaw(value.to!ulong).idup.assumeUTF;
    }

    if(key.index == 2) {
      layer.features ~= range.readRaw(value.to!ulong).readFeature;
    }

    if(key.index == 3) {
      layer.keys ~= range.readRaw(value.to!ulong).idup.assumeUTF;
    }

    if(key.index == 4) {
      layer.values ~= range.readRaw(value.to!ulong).readLayerValue;
    }

    if(key.index == 5) {
      layer.extent = value.to!uint;
    }
  }

  return layer;
}

/// Convert a layer to proto buf and back
unittest {
  Layer layer;
  layer.version_ = 3;
  layer.name = "test layer";
  layer.extent = 512;
  uint[] geometry = [ 9, 2410, 3080 ];

  layer.addFeature(VectorFeature.GeomType.point, Geometry.deserialize(geometry));
  layer.addProperty("hello", "world");
  layer.addProperty("h", "world");
  layer.addProperty("count", 1.23);

  layer.addFeature(VectorFeature.GeomType.point, Geometry.deserialize(geometry));
  layer.addProperty("hello", "again");
  layer.addProperty("count", 2);

  auto result = layer.toProtoBuf;

  layer = result.readLayer;

  layer.version_.should.equal(3);
  layer.name.should.equal("test layer");
  layer.extent.should.equal(512);

  layer.features.length.should.equal(2);
  layer.features[0].type.should.equal(VectorFeature.GeomType.point);
  layer.features[0].geometry.serialize.array.should.equal(geometry);
  layer.features[0].tags.should.equal(cast(ulong[])[0, 0, 1, 0, 2, 1]);

  layer.features[1].type.should.equal(VectorFeature.GeomType.point);
  layer.features[1].geometry.serialize.array.should.equal(geometry);
  layer.features[1].tags.should.equal(cast(ulong[])[0, 2, 2, 3]);

  layer.keys.should.equal(["hello", "h", "count"]);
  layer.values.map!(a => a.to!string).should.equal(["world", "1.23", "again", "2"]);
}

/// ditto
size_t toProtoBuf(Layer layer, ubyte[] data) {
  size_t start;

  start += ProtoKey(15, WireType.varint).write(data[start..$]);
  start += Varint(layer.version_).write(data[start..$]);
  start += stringToProtoBuf(1, layer.name, data[start..$]);

  foreach(feature; layer.features) {
    auto protoFeature = feature.toProtoBuf.array;
    size_t protoFeatureLength = feature.rawLength;

    start += ProtoKey(2, WireType.fixedLength).write(data[start..$]);
    start += Varint(protoFeatureLength).write(data[start..$]);
    start += feature.toProtoBuf(data[start..$]);
  }

  foreach(key; layer.keys) {
    start += stringToProtoBuf(3, key, data[start..$]);
  }

  foreach(value; layer.values) {
    start += ProtoKey(4, WireType.fixedLength).write(data[start..$]);
    start += Varint(value.layerValueToProtoBufLength).write(data[start..$]);
    start += layerValueToProtoBuf(value, data[start..$]);
  }

  start += ProtoKey(5, WireType.varint).write(data[start..$]);
  start += Varint(layer.extent).write(data[start..$]);

  return start;
}

/// both toProtoBuf functions have the same output
unittest {
  Layer layer;
  layer.version_ = 3;
  layer.name = "test layer";
  layer.extent = 512;
  uint[] geometry = [ 9, 2410, 3080 ];

  layer.addFeature(VectorFeature.GeomType.point, Geometry.deserialize(geometry));
  layer.addProperty("hello", "world");
  layer.addProperty("h", "world");
  layer.addProperty("count", 1.23);

  layer.addFeature(VectorFeature.GeomType.point, Geometry.deserialize(geometry));
  layer.addProperty("hello", "again");
  layer.addProperty("count", 2);

  ubyte[] buffer;
  buffer.length = 1024;

  auto result1 = layer.toProtoBuf;
  auto size = layer.toProtoBuf(buffer);

  result1.array.should.equal(buffer[0..size]);
}

///
struct Tile {
  ///
  Layer[] layers;
}

/// Convert a feature to proto buf
InputRange!(immutable(ubyte)) toProtoBuf(Tile tile) {
  return tile.layers
    .map!(a => a.toProtoBuf.array.idup)
    .map!(a => ProtoKey(3, WireType.fixedLength).value.raw ~ Varint(a.length).raw ~ a)
    .joiner.inputRangeObject;
}

/// ditto
size_t toProtoBuf(Tile tile, ref ubyte[] data) {
  size_t start;

  foreach (layer; tile.layers) {
    start += ProtoKey(3, WireType.fixedLength).write(data[start..$]);
    start += Varint(layer.rawLength).write(data[start..$]);
    start += layer.toProtoBuf(data[start..$]);
  }

  return start;
}

/// Create a layer from protobuf data
Tile readTile(T)(T data) if(isInputRange!T && is(Unqual!(ElementType!T) == ubyte)) {
  scope range = VarintInputRange(data);
  return readTile(range);
}

/// ditto
Tile readTile(VarintInputRange range) {
  Tile tile;

  while(!range.empty) {
    auto key = ProtoKey(range.front);
    range.popFront;

    auto value = range.front;
    range.popFront;

    if(key.index == 3) {
      tile.layers ~= range.readRaw(value.to!ulong).readLayer;
    }
  }

  return tile;
}

/// convert a Tile to proto buf and back
unittest {
  Tile tile;
  tile.layers ~= Layer();
  tile.layers ~= Layer();

  tile = readTile(tile.toProtoBuf);

  tile.layers.length.should.equal(2);
}
