/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module geo.geopackage.exception;

///
class GeoPackageException : Exception {
  this(string msg = null, Throwable next = null, string file = __FILE__, size_t line = __LINE__) @safe {
    super(msg, next, file, line);
  }

  this(string msg, string file, size_t line, Throwable next = null) @safe {
    super(msg, file, line, next);
  }
}
