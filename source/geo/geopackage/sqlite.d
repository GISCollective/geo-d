/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module geo.geopackage.sqlite;

import std.uni;

///
string formatSqlName(ref const string name) pure {
  char[] result = cast(char[]) name.dup.toLower;
  foreach (i; 0..result.length) {
    if(!isAlphaNum(result[i])) {
      result[i] = '_';
    }
  }

  return cast(string) result;
}
