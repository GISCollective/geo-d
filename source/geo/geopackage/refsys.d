/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module geo.geopackage.refsys;

import d2sqlite3;

///
struct GpkgRefSys {
  string srs_name;
  int srs_id;
  string organization;
  int organization_coordsys_id;
  string definition;
  string description;
  string definition_12_063;

  static GpkgRefSys undefinedGeographicSRS() {
    return GpkgRefSys(
      "Undefined geographic SRS",
      0,
      "NONE",
      0,
      `undefined`,
      `undefined geographic coordinate reference system`,
      "NONE"
    );
  }

  static GpkgRefSys undefinedCartesianSRS() {
    return GpkgRefSys(
      "Undefined cartesian SRS",
      -1,
      "NONE",
      0,
      `undefined`,
      `undefined cartesian coordinate reference system`,
      "NONE"
    );
  }

  static GpkgRefSys wgs84() {
    return GpkgRefSys(
      "WGS 84 geodetic",
      4326,
      "EPSG",
      4326,
      `GEOGCS["WGS 84",DATUM["WGS_1984",SPHEROID["WGS 84",6378137,298.257223563,AUTHORITY["EPSG","7030"]],AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.0174532925199433,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4326"]]`,
      `longitude/latitude coordinates in decimal degrees on the WGS 84 spheroid`,
      "NONE"
    );
  }
}

///
class GpkgSpatialRefSysList {
  protected {
    Database* db;
    Statement insertSrsStatement;
  }

  ///
  this(Database* db) {
    this.db = db;

    db.run(`CREATE TABLE IF NOT EXISTS gpkg_spatial_ref_sys (
        srs_name TEXT NOT NULL,
        srs_id INTEGER PRIMARY KEY,
        organization TEXT NOT NULL,
        organization_coordsys_id INTEGER NOT NULL,
        definition  TEXT NOT NULL,
        description TEXT
      );`);

    insertSrsStatement = db.prepare(`INSERT INTO "gpkg_spatial_ref_sys"
      ("srs_name", "srs_id", "organization", "organization_coordsys_id", "definition", "description", "definition_12_063") VALUES
      (:srs_name,  :srs_id,  :organization,  :organization_coordsys_id,  :definition,  :description,  :definition_12_063);`);
  }

  ///
  void setupDefault() {
    add(GpkgRefSys.undefinedCartesianSRS);
    add(GpkgRefSys.undefinedGeographicSRS);
    add(GpkgRefSys.wgs84);
  }

  ///
  void add(GpkgRefSys value) {
    insertSrsStatement.bindAll(
      value.srs_name,
      value.srs_id,
      value.organization,
      value.organization_coordsys_id,
      value.definition,
      value.description,
      value.definition_12_063);

    scope(exit) insertSrsStatement.reset;

    insertSrsStatement.execute;
  }

  ///
  void close() {
    insertSrsStatement.finalize;
  }
}
