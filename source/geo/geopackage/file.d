/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
/// Implementation for the geo package spec
/// http://www.geopackage.org/spec/
module geo.geopackage.file;

import geo.geopackage.standardGeoPackageBinary;
import geo.geopackage.exception;
import geo.geopackage.layer;
import geo.geopackage.refsys;
import geo.json;

import std.conv;
import std.exception;

import d2sqlite3;

version(unittest) {
  import fluent.asserts;
  import vibe.data.json;
  import std.array;
}

///
class GeoPackage {
  protected {
    Database* db;

    GpkgLayerList _geometryLayers;
    GpkgSpatialRefSysList  _spatialRefSysList;
  }

  ///
  this(string path) {
    db = new Database(path);

    db.run(`CREATE TABLE IF NOT EXISTS gpkg_spatial_ref_sys  (
              srs_name                  TEXT NOT NULL,
              srs_id                    INTEGER NOT NULL PRIMARY KEY,
              organization              TEXT NOT NULL,
              organization_coordsys_id  INTEGER NOT NULL,
              definition                TEXT NOT NULL,
              description               TEXT,
              definition_12_063         TEXT NOT NULL
    )`);

    _geometryLayers = new GpkgLayerList(db);
    _spatialRefSysList = new GpkgSpatialRefSysList(db);
  }

  ///
  GpkgLayerList geometryLayers() {
    return _geometryLayers;
  }

  ///
  GpkgSpatialRefSysList spatialRefSys() {
    return _spatialRefSysList;
  }

  ///
  void close() {
    _geometryLayers.close;
    _spatialRefSysList.close;

    db.close;
    db = null;
  }
}

/// Should be able to create a new geometry layer with a point feature
unittest {
  import std.file;
  if("./test/test.gpkg".exists) {
    "./test/test.gpkg".remove;
  }

  auto gpkg = new GeoPackage("./test/test.gpkg");
  scope(exit) gpkg.close;

  gpkg.spatialRefSys.setupDefault;

  auto layer = gpkg.geometryLayers.create("some layer", 4326, 0, 0, GpkgColumn("geometry", "POINT"),
    [ GpkgColumn("some number", "INTEGER"), GpkgColumn("some text", "TEXT") ]);

  auto geometry1 = GeoJsonGeometry.fromJson(`{"type": "Point", "coordinates": [1,2]}`.parseJsonString);
  auto geometry2 = GeoJsonGeometry.fromJson(`{"type": "Point", "coordinates": [3,4]}`.parseJsonString);

  string[string] attributes1;
  attributes1["some number"] = "1";
  attributes1["some text"] = "hello";

  string[string] attributes2;
  attributes2["some number"] = "2";
  attributes2["some text"] = "world";

  layer.count.should.equal(0);

  layer.add(geometry1, attributes1);
  layer.add(geometry2, attributes2);

  layer.count.should.equal(2);
  layer.fields.should.containOnly([ "geometry", "fid", "some number", "some text" ]);
  layer.geometryField.should.equal("geometry");

  layer.get.array.serializeToJson.to!string.should.equal(`[
    {"geometry":{"type":"Point","coordinates":[1,2]},"fid":1,"some number":1,"some text":"hello"},
    {"geometry":{"type":"Point","coordinates":[3,4]},"fid":2,"some number":2,"some text":"world"}]`
    .parseJsonString.to!string);
}

/// Should be able to list geometry layers
unittest {
  import std.file;
  if("./test/test.gpkg".exists) {
    "./test/test.gpkg".remove;
  }

  auto gpkg = new GeoPackage("./test/test.gpkg");
  scope(exit) gpkg.close;

  gpkg.spatialRefSys.setupDefault;

  gpkg.geometryLayers.create("some layer", 4326, 0, 0, GpkgColumn("geometry", "POINT"),
    [ GpkgColumn("some number", "INTEGER"), GpkgColumn("some text", "TEXT") ]);
  gpkg.geometryLayers.create("other layer", 4326, 0, 0, GpkgColumn("geometry", "POINT"),
    [ GpkgColumn("other number", "INTEGER"), GpkgColumn("other text", "TEXT") ]);

  gpkg.geometryLayers.names.should.containOnly(["some_layer", "other_layer"]);
}

/// Should be able to get geometry layers by name
unittest {
  import std.file;
  if("./test/test.gpkg".exists) {
    "./test/test.gpkg".remove;
  }

  auto gpkg = new GeoPackage("./test/test.gpkg");
  gpkg.spatialRefSys.setupDefault;

  gpkg.geometryLayers.create("other layer", 4326, 0, 0, GpkgColumn("other geometry", "POINT"),
    [ GpkgColumn("other number", "INTEGER"), GpkgColumn("other text", "TEXT") ]);
  gpkg.close;

  gpkg = new GeoPackage("./test/test.gpkg");
  scope(exit) gpkg.close;
  auto layer = gpkg.geometryLayers.get("other_layer");

  layer.fields.should.containOnly([ "other geometry", "fid", "other number", "other text" ]);
  layer.geometryField.should.equal("other geometry");
}

/// Should be able to get geometry layers by name after it was added
unittest {
  import std.file;
  if("./test/test.gpkg".exists) {
    "./test/test.gpkg".remove;
  }

  auto gpkg = new GeoPackage("./test/test.gpkg");
  scope(exit) gpkg.close;

  gpkg.spatialRefSys.setupDefault;

  gpkg.geometryLayers.create("some layer", 4326, 0, 0, GpkgColumn("geometry", "POINT"),
    [ GpkgColumn("some number", "INTEGER"), GpkgColumn("some text", "TEXT") ]);
  gpkg.geometryLayers.create("other layer", 4326, 0, 0, GpkgColumn("other geometry", "POINT"),
    [ GpkgColumn("other number", "INTEGER"), GpkgColumn("other text", "TEXT") ]);

  auto layer = gpkg.geometryLayers.get("other_layer");

  layer.fields.should.containOnly([ "other geometry", "fid", "other number", "other text" ]);
  layer.geometryField.should.equal("other geometry");
}
