/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module geo.geopackage.layer;

import geo.geopackage.exception;
import geo.geopackage.sqlite;
import geo.geopackage.standardGeoPackageBinary;
import geo.json;

import d2sqlite3;

import std.exception;
import std.range;
import std.algorithm;


import vibe.data.json;

///
struct GpkgColumn {
  /// column name
  string name;

  /// sqlite type
  string type;

  ///
  string toString() {
    return `"` ~ name ~ `" ` ~ type;
  }
}

///
class GpkgLayerList {
  protected {
    Database* db;
    Statement insertLayerStatement;
    Statement insertGeometryStatement;

    GpkgLayer[string] layers;
  }

  ///
  this(Database* db) {
    this.db = db;
    db.run(`CREATE TABLE IF NOT EXISTS gpkg_contents (
              table_name TEXT NOT NULL PRIMARY KEY,
              data_type TEXT NOT NULL,
              identifier TEXT UNIQUE,
              description TEXT DEFAULT '',
              last_change DATETIME NOT NULL DEFAULT (strftime('%Y-%m-%dT%H:%M:%fZ','now')),
              min_x DOUBLE,
              min_y DOUBLE,
              max_x DOUBLE,
              max_y DOUBLE,
              srs_id INTEGER,
              CONSTRAINT fk_gc_r_srs_id FOREIGN KEY (srs_id) REFERENCES gpkg_spatial_ref_sys(srs_id)
    );`);

    db.run(`CREATE TABLE IF NOT EXISTS gpkg_geometry_columns (
              table_name TEXT NOT NULL,
              column_name TEXT NOT NULL,
              geometry_type_name TEXT NOT NULL,
              srs_id INTEGER NOT NULL,
              z TINYINT NOT NULL,
              m TINYINT NOT NULL,
              CONSTRAINT pk_geom_cols PRIMARY KEY (table_name, column_name),
              CONSTRAINT uk_gc_table_name UNIQUE (table_name),
              CONSTRAINT fk_gc_tn FOREIGN KEY (table_name) REFERENCES gpkg_contents(table_name),
              CONSTRAINT fk_gc_srs FOREIGN KEY (srs_id) REFERENCES gpkg_spatial_ref_sys (srs_id)
      );`);

    insertLayerStatement = db.prepare(`INSERT INTO "gpkg_contents"
      ("table_name", "data_type", "identifier", "description", "last_change", "min_x", "min_y", "max_x", "max_y", "srs_id") VALUES
      (:table_name,  :data_type,  :identifier,  :description,  :last_change,  :min_x,  :min_y,  :max_x,  :max_y,  :srs_id);`);

    insertGeometryStatement = db.prepare(`INSERT INTO "gpkg_geometry_columns"
        ("table_name", "column_name", "geometry_type_name", "srs_id", "z", "m") VALUES
        (:table_name,  :column_name,  :geometry_type_name,  :srs_id,  :z,  :m);`);
  }

  string[] names() {
    auto selector = db.prepare(`SELECT table_name FROM "gpkg_contents"`);
    auto rows = selector.execute;
    selector.finalize;

    return rows.map!(a => a.peek!string("table_name")).array;
  }

  ///
  GpkgLayer create(string layerName, int srs_id, uint z , uint m, GpkgColumn geometryColumn, GpkgColumn[] otherColumns = []) {
    auto tableName = layerName.formatSqlName;

    enforce!GeoPackageException(!names.canFind(tableName), "The layer `" ~ layerName ~ "` already exists.");

    string createTable = `CREATE TABLE IF NOT EXISTS "` ~ tableName ~ `" ( "fid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL `;

    createTable ~= `, ` ~ geometryColumn.toString;
    foreach (column; otherColumns) {
      createTable ~= `, ` ~ column.toString;
    }

    createTable ~= `);`;

    db.run(createTable);

    insertLayerStatement.bindAll(tableName, "features", layerName, "", "", 0, 0, 0, 0, 0);
    insertGeometryStatement.bindAll(tableName, geometryColumn.name, geometryColumn.type, srs_id, z, m);

    scope(exit) {
      insertLayerStatement.reset;
      insertGeometryStatement.reset;
    }

    insertLayerStatement.execute;
    insertGeometryStatement.execute;

    layers[tableName] = new GpkgLayer(db, tableName);
    return layers[tableName];
  }

  GpkgLayer get(string tableName) {
    if(tableName !in layers) {
      layers[tableName] = new GpkgLayer(db, tableName);
    }

    return layers[tableName];
  }

  ///
  void close() {
    insertGeometryStatement.finalize;
    insertLayerStatement.finalize;

    foreach(layer; layers) {
      layer.close;
    }
  }
}

///
class GpkgLayer {
  protected {
    Database* db;
    string tableName;

    string[string] _columns;
    string _geometry_column;
    string _type;
    int _srs_id;
    uint _z;
    uint _m;

    Statement[] selectors;
  }

  ///
  this(Database* db, string tableName) {
    this.db = db;
    this.tableName = tableName;

    updateMeta();
  }

  private void updateMeta() {
    auto prepareColumn = db.prepare("SELECT * FROM gpkg_geometry_columns WHERE table_name=:table_name");
    scope(exit) prepareColumn.finalize;
    prepareColumn.bindAll(tableName);

    auto result = prepareColumn.execute;

    enforce!GeoPackageException(!result.empty, "The geometry column is not set for `" ~ tableName ~ "`");

    _geometry_column = result.front.peek!string("column_name");
    _type = result.front.peek!string("geometry_type_name");
    _srs_id = result.front.peek!int("srs_id");
    _z = result.front.peek!uint("z");
    _m = result.front.peek!uint("m");

    auto prepareAllColumns = db.prepare(`PRAGMA table_info("` ~ tableName ~ `")`);
    scope(exit) prepareAllColumns.finalize;
    result = prepareAllColumns.execute;

    enforce!GeoPackageException(!result.empty, "The fields for the table `" ~ tableName ~ "` can not be fetched");

    foreach(column; result) {
      _columns[column.peek!string("name")] = column.peek!string("type");
    }
  }

  ///
  void add(Json geometry, string[string] attributes) {
    add(GeoJsonGeometry.fromJson(geometry), attributes);
  }

  ///
  void add(GeoJsonGeometry geometry, string[string] attributes) {
    string insert = `INSERT INTO "` ~ tableName ~ `" `;
    string keys = `"` ~ _geometry_column ~ `"`;
    string values = `:` ~ _geometry_column;

    foreach (key, value; attributes) {
      keys ~= `, "` ~ key ~ `"`;
      values ~= `, :` ~ key.formatSqlName;
    }

    insert ~= `(` ~ keys ~ `) VALUES (` ~ values ~ `)`;
    auto prepared = db.prepare(insert);
    scope(exit) prepared.finalize;

    prepared.bind(":" ~ _geometry_column, geometry.toStandardGeoPackageBinary(_srs_id));

    foreach (key, value; attributes) {
      prepared.bind(":" ~ key.formatSqlName, value);
    }

    prepared.execute;
  }

  ///
  InputRange!Json get(uint begin = 0, uint length = uint.max) {
    auto selector = db.prepare(`SELECT * FROM "` ~ tableName ~ `" LIMIT :begin, :len`);
    selector.bindAll(begin, length);

    auto rows = selector.execute;
    selector.finalize;

    Json transform(Row row) {
      auto result = Json.emptyObject;

      foreach(i; 0..row.length()) {
        auto key = row.columnName(i);
        auto type = row.columnType(i);

        if(key == _geometry_column) {
          result[key] = row[i].as!(ubyte[]).parseStandardGeoPackageBinary.geometry.toJson;
          continue;
        }

        switch(type) {
          case SqliteType.INTEGER:
            result[key] = row.peek!long(i);
            break;

          case SqliteType.FLOAT:
            result[key] = row.peek!double(i);
            break;

          case SqliteType.TEXT:
            result[key] = row.peek!string(i);
            break;

          default:
            result[key] = row.peek!string(i);
        }
      }

      return result;
    }

    return rows.map!(a => transform(a)).array.inputRangeObject;
  }

  string geometryField() {
    return this._geometry_column;
  }

  string[] fields() {
    return _columns.keys;
  }

  size_t count() {
    return db.prepare(`select Count(1) from "` ~ tableName ~ `"`).execute.oneValue!size_t;
  }

  void close() {
  }
}