/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module geo.geopackage.standardGeoPackageBinary;

import geo.geopackage.exception;
import geo.geometries;
import geo.json;
import geo.wkb;

import std.bitmanip;
import std.exception;

version(unittest) {
  import fluent.asserts;
  import vibe.data.json;
}

///
struct GeoPackageBinaryHeaderFlags {
  mixin(bitfields!(
      bool,  "byteOrderForHeaderValues",      1,
      uint,  "envelopeContentsIndicatorCode", 3,
      bool,  "emptyGeometryFlag",             1,
      bool,  "geoPackageBinaryType",          1,
      uint,  "reserved",                      2
      ));

  static if(__traits(hasMember, typeof(this), "_byteOrderForHeaderValues_envelopeContentsIndicatorCode_emptyGeometryFlag_geoPackageBinaryType_reserved")) {
    alias raw = _byteOrderForHeaderValues_envelopeContentsIndicatorCode_emptyGeometryFlag_geoPackageBinaryType_reserved;
  }

  static if(__traits(hasMember, typeof(this), "_byteOrderForHeaderValues_envelopeContentsIndicatorCode_emptyGeometryFlag_geoPackageBinaryType_reserved_bf")) {
    alias raw = _byteOrderForHeaderValues_envelopeContentsIndicatorCode_emptyGeometryFlag_geoPackageBinaryType_reserved_bf;
  }
}

////
struct GeoPackageBinaryHeader {
  /// 'GP' in ASCII
  enum ubyte[2] magic = [ 0x47, 0x50 ];

  /// 8-bit unsigned integer, 0 = version 1
  byte version_;

  /// see bit layout of GeoPackageBinary flags byte
  GeoPackageBinaryHeaderFlags flags;

  ///
  int srs_id;

  /// see flags envelope contents indicator code below
  double[] envelope;
}

///
struct StandardGeoPackageBinary {
  /// The X bit in the header flags field must be set to 0.
  GeoPackageBinaryHeader header;

  ///
  GeoJsonGeometry geometry;
}

///
StandardGeoPackageBinary parseStandardGeoPackageBinary(const ubyte[] value) @trusted {
  StandardGeoPackageBinary result;
  ubyte[] remainingData = value.dup;

  T next(T)() {
    if(result.header.flags.byteOrderForHeaderValues) {
      return remainingData.read!(T, Endian.littleEndian);
    }

    return remainingData.read!T;
  }

  enforce!GeoPackageException(remainingData.read!ubyte == GeoPackageBinaryHeader.magic[0], "The blob must start with [ 0x47, 0x50 ]");
  enforce!GeoPackageException(remainingData.read!ubyte == GeoPackageBinaryHeader.magic[1], "The blob must start with [ 0x47, 0x50 ]");

  result.header.version_ = remainingData.read!ubyte;
  result.header.flags = cast(GeoPackageBinaryHeaderFlags) remainingData.read!ubyte;
  result.header.srs_id = next!int;

  if(result.header.flags.emptyGeometryFlag) {
    result.header.flags.envelopeContentsIndicatorCode = 0;
    result.geometry = GeoJsonGeometry.nullPoint;

    return result;
  }

  if(result.header.flags.envelopeContentsIndicatorCode > 0) {
    result.header.envelope ~= next!double;
    result.header.envelope ~= next!double;
    result.header.envelope ~= next!double;
    result.header.envelope ~= next!double;
  }

  if(result.header.flags.envelopeContentsIndicatorCode > 1) {
    result.header.envelope ~= next!double;
    result.header.envelope ~= next!double;
  }

  if(result.header.flags.envelopeContentsIndicatorCode > 3) {
    result.header.envelope ~= next!double;
    result.header.envelope ~= next!double;
  }

  result.geometry = remainingData.parseWKB;

  return result;
}

/// It should parse binary data encoding a polygon
unittest {
  ubyte[] data = [
    0x47, 0x50, // header
    0x00,       // version
    0x03,       // flags
    0xe6, 0x10, // srs_id
                // no envelope
    0x00,
    0x00, 0x64, 0xd6, 0x84,
    0x0c, 0xd8, 0x50, 0x28,
    0x40, 0x6f, 0x2c, 0x83,
    0x89, 0xac,
    0x15, 0x35, 0x40, 0x70, 0xf5, 0xd7, 0xea, 0x89,
    0x23, 0x47, 0x40, 0x51, 0xd8, 0x99, 0x33, 0xfb,
    0x61, 0x48, 0x40, 0x01, 0x03, 0x00, 0x00, 0x00,
    0x01, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00,
    0x92, 0x5c, 0x3d, 0x56, 0xf2, 0xd1, 0x2f, 0x40,
    0x51, 0xd8, 0x99, 0x33, 0xfb, 0x61, 0x48, 0x40,
    0x6f, 0x2c, 0x83, 0x89, 0xac, 0x15, 0x35, 0x40,
    0x94, 0xc1, 0x86, 0x6c, 0xbf, 0xb1, 0x47, 0x40,
    0xca, 0x9e, 0x1e, 0x02, 0x3c, 0x09, 0x2f, 0x40,
    0x70, 0xf5, 0xd7, 0xea, 0x89, 0x23, 0x47, 0x40,
    0x64, 0xd6, 0x84, 0x0c, 0xd8, 0x50, 0x28, 0x40,
    0x30, 0x76, 0x7e, 0x93, 0x4a, 0xc0, 0x47, 0x40,
    0x64, 0xfd, 0x84, 0xae, 0x80, 0xea, 0x2d, 0x40,
    0xdc, 0xda, 0x96, 0x3f, 0x45, 0xf7, 0x47, 0x40,
    0x92, 0x5c, 0x3d, 0x56, 0xf2, 0xd1, 0x2f, 0x40,
    0x51, 0xd8, 0x99, 0x33, 0xfb, 0x61, 0x48, 0x40
  ];

  auto result = data.parseStandardGeoPackageBinary;

  result.geometry.toJson.to!string.should.equal(`{
    "type": "Polygon",
    "coordinates": [[
      [ 15.91005200861972, 48.7654785634228 ],
      [ 21.08466395809631, 47.38865429477588 ],
      [ 15.51803595184119, 46.2776464037405 ],
      [ 12.15789832231093, 47.50227588343512 ],
      [ 14.95801301358615, 47.93180079332316 ],
      [ 15.91005200861972, 48.7654785634228 ]
    ]]}`.parseJsonString.to!string);
}

///
ubyte[] toBytes(StandardGeoPackageBinary pack) {
  ubyte[] result;
  size_t index;
  result.length = 4 + uint.sizeof + pack.header.envelope.length * double.sizeof;

  void add(T)(T value) {
    result.write!T(value, index);
    index += T.sizeof;
  }

  add(GeoPackageBinaryHeader.magic[0]);
  add(GeoPackageBinaryHeader.magic[1]);
  add(pack.header.version_);
  add(pack.header.flags.raw);
  add(pack.header.srs_id);

  foreach(value; pack.header.envelope) {
    add(value);
  }

  return result ~ pack.geometry.toWKB;
}

/// It should be able to serialize all commponents of a StandardGeoPackageBinary
unittest {
  StandardGeoPackageBinary pack;

  pack.header.flags.byteOrderForHeaderValues = 1;
  pack.header.flags.envelopeContentsIndicatorCode = 1;
  pack.header.srs_id = 4086;
  pack.header.envelope = [1,2,3,4];
  pack.geometry = GeoJsonGeometry.fromJson(`{"type": "Point", "coordinates": [1,2]}`.parseJsonString);

  pack.toBytes.should.equal(cast(ubyte[]) [
    71, 80,
    0, 3, 0, 0,
    15, 246, 63, 240,
    0,    0,  0, 0,
    0,    0, 64,   0, 0, 0, 0, 0,
    0,    0, 64,   8, 0, 0, 0, 0,
    0,    0, 64,  16, 0, 0, 0, 0,
    0,    0,  0,

    0, 0, 0,  1,
    63, 240,  0,   0, 0, 0, 0, 0,
    64,   0,  0,   0, 0, 0, 0, 0]);
}

/// It should be able to serialize and deserialize a header with empty envelope
unittest {
  StandardGeoPackageBinary pack;
  pack.header.flags.byteOrderForHeaderValues = 0;
  pack.header.flags.envelopeContentsIndicatorCode = 0;
  pack.geometry = GeoJsonGeometry.fromJson(`{"type": "Point", "coordinates": [1,2]}`.parseJsonString);

  pack.toBytes.should.equal(cast(ubyte[]) [71, 80, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 1, 63, 240, 0, 0, 0, 0, 0, 0, 64, 0, 0, 0, 0, 0, 0, 0]);

  auto result = pack.toBytes.parseStandardGeoPackageBinary;
  result.header.flags.raw.should.equal(pack.header.flags.raw);
  result.header.envelope.should.equal(pack.header.envelope);
}

/// It should be able to serialize and deserialize a header with z envelope
unittest {
  StandardGeoPackageBinary pack;
  pack.header.flags.byteOrderForHeaderValues = 0;
  pack.header.flags.envelopeContentsIndicatorCode = 2;
  pack.header.envelope = [1,2,3,4,5,6];
  pack.geometry = GeoJsonGeometry.fromJson(`{"type": "Point", "coordinates": [1,2,3]}`.parseJsonString);

  pack.toBytes.should.equal(cast(ubyte[]) [
    71, 80, 0, 4, 0, 0, 0, 0,
    63, 240, 0, 0, 0, 0, 0, 0,
    64, 0, 0, 0, 0, 0, 0, 0,
    64, 8, 0, 0, 0, 0, 0, 0,
    64, 16, 0, 0, 0, 0, 0, 0,
    64, 20, 0, 0, 0, 0, 0, 0,
    64, 24, 0, 0, 0, 0, 0, 0, 0,

    0, 0, 0, 1, 63, 240, 0, 0, 0, 0, 0, 0, 64, 0, 0, 0, 0, 0, 0, 0]);

  auto result = pack.toBytes.parseStandardGeoPackageBinary;
  result.header.flags.raw.should.equal(pack.header.flags.raw);
  result.header.envelope.should.equal(pack.header.envelope);
}

/// It should be able to serialize and deserialize a header with m envelope
unittest {
  StandardGeoPackageBinary pack;

  pack.header.flags.byteOrderForHeaderValues = 0;
  pack.header.flags.envelopeContentsIndicatorCode = 3;

  pack.header.envelope = [1,2,3,4,5,6];
  pack.geometry = GeoJsonGeometry.fromJson(`{"type": "Point", "coordinates": [1,2,3]}`.parseJsonString);

  pack.toBytes.should.equal(cast(ubyte[]) [
    71, 80, 0, 6, 0, 0, 0, 0,
    63, 240, 0, 0, 0, 0, 0, 0,
    64, 0, 0, 0, 0, 0, 0, 0,
    64, 8, 0, 0, 0, 0, 0, 0,
    64, 16, 0, 0, 0, 0, 0, 0,
    64, 20, 0, 0, 0, 0, 0, 0,
    64, 24, 0, 0, 0, 0, 0, 0, 0,

    0, 0, 0, 1, 63, 240, 0, 0, 0, 0, 0, 0, 64, 0, 0, 0, 0, 0, 0, 0]);

  auto result = pack.toBytes.parseStandardGeoPackageBinary;

  result.header.flags.raw.should.equal(pack.header.flags.raw);
  result.header.envelope.should.equal(pack.header.envelope);
}

/// It should be able to serialize and deserialize a header with zm envelope
unittest {
  StandardGeoPackageBinary pack;
  pack.header.flags.envelopeContentsIndicatorCode = 4;
  pack.header.envelope = [1,2,3,4,5,6,7,8];
  pack.geometry = GeoJsonGeometry.fromJson(`{"type": "Point", "coordinates": [1,2,3]}`.parseJsonString);

  pack.toBytes.should.equal(cast(ubyte[]) [71, 80,
  0, 8, 0, 0, 0, 0,
  63, 240, 0, 0, 0, 0, 0, 0,
  64, 0, 0, 0, 0, 0, 0, 0,
  64, 8, 0, 0, 0, 0, 0, 0,
  64, 16, 0, 0, 0, 0, 0, 0,
  64, 20, 0, 0, 0, 0, 0, 0,
  64, 24, 0, 0, 0, 0, 0, 0,
  64, 28, 0, 0, 0, 0, 0, 0,
  64, 32, 0, 0, 0, 0, 0, 0, 0,
  0, 0, 0, 1,
  63, 240, 0, 0, 0, 0, 0, 0,
  64, 0, 0, 0, 0, 0, 0, 0]);

  auto result = pack.toBytes.parseStandardGeoPackageBinary;
  result.header.flags.raw.should.equal(pack.header.flags.raw);
  result.header.envelope.should.equal(pack.header.envelope);
}

/// It shoud be able to deserialize a multi point generated with qgis
unittest {
  ubyte[] pack = [
    0x47, 0x50, 0x00, 0x03, 0xe6, 0x10, 0x00, 0x00, 0xe2, 0x2a, 0x69, 0x54, 0xf0, 0xce, 0x2a, 0x40,
    0x9f, 0xda, 0x72, 0x95, 0x2b, 0xcf, 0x2a, 0x40, 0xcf, 0x96, 0xab, 0xa4, 0x72, 0x3c, 0x4a, 0x40,
    0xf1, 0xbe, 0x26, 0x44, 0x84, 0x3c, 0x4a, 0x40, 0x01, 0x04, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00,
    0x00, 0x01, 0x01, 0x00, 0x00, 0x00, 0xe2, 0x2a, 0x69, 0x54, 0xf0, 0xce, 0x2a, 0x40, 0xe4, 0x8f,
    0x58, 0xc0, 0x77, 0x3c, 0x4a, 0x40, 0x01, 0x01, 0x00, 0x00, 0x00, 0x9f, 0xda, 0x72, 0x95, 0x2b,
    0xcf, 0x2a, 0x40, 0xcf, 0x96, 0xab, 0xa4, 0x72, 0x3c, 0x4a, 0x40, 0x01, 0x01, 0x00, 0x00, 0x00,
    0x9f, 0xda, 0x72, 0x95, 0x2b, 0xcf, 0x2a, 0x40, 0xf1, 0xbe, 0x26, 0x44, 0x84, 0x3c, 0x4a, 0x40 ];

  auto result = pack.parseStandardGeoPackageBinary;
  result.header.flags.raw.should.equal(3);
  result.geometry.toJson.to!string.should.equal(`{
    "type":"MultiPoint",
    "coordinates":[[13.40417732032569,52.4724045212204],[13.40462939288164,52.47224863413214],[13.40462939288164,52.47278644458664]]
  }`.parseJsonString.to!string);
}

/// It shoud be able to deserialize a multi line generated with qgis
unittest {
  ubyte[] pack = [ 0x47, 0x50, 0x00, 0x03, 0xe6, 0x10, 0x00, 0x00, 0xab, 0x92, 0x46, 0xb5, 0x29, 0xcd, 0x2a, 0x40,
  0x47, 0x1a, 0xd5, 0x96, 0xf0, 0xd1, 0x2a, 0x40, 0x16, 0x4c, 0x91, 0x4f, 0x4b, 0x3c, 0x4a, 0x40,
  0xab, 0x80, 0x3b, 0x3d, 0xb2, 0x3c, 0x4a, 0x40, 0x01, 0x05, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00,
  0x00, 0x01, 0x02, 0x00, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00, 0xab, 0x92, 0x46, 0xb5, 0x29, 0xcd,
  0x2a, 0x40, 0xab, 0x80, 0x3b, 0x3d, 0xb2, 0x3c, 0x4a, 0x40, 0xa9, 0x1b, 0x4c, 0x91, 0xfb, 0xce,
  0x2a, 0x40, 0xdd, 0x3c, 0x74, 0x8c, 0x56, 0x3c, 0x4a, 0x40, 0xe7, 0x06, 0x53, 0xe4, 0x0a, 0xcf,
  0x2a, 0x40, 0x16, 0x4c, 0x91, 0x4f, 0x4b, 0x3c, 0x4a, 0x40, 0x47, 0x1a, 0xd5, 0x96, 0xf0, 0xd1,
  0x2a, 0x40, 0xdc, 0xc5, 0x79, 0xe8, 0x4f, 0x3c, 0x4a, 0x40, 0x47, 0x1a, 0xd5, 0x96, 0xf0, 0xd1,
  0x2a, 0x40, 0xdc, 0xc5, 0x79, 0xe8, 0x4f, 0x3c, 0x4a, 0x40 ];

  auto result = pack.parseStandardGeoPackageBinary;
  result.header.flags.raw.should.equal(3);
  result.geometry.toJson.to!string.should.equal(`{
    "type": "MultiLineString",
    "coordinates": [[
      [ 13.4007088326119,  52.47418942838097 ],
      [ 13.40426305822423, 52.47139125514671 ],
      [ 13.40437997354043, 52.47104830355254 ],
      [ 13.41003867484427, 52.47118860193197 ],
      [ 13.41003867484427, 52.47118860193197 ]]]
  }`.parseJsonString.to!string);
}

/// It shoud be able to deserialize a multi polygon generated with qgis
unittest {
  ubyte[] pack = [
    0x47, 0x50, 0x00, 0x03, 0xe6, 0x10, 0x00, 0x00, 0x25, 0x04, 0x65, 0xef, 0x8b, 0xbf, 0x2a, 0x40,
    0x17, 0x5e, 0x9c, 0x87, 0x2e, 0xf5, 0x2a, 0x40, 0xfc, 0x64, 0xef, 0x2b, 0x5c, 0x3f, 0x4a, 0x40,
    0xc2, 0xde, 0xd7, 0x44, 0xea, 0x44, 0x4a, 0x40, 0x01, 0x06, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00,
    0x00, 0x01, 0x03, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00, 0x25, 0x04,
    0x65, 0xef, 0x8b, 0xbf, 0x2a, 0x40, 0xab, 0xf7, 0x35, 0xe1, 0x5d, 0x43, 0x4a, 0x40, 0x83, 0xa0,
    0xec, 0x7d, 0x8d, 0xd1, 0x2a, 0x40, 0x96, 0xfe, 0x88, 0xc5, 0x15, 0x44, 0x4a, 0x40, 0xc1, 0x8b,
    0xf3, 0xd0, 0x41, 0xd8, 0x2a, 0x40, 0x6e, 0x83, 0x29, 0xb2, 0xbf, 0x40, 0x4a, 0x40, 0xbc, 0xaf,
    0x09, 0x41, 0xe9, 0xc7, 0x2a, 0x40, 0xd1, 0xfb, 0x9a, 0xd0, 0xa9, 0x3f, 0x4a, 0x40, 0x25, 0x04,
    0x65, 0xef, 0x8b, 0xbf, 0x2a, 0x40, 0xab, 0xf7, 0x35, 0xe1, 0x5d, 0x43, 0x4a, 0x40, 0x01, 0x03,
    0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0xa3, 0x51, 0x6d, 0xb9, 0xda,
    0xdc, 0x2a, 0x40, 0xc2, 0xde, 0xd7, 0x44, 0xea, 0x44, 0x4a, 0x40, 0x17, 0x5e, 0x9c, 0x87, 0x2e,
    0xf5, 0x2a, 0x40, 0xda, 0xc5, 0x79, 0xa8, 0x82, 0x43, 0x4a, 0x40, 0x96, 0x22, 0x9f, 0x75, 0x23,
    0xf3, 0x2a, 0x40, 0xfc, 0x64, 0xef, 0x2b, 0x5c, 0x3f, 0x4a, 0x40, 0x48, 0xa3, 0xda, 0x72, 0xd5,
    0xde, 0x2a, 0x40, 0x00, 0xca, 0xde, 0x97, 0xce, 0x3f, 0x4a, 0x40, 0x30, 0xbc, 0x38, 0x0f, 0x3d,
    0xe0, 0x2a, 0x40, 0x3a, 0x50, 0xf6, 0x7e, 0x1c, 0x43, 0x4a, 0x40, 0x30, 0xbc, 0x38, 0x0f, 0x3d,
    0xe0, 0x2a, 0x40, 0x3a, 0x50, 0xf6, 0x7e, 0x1c, 0x43, 0x4a, 0x40, 0xa3, 0x51, 0x6d, 0xb9, 0xda,
    0xdc, 0x2a, 0x40, 0xc2, 0xde, 0xd7, 0x44, 0xea, 0x44, 0x4a, 0x40 ];

  auto result = pack.parseStandardGeoPackageBinary;
  result.header.flags.raw.should.equal(3);
  result.geometry.toJson.to!string.should.equal(`{
    "type":"MultiPolygon",
    "coordinates":[[[
      [13.37411449535473, 52.52630248198633],
      [13.40928262246621, 52.53191441716369],
      [13.42237713788006,  52.5058500960066],
      [13.39045146220439, 52.49736983840524],
      [13.37411449535473,52.52630248198633]]],
      [[[13.43135623416385,52.53839932003531],
      [13.47887061866554,52.5274248690218],
      [13.47487990920608,52.49500035466369],
      [13.4352222339527,52.49849222544071],
      [13.43796584670608,52.52430712725659],
      [13.43796584670608,52.52430712725659],
      [13.43135623416385,52.53839932003531]]]]
    }`.parseJsonString.to!string);
}

///
StandardGeoPackageBinary toStandardGeoPackage(GeoJsonGeometry geometry, int srs_id = 1) {
  StandardGeoPackageBinary result;

  const bbox = BBox!double(geometry);

  result.header.flags.byteOrderForHeaderValues = 0;

  bool isEmpty;

  if(geometry.type == "Point") {
    auto point = geometry.to!Point;

    isEmpty = point.coordinates[0] == 0 && point.coordinates[1] == 0;
  }

  if(!isEmpty) {
    result.header.flags.envelopeContentsIndicatorCode = 1;
    result.header.envelope = [ bbox.left, bbox.right, bbox.top, bbox.bottom ];
    result.header.srs_id = srs_id;
  }

  result.geometry = geometry;

  return result;
}

///
ubyte[] toStandardGeoPackageBinary(GeoJsonGeometry geometry, uint srs_id = 1) {
  return geometry.toStandardGeoPackage(srs_id).toBytes;
}

/// It should convert a point to a StandardGeoPackageBinary
unittest {
  auto geometry = GeoJsonGeometry.fromJson(`{"type": "Point", "coordinates": [1,2]}`.parseJsonString);

  ubyte[] expected = [71, 80,
    0, 2, 0, 0, 0, 1, 63,
    240, 0, 0, 0, 0, 0, 0, 63,
    240, 0, 0, 0, 0, 0, 0, 64,
    0, 0, 0, 0, 0, 0, 0, 64,
    0, 0, 0, 0, 0, 0, 0, 0,

    0, 0, 0, 1,
    63, 240, 0, 0, 0, 0, 0, 0,
    64, 0, 0, 0, 0, 0, 0, 0];

  geometry.toStandardGeoPackageBinary.should.equal(expected);
}

/// It should convert a point to a StandardGeoPackageBinary and back
unittest {
  auto geometry = GeoJsonGeometry.fromJson(`{"type": "Point", "coordinates": [1,2]}`.parseJsonString);

  auto result = geometry
    .toStandardGeoPackageBinary(4020)
    .parseStandardGeoPackageBinary;

  result.header.flags.raw.should.equal(2);

  double[] expectedEnvelope = [1, 1, 2, 2];
  result.header.envelope.should.equal(expectedEnvelope);
  result.header.srs_id.should.equal(4020);

  result.geometry.toJson.should.equal(geometry.toJson);
}

/// It should convert a null point to a StandardGeoPackageBinary and back
unittest {
  auto geometry = GeoJsonGeometry.nullPoint;

  auto result = geometry
    .toStandardGeoPackageBinary(4020)
    .parseStandardGeoPackageBinary;

  result.header.flags.raw.should.equal(0);
  result.header.envelope.length.should.equal(0);
  result.header.srs_id.should.equal(0);

  result.geometry.toJson.should.equal(`{
    "type": "Point",
    "coordinates": [ 0.0, 0.0 ]
  }`.parseJsonString);
}
