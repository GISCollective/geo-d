/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module geo.style;

import geo.filter;

import std.exception;
import std.conv;
import std.array;
import std.traits;
import std.algorithm;
import std.math;

import vibe.data.json;
import vibe.data.serialization: nameAlias = name;

version(unittest) import fluent.asserts;

///
struct MapBoxLight {
  ///
  enum Anchor : string {
    viewport = "viewport",
    map = "map"
  }

  /// Whether extruded geometries are lit relative to the map or viewport.
  @optional Anchor anchor = Anchor.viewport;

  /// Position of the light source relative to lit (extruded) geometries, in [r radial coordinate, a azimuthal angle, p polar angle]
  /// where r indicates the distance from the center of the base of an object to its light, a indicates the position of the light
  /// relative to 0° (0° when light.anchor is set to viewport corresponds to the top of the viewport, or 0° when light.anchor is set
  /// to map corresponds to due north, and degrees proceed clockwise), and p indicates the height of the light (from 0°, directly
  /// above, to 180°, directly below).
  @optional double[] position = [1.15, 210, 30];

  /// Color tint for lighting extruded geometries.
  /// Defaults to "#ffffff".
  @optional string color = "#ffffff";

  // Intensity of lighting (on a scale from 0 to 1). Higher numbers will present as more extreme contrast.
  /// Optional number between 0 and 1 inclusive. Defaults to 0.5.
  @optional double intensity = 0.5;
}

///
struct MapBoxTransition {
  /// Time allotted for transitions to complete.
  /// Optional number greater than or equal to 0. Units in milliseconds. Defaults to 300.
  @optional ulong duration = 300;

  /// Optional number greater than or equal to 0. Units in milliseconds. Defaults to 0.
  /// Length of time before a transition begins.
  @optional ulong delay;
}

///
struct InterpolateValue(T) {
  Json value;

  this(T value) {
    this.value = value.serializeToJson;
  }

  string toString() inout {
    return base.to!string;
  }

  inout T base() {
    string[string] attributes;
    return value.resolveValue!T(attributes);
  }

  inout T render(string[string] attributes) {

    static if(isArray!T && !isSomeString!T) {
      if(value.type != Json.Type.array || value.length == 0) {
        T defaultValue_;
        return defaultValue_;
      }

      if(value[0].isOperation) {
        return value.resolveValue!T(attributes);
      }

      return value.deserializeJson!T;
    } else {
      return value.resolveValue!T(attributes);
    }
  }

  typeof(this) base(T newValue) {
    this.value = newValue.serializeToJson;

    return this;
  }

  auto opAssign(T newValue) {
    this.value = newValue.serializeToJson;

    return this;
  }

  ///
  Json toJson() const @safe {
    return value;
  }

///
  static InterpolateValue!T fromJson(Json src) @trusted {
    InterpolateValue!T result;

    result.value = src;

    return result;
  }
}

/// it can be deserialized from a simple value
unittest {
  auto value = Json(5.3).deserializeJson!(InterpolateValue!double);

  value.base.should.equal(5.3);
}

/// it can be serialized to a simple value
unittest {
  InterpolateValue!double value;
  value.base = 5.3;

  value.serializeToJson.should.equal(5.3);
}

///
struct MapBoxLayer {
  ///
  enum Visibility : string {
    /// The layer is shown.
    visible = "visible",

    /// The layer is not shown.
    none = "none"
  }

  ///
  enum Anchor : string {
    /// The fill is translated relative to the map.
    map = "map",

    /// The fill is translated relative to the viewport.
    viewport = "viewport"
  }

  /// The display of line endings.
  enum LineCap : string {
    /// A cap with a squared-off end which is drawn to the exact endpoint of the line.
    butt = "butt",

    /// A cap with a rounded end which is drawn beyond the endpoint of the line at a radius of
    /// one-half of the line's width and centered on the endpoint of the line.
    round = "round",

    /// A cap with a squared-off end which is drawn beyond the endpoint of the line at a
    /// distance of one-half of the line's width.
    square = "square"
  }

  ///
  enum LineJoin : string {
    /// A join with a squared-off end which is drawn beyond the endpoint of the line at a distance of one-half of the line's width.
    bevel = "bevel",

    /// A join with a rounded end which is drawn beyond the endpoint of the line at a radius of one-half of the line's width
    /// and centered on the endpoint of the line.
    round = "round",

    /// A join with a sharp, angled corner which is drawn with the outer sides beyond the endpoint of the path until they meet.
    mitter = "miter"
  }

  ///
  enum Type : string {
    unknown = "",

    /// A filled polygon with an optional stroked border.
    fill = "fill",

    /// A stroked line.
    line = "line",

    /// An icon or a text label.
    symbol = "symbol",

    /// A filled circle.
    circle = "circle",

    /// A heatmap.
    heatmap = "heatmap",

    /// An extruded (3D) polygon.
    fillExtrusion = "fill-extrusion",

    /// Raster map textures such as satellite imagery.
    raster = "raster",

    /// Client-side hillshading visualization based on DEM data. Currently, the implementation only supports Mapbox Terrain RGB and Mapzen Terrarium tiles.
    hillshade = "hillshade",

    /// The background color or pattern of the map.
    background = "background"
  }

  ///
  enum SymbolPlacement {
    /// The label is placed at the point where the geometry is located.
    point = "point",

    /// The label is placed along the line of the geometry. Can only be used on LineString and Polygon geometries.
    line = "line",

    /// The label is placed at the center of the line of the geometry. Can only be used on LineString and Polygon
    /// geometries. Note that a single feature in a vector tile may contain multiple line geometries.
    lineCenter = "line-center"
  }

  ///
  enum SymbolZOrder {
    /// Symbols will be sorted by their y-position relative to the viewport.
    viewportY = "viewport-y",

    /// Symbols will be rendered in the same order as the source data with no sorting applied.
    source = "source"
  }

  ///
  enum IconAlignment {
    /// When symbol-placement is set to point, aligns icons east-west. When symbol-placement is set
    /// to line or line-center, aligns icon x-axes with the line.
    map = "map",

    /// Produces icons whose x-axes are aligned with the x-axis of the viewport,
    /// regardless of the value of symbol-placement.
    viewport = "viewport",

    /// When symbol-placement is set to point, this is equivalent to viewport. When
    /// symbol-placement is set to line or line-center, this is equivalent to map.
    auto_ = "auto"
  }

  ///
  enum IconTextFit {
    /// The icon is displayed at its intrinsic aspect ratio.
    none = "none",

    /// The icon is scaled in the x-dimension to fit the width of the text.
    width = "width",

    /// The icon is scaled in the y-dimension to fit the height of the text.
    height = "height",

    /// The icon is scaled in both x- and y-dimensions.
    both = "both"
  }

  ///
  enum SymbolAnchor {
    /// The center of the icon is placed closest to the anchor.
    center = "center",

    /// The left side of the icon is placed closest to the anchor.
    left = "left",

    /// The right side of the icon is placed closest to the anchor.
    right = "right",

    /// The top of the icon is placed closest to the anchor.
    top = "top",

    /// The bottom of the icon is placed closest to the anchor.
    bottom = "bottom",

    /// The top left corner of the icon is placed closest to the anchor.
    topLeft = "top-left",

    /// The top right corner of the icon is placed closest to the anchor.
    topRight = "top-right",

    /// The bottom left corner of the icon is placed closest to the anchor.
    bottomLeft = "bottom-left",

    /// The bottom right corner of the icon is placed closest to the anchor.
    bottomRight = "bottom-right"
  }

  ///
  enum Justify {
    /// The text is aligned to the left.
    left = "left",

    /// The text is centered.
    center = "center",

    /// The text is aligned to the right.
    right = "right",

    /// The text is aligned to the right.
    auto_ = "auto"
  }

  ///
  enum TextTransform {
    /// The text is not altered.
    none = "none",

    /// Forces all letters to be displayed in uppercase.
    uppercase = "uppercase",

    /// Forces all letters to be displayed in lowercase.
    lowercase = "lowercase"
  }

  ///
  struct Layout {
    /// Whether this layer is displayed.
    @optional InterpolateValue!Visibility visibility = InterpolateValue!Visibility(Visibility.visible);

    /// The display of line endings.
    @optional @nameAlias("line-cap") InterpolateValue!LineCap lineCap = InterpolateValue!LineCap(LineCap.butt);

    /// The display of lines when joining.
    @optional @nameAlias("line-join") InterpolateValue!LineJoin lineJoin = InterpolateValue!LineJoin(LineJoin.mitter);

    /// Used to automatically convert miter joins to bevel joins for sharp angles.
    @optional @nameAlias("line-miter-limit") InterpolateValue!double lineMiterLimit = InterpolateValue!double(2);

    /// Used to automatically convert round joins to miter joins for shallow angles.
    @optional @nameAlias("line-round-limit") InterpolateValue!double lineRoundLimit = InterpolateValue!double(1.05);


    /// Label placement relative to its geometry.
    @optional @nameAlias("symbol-placement") InterpolateValue!SymbolPlacement symbolPlacement = InterpolateValue!SymbolPlacement(SymbolPlacement.point);

    /// Distance between two symbol anchors. Requires symbol-placement to be "line".
    @optional @nameAlias("symbol-spacing") InterpolateValue!int symbolSpacing = InterpolateValue!int(250);

    /// If true, the symbols will not cross tile edges to avoid mutual collisions. Recommended in layers
    /// that don't have enough padding in the vector tile to prevent collisions, or if it is a point symbol layer placed after a line symbol layer.
    @optional @nameAlias("symbol-avoid-edges") InterpolateValue!bool symbolAvoidEdges = InterpolateValue!bool(false);

    /// Controls the order in which overlapping symbols in the same layer are rendered
    @optional @nameAlias("symbol-z-order") InterpolateValue!SymbolZOrder symbolZOrder = InterpolateValue!SymbolZOrder(SymbolZOrder.viewportY);


    /// If true, the icon will be visible even if it collides with other previously drawn symbols. Requires icon-image.
    @optional @nameAlias("icon-allow-overlap") InterpolateValue!bool iconAllowOverlap;

    /// If true, other symbols can be visible even if they collide with the icon. Requires icon-image.
    @optional @nameAlias("icon-ignore-placement") InterpolateValue!bool iconIgnorePlacement;

    /// If true, text will display without their corresponding icons when the icon collides with other
    /// symbols and the text does not. Requires icon-image. Requires text-field.
    @optional @nameAlias("icon-optional") InterpolateValue!bool iconOptional;

    /// In combination with symbol-placement, determines the rotation behavior of icons. Requires icon-image.
    @optional @nameAlias("icon-rotation-alignment") InterpolateValue!IconAlignment iconRotationAlignment = InterpolateValue!IconAlignment(IconAlignment.auto_);

    /// Scales the original size of the icon by the provided factor. The new pixel size of the image will be the original pixel
    /// size multiplied by icon-size. 1 is the original size; 3 triples the size of the image. Units in factor of the original icon size. Requires icon-image.
    @optional @nameAlias("icon-size") InterpolateValue!double iconSize = InterpolateValue!double(1);

    /// Scales the icon to fit around the associated text. Requires text-field.
    @optional @nameAlias("icon-text-fit") InterpolateValue!IconTextFit iconTextFit = InterpolateValue!IconTextFit(IconTextFit.none);

    /// Size of the additional area added to dimensions determined by icon-text-fit, in clockwise order: top, right, bottom, left.
    @optional @nameAlias("icon-text-fit-padding") InterpolateValue!(double[]) iconTextFitPadding = InterpolateValue!(double[])([0, 0, 0, 0]);

    /// Name of image in sprite to use for drawing an image background.
    @optional @nameAlias("icon-image") InterpolateValue!string iconImage = InterpolateValue!string("");

    /// Rotates the icon clockwise.
    @optional @nameAlias("icon-rotate") InterpolateValue!double iconRotate = InterpolateValue!double(0);

    /// Size of the additional area around the icon bounding box used for detecting symbol collisions.
    @optional @nameAlias("icon-padding") InterpolateValue!uint iconPadding = InterpolateValue!uint(2);

    /// If true, the icon may be flipped to prevent it from being rendered upside-down.
    /// Requires icon-image. Requires icon-rotation-alignment to be "map". Requires symbol-placement to be "line", or "line-center".
    @optional @nameAlias("icon-keep-upright") InterpolateValue!bool iconKeepUpright = InterpolateValue!bool(false);

    /// Offset distance of icon from its anchor. Positive values indicate right and down, while negative values indicate left and up. Each component is multiplied by the value
    /// of icon-size to obtain the final offset in pixels. When combined with icon-rotate the offset will be as if the rotated direction was up.
    @optional @nameAlias("icon-offset") InterpolateValue!(double[]) iconOffset = InterpolateValue!(double[])([0, 0]);

    /// Part of the icon placed closest to the anchor. Requires icon-image.
    @optional @nameAlias("icon-anchor") InterpolateValue!SymbolAnchor iconAnchor = InterpolateValue!SymbolAnchor(SymbolAnchor.center);

    /// Orientation of icon when map is pitched.
    @optional @nameAlias("icon-pitch-alignment") InterpolateValue!IconAlignment iconPitchAlignment = InterpolateValue!IconAlignment(IconAlignment.auto_);

    /// Orientation of text when map is pitched. Requires text-field.
    @optional @nameAlias("text-pitch-alignment") InterpolateValue!IconAlignment textPitchAlignment = InterpolateValue!IconAlignment(IconAlignment.auto_);

    /// In combination with symbol-placement, determines the rotation behavior of the individual glyphs forming the text. Requires text-field.
    @optional @nameAlias("text-rotation-alignment") InterpolateValue!IconAlignment textRotationAlignment = InterpolateValue!IconAlignment(IconAlignment.auto_);

    /// Value to use for a text label. If a plain string is provided, it will be treated as a formatted with default/inherited formatting options.
    @optional @nameAlias("text-field") InterpolateValue!string textField;

    /// Font stack to use for displaying text.
    @optional @nameAlias("text-font") InterpolateValue!(string[]) textFont = InterpolateValue!(string[])(["Open Sans Regular", "Arial Unicode MS Regular"]);

    /// Font size. Units in pixels. Requires text-field.
    @optional @nameAlias("text-size") InterpolateValue!double textSize = InterpolateValue!double(16);

    /// The maximum line width for text wrapping. Units in ems. Requires text-field.
    @optional @nameAlias("text-max-width") InterpolateValue!double textMaxWidth = InterpolateValue!double(10);

    /// Text leading value for multi-line text. Units in ems. Defaults to 1.2. Requires text-field.
    @optional @nameAlias("text-line-height") InterpolateValue!double textLineHeight = InterpolateValue!double(1.2);

    /// Text tracking amount. Units in ems. Requires text-field.
    @optional @nameAlias("text-letter-spacing") InterpolateValue!double textLetterSpacing = InterpolateValue!double(0);

    /// Text justification options. Requires text-field.
    @optional @nameAlias("text-justify") InterpolateValue!Justify textJustify = InterpolateValue!Justify(Justify.center);

    /// Part of the text placed closest to the anchor. Requires text-field.
    @optional @nameAlias("text-anchor") InterpolateValue!SymbolAnchor textAnchor = InterpolateValue!SymbolAnchor(SymbolAnchor.center);

    /// Maximum angle change between adjacent characters. Requires text-field. Requires symbol-placement to be "line", or "line-center".
    @optional @nameAlias("text-max-angle") InterpolateValue!double textMaxAngle = InterpolateValue!double(45);

    /// Rotates the text clockwise. Units in degrees. Defaults to 0. Requires text-field.
    @optional @nameAlias("text-rotate") InterpolateValue!double textRotate = InterpolateValue!double(0);

    /// Size of the additional area around the text bounding box used for detecting symbol collisions. Units in pixels. Defaults to 2. Requires text-field.
    @optional @nameAlias("text-padding") InterpolateValue!uint textPadding = InterpolateValue!uint(2);

    /// If true, the text may be flipped vertically to prevent it from being rendered upside-down.
    /// Requires text-field. Requires text-rotation-alignment to be "map". Requires symbol-placement to be "line", or "line-center".
    @optional @nameAlias("text-keep-upright") InterpolateValue!bool textKeepUpright = InterpolateValue!bool(true);

    /// Specifies how to capitalize text, similar to the CSS text-transform property.
    /// Defaults to "none". Requires text-field.
    @optional @nameAlias("text-transform") TextTransform textTransform = TextTransform.none;

    /// Offset distance of text from its anchor. Positive values indicate right and down, while negative values indicate left and up.
    /// Units in ems. Defaults to [0,0]. Requires text-field.
    @optional @nameAlias("text-offset") InterpolateValue!(double[]) textOffset = InterpolateValue!(double[])([0, 0]);

    /// If true, the text will be visible even if it collides with other previously drawn symbols.
    /// Requires text-field.
    @optional @nameAlias("text-allow-overlap") bool textAllowOverlap;

    /// If true, other symbols can be visible even if they collide with the text.
    /// Requires text-field.
    @optional @nameAlias("text-ignore-placement") bool textIgnorePlacement;

    /// If true, icons will display without their corresponding text when the text collides with other symbols and the icon does not.
    /// Requires text-field. Requires icon-image.
    @optional @nameAlias("text-optional") bool textOptional;

    mixin StyleSerializer!(Layout);
  }

  ///
  struct Paint {
    /// The color with which the background will be drawn.
    /// Paint property. Optional color. Defaults to "#000000". Disabled by background-pattern.
    @optional @nameAlias("background-color") InterpolateValue!string backgroundColor = InterpolateValue!string("#000000");

    /// Name of image in sprite to use for drawing an image background. For seamless patterns, image width and
    /// height must be a factor of two (2, 4, 8, ..., 512). Note that zoom-dependent expressions will be evaluated
    /// only at integer zoom levels.
    @optional @nameAlias("background-pattern") InterpolateValue!string backgroundPattern = InterpolateValue!string("");

    /// The opacity at which the background will be drawn.
    /// Paint property. Optional number between 0 and 1 inclusive. Defaults to 1.
    @optional @nameAlias("background-opacity") InterpolateValue!double backgroundOpacity = InterpolateValue!double(1);


    /// Whether or not the fill should be antialiased.
    @optional @nameAlias("fill-antialias") InterpolateValue!bool fillAntialias = InterpolateValue!bool(true);

    /// The opacity of the entire fill layer. In contrast to the fill-color, this value will also affect
    /// the 1px stroke around the fill, if the stroke is used.
    @optional @nameAlias("fill-opacity") InterpolateValue!double fillOpacity = InterpolateValue!double(1);

    /// The color of the filled part of this layer. This color can be specified as rgba with an alpha
    /// component and the color's opacity will not affect the opacity of the 1px stroke, if it is used.
    @optional @nameAlias("fill-color") InterpolateValue!string fillColor = InterpolateValue!string("#000000");

    /// The outline color of the fill. Matches the value of fill-color if unspecified.
    @optional @nameAlias("fill-outline-color") InterpolateValue!string fillOutlineColor;

    /// The geometry's offset. Values are [x, y] where negatives indicate left and up, respectively.
    @optional @nameAlias("fill-translate") InterpolateValue!(double[2]) fillTranslate = InterpolateValue!(double[2])([0, 0]);

    /// Controls the frame of reference for fill-translate.
    @optional @nameAlias("fill-translate-anchor") InterpolateValue!Anchor fillTranslateAnchor = InterpolateValue!Anchor(Anchor.map);

    /// Name of image in sprite to use for drawing image fills. For seamless patterns,
    /// image width and height must be a factor of two (2, 4, 8, ..., 512). Note that zoom-dependent
    /// expressions will be evaluated only at integer zoom levels.
    @optional @nameAlias("fill-pattern") InterpolateValue!string fillPattern = InterpolateValue!string("");


    /// Optional number between 0 and 1 inclusive.
    /// The opacity at which the line will be drawn.
    @optional @nameAlias("line-opacity") InterpolateValue!double lineOpacity = InterpolateValue!double(1);

    /// The color with which the line will be drawn.
    @optional @nameAlias("line-color") InterpolateValue!string lineColor = InterpolateValue!string("#000000");

    /// Controls the frame of reference for line-translate.
    @optional @nameAlias("line-translate-anchor") InterpolateValue!Anchor lineTranslateAnchor = InterpolateValue!Anchor(Anchor.map);

    /// Units in pixels. Stroke thickness.
    @optional @nameAlias("line-width") InterpolateValue!double lineWidth = InterpolateValue!double(1);

    /// Units in pixels.
    /// Draws a line casing outside of a line's actual path. Value indicates the width of the inner gap.
    @optional @nameAlias("line-gap-width") InterpolateValue!double lineGapWidth;

    /// Units in pixels.
    /// The line's offset. For linear features, a positive value offsets the line to the right, relative to
    /// the direction of the line, and a negative value to the left. For polygon features, a positive value
    /// results in an inset, and a negative value results in an outset.
    @optional @nameAlias("line-offset") InterpolateValue!int lineOffset;

    /// Blur applied to the line, in pixels.
    @optional @nameAlias("line-blur") InterpolateValue!double lineBlur;

    /// Specifies the lengths of the alternating dashes and gaps that form the dash pattern. The lengths
    /// are later scaled by the line width. To convert a dash length to pixels, multiply the length by the current
    /// line width. Note that GeoJSON sources with lineMetrics: true specified won't render dashed lines to the
    /// expected scale. Also note that zoom-dependent expressions will be evaluated only at integer zoom levels.
    @optional @nameAlias("line-dasharray") InterpolateValue!(double[]) lineDasharray;

    /// Name of image in sprite to use for drawing image lines. For seamless patterns, image width must be a
    /// factor of two (2, 4, 8, ..., 512). Note that zoom-dependent expressions will be evaluated only at
    /// integer zoom levels.
    @optional @nameAlias("line-pattern") InterpolateValue!string linePattern = InterpolateValue!string("");

    /// Defines a gradient with which to color a line feature. Can only be used with GeoJSON sources that
    /// specify "lineMetrics": true.
    @optional @nameAlias("line-gradient") InterpolateValue!string lineGradient = InterpolateValue!string("");


    /// The opacity at which the icon will be drawn.
    /// Requires icon-image. Transitionable.
    @optional @nameAlias("icon-opacity") InterpolateValue!double iconOpacity = InterpolateValue!double(1);

    /// The color of the icon. This can only be used with sdf icons.
    /// Requires icon-image. Transitionable.
    @optional @nameAlias("icon-color") InterpolateValue!string iconColor = InterpolateValue!string("#000000");

    /// The color of the icon's halo. Icon halos can only be used with SDF icons.
    /// Defaults to "rgba(0, 0, 0, 0)". Requires icon-image.
    @optional @nameAlias("icon-halo-color") InterpolateValue!string iconHaloColor = InterpolateValue!string("rgba(0, 0, 0, 0)");

    /// Distance of halo to the icon outline.
    /// Requires icon-image. Units in pixels.
    @optional @nameAlias("icon-halo-width") InterpolateValue!ulong iconHaloWidth = InterpolateValue!ulong(0);

    /// Fade out the halo towards the outside.
    /// Units in pixels. Defaults to 0. Requires icon-image.
    @optional @nameAlias("icon-halo-blur") InterpolateValue!ulong iconHaloBlur = InterpolateValue!ulong(0);

    /// Distance that the icon's anchor is moved from its original placement. Positive values indicate right and down, while negative values indicate left and up.
    /// Paint property. Optional array of numbers. Units in pixels. Defaults to [0,0]. Requires icon-image. Transitionable.
    @optional @nameAlias("icon-translate") InterpolateValue!(double[]) iconTranslate = InterpolateValue!(double[])([0, 0]);

    /// Controls the frame of reference for icon-translate.
    /// Paint property. Optional enum. One of "map", "viewport". Defaults to "map". Requires icon-image. Requires icon-translate.
    @optional @nameAlias("icon-translate-anchor") Anchor iconTranslateAnchor = Anchor.map;


    /// The color with which the text will be drawn.
    /// Paint property. Optional color. Defaults to "#000000". Requires text-field. Transitionable.
    @optional @nameAlias("text-color") InterpolateValue!string textColor = InterpolateValue!string("#000000");

    /// The color of the text's halo, which helps it stand out from backgrounds.
    /// Paint property. Optional color. Defaults to "rgba(0, 0, 0, 0)". Requires text-field. Transitionable.
    @optional @nameAlias("text-halo-color") InterpolateValue!string textHaloColor = InterpolateValue!string("#000000");

    /// Distance of halo to the font outline. Max text halo width is 1/4 of the font-size.
    /// Paint property. Optional number greater than or equal to 0. Units in pixels. Defaults to 0. Requires text-field. Transitionable.
    @optional @nameAlias("text-halo-width") InterpolateValue!double textHaloWidth = InterpolateValue!double(0);

    /// The halo's fadeout distance towards the outside.
    /// Paint property. Optional number greater than or equal to 0. Units in pixels. Defaults to 0. Requires text-field. Transitionable.
    @optional @nameAlias("text-halo-blur") InterpolateValue!double textHaloBlur = InterpolateValue!double(0);

    /// Distance that the text's anchor is moved from its original placement. Positive values indicate right and down, while negative values indicate left and up.
    /// Paint property. Optional array of numbers. Units in pixels. Defaults to [0,0]. Requires text-field. Transitionable.
    @optional @nameAlias("text-translate") InterpolateValue!(ulong[]) textTranslate = InterpolateValue!(ulong[])([0, 0]);

    /// Controls the frame of reference for text-translate.
    /// Paint property. Optional enum. One of "map", "viewport". Defaults to "map". Requires text-field. Requires text-translate.
    @optional @nameAlias("text-translate-anchor") Anchor textTranslateAnchor = Anchor.map;

    mixin StyleSerializer!(Paint);
  }

  /// Unique layer name.
  string id;

  /// Rendering type of this layer.
  Type type;

  /// Arbitrary properties useful to track with the layer, but do not influence rendering.
  /// Properties should be prefixed to avoid collisions, like 'mapbox:'.
  @optional Json metadata;

  /// Name of a source description to be used for this layer. Required for all layer types except background.
  @optional string source;

  /// Layer to use from a vector tile source. Required for vector tile sources; prohibited for all
  /// other source types, including GeoJSON sources.
  @optional @nameAlias("source-layer") string sourceLayer = "";

  /// Optional number between 0 and 24 inclusive.
  /// The minimum zoom level for the layer. At zoom levels less than the minzoom, the layer will be hidden.
  @optional double minzoom = 0;

  /// Optional number between 0 and 24 inclusive.
  /// The maximum zoom level for the layer. At zoom levels equal to or greater than the maxzoom, the layer will be hidden.
  @optional double maxzoom = 24;


  /// A expression specifying conditions on source features. Only features that match the filter are displayed.
  /// Zoom expressions in filters are only evaluated at integer zoom levels. The feature-state expression is not
  /// supported in filter expressions.
  @optional Filter filter;

  /// Layout properties for the layer.
  @optional Layout layout;

  /// Default paint properties for this layer.
  @optional Paint paint;

  mixin StyleSerializer!(MapBoxLayer);
}

///
struct MapboxStyle {

  /// Style specification version number. Must be 8.
  @nameAlias("version") uint version_ = 8;

  /// A human-readable name for the style.
  @optional string name;

  /// Arbitrary properties useful to track with the stylesheet,
  /// but do not influence rendering. Properties should be prefixed to avoid collisions, like 'mapbox:'.
  @optional Json metadata;

  /// Default map center in longitude and latitude. The style center will be used only if the map has not been
  /// positioned by other means (e.g. map options or user interaction).
  @optional double[] center;

  /// Default zoom level. The style zoom will be used only if the map has not been positioned
  /// by other means (e.g. map options or user interaction).
  @optional double zoom;

  /// Default bearing, in degrees. The bearing is the compass direction that is "up"; for example,
  /// a bearing of 90° orients the map so that east is up. This value will be used only if the map
  /// has not been positioned by other means (e.g. map options or user interaction).
  /// Units in degrees. Defaults to 0.
  @optional double bearing = 0;

  /// Default pitch, in degrees. Zero is perpendicular to the surface, for a look straight down at the map, while a greater
  /// value like 60 looks ahead towards the horizon. The style pitch will be used only if the map has not been positioned by
  /// other means (e.g. map options or user interaction).
  /// Units in degrees. Defaults to 0.
  @optional double pitch = 0;

  /// The global light source.
  @optional MapBoxLight light;

  /// Data source specifications.
  @optional Json[string] sources;

  /// A URL template for loading signed-distance-field glyph sets in PBF format. The URL must include {fontstack} and
  /// {range} tokens. This property is required if any layer uses the text-field layout property. The URL must be absolute,
  /// containing the scheme, authority and path
  @optional string glyphs;

  /// A URL for the images used on the map.
  /// https://docs.mapbox.com/mapbox-gl-js/style-spec/sprite/
  @optional string sprite;

  /// A global transition definition to use as a default across properties, to be used for timing transitions between one
  /// value and the next when no property-specific transition is set. Collision-based symbol fading is controlled independently
  /// of the style's transition property.
  @optional MapBoxTransition transition;

  /// Layers will be drawn in the order of this array.
  MapBoxLayer[] layers;
}

///
MapboxStyle parseMapboxStyleString(string value) {
  return value.parseJsonString.deserializeJson!MapboxStyle;
}

/// parse a mapbox style json
unittest {
  import std.file;

  auto style = readText("test/style.json").parseMapboxStyleString;

  style.version_.should.equal(8);
  style.name.should.equal("Klokantech Basic");
  style.metadata["mapbox:type"].to!string.should.equal("template");
  style.layers.length.should.equal(46);
}

/// the paint serializes a property when it don't have the default value
unittest {
  auto paint = MapBoxLayer.Paint();
  paint.backgroundColor = "#33cc33";

  auto jsonPaint = paint.serializeToJson;

  jsonPaint["background-color"].to!string.should.equal("#33cc33");
}

/// a paint struct property can be deserialized
unittest {
  auto paint = `{ "background-color": "#33cc33" }`
    .parseJsonString
    .deserializeJson!(MapBoxLayer.Paint);

  paint.backgroundColor.base.should.equal("#33cc33");
}

/// the layout serializes a property when it don't have the default value
unittest {
  auto layout = MapBoxLayer.Layout();
  layout.textField = "test";

  auto jsonLayout = layout.serializeToJson;

  jsonLayout["text-field"].to!string.should.equal("test");
}

/// a layout struct property can be deserialized
unittest {
  auto layout = `{ "text-field": "test" }`
    .parseJsonString
    .deserializeJson!(MapBoxLayer.Layout);

  layout.textField.base.should.equal("test");
}

///
string getNameAlias(alias T)(string defaultName) const {
  enum udas = getUDAs!(T, NameAttribute);

  static if(udas.length > 0) {
    return udas[0].name;
  } else {
    return defaultName;
  }
}

///
mixin template StyleSerializer(T) {
    ///
    Json toJson() const @trusted {
      auto result = Json.emptyObject;
      auto defaultValues = T();

      static foreach (key; __traits(allMembers, T)) {{
        mixin(`enum canCompare = __traits(compiles, typeof(this.` ~ key ~ `));`);
        // mixin(`alias FT = typeof(this.` ~ key ~ `);`);

        static if(!isCallable!(__traits(getMember, T, key)) && canCompare) {
          mixin(`

              auto k = getNameAlias!(this.` ~ key ~ `)("` ~ key ~ `");
              auto r = this.` ~ key ~ `.serializeToJson;

              import std.stdio;
              writeln(k);

              if(r.type != Json.Type.null_ && r.type != Json.Type.undefined) {
                result[k] = r;
              }
          `);
        }
      }}

      return result;
    }

    ///
    static T fromJson(Json src) @safe {
      auto defaultValues = T();

      static foreach (key; __traits(allMembers, T)) {{
        alias m = __traits(getMember, defaultValues, key);

        // mixin(`enum canCompare = __traits(compiles, defaultValues.` ~ key ~ ` != defaultValues.` ~ key ~ `);`);
        mixin(`enum canCompare = __traits(compiles, typeof(this.` ~ key ~ `));`);

        static if(!isCallable!(m) && canCompare) {
          mixin(`
            enum udas = getUDAs!(m, NameAttribute);

            static if(udas.length > 0) {
              string k = udas[0].name;
            } else {
              string k = "` ~ key ~ `";
            }

            if(k in src) {
              defaultValues.` ~ key ~ ` = src[k].deserializeJson!(typeof(defaultValues.` ~ key ~ `));
            }
          `);
        }
      }}

      return defaultValues;
    }
}