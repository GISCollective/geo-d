/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module geo.feature;

import geo.geometries;
import geo.json;
import geo.conv;
import geo.epsg;

import vibe.data.json;

version(unittest) {
  import fluent.asserts;
}

///
struct Feature {
  ///
  GeometryVariant geometry;

  ///
  Json[string] properties;

  /// Convert a feature to a json
  Json toJson() const @safe {
    Json result = Json.emptyObject;

    result["type"] = "Feature";
    result["properties"] = Json.emptyObject;
    result["geometry"] = this.geometry.toJson;

    foreach(string key, value; this.properties) {
      result["properties"][key] = value;
    }

    return result;
  }

  ///
  static Feature fromJson(Json src) @trusted {
    Json[string] properties;

    foreach (string key, Json value; src["properties"]) {
      properties[key] = value;
    }

    return Feature(src["geometry"].toGeometryVariant, properties);
  }

  ///
  long[] longPosition() {
    Point point = geometry.get!Point;
    EPSG4326 value;
    value.latitude = point.coordinates[0];
    value.longitude = point.coordinates[1];

    auto result = value.projectTo!EPSG3857;

    return [ result.x, result.y ];
  }
}

/// Convert a Line String feature to json
unittest {
  LineString line;
  line.coordinates = [ [0, 0], [1, 1] ].toPositions!double;

  Feature feature;
  feature.geometry = line;
  feature.properties["name"] = "Dinagat Islands";

  feature.toJson.should.equal(`{
    "type": "Feature",
    "geometry": {
      "type": "LineString",
      "coordinates": [ [0.0,0.0], [1.0, 1.0] ] },
    "properties": {
      "name": "Dinagat Islands"
    }}`.parseJsonString);
}

/// Convert a Point feature to json
unittest {
  Point point;
  point.coordinates = Position(0, 0);

  Feature feature;
  feature.geometry = point;
  feature.properties["name"] = "Dinagat Islands";

  feature.toJson.should.equal(`{
    "type": "Feature",
    "geometry": {
      "type": "Point",
      "coordinates": [ 0.0, 0.0 ] },
    "properties": {
      "name": "Dinagat Islands"
    }}`.parseJsonString);
}

///
struct FeatureCollection {
  ///
  immutable type = "FeatureCollection";

  ///
  Feature features;
}
