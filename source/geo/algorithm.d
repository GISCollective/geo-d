/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module geo.algorithm;

import std.algorithm;
import std.math;
import std.array;
import std.traits;
import std.conv;
import std.bitmanip;
import core.stdc.math : hypot;
import vibe.data.json;

version(unittest) {
  import fluent.asserts;
  import geo.json;
  import std.file;
}

import geo.feature;
import geo.geometries;
import geo.epsg;
import geo.conv;

/// Compute distance between 2 points in meters
double metricDistance(U)(PositionT!U point1, PositionT!U point2) pure {
  immutable deg2radMultiplier = PI / 180.;
  immutable lat1 = point1[1] * deg2radMultiplier;
  immutable lon1 = point1[0] * deg2radMultiplier;
  immutable lat2 = point2[1] * deg2radMultiplier;
  immutable lon2 = point2[0] * deg2radMultiplier;

  enum earthRadius = 6371. * 1000; // earth mean radius defined by WGS84
  immutable dlon = lon2 - lon1;
  immutable distance = acos(sin(lat1) * sin(lat2) + cos(lat1) * cos(lat2) * cos(dlon)) * earthRadius;

  return distance;
}

/// ditto
double metricDistance(U)(PositionT!U[] points) pure {
  double distance = 0;

  if(points.length <= 1) {
    return distance;
  }

  foreach(i; 1 .. points.length) {
    distance += metricDistance(points[i-1], points[i]);
  }

  return distance;
}

/// ditto
double metricDistance(const EPSG4326 point1, const EPSG4326 point2) pure {
  immutable deg2radMultiplier = PI / 180.;
  immutable lat1 = point1.latitude * deg2radMultiplier;
  immutable lon1 = point1.longitude * deg2radMultiplier;
  immutable lat2 = point2.latitude * deg2radMultiplier;
  immutable lon2 = point2.longitude * deg2radMultiplier;

  enum earthRadius = 6371. * 1000; // earth mean radius defined by WGS84
  immutable dlon = lon2 - lon1;
  immutable distance = acos(sin(lat1) * sin(lat2) + cos(lat1) * cos(lat2) * cos(dlon)) * earthRadius;

  return distance;
}

/// Get the geometric distance between 2 points
Unqual!U distance(Point: U[], U)(const ref Point point1, const ref Point point2) if(isBasicType!U) {
  real dx = abs(point1[0].to!real - point2[0].to!real);
  real dy = abs(point1[1].to!real - point2[1].to!real);

  return sqrt(dx * dx + dy * dy).to!U;
}

/// ditto
Unqual!long distance(EPSG3857 point1, EPSG3857 point2) {
  real dx = abs(point1.x.to!real - point2.x.to!real);
  real dy = abs(point1.y.to!real - point2.y.to!real);

  return sqrt(dx * dx + dy * dy).to!long;
}

/// ditto
Unqual!U distance(U)(PositionT!U point1, PositionT!U point2) {
  real dx = abs(point1[0].to!real - point2[0].to!real);
  real dy = abs(point1[1].to!real - point2[1].to!real);

  return sqrt(dx * dx + dy * dy).to!U;
}

/// It should be able to compute te distance between 2 int points
unittest {
  int[] point1 = [0, 0];
  int[] point2 = [10, 10];

  point1.distance(point2).should.equal(14);
  point2.distance(point1).should.equal(14);
}

/// It should be able to compute te distance between 2 uint points
unittest {
  uint[] point1 = [0, 0];
  uint[] point2 = [10, 10];

  point1.distance(point2).should.equal(14);
  point2.distance(point1).should.equal(14);
}

/// Get the geometric line length
Unqual!U distance(Line: Point[], Point : U[], U)(Line line) {
  Unqual!U result = 0;

  foreach(i; 1..line.length) {
    result += line[i-1].distance(line[i]);
  }

  return result;
}

/// ditto
Unqual!T distance(T)(PositionT!T[] line) {
  Unqual!T result = 0;

  foreach(i; 1..line.length) {
    result += line[i-1].distance(line[i]);
  }

  return result;
}

/// It should be able to compute te distance between 2 int points
unittest {
  int[] point1 = [0, 0];
  int[] point2 = [10, 10];

  [ point1, point2 ].distance.should.equal(14);
  [ point2, point1 ].distance.should.equal(14);
}

/// It should be able to compute te distance between 2 uint points
unittest {
  uint[] point1 = [0, 0];
  uint[] point2 = [10, 10];

  [ point1, point2 ].distance.should.equal(14);
  [ point2, point1 ].distance.should.equal(14);
}

/// It should be able to compute te distance between 3 int points
unittest {
  uint[] point1 = [0, 0];
  uint[] point2 = [10, 10];
  uint[] point3 = [0, 10];

  [ point1, point2, point3 ].distance.should.equal(24);
  [ point3, point2, point1 ].distance.should.equal(24);
}

/// distance between two EPSG4326 points
unittest {
  auto point1 = EPSG4326(2.1699187, 41.3879169);
  auto point2 = EPSG4326(-3.7032498, 40.4167413);

  point1.metricDistance(point2).to!string.should.equal("505171");
}

/// Calculate a polygon's area in meters
double areaMeters(const Position[] coordinates) @safe pure {
  if (coordinates.length <= 2) {
    return 0;
  }

  ///
  double rad(const double a) pure @safe @nogc {
    return a * PI / 180;
  }

  double result = 0;
  enum radius = 6_378_137;


  foreach(i; 0 .. coordinates.length - 1) {
      auto p1 = coordinates[i];
      auto p2 = coordinates[i + 1];

      result += rad(p2[0] - p1[0]) * (2 + sin(rad(p1[1])) + sin(rad(p2[1])));
  }

  return result * radius * radius / 2;
}


/// polygon area using Shoelace formula
double absoluteArea(T)(const T[] coordinates) @safe pure {
  double sum = 0;

  if (coordinates.length > 2) {
    foreach(i; 0..coordinates.length - 1) {
      auto p1 = coordinates[i];
      auto p2 = coordinates[i + 1];

      sum += (p2[0] - p1[0]) * (p2[1] + p1[1]);
    }
  }

  return sum;
}

/// can get the absolut area of a clockwise multi polygon
unittest {
  auto multiPolygon = readText("test/largeMultiPolygon.json").parseJsonString.deserializeJson!MultiPolygon;

  multiPolygon.coordinates[0][0].absoluteArea.should.be.greaterThan(0);
  multiPolygon.coordinates[0][0].isClockWise.should.equal(true);
}

/// check if a list of points is clockwise (lon,lat) pairs
bool isClockWise(T)(const T[] coordinates) @safe pure {
  return coordinates.absoluteArea >= 0;
}

/// 0,0 and 0,1 are clockwise (lon,lat) pairs
unittest {
  Position[] coordinates = [[0,0], [1, 0]].toPositions!double;
  coordinates.isClockWise.should.equal(true);
}

/// 1,0 and 0,0 are not clockwise (lon,lat) pairs
unittest {
  Position[] coordinates = [[1,0], [0, 0], [-1, -1], [1,0]].toPositions!double;
  coordinates.isClockWise.should.equal(false);
}

/// is not clock wise when there is only one point
unittest {
  Position[] coordinates = [[0, 0]].toPositions!double;
  coordinates.isClockWise.should.equal(true);
}

/// is clock wise when there is no point
unittest {
  Position[] coordinates = [];
  coordinates.isClockWise.should.equal(true);
}

/// Change the point order
T[] rewind(T)(T[] points) @safe pure {
  if(points.isClockWise) {
    return points.reverse;
  }

  return points;
}

/// Should not change the point order for a ccv polygon
unittest {
  Position[] points = [ [100.0, 0.0], [101.0, 0.0], [101.0, 1.0], [100.0, 1.0], [100.0, 0.0] ].toPositions!double;

  points
    .rewind
    .should
    .equal(points);
}

/// Should change the point order for a cv polygon
unittest {
  Position[] points =   [ [100.0, 0.0], [100.0, 1.0], [101.0, 1.0], [101.0, 0.0], [100.0, 0.0] ].toPositions!double;
  Position[] expected = [ [100.0, 0.0], [101.0, 0.0], [101.0, 1.0], [100.0, 1.0], [100.0, 0.0] ].toPositions!double;

  points
    .rewind
    .should
    .equal(expected);
}

/// Change the point order in a polygon
Position[][] rewind(Position[][] points) @safe pure {
  foreach(index, polygon; points) {
    if(polygon.isClockWise && index == 0) {
      points[index] = polygon.reverse;
    }

    if(!polygon.isClockWise && index > 0) {
      points[index] = polygon.reverse;
    }
  }

  return points;
}

/// rewind poligon points
unittest {
  Position[][] points = [[[13.3109815,52.48923000000001],[13.3110728,52.4891147],[13.3112592,52.4891708],
    [13.3113031,52.4891304],[13.3113524,52.4890897],[13.3114076,52.4890499],[13.3115579,52.4889536],
    [13.3116843,52.48888770000001],[13.311898,52.4888003],[13.3120039,52.4887706],[13.3121401,52.4887337],
    [13.3122133,52.48872040000001],[13.3122157,52.48872780000001],[13.3122635,52.4887201],[13.3122855,52.4887471],
    [13.3123016,52.4887474],[13.3123065,52.4887584],[13.3125817,52.4893247],[13.312481,52.48934800000001],
    [13.3124217,52.4893671],[13.3123854,52.48938020000001],[13.312354,52.4893955],[13.3123077,52.48941850000001],
    [13.3122905,52.48942700000001],[13.3121963,52.4894903],[13.3121273,52.48956130000001],
    [13.3109815,52.48923000000001]],

    [[13.3114638,52.48923060000001],[13.311587,52.489114], [13.3117387,52.48901540000001],[13.3119128,52.4889397],
    [13.3121467,52.4888735],[13.3123101,52.48921290000001], [13.3122029,52.4892611],[13.3120854,52.4893198],
    [13.3119893,52.4893851],[13.3114638,52.48923060000001]]].toPositions!double;

  Position[][] expected = [[[13.3109815,52.48923000000001],[13.3110728,52.4891147],[13.3112592,52.4891708],
    [13.3113031,52.4891304],[13.3113524,52.4890897],[13.3114076,52.4890499],[13.3115579,52.4889536],
    [13.3116843,52.48888770000001],[13.311898,52.4888003],[13.3120039,52.4887706],[13.3121401,52.4887337],
    [13.3122133,52.48872040000001],[13.3122157,52.48872780000001],[13.3122635,52.4887201],[13.3122855,52.4887471],
    [13.3123016,52.4887474],[13.3123065,52.4887584],[13.3125817,52.4893247],[13.312481,52.48934800000001],
    [13.3124217,52.4893671],[13.3123854,52.48938020000001],[13.312354,52.4893955],[13.3123077,52.48941850000001],
    [13.3122905,52.48942700000001],[13.3121963,52.4894903],[13.3121273,52.48956130000001],[13.3109815,52.48923000000001]],

    [[13.3114638,52.48923060000001],[13.3119893,52.4893851],[13.3120854,52.4893198],[13.3122029,52.4892611],
    [13.3123101,52.48921290000001],[13.3121467,52.4888735],[13.3119128,52.4889397],[13.3117387,52.48901540000001],
    [13.311587,52.489114],[13.3114638,52.48923060000001]]].toPositions!double;

  auto result = points.rewind;

  result[0][0].should.equal(expected[0][0]);
  result[0][1].should.equal(expected[0][1]);
  result[0].should.equal(expected[0]);
  result.should.equal(expected);
}

///
T[] line(T : U[], U)(T begin, T end) @safe pure {
  Unqual!T[] result = [];
  Unqual!T d = [end[0] - begin[0], end[1] - begin[1]];

  if((d[0] < 0 && d[1] < 0) || (d[0] > 0 && d[1] < 0)) {
    auto tmp = line(end, begin);
    tmp = tmp.reverse;
    return tmp;
  }

  Unqual!U index = 1;

  void plotLineLow() @safe pure {
    if(d[1] < 0) {
      index = -1;
      d[1] = -d[1];
    }

    auto delta = 2 * d[1] - d[0];
    Unqual!U y = begin[1];

    foreach(x; begin[0]..end[0] + 1) {
      result ~= [x, y];

      if(delta > 0) {
        y = y + index;
        delta = delta - 2 * d[0];
      }
      delta = delta + 2 * d[1];
    }
  }

  void plotLineHigh() @safe pure nothrow {
    if(d[0] < 0) {
      index = -1;
      d[0] = -d[0];
    }

    auto delta = 2 * d[0] - d[1];
    Unqual!U x = begin[0];

    foreach(y; begin[1]..end[1] + 1) {
      result ~= [x, y];

      if(delta > 0) {
        x = x + index;
        delta = delta - 2 * d[1];
      }
      delta = delta + 2 * d[0];
    }
  }

  if(d[0] <= 0 && d[1] >= 0) {
    plotLineHigh;
  } else {
    plotLineLow;
  }

  if(result.length > 0) {
    auto last = result[result.length - 1];
    if(last[0] != end[0] || last[1] != end[1]) {
      result ~= end;
    }
  }

  return result;
}

/// It should find points betweeen two points
unittest {
  int[] begin = [0, 1];
  int[] end1 = [6, 4];

  int[] end2 = [-4, 6];

  line(begin, end1).should.equal([[0, 1], [1, 1], [2, 2], [3, 2], [4, 3], [5, 3], [6, 4]]);
  line(end1, begin).should.equal([[6, 4], [5, 3], [4, 3], [3, 2], [2, 2], [1, 1], [0, 1]]);

  line(begin, end2).should.equal([[0, 1], [-1, 2], [-2, 3], [-2, 4], [-3, 5], [-4, 6]]);
  line(end2, begin).should.equal([[-4, 6], [-3, 5], [-2, 4], [-2, 3], [-1, 2], [0, 1]]);
}

/// It should find points between two vertical points with distance 1
unittest {
  int[] begin = [68, 41];
  int[] end = [68, 42];

  line(begin, end).should.equal([[68, 41], [68, 42]]);
  line(end, begin).should.equal([[68, 42], [68, 41]]);
}

/// It should find points between two horizontal points with distance 1
unittest {
  int[] begin = [68, 41];
  int[] end = [69, 41];

  line(begin, end).should.equal([[68, 41], [69, 41]]);
  line(end, begin).should.equal([[69, 41], [68, 41]]);
}

///
private bool isOutside(Point, U)(U coord, size_t index, Point[] line) {
  auto diff1 = coord - line[0][index];
  auto diff2 = coord - line[1][index];

  static if(is(U == double)) {
    diff1 = diff1.quantize(0.00000001);
    diff2 = diff1.quantize(0.00000001);
  }

  if(diff1 < 0 && diff2 < 0) {
    return true;
  }

  if(diff1 > 0 && diff2 > 0) {
    return true;
  }

  return false;
}

private auto det(Point)(Point a, Point b) @safe pure nothrow {
  return a[0] * b[1] - a[1] * b[0];
}

/// Returns true if two segments with the same slope have an intersection
private bool intersectsWith(Point)(Point[] line1, Point[] line2) {
  if(isOutside(line1[0][0], 0, line2) && isOutside(line1[1][0], 0, line2) && isOutside(line2[0][0], 0, line1) && isOutside(line2[1][0], 0, line1)) {
    return false;
  }

  if(isOutside(line1[0][1], 1, line2) && isOutside(line1[1][1], 1, line2) && isOutside(line2[0][1], 1, line1) && isOutside(line2[1][1], 1, line1)) {
    return false;
  }

  return true;
}

///
enum Orientation {
  ///
  colinear,
  ///
  clockwise,
  ///
  counterclockwise
}

/// To find orientation of ordered triplet (p, q, r).
Orientation orientation(Point)(const ref Point p, const ref Point q, const ref Point r) @safe pure {
  auto val = (q[1] - p[1]) * (r[0] - q[0]) -
            (r[1] - q[1]) * (q[0] - p[0]);

  if (val == 0) {
    return Orientation.colinear;
  }

  return (val > 0) ? Orientation.clockwise : Orientation.counterclockwise;
}

//// Given three colinear points p, q, r, the function checks if
//// point q lies on line segment 'pr'
bool onSegment(Point)(const ref Point p, const ref Point q, const ref Point r) @safe pure {
  if (q[0] <= max(p[0], r[0]) &&
      q[0] >= min(p[0], r[0]) &&
      q[1] <= max(p[1], r[1]) &&
      q[1] >= min(p[1], r[1])) {
        return true;
  }

  return false;
}

///
bool onSameLine(Point)(const ref Point p, const ref Point q, const ref Point r) {
  auto d1 = distance(p, q);
  auto d2 = distance(q, r);
  auto d = distance(p, r);

  return d1 + d2 == d;
}

/// The function that returns true if line segment 'p1q1'
/// and 'p2q2' intersect.
bool doIntersect(Point)(Point[] line1, Point[] line2) @safe pure {
  auto p1 = line1[0];
  auto q1 = line1[1];
  auto p2 = line2[0];
  auto q2 = line2[1];

  // Find the four orientations needed for general and
  // special cases
  auto o1 = orientation(p1, q1, p2);
  auto o2 = orientation(p1, q1, q2);
  auto o3 = orientation(p2, q2, p1);
  auto o4 = orientation(p2, q2, q1);

  // General case
  if (o1 != o2 && o3 != o4) {
    return true;
  }

  // Special Cases
  // p1, q1 and p2 are colinear and p2 lies on segment p1q1
  if (o1 == Orientation.colinear && onSegment(p1, p2, q1)) {
    return true;
  }

  // p1, q1 and p2 are colinear and q2 lies on segment p1q1
  if (o2 == Orientation.colinear && onSegment(p1, q2, q1)) {
    return true;
  }

  // p2, q2 and p1 are colinear and p1 lies on segment p2q2
  if (o3 == Orientation.colinear && onSegment(p2, p1, q2)) {
    return true;
  }

  // p2, q2 and q1 are colinear and q1 lies on segment p2q2
  if (o4 == Orientation.colinear && onSegment(p2, q1, q2)) {
    return true;
  }

  return false;
}

/// doIntersect test
unittest {
  [[-70, 30], [-70, 50]].doIntersect([[-63, 40], [1000, 40]]).should.equal(false);
  [[-70, 30], [-70, 50]].doIntersect([[-75, 40], [-50, 40]]).should.equal(true);
}

///
bool doOverlap(Point : U[], U)(const Point[] line1, const Point[] line2) {
  if((line1[0] == line2[0] && line1[1] == line2[1]) || (line1[0] == line2[1] && line1[1] == line2[0]) )
  {
    return true;
  }
    return false;
}

/// doOverlap test
unittest {
  [[1, 1], [3, 3]].doOverlap([[3,3], [1,1]]).should.equal(true);
}

/// Get the point intersection of two segments
Point[] intersection(Point : U[], U)(Point[] line1, Point[] line2, bool ignoreFillPoints = false) if(is(U == long) || is(U == double)) {
  Point[] result;

  if(line1.length == 0 || line2.length == 0) {
    return [];
  }

  auto xdiff = [line1[0][0] - line1[1][0], line2[0][0] - line2[1][0]];
  auto ydiff = [line1[0][1] - line1[1][1], line2[0][1] - line2[1][1]];

  auto div = det(xdiff, ydiff);

  if(div == 0) {
    if(!line1.intersectsWith(line2)) {
      return [];
    }

    auto xlist = [ line1[0][0], line1[1][0], line2[0][0], line2[1][0] ].sort;
    auto ylist = [ line1[0][1], line1[1][1], line2[0][1], line2[1][1] ].sort;

    if(ignoreFillPoints) {
      return [[xlist[1], ylist[1]], [xlist[2], ylist[2]]];
    }

    return line([xlist[1], ylist[1]], [xlist[2], ylist[2]]);
  }

  auto d = [det(line1[0], line1[1]), det(line2[0], line2[1])];
  auto x = det(d, xdiff) / div;
  auto y = det(d, ydiff) / div;
  if(isOutside(x, 0, line1) || isOutside(x, 0, line2)) {
    return [];
  }

  if(isOutside(y, 1, line1) || isOutside(y, 1, line2)) {
    return [];
  }

  return [[x, y]];
}

///
Point[] intersectionNoCheck(Point)(Point[] line1, Point[] line2, bool ignoreFillPoints = false) {
  Point[] result;

  if(line1.length == 0 || line2.length == 0) {
    return [];
  }

  auto xdiff = [line1[0][0] - line1[1][0], line2[0][0] - line2[1][0]];
  auto ydiff = [line1[0][1] - line1[1][1], line2[0][1] - line2[1][1]];

  auto div = det(xdiff, ydiff);

  if(div == 0) {
    if(!line1.intersectsWith(line2)) {
      return [];
    }

    auto xlist = [ line1[0][0], line1[1][0], line2[0][0], line2[1][0] ].sort;
    auto ylist = [ line1[0][1], line1[1][1], line2[0][1], line2[1][1] ].sort;

    static if(is(Point == double[])) {
      return [[xlist[1], ylist[1]], [xlist[2], ylist[2]]];
    } else {
      return [Point(xlist[1], ylist[1]), Point(xlist[2], ylist[2])];
    }
  }

  auto d = [det(line1[0], line1[1]), det(line2[0], line2[1])];
  auto x = det(d, xdiff) / div;
  auto y = det(d, ydiff) / div;

  static if(is(Point == double[])) {
    return [[x, y]];
  } else {
    return [Point(x, y)];
  }
}

/// It should find the intersection between a horisontal and a diagonal line
unittest {
  auto line1 = [[1382, 900], [1382, 908]];
  auto line2 = [[1382, 901], [1388, 901]];

  line1.intersection(line2).should.equal([[1382, 901]]);
  line2.intersection(line1).should.equal([[1382, 901]]);
}

/// ditto
Point[] intersection(Point)(Point[] line1, Point[] line2, bool ignoreFillPoints = false) if(is(Point == EPSG3857) || is(Point == EPSG4326) || is(Point == PositionT!double)) {
  if(line1.length == 0 || line2.length == 0) {
    return [];
  }

  auto point1 = line1[0];
  auto point2 = line1[1];
  auto point3 = line2[0];
  auto point4 = line2[1];

  return intersection([point1.toArray, point2.toArray], [point3.toArray, point4.toArray], ignoreFillPoints)
    .map!(a => Point(a))
    .array;
}

/// ditto
Point[] intersection(Point : U[], U)(Point[] line1, Point[] line2) if(!is(U == long) && !is(U == double)) {
  if(line1.length == 0 || line2.length == 0) {
    return [];
  }

  auto result = intersection(line1.to!(long[][]), line2.to!(long[][]));

  return result.map!(a => [a[0].to!U, a[1].to!U]).array;
}

/// ditto
Line[] intersection(Line : Point[], Point, U)(Line line, BBox!U bbox) {
  if(line.length < 2) {
    return [];
  }

  if(line[0] != line[$-1]) {
    return lineIntersection(line, bbox);
  }

  Line[] result;
  ubyte edge;
  ubyte isPreviousPointInside, isInside;
  Point previousPoint;

  for(edge = 1; edge <= 8; edge *= 2) {
    result = [[]];
    previousPoint = line[$-1];

    isPreviousPointInside = !(bbox.relativeDirection(previousPoint) & edge);

    foreach(point; line) {
      isInside = !(bbox.relativeDirection(point) & edge);

      if(isInside != isPreviousPointInside) {
        auto intersections = intersectBbox([
          [previousPoint[0], previousPoint[1]],
          [point[0], point[1]]], edge, bbox);

        static if(is(Point == EPSG3857)) {
          result[0] ~= intersections.toEPSG3857;
        } else static if(is(Point == struct)) {
          result[0] ~= Point(intersections);
        } else {
          result[0] ~= intersections;
        }
      }
      if(isInside) result[0] ~= point;

      previousPoint = point;
      isPreviousPointInside = isInside;
    }

    line = result[0];
    if (!line.length) break;
  }

  if(result.length > 0 && result[0].length > 0 && result[0][$-1] != result[0][0]) {
    result[0] ~= result[0][0];
  }

  return result;
}

/// Should find no intersection points, for two segments that do not intersect
unittest {
  [[0, 0], [10, 10]].intersection([[20, 20], [30, 30]]).length.should.equal(0);
  [[0, 0], [10, 10]].intersection([[20, 20], [25, 30]]).length.should.equal(0);
  [[10, 10], [0, 0]].intersection([[30, 20], [20, 30]]).length.should.equal(0);
  [[10, 10], [0, 0]].intersection([[20, 20], [25, 30]]).length.should.equal(0);
}

/// Should find an intersection point, for two segments that intersect
unittest {
  [[0, 0], [10, 10]].intersection([[0, 10], [10, 0]]).should.equal([[5, 5]]);
  [[0, 10], [10, 0]].intersection([[0, 0], [10, 10]]).should.equal([[5, 5]]);
  [[3, 7], [9, 1]].intersection([[0, 0], [10, 10]]).should.equal([[5, 5]]);
  [[0,0], [2,2]].intersection([[1,1], [2,1]]).should.equal([[1,1]]);
  [[2,2], [2,0]].intersection([[1,1], [40,1]]).should.equal([[2,1]]);
  [[8,43], [0,35]].intersection([[1,1], [1,40]]).should.equal([[1,36]]);
}

/// Should find an intersection points for segments on the same axis
unittest {
  [[0, 0], [3, 3]].intersection([[0, 0], [3, 3]]).should.equal([[0, 0], [1, 1], [2, 2], [3, 3]]);
  [[1, 1], [3, 3]].intersection([[0, 0], [3, 3]]).should.equal([[1, 1], [2, 2], [3, 3]]);
  [[1, 1], [2, 2]].intersection([[0, 0], [3, 3]]).should.equal([[1, 1], [2, 2]]);
  [[0, 0], [3, 3]].intersection([[1, 1], [3, 3]]).should.equal([[1, 1], [2, 2], [3, 3]]);
  [[0, 0], [3, 3]].intersection([[1, 1], [2, 2]]).should.equal([[1, 1], [2, 2]]);
}

///
Line[] getPolygonSides(Line: Point[], Point: U[], U)(Line polygon) {
  Line[] sides;
  for(int i = 0; i < polygon.length-1; i++) {
    sides ~= [polygon[i], polygon[i+1]];
  }
  return sides;
}

/// ditto
Line[] getPolygonSides(Line: PositionT!U[], U)(Line polygon) {
  Line[] sides;
  for(int i = 0; i < polygon.length-1; i++) {
    sides ~= [polygon[i], polygon[i+1]];
  }
  return sides;
}

/// Return the intersection of a line with the edge of a bbox - used in the polygon and bbox intersection function
Point intersectBbox(Point : U[], U)(Point[] line, ubyte edge, BBox!U bbox) {
  if(edge & 8 && line[1][1] - line[0][1] != 0) {
    return [line[0][0] + (line[1][0] - line[0][0]) * (bbox.top - line[0][1]) / (line[1][1] - line[0][1]) , bbox.top];
  }
  if(edge & 4 && line[1][1] - line[0][1] != 0) {
    return [line[0][0] + (line[1][0] - line[0][0]) * (bbox.bottom - line[0][1]) / (line[1][1] - line[0][1]) , bbox.bottom];
  }
  if(edge & 2 && line[1][0] - line[0][0] != 0) {
    return [bbox.right, line[0][1] + (line[1][1]- line[0][1]) * (bbox.right - line[0][0]) / (line[1][0] - line[0][0])];
  }
  if(edge & 1 && line[1][0] - line[0][0] != 0) {
    return [bbox.left, line[0][1] + (line[1][1]- line[0][1]) * (bbox.left - line[0][0]) / (line[1][0] - line[0][0])];
  }
  return [];
}

/// ditto
PositionT!U intersectBbox(Point : PositionT!U, U)(Point[] line, ubyte edge, BBox!U bbox) {
  U lon;
  U lat;

  if(edge & 8 && line[1][1] - line[0][1] != 0) {
    lon = line[0][0] + (line[1][0] - line[0][0]) * (bbox.top - line[0][1]) / (line[1][1] - line[0][1]);
    lat = bbox.top;
  } else if(edge & 4 && line[1][1] - line[0][1] != 0) {
    lon = line[0][0] + (line[1][0] - line[0][0]) * (bbox.bottom - line[0][1]) / (line[1][1] - line[0][1]);
    lat = bbox.bottom;
  } else if(edge & 2 && line[1][0] - line[0][0] != 0) {
    lon = bbox.right;
    lat = line[0][1] + (line[1][1]- line[0][1]) * (bbox.right - line[0][0]) / (line[1][0] - line[0][0]);
  } else if(edge & 1 && line[1][0] - line[0][0] != 0) {
    lon = bbox.left;
    lat = line[0][1] + (line[1][1]- line[0][1]) * (bbox.left - line[0][0]) / (line[1][0] - line[0][0]);
  }
  return Point(lon, lat);
}

/// Get the intersection between a box and a line
Line[] lineIntersection(Line: Point[], Point, U)(Line points, BBox!U bbox) {
  Line[] result;
  Line part;

  byte codeA = bbox.relativeDirection(points[0]);
  byte codeB;
  byte lastCode;

  foreach(i; 1..points.length) {
    auto a = points[i - 1];
    auto b = points[i];
    codeB = bbox.relativeDirection(b);
    lastCode = codeB;

    while (true) {
      if (!(codeA | codeB)) { // accept
        part ~= a;

        if(codeB != lastCode) { // segment went outside
          part ~= b;

          if (i < points.length - 1) { // start a new line
            result ~= part;
            part = [];
          }
        } else if (i == points.length - 1) {
          part ~= b;
        }

        break;
      } else if (codeA & codeB) { // trivial
        if(bbox.contains(a[0], a[1])) {
          part ~= a;
        }

        break;
      } else if (codeA) { // a outside, intersect with clip edge
        auto tmp = intersectBbox([[a[0], a[1]], [b[0], b[1]]], codeA, bbox);

        static if(is(Point == EPSG3857)) {
          a = tmp.toEPSG3857;
        } else static if(is(Point == struct)) {
          a = Point(tmp);
        } else {
          a = tmp;
        }

        codeA = bbox.relativeDirection(a);
      } else { // b outside
        auto tmp = intersectBbox([[a[0], a[1]], [b[0], b[1]]], codeB, bbox);

        static if(is(Point == EPSG3857)) {
          b = tmp.toEPSG3857;
        } else static if(is(Point == struct)) {
          b = Point(tmp);
        } else {
          b = tmp;
        }

        codeB = bbox.relativeDirection(b);
      }
    }

    codeA = lastCode;
  }

  if(part.length) {
    result ~= part;
  }

  foreach (i; 0 .. result.length) {
    foreach (j; 0 .. result.length) {
      if(i == j || result[i].length == 0 || result[j].length == 0) {
        continue;
      }

      const last1 = result[j].length - 1;
      const last2 = result[i].length - 1;

      if(result[j][last1] == result[i][0]) {
        result[j] ~= result[i];
        result[j] = result[j].uniq.array;
        result[i] = [];
        continue;
      }

      if(result[i][last2] == result[j][0]) {
        result[i] ~= result[j];
        result[i] = result[i].uniq.array;
        result[j] = [];
        continue;
      }
    }
  }

  return result.filter!(a => a.length > 0).array;
}

/// It should return the line that touches a corner of a bbox
unittest {
  double[][] line = [
    [ 13.41328688344344, 52.51689312637657 ],
    [ 13.52172241827204, 52.48608886620525 ],
    [ 13.57051840894491, 52.45746558075007 ],
    [ 13.71148460422209, 52.44535012458942 ]
  ];
  auto bbox = BBox!double(13.4297, 13.5527, 52.4935, 52.4185);

  line.lineIntersection(bbox).to!string.should.equal(`[[[13.5527, 52.4773], [13.5217, 52.4861], [13.5527, 52.4679]]]`);
}

/// It should return all points inside a bbox
unittest {
  int[][] line = [
    [628, 3443], [1891, 4032], [2459, 4580], [4102, 4811]
  ];
  auto bbox = BBox!int(-1, 4096, -1, 4096);

  line.lineIntersection(bbox).should.equal([[[628, 3443], [1891, 4032], [-1, 2207]]]);
}

/// It should return all line points when they are inside the bbox
unittest {
  double[][] line = [[13.2021, 52.7043], [13.593, 52.6044], [13.8039, 52.4536], [13.7423, 52.3268]];
  auto bbox = BBox!double(180., -180., -85.0511, 85.0511);

  line.lineIntersection(bbox).should.equal([ line ]);
}

/// It should return at least 2 points
unittest {
  import geo.xyz;
  auto bbox = ZXY(11, 1102, 673).toBBox(-0.2);

  double[][] line = [
    [ 13.7565714114207, 52.47858229266762 ],
    [ 13.76115158759581, 52.47586235570887 ],
    [ 13.7917896789247, 52.46319202204839 ],
    [ 13.79996564814427, 52.4531243309259 ],
    [ 13.80166897506502, 52.4430543377234 ],
    [ 13.78735695571817, 52.42518018732963 ],
    [ 13.74228777750957, 52.32684143616353 ]
  ];

  line.lineIntersection(bbox).to!string.should.equal(`[[[13.7055, 52.2467], [13.7423, 52.3268]]]`);
}

/// Should find no intersection if a line is outside the box
unittest {
  auto bbox = BBox!int(2,3,2,3);
  [[0, 0], [1, 1]].intersection(bbox).length.should.equal(0);
}

/// Should find two intersected lines if the line enters in the box twice
unittest {
  auto bbox = BBox!int(0, 10, 0, 10);
  [[3, -3], [3, 20], [5, -3]].intersection(bbox).should.equal([[[3, 10], [3, 0]], [[4, 0], [1, 10]]]);
}

/// Should find one line intersection if the line enters in the box twice through the same point
unittest {
  auto bbox = BBox!int(0, 100, 0, 100);
  [[30, -30], [30, 200], [50, -30]].intersection(bbox).should.equal([[[30, 100], [30, 0]], [[47, 0], [37, 100]]]);
}

/// Should get the diagonal lines intersecting the box
unittest {
  auto bbox = BBox!int(1,2,2,1);
  //diagonal left-bottom to right-top
  [[0, 0], [2, 2]].intersection(bbox).should.equal([[[2, 2], [2, 2]]]);
  //diagonal left-top to right-bottom
  [[3,0], [1,2]].intersection(bbox).should.equal([[[1, 2], [1, 2]]]);
}

/// Should return a line that is inside the box, touching one of the sides
unittest {
  auto bbox = BBox!int(1,4,4,1);
  [[1, 1], [2, 2]].intersection(bbox).should.equal([[[1,1], [2,2]]]);
}

/// Should return a line completely inside the polygon
unittest {
  auto bbox = BBox!int(1,4,4,1);
  [[2, 2], [3, 3]].intersection(bbox).should.equal([[[2,2], [3,3]]]);
}

/// Should return a polygon inside the box
unittest {
  auto bbox = BBox!int(1,4,4,1);
  [[2, 2], [2, 3], [3, 3], [3, 2], [2,2]].intersection(bbox).should.equal([[[2,2], [2, 3], [3, 3], [3, 2], [2, 2]]]);
}

/// Should return a polygon inside the box, touching the box sides
unittest {
  auto bbox = BBox!int(1,40,40,1);
  [[1,1], [1,2], [2,2], [2,1], [1,1]].intersection(bbox).should.equal([[[1,1], [1,2], [2,2], [2,1], [1,1]]]);
  [[39,39], [39,40], [40, 40], [40,39], [39,39]].intersection(bbox).should.equal([[[39,39], [39,40], [40, 40], [40,39], [39,39]]]);
}

///Should return the whole box when the polygon includes the box
unittest {
  auto bbox = BBox!int(1,2,2,1);
  [[0, 0], [0, 3], [3, 3], [3, 0], [0, 0]].intersection(bbox).should.equal([[[1, 2], [1, 1], [2, 1], [2, 2], [1, 2]]]);
}

///Should return the whole box when the polygon and the box are identical
unittest {
  auto bbox = BBox!int(1,2,2,1);
  [[1, 1], [1, 2], [2, 2], [2, 1], [1, 1]].intersection(bbox).should.equal([[[1,1], [1,2], [2,2], [2,1], [1,1]]]);
}

/// Should get the intersection of a polygon and the box, which is a polygon
unittest {
  auto bbox = BBox!int(1,40,40,1);
  [[0, 0], [0, 2], [2, 2], [2,0], [0,0]].intersection(bbox).should.equal([[[1, 40], [1, 2], [2, 2], [2, 40], [1, 40]]]);
  [[39, 39], [39, 41], [41, 41], [41, 39], [39, 39]].intersection(bbox).should.equal([[[39, 39], [39, 1], [40, 1], [40, 39], [39, 39]]]);
}

/// Should get the interesection of a polygon with a diagonal line inside the bbox
unittest {
  auto bbox = BBox!double(1.46118, 1.41174, 1.4116, 1.46102);
  double[][] points = [[1., 1.], [2., 2.], [3., 1.], [1.7, 3.], [1., 1.]];
  auto expected = [[[1.41174, 1.4116], [1.41174, 1.41174], [1.4116, 1.4116], [1.41174, 1.4116]]];

  points.intersection(bbox).should.equal(expected);
}

/// Should get the intersection which is a triangle
unittest {
  auto bbox = BBox!double(1.0,40.0,40.0,1.0);
  double[][] polygon = [[0.0, 35.0], [0.0, 43.0], [8.0, 43.0], [0.0,35.0]];

  double [][][] expected = [[[1, 1], [-34, 1], [1, 36], [1, 1]]];
  polygon.intersection(bbox).should.equal(expected);
}

/// Should get the intersection which is a triangle int
unittest {
  auto bbox = BBox!int(1,40,40,1);

  [[0, 35], [0, 43], [8, 43], [0,35]].intersection(bbox).should.equal([[[1, 1], [-34, 1], [1, 36], [1, 1]]]);
}

/// Should get the interesection of a polygon with 5 sides and the box, which is a polygon
unittest {
  auto bbox = BBox!int(2,40,40,2);
  [[0, 3], [0, 10], [3, 10], [4, 7], [3, 3], [0, 3]].intersection(bbox).should.equal([[[2, 10], [3, 10], [4, 7], [3, 3], [2, 3], [2, 10]]]);
}

/// Should get the intersection of a concave polygon and the box, which results in 2 distinct polygons
unittest {
  auto bbox = BBox!double(2.0, 40.0, 40.0, 2.0);
  double[][] polygon = [[0.0, 3.0], [0.0, 10.0], [3.0, 10.0], [1.0, 7.0], [3.0, 3.0], [0.0, 3.0]];

  double[][][] expected = [[[2., 10.], [3., 10.], [2., 8.5], [2., 5.], [3., 3.], [2., 3.], [2., 10.]]];

  polygon.intersection(bbox).to!string.should.equal(expected.to!string);
}

/// Split a line in equal segments
Point[] lineSplit(Line: Point[], Point : U[], U)(Line line, U segmentLength) {
  return line.toPositions.lineSplit(segmentLength).toList;
}

/// ditto
PositionT!U[] lineSplit(U)(PositionT!U[] line, U segmentLength) {
  alias Point = PositionT!U;
  alias UU = Unqual!U;

  auto calculateSplitPointCoords(PositionT!U startNode, PositionT!U nextNode, real distance) {
    const length = startNode.distance(nextNode);
    const partition = distance / length.to!real;

    real x = startNode[0] * (1 - partition) + nextNode[0] * partition;
    real y = startNode[1] * (1 - partition) + nextNode[1] * partition;

    return Point(x.to!UU, y.to!UU);
  }

  Point[] splitPoints;
  UU leftDistance = 0;

  Point[][] segments;

  foreach(i; 1..line.length) {
    segments ~= [ line[i - 1], line[i] ];
  }

  auto bbox = BBox!UU(line);

  foreach(segment; segments) {
    auto point = segment[0];
    auto nextPoint = segment[1];

    auto remainingDistance = point.distance(nextPoint);

    while (remainingDistance + leftDistance > segmentLength) {
      const splitPoint = calculateSplitPointCoords(point, nextPoint, segmentLength - leftDistance);
      leftDistance = 0;
      remainingDistance -= point.distance(splitPoint);

      if(bbox.contains(splitPoint)) {
        splitPoints ~= splitPoint;
      }

      point = splitPoint;
    }

    leftDistance += remainingDistance;
  }

  return splitPoints;
}

/// It should split a line with a segment
unittest {
  [[0, 0], [100, 100]].lineSplit(10).should.equal([
    [7, 7],   [14, 14], [21, 21], [28, 28], [35, 35], [42, 42], [49, 49],
    [56, 56], [63, 63], [70, 70], [77, 77], [84, 84], [91, 91], [98, 98]]);


  [[100, 100], [0, 0]].lineSplit(10).should.equal([[92, 92], [84, 84], [76, 76], [68, 68],
  [60, 60], [52, 52], [44, 44], [36, 36], [28, 28], [20, 20], [12, 12], [4, 4]]);
}

/// It should split a line with two segments
unittest {
  [[0, 0], [50, 50], [0, 100]].lineSplit(20).should.equal([[14, 14], [28, 28],
    [42, 42], [45, 55], [30, 69], [16, 83], [2, 97]]);
}

///
Point centroid(Polygon: Point[], Point: U[], U)(Polygon polygon) {
  auto tmp = polygon.toPositions;

  return tmp.centroid.toList;
}

/// ditto
Point centroid(Polygon: Point[], Point: PositionT!U, U)(const ref Polygon polygon) @nogc nothrow {
  Unqual!U lon = 0;
  Unqual!U lat = 0;

  Unqual!U formula = 0;
  Unqual!U signedAreaTimesTwo = 0;

  foreach(i; 0..polygon.length-1) {
    formula = polygon[i][0] * polygon[i+1][1] - polygon[i+1][0] * polygon[i][1];
    lon += (polygon[i][0] + polygon[i+1][0]) * formula;
    lat += (polygon[i][1] + polygon[i+1][1]) * formula;
    signedAreaTimesTwo += formula;
  }

  if(signedAreaTimesTwo == 0) {
    return Point(polygon[0][0], polygon[0][1]);
  }

  lon /= 3 * signedAreaTimesTwo;
  lat /= 3 * signedAreaTimesTwo;

  return Point(lon, lat);
}

/// It should get the Centroid of a square polygon
unittest {
  [[0, 0], [0, 4], [4, 4], [4, 0], [0, 0]].centroid().should.equal([2, 2]);
}

/// It should return the first point of the polygon if its the area is 0
unittest {
  [[0, 0], [0,0], [0,0]].centroid().should.equal([0, 0]);
}

///
double[] visualCenter(Polygon: Point[], Point: U[], U)(Polygon polygon, real precision = 0.1) {
  QuadtreeBBox!real[] bboxes;

  real[] minPoint = polygon[0].to!(real[]);
  real[] maxPoint = polygon[0].to!(real[]);

  foreach(point; polygon) {
    if(point[0] < minPoint[0]) minPoint[0] = point[0];
    if(point[0] > maxPoint[0]) maxPoint[0] = point[0];

    if(point[1] < minPoint[1]) minPoint[1] = point[1];
    if(point[1] > maxPoint[1]) maxPoint[1] = point[1];
  }

  real boxWidth = maxPoint[0] - minPoint[0];
  real boxHeight = maxPoint[1] - minPoint[1];
  real boxDimension = min(boxWidth, boxHeight);
  real halfDimension = boxDimension / 2;

  for(auto x = minPoint[0]; x < maxPoint[0]; x += boxDimension) {
    for(auto y = minPoint[1]; y < maxPoint[1]; y += boxDimension) {
      bboxes ~= QuadtreeBBox!real(x + halfDimension, y + halfDimension, halfDimension, polygon.to!(real[][]));
      bboxes.sort!("a.potentialMax() > b.potentialMax()");
    }
  }

  real[] polygonCentroid = centroid(polygon.to!(real[][]));
  auto bestBox = QuadtreeBBox!real(polygonCentroid[0], polygonCentroid[1], 0, polygon.to!(real[][]));

  while(bboxes.length) {
    auto bbox = bboxes[0];
    bboxes = bboxes[1..$];
    if(bbox.centerToPolygonDistance() > bestBox.centerToPolygonDistance()) {
      bestBox = bbox;
    }
    if(bbox.potentialMax() - bestBox.centerToPolygonDistance() <= precision) continue;

    halfDimension = bbox.halfDimension / 2;
    bboxes ~= QuadtreeBBox!real(bbox.x - halfDimension, bbox.y - halfDimension, halfDimension, polygon.to!(real[][]));
    bboxes ~= QuadtreeBBox!real(bbox.x + halfDimension, bbox.y - halfDimension, halfDimension, polygon.to!(real[][]));
    bboxes ~= QuadtreeBBox!real(bbox.x - halfDimension, bbox.y + halfDimension, halfDimension, polygon.to!(real[][]));
    bboxes ~= QuadtreeBBox!real(bbox.x + halfDimension, bbox.y + halfDimension, halfDimension, polygon.to!(real[][]));

  }

  return [bestBox.x.to!(double), bestBox.y.to!(double)];
}

/// It should return the polygon Centroid for a square polygon
unittest {
  [[0, 0], [0, 4], [4, 4], [4, 0], [0, 0]].visualCenter().should.equal([2.0, 2.0]);
}

/// It should return a point inside the polygon in the case of a concave polygon
unittest {
  [[1,1], [4, 10], [6, 1], [4, 9], [1,1]].visualCenter().should.equal([3.96875, 9.28125]);
}

/// It should return a point inside the polygon for an L-shaped polygon
unittest {
  [ [
      3.5760498046875,
      7.340674831854912
    ],
    [
      3.5595703125,
      5.5941182188847876
    ],
    [
      4.9053955078125,
      5.572249801113911
    ],
    [
      4.910888671875,
      5.84554547217311
    ],
    [
      3.9166259765625,
      5.878332109674327
    ],
    [
      3.9166259765625,
      7.351570982365025
    ],
    [
      3.5760498046875,
      7.340674831854912
    ]].visualCenter().to!string.should.equal(`[3.72849, 6.07899]`);

 [[0,0], [0, 10], [3, 10], [3, 3], [7, 3], [7, 0], [0,0]].visualCenter().should.equal([1.75, 1.75]);
}

struct QuadtreeBBox(T) {
  /// x coordinate of the center of the box
  T x;

  /// y coordinate of the center of the box
  T y;

  /// the minimum between height and width divided by two
  T halfDimension;

  /// the polygon associated to it
  T[][] polygon;

  /// distance + radius, used to prioritize bboxes that have higher chances of containing the visual center, and to discard non-promising bboxes
  T potentialMax() {
    return (centerToPolygonDistance() + halfDimension * sqrt(2.0));
  }

  T centerToPolygonDistance() {
    auto minDistanceSquare = T.infinity;
    bool inside = false;
    size_t countIntersections;

    foreach(side; getPolygonSides(polygon)) {
      T[] center = [x, y];
      minDistanceSquare = min(minDistanceSquare, getDistanceToSegmentSquared(center, side));
      auto a = side[0];
      auto b = side[1];

      //Create a segment that starts from the center of the box and goes to infinity on the x axis, to the right
      //Based on how many times it intersects the side of the polygon, we can tell if the point is inside or outside it.
      auto extremePoint = [T.max, center[1]];
      if(doIntersect([center, extremePoint], side)) {
          if (orientation(a, b, center) == Orientation.colinear) {
              inside = onSegment(a, center, b);
          }
          countIntersections++;
      }
    }

    inside = (countIntersections % 2 == 1);
    if(!inside) {
        return sqrt(minDistanceSquare) * -1;
    }

    if(minDistanceSquare == 0) {
      return 0;
    }

    return sqrt(minDistanceSquare);
  }
}

/// It should return the distance from the center to the polygon when the center is inside the polygon
unittest {
  auto box = QuadtreeBBox!double(2.0, 2.0, 2.0, [[0.0, 0.0], [0.0, 4.0], [4.0, 4.0], [4.0, 0.0], [0.0, 0.0]]);
  box.centerToPolygonDistance().should.equal(2);
}

/// It should return a negative distance if the center is outside the polygon
unittest {
  auto box = QuadtreeBBox!double(2.0, 2.0, 2.0, [[0.0, 0.0], [0.0, 1.0], [4.0, 1.0], [4.0, 0.0], [0.0, 0.0]]);
  box.centerToPolygonDistance().should.equal(-1);
}

U getDistanceToSegmentSquared(Point : U[], U)(U[] point, Point[] segment) {
  auto x = segment[0][0].to!real;
  auto y = segment[0][1].to!real;
  auto dx = segment[1][0] - x;
  auto dy = segment[1][1] - y;

  auto px = point[0].to!real;
  auto py = point[1].to!real;

  if(dx != 0 || dy != 0) {
    //r = dotProduct((dx, dy), (px-x, py-y);
    auto r = dx * (px-x) + dy * (py-y);
    //divide r by ||segment[1]-segment[0||^2
    r /= (dx * dx + dy * dy);

    if(r > 1) {
      x = segment[1][0];
      y = segment[1][1];
    } else if (r > 0) {
      x += dx * r;
      y += dy * r;
    }
  }

  dx = px - x;
  dy = py - y;

  return (dx * dx + dy * dy).to!U;
}

/// It should return the squared distance when the distance is the length of the segment from the point and the first segment point
unittest {
  [1, 0].getDistanceToSegmentSquared([[1, 1], [2, 1]]).should.equal(1);
}

/// It should return the squared distance when the point is in the middle of the segment relative to the x axis
unittest {
  [4, 3].getDistanceToSegmentSquared([[2, 1], [6, 1]]).should.equal(4);
}

/// It should return the squared distance between the point and the rightmost point in the segment when the point is to the right of the segment
unittest {
  [6, 3].getDistanceToSegmentSquared([[1, 1], [4, 1]]).should.equal(8);
}

/// Joins all line strings that are connected through the first or last node and convert them to a
/// polygon. All other features will be kept in the list.
Feature[] createRings(ref const(Feature) feature) {
  if(feature.geometry.peek!GeometryCollection !is null) {
    Feature extract(BasicGeometryVariant geometry) {
      auto newFeature = Feature();

      foreach(key, val; feature.properties) {
        newFeature.properties[key] = val;
      }
      newFeature.geometry = geometry;
      return newFeature;
    }

    Position[][] toLines(BasicGeometryVariant geometry) {
      if(geometry.peek!MultiLineString) {
        return geometry.get!MultiLineString.coordinates;
      }

      if(geometry.peek!LineString) {
        return [ geometry.get!LineString.coordinates ];
      }

      return [];
    }

    auto features = feature.geometry.get!GeometryCollection.geometries
      .filter!(a => a.peek!MultiLineString is null && a.peek!LineString is null)
      .map!(a => extract(a))
      .array;

    const(Position)[][] lines = cast(const(Position)[][]) feature.geometry.get!GeometryCollection.geometries
      .filter!(a => a.peek!MultiLineString !is null || a.peek!LineString !is null)
      .map!(a => toLines(a))
      .joiner
      .array;

    return features ~ lines.createRings(feature.properties);
  }

  if(feature.geometry.peek!MultiLineString is null) {
    return [ cast() feature ];
  }

  auto lines = feature.geometry.get!MultiLineString.coordinates.dup;

  return createRings(lines, feature.properties);
}

/// ditto
Feature[] createRings(T)(ref const(PositionT!T)[][] lines, const(Json[string]) properties) {
  auto selectedLine = 0;
  bool changed;

  while(selectedLine < lines.length) {
    do {
      changed = false;
      foreach(ref line; lines[selectedLine+1..$]) {
        auto tmpLine = cast(Position[]) line.dup;
        auto link = lines[selectedLine].isLinkedWith(tmpLine);

        if(link == LineLink.after) {
          lines[selectedLine] ~= tmpLine[1..$];
          line.length = 0;
          changed = true;
        }

        if(link == LineLink.afterReverse) {
          tmpLine = tmpLine.reverse;
          lines[selectedLine] ~= tmpLine[1..$];
          line.length = 0;
          changed = true;
        }

        if(link == LineLink.before) {
          lines[selectedLine] = join([tmpLine[0..$-1], lines[selectedLine]]);
          line.length = 0;
          changed = true;
        }

        if(link == LineLink.beforeReverse) {
          tmpLine = tmpLine.reverse;
          lines[selectedLine] = join([tmpLine[0..$-1], lines[selectedLine]]);
          line.length = 0;
          changed = true;
        }
      }

      lines = lines.filter!(a => a.length > 0).array;
    } while(changed);

    selectedLine++;
  }

  Feature convert(Position[] coordinates) {
    auto newFeature = Feature();

    foreach(key, val; properties) {
      newFeature.properties[key] = val;
    }

    if(coordinates[0] == coordinates[coordinates.length - 1]) {
      newFeature.geometry = Polygon([coordinates]);
    } else {
      newFeature.geometry = LineString(coordinates);
    }

    return newFeature;
  }

  return lines.map!(a => convert(cast(Position[]) a)).array;
}

/// It should ignore a point feature
unittest {
  auto point = Feature();
  point.geometry = Point(Position(0, 0));
  auto result = createRings(point);

  result[0].should.equal(point);
}

/// It should work with a GeometryCollection
unittest {
  auto feature = Feature();

  auto geometry1 = MultiLineString([
    [[5, 5], [1, 1]],
    [[1, 1], [2, 2], [0, 0]]
  ].toPositions!double);

  auto geometry2 = LineString([[0, 0], [5, 5]].toPositions!double);
  auto geometry3 = Polygon([[[10, 10], [51, 51]]].toPositions!double);


  feature.geometry = GeometryCollection([
    BasicGeometryVariant(geometry1),
    BasicGeometryVariant(geometry2),
    BasicGeometryVariant(geometry3) ]);

  auto result = createRings(feature);

  Position[][] expected1 = [[[10, 10], [51, 51]]].toPositions!double;
  Position[][] expected2 = [[[5, 5], [1, 1], [2, 2], [0, 0], [5, 5]]].toPositions!double;
  result[0].geometry.get!Polygon.coordinates.should.equal(expected1);
  result[1].geometry.get!Polygon.coordinates.should.equal(expected2);
}


/// It should work with a osm postal code
unittest {
  auto feature = Feature();
  feature.geometry = `{
		"type": "GeometryCollection",
		"geometries": [
			{ "type": "LineString", "coordinates": [[ 13.3819342, 52.3972233], [ 13.382036, 52.39796550000001 ]]},
			{ "type": "LineString", "coordinates": [[ 13.381988, 52.3975992], [ 13.3819342, 52.3972233 ]]},
			{ "type": "LineString", "coordinates": [[ 13.3614067, 52.3976647], [ 13.381988, 52.3975992 ]]},
			{ "type": "LineString", "coordinates": [[ 13.3614067, 52.3976647], [ 13.3703524, 52.3884274 ]]},
			{ "type": "LineString", "coordinates": [[ 13.3703524, 52.3884274], [ 13.3821666, 52.3883085 ]]},
			{ "type": "LineString", "coordinates": [[ 13.3821666, 52.3883085 ], [ 13.3884283, 52.3778616 ]]},
			{ "type": "LineString", "coordinates": [[ 13.3884283, 52.3778616 ], [ 13.4122347, 52.3768129 ]]},
			{ "type": "LineString", "coordinates": [[ 13.4122347, 52.3768129 ] ,[ 13.4097641, 52.3873995 ]]},
			{ "type": "LineString", "coordinates": [[ 13.3895319, 52.39867810000001 ], [ 13.4097641, 52.3873995 ]]},
			{ "type": "LineString", "coordinates": [[ 13.3887767, 52.4004879 ], [ 13.3895319, 52.39867810000001 ]]},
			{ "type": "LineString", "coordinates": [[ 13.3845333, 52.3985047 ], [ 13.3887767, 52.4004879 ]]},
			{ "type": "LineString", "coordinates": [[ 13.3845333, 52.3985047 ], [ 13.382036, 52.39796550000001 ]]}
		]
	}`.parseJsonString.toGeometryVariant;

  auto result = createRings(feature);

  Position[][] expected = [[
          [ 13.3845333, 52.3985047 ],
					[ 13.3887767,52.4004879 ],
					[ 13.3895319, 52.39867810000001 ],
					[ 13.4097641, 52.3873995 ],
					[ 13.4122347, 52.3768129 ],
					[ 13.3884283, 52.3778616 ],
					[ 13.3821666, 52.3883085 ],
					[ 13.3703524, 52.3884274 ],
					[ 13.3614067, 52.3976647 ],
					[ 13.381988, 52.3975992 ],
					[ 13.3819342, 52.3972233 ],
					[ 13.382036, 52.39796550000001 ],
					[ 13.3845333, 52.3985047 ]
  ]].toPositions!double;

  result[0].geometry.get!Polygon.coordinates.should.equal(expected);
}

/// It should join a multi line string that has common coordinates
unittest {
  auto point = Feature();
  point.geometry = MultiLineString([
    [[5, 5], [1, 1]],
    [[1, 1], [2, 2], [0, 0]],
    [[0, 0], [5, 5]],
  ].toPositions!double);
  auto result = createRings(point);

  Position[][] expected = [[[5, 5], [1, 1], [2, 2], [0, 0], [5, 5]]].toPositions!double;
  result[0].geometry.get!Polygon.coordinates.should.equal(expected);
}

/// It should copy the properties
unittest {
  auto point = Feature();
  point.properties["key1"] = "value1";
  point.properties["key2"] = "value2";

  point.geometry = MultiLineString([
    [[5, 5], [1, 1]],
    [[1, 1], [2, 2], [0, 0]],
    [[0, 0], [5, 5]],
  ].toPositions!double);

  auto result = createRings(point);
  result[0].properties["key1"].to!string.should.equal("value1");
  result[0].properties["key2"].to!string.should.equal("value2");
}

/// It should join a multi line string that has common coordinates in reverse order
unittest {
  auto point = Feature();
  point.geometry = MultiLineString([
    [[1, 1], [2, 2], [5, 5]],
    [[0, 0], [1, 1]],
    [[5, 5], [0, 0]]
  ].toPositions!double);

  auto result = createRings(point);

  Position[][] expected = [[[0, 0], [1, 1], [2, 2], [5, 5], [0, 0]]].toPositions!double;
  result[0].geometry.get!Polygon.coordinates.should.equal(expected);
}

/// It should join a multi line with 6 segments
unittest {
  auto point = Feature();
  point.geometry = MultiLineString([
    [[1, 1], [2, 2]],
    [[3, 3], [4, 4]],
    [[2, 2], [3, 3]],
    [[5, 5], [6, 6]],
    [[4, 4], [5, 5]],
    [[6, 6], [1, 1]]
  ].toPositions!double);

  auto result = createRings(point);

  Position[][] expected = [[[5, 5], [6, 6], [1, 1], [2, 2], [3, 3], [4, 4], [5, 5]]].toPositions!double;
  result[0].geometry.get!Polygon.coordinates.should.equal(expected);
}

/// It should extract 2 polygons
unittest {
  auto point = Feature();
  point.geometry = MultiLineString([
    [[1, 1], [2, 2]],
    [[2, 2], [1, 1]],
    [[2, 2], [3, 3]],
    [[3, 3], [4, 4]],
    [[4, 4], [5, 5]],
    [[5, 5], [2, 2]]
  ].toPositions!double);

  auto result = createRings(point);

  Position[][] expected1 = [[[1, 1], [2, 2], [1, 1]]].toPositions!double;
  Position[][] expected2 = [[[2, 2], [3, 3], [4, 4], [5, 5], [2, 2]]].toPositions!double;
  result[0].geometry.get!Polygon.coordinates.should.equal(expected1);
  result[1].geometry.get!Polygon.coordinates.should.equal(expected2);
}

/// It should create new line string features with the unclosed coordinates
unittest {
  auto point = Feature();
  point.geometry = MultiLineString([
    [[1, 1], [2, 2], [5, 5]],
    [[0, 0], [10, 10]],
    [[5, 5], [1, 1]]
  ].toPositions!double);

  auto result = createRings(point);

  Position[][] expected1 = [[[1, 1], [2, 2], [5, 5], [1, 1]]].toPositions!double;
  Position[] expected2 = [[0, 0], [10, 10]].toPositions!double;

  result[0].geometry.get!Polygon.coordinates.should.equal(expected1);
  result[1].geometry.get!LineString.coordinates.should.equal(expected2);
}

enum LineLink {
  no,
  before,
  after,
  beforeReverse,
  afterReverse
}

/// Check if line 1 is linked with line two
LineLink isLinkedWith(const ref Position[] line1, const ref Position[] line2) {
  if(line1.length == 0 || line2.length == 0) {
    return LineLink.no;
  }

  auto lastPoint1 = line1[line1.length - 1];
  auto lastPoint2 = line2[line2.length - 1];
  auto firstPoint1 = line1[0];
  auto firstPoint2 = line2[0];

  if(distance(lastPoint1, firstPoint2) == 0) {
    return LineLink.after;
  }

  if (distance(lastPoint1, lastPoint2) == 0) {
    return LineLink.afterReverse;
  }

  if(distance(lastPoint2, firstPoint1) == 0) {
    return LineLink.before;
  }

  if(distance(firstPoint2, firstPoint1) == 0) {
    return LineLink.beforeReverse;
  }

  return LineLink.no;
}

/// It should find two unlinked lines
unittest {
  auto line1 = [[0.,0.], [1.,2.]].toPositions;
  auto line2 = [[2.,2.], [3.,2.]].toPositions;

  line1.isLinkedWith(line2).should.equal(LineLink.no);
}

/// It should find two linked lines when the last point is the same as
/// the first point from the second line
unittest {
  auto line1 = [[0.,0.], [1.,2.]].toPositions;
  auto line2 = [[1.,2.], [3.,2.]].toPositions;

  line1.isLinkedWith(line2).should.equal(LineLink.after);
}

/// It should find two linked lines when the first point is the same as
/// the last point from the second line
unittest {
  auto line1 = [[0.,0.], [1.,2.]].toPositions;
  auto line2 = [[2.,2.], [0.,0.]].toPositions;

  line1.isLinkedWith(line2).should.equal(LineLink.before);
}

/// It should find two linked lines when the first point is the same for both
unittest {
  auto line1 = [[0.,0.], [1.,2.]].toPositions;
  auto line2 = [[0.,0.], [2.,2.]].toPositions;

  line1.isLinkedWith(line2).should.equal(LineLink.beforeReverse);
}

/// It should find two linked lines when the last point is the same for both
unittest {
  auto line1 = [[0.,0.], [1.,2.]].toPositions;
  auto line2 = [[3.,2.], [1.,2.]].toPositions;

  line1.isLinkedWith(line2).should.equal(LineLink.afterReverse);
}

/// Find duplicated points
Point[] getDuplicatedPoints(Line: Point[], Point : U[], U)(Line line, U minDistance) {
  Point[] result;

  foreach (index; 0..line.length) {
    auto point = line[index];
    auto lastIndex = index == 0 ? line.length-1 : line.length;


    foreach (secondIndex; index+1..lastIndex) {
      auto secondPoint = line[secondIndex];

      if(point.distance(secondPoint) <= minDistance) {
        result ~= point;
      }
    }
  }

  return result;
}

/// It returns an empty list when all points are unique
unittest {
  double[][] line = [[1, 2], [3, 4], [5, 6]];

  line.getDuplicatedPoints(1.).length.should.equal(0);
}

/// It returns the duplicated point when there is one
unittest {
  double[][] line = [[1., 2.], [1., 2.], [5., 6.]];

  line.getDuplicatedPoints(1.).to!string.should.equal("[[1, 2]]");
}

/// It does not match the first and last point as duplicates
unittest {
  double[][] line = [[1., 2.], [5., 6.], [1., 2.]];

  line.getDuplicatedPoints(1.).length.should.equal(0);
}

/// It returns the duplicated points when there are 2
unittest {
  double[][] line = [[1., 2.], [1., 2.], [5., 6.], [5., 6.]];

  line.getDuplicatedPoints(1.).to!string.should.equal("[[1, 2], [5, 6]]");
}

/// ditto
Point[] getDuplicatedPoints(Polygon: Line[], Line: Point[], Point : U[], U)(Polygon polygon, U minDistance) {
  Point[] result;

  foreach (component; polygon) {
    result ~= component.getDuplicatedPoints(minDistance);
  }

  return result;
}

/// It returns an empty list when all points are unique
unittest {
  double[][][] polygon = [[[1, 2], [3, 4], [5, 6]]];

  polygon.getDuplicatedPoints(1.).length.should.equal(0);
}

/// It returns the duplicated point when there is one
unittest {
  double[][][] polygon = [[[1., 2.], [1., 2.], [5., 6.]]];

  polygon.getDuplicatedPoints(1.).to!string.should.equal("[[1, 2]]");
}

/// ditto
Point[] getDuplicatedPoints(MultiPolygon: Polygon[], Polygon: Line[], Line: Point[], Point : U[], U)(MultiPolygon multiPolygon, U minDistance) {
  Point[] result;

  foreach (component; multiPolygon) {
    result ~= component.getDuplicatedPoints(minDistance);
  }

  return result;
}

/// It returns an empty list when all points are unique
unittest {
  double[][][][] polygon = [[[[1, 2], [3, 4], [5, 6]]]];

  polygon.getDuplicatedPoints(1.).length.should.equal(0);
}

/// It returns the duplicated point when there is one
unittest {
  double[][][][] polygon = [[[[1., 2.], [1., 2.], [5., 6.]]]];

  polygon.getDuplicatedPoints(1.).to!string.should.equal("[[1, 2]]");
}

///
double[] reduceEnd(Point, T)(Point p1, Point p2, T r) {
  auto dx = p2[0] - p1[0];
  auto dy = p2[1] - p1[1];
  auto mag = hypot(dx, dy);

  return [ p2[0] - r * dx / mag, p2[1] - r * dy / mag ];
}

///
void reduceEndInPlace(Point, T)(ref Point p1, ref Point p2, ref T r) {
  auto dx = p2[0] - p1[0];
  auto dy = p2[1] - p1[1];
  auto mag = hypot(dx, dy);

  p2[0] = p2[0] - r * dx / mag;
  p2[1] = p2[1] - r * dy / mag;
}

///
double[] reduceStart(Point, T)(Point p1, Point p2, T r) {
  auto dx = p2.x - p1.x;
  auto dy = p2.y - p1.y;
  auto mag = hypot(dx, dy);

  return [ p1.x + r * dx / mag, p1.y + r * dy / mag ];
}

double[][] reduceEqually(Point, T)(Point p1, Point p2, T r) {
  auto r2 = r / 2;
  auto dx = p2.x - p1.x;
  auto dy = p2.y - p1.y;
  auto mag = hypot(dx, dy);

  return [[
    p1.x + r2 * dx / mag,
    p1.y + r2 * dy / mag
  ], [
    p2.x - r2 * dx / mag,
    p2.y - r2 * dy / mag
  ]];
}