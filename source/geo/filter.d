/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module geo.filter;

import geo.feature;

import vibe.data.json;

import std.algorithm;
import std.array;
import std.conv;
import std.exception;
import std.math;
import std.traits;

version(unittest) import fluent.asserts;

Unqual!R deserialize(R)(Json value) {
  Unqual!R defaultValue;

  if(value.type == Json.Type.null_ || value.type == Json.Type.undefined) {
    return defaultValue;
  }

  static if(is(Unqual!R == Json)) {
    return value;
  } else static if(__traits(compiles, value.to!(Unqual!R))) {
    return value.to!(Unqual!R);
  } else {
    return value.deserializeJson!(Unqual!R);
  }
}

Unqual!R deserialize(R)(string value) {
  return deserialize!R(Json(value));
}

bool isOperation(const Json op) {
  static immutable ops = ["get", "literal", "format", "case", "step", "coalesce"];

  if(op.type == Json.Type.array) {
    return true;
  }

  return ops.canFind(op.to!string);
}

///
R resolveValue(R, T)(Json value, inout ref T[string] attributes) {
  if(value.type == Json.Type.array && value.length > 0 && value[0].type == Json.Type.string) {
    string op = value[0].to!string;
    Json[] rest = value[1..$];

    if(op in attributes) {
      string newValue = attributes[op].to!string;
      return newValue.deserialize!R;
    }

    if(op == "get") {
      const key = rest[0].to!string;

      if(key !in attributes) {
        R default_;
        return default_;
      }

      return attributes[key].deserialize!R;
    }

    if(op == "literal") {
      return rest[0].deserialize!R;
    }

    if(op == "format") {
      R formatResult;

      if(rest.length == 0) {
        return formatResult;
      }

      return rest[0].resolveValue!string(attributes).deserialize!R;
    }

    if(op == "case") {
      auto end = rest.length/2*2;

      for(size_t i = 0; i<end; i+=2) {
        auto condition = rest[i];
        string operation = condition[0].to!string;

        auto matcher = matchFactory(operation, condition[1..$]);

        if(matcher.match(attributes)) {
          return resolveValue!(R, T)(rest[i+1], attributes);
        }

        auto result = rest[i+1];
      }

      return resolveValue!(R, T)(rest[rest.length - 1], attributes);
    }

    if(op == "step") {
      double currentValue = resolveValue!double(rest[0], attributes);
      auto result = rest[1].resolveValue!string(attributes);
      double stop = rest[2].resolveValue!double(attributes);

      if(currentValue < stop) {
        return result.deserialize!R;
      }

      result = rest[3].resolveValue!string(attributes);
      size_t index = 2;

      if(rest.length <= index) {
        return result.deserialize!R;
      }

      do {
        double nextStop = rest[index].resolveValue!double(attributes);
        auto nextResult = rest[index + 1].resolveValue!string(attributes);

        if(currentValue >= stop && currentValue < nextStop) {
          return to!R(result);
        }

        result = nextResult;
        stop = nextStop;

        index+=2;
      } while(index < rest.length);

      return result.deserialize!R;
    }

    if(op == "coalesce") {
      foreach(Json currentRest; rest) {
        try {
          auto tmpResult = currentRest.resolveValue!string(attributes);

          if(tmpResult == "") {
            continue;
          }

          return tmpResult.deserialize!R;
        } catch(Exception e) {}
      }

      R _defaultValue;
      return _defaultValue;
    }

    static if(isNumeric!R) {
      if(op == "interpolate") {
        double currentValue = resolveValue!double(rest[1], attributes);
        double stop = rest[2].resolveValue!double(attributes);
        double result = rest[3].resolveValue!double(attributes);
        size_t index = 4;

        if(currentValue <= stop) {
          return result.to!R;
        }

        do {
          double nextStop = rest[index].resolveValue!double(attributes);
          double nextResult = rest[index + 1].resolveValue!double(attributes);

          if(currentValue >= stop && currentValue < nextStop) {
            double t = (currentValue - stop) / (nextStop - stop);

            return to!R(result + t * (nextResult - result));
          }

          result = nextResult;
          stop = nextStop;

          index+=2;
        } while(index < rest.length);

        return result.to!R;
      }

      if(op == "+") {
        return resolveSum(rest, attributes).to!R;
      }
    }

    throw new Exception("Unimplemented operation: " ~ value.to!string ~ " attributes:" ~ attributes.to!string);
  }

  auto key = value.to!string;

  if(key in attributes) {
    return attributes[key].deserialize!R;
  }

  return value.deserialize!R;
}

/// it resolves a format value without styles
unittest {
  auto condition = `[ "format",
    ["get", "name:en"]
  ]`.parseJsonString;

  string[string] attributes;
  resolveValue!string(condition, attributes).should.equal("");

  attributes["name:en"] = "test";
  resolveValue!string(condition, attributes).should.equal("test");
}

/// it resolves a format value with coalesce
unittest {
  auto condition = `[ "format",
    [ "coalesce", ["get", "name:en"], ["get", "name:fr"] ]
  ]`.parseJsonString;

  string[string] attributes;
  resolveValue!string(condition, attributes).should.equal("");

  attributes["name:fr"] = "test";
  resolveValue!string(condition, attributes).should.equal("test");
}

/// it can handle an interpoalte operation
unittest {
  auto condition = `[ "interpolate",
    [ "linear" ], [ "zoom" ],
    5, 1,
    7, 0
  ]`.parseJsonString;

  string[string] attributes;
  attributes["zoom"] = "0";
  resolveValue!double(condition, attributes).should.equal(1);

  attributes["zoom"] = "5";
  resolveValue!double(condition, attributes).should.equal(1);

  attributes["zoom"] = "6";
  resolveValue!double(condition, attributes).should.equal(0.5);

  attributes["zoom"] = "7";
  resolveValue!double(condition, attributes).should.equal(0);
}

/// it can handle a step operation
unittest {
  auto condition = `[ "step",
    [ "zoom" ],
    2,
    5, 1,
    7, 0
  ]`.parseJsonString;

  string[string] attributes;
  attributes["zoom"] = "0";
  resolveValue!double(condition, attributes).should.equal(2);

  attributes["zoom"] = "5";
  resolveValue!double(condition, attributes).should.equal(1);

  attributes["zoom"] = "6";
  resolveValue!double(condition, attributes).should.equal(1);

  attributes["zoom"] = "7";
  resolveValue!double(condition, attributes).should.equal(0);

  attributes["zoom"] = "8";
  resolveValue!double(condition, attributes).should.equal(0);
}

/// it can resolve an operation
unittest {
  string[string] attributes;
  attributes["min_zoom"] = "2";

  auto value = resolveValue!double(`["+", 1, ["get","min_zoom"] ]`.parseJsonString, attributes);

  value.should.equal(3);
}

/// it can handle a case operation
unittest {
  auto condition = `["case",
    [ "==", [ "get", "script" ], "Devanagari" ],
      [ "literal", [ "Noto Sans Devanagari Regular v1" ] ],
    [ "literal", [ "Noto Sans Regular" ] ]
  ]`.parseJsonString;

  string[string] attributes;
  resolveValue!(string[])(condition, attributes).should.equal([ "Noto Sans Regular" ]);

  attributes["script"] = "Devanagari";
  resolveValue!(string[])(condition, attributes).should.equal([ "Noto Sans Devanagari Regular v1" ]);
}

/// it can handle a case operation with "in"
unittest {
  auto condition = `["case",
    [ "in", [ "get", "kind" ],
    [ "literal", [ "national_park", "park", "cemetery", "protected_area", "nature_reserve", "forest", "golf_course" ] ]],
    "#9cd3b4",
    "red"
  ]`.parseJsonString;

  string[string] attributes;
  resolveValue!(string)(condition, attributes).should.equal("red");

  attributes["kind"] = "park";
  resolveValue!(string)(condition, attributes).should.equal("#9cd3b4");
}

double resolveSum(T)(Json[] params, const ref T[string] attributes) {
  double result = 0;

  foreach(value; params) {
    auto tmp = resolveValue!double(value, attributes);

    if(isNaN(tmp)) {
      continue;
    }

    result += tmp;
  }

  return result;
}

/// it can add 2 numeric values
unittest {
  string[string] params;
  resolveSum([Json(1), Json(2)], params).should.equal(3);
}

/// it can add an attribute to a value
unittest {
  string[string] params;
  params["min_zoom"] = "2";

  resolveSum([Json(1), ["get","min_zoom"].serializeToJson], params).should.equal(3);
}

///
interface IMatcher {
  const {
    bool match(const string[string]);
    bool match(const Json[string]);
  }
}

///
class AllMatch : IMatcher {
  private IMatcher[] matchers;

  this(Json[] params) {
    foreach(item; params) {
      auto params2 = cast(Json[]) cast() item;

      const op = params2[0].to!string;
      this.matchers ~= matchFactory(op, params2[1..$]);
    }
  }

  bool match(const string[string] properties) const {
    return matchT!string(properties);
  }

  bool match(const Json[string] properties) const {
    return matchT!Json(properties);
  }

  bool matchT(T)(const T[string] properties) const {
    foreach(matcher; matchers) {
      if(!matcher.match(properties)) {
        return false;
      }
    }

    return true;
  }
}


///
class AnyMatch : IMatcher {
  private IMatcher[] matchers;

  this(Json[] params) {
    foreach(item; params) {
      auto params2 = cast(Json[]) cast() item;

      const op = params2[0].to!string;
      this.matchers ~= matchFactory(op, params2[1..$]);
    }
  }

  bool match(const string[string] properties) const {
    return matchT!string(properties);
  }

  bool match(const Json[string] properties) const {
    return matchT!Json(properties);
  }

  bool matchT(T)(const T[string] properties) const {
    foreach(matcher; matchers) {
      if(matcher.match(properties)) {
        return true;
      }
    }

    return false;
  }
}

///
class EqualMatch(string op) : IMatcher {
  private const Json key;
  private const Json value;

  this(Json[] params) {
    this.key = params[0];
    this.value = params[1];
  }

  bool match(const string[string] properties) const {
    return matchT!string(properties);
  }

  bool match(const Json[string] properties) const {
    return matchT!Json(properties);
  }

  bool matchT(T)(const T[string] properties) const {
    mixin("return resolveValue!Json(key, properties) " ~ op ~ " resolveValue!Json(value, properties);");
  }
}

///
class MathMatch(string op) : IMatcher {
  private const string key;
  private const Json value;

  this(Json[] params) {
    this.key = params[0].to!string;
    this.value = params[1];
  }

  bool match(const string[string] properties) const {
    return matchT!string(properties);
  }

  bool match(const Json[string] properties) const {
    return matchT!Json(properties);
  }

  bool matchT(T)(const T[string] properties) const {
    if(key !in properties) {
      return false;
    }

    try {
      double prop = properties[key].to!double;

      mixin("return prop " ~ op ~ " value.resolveValue!double(properties);");
    } catch(Exception e) {
      return false;
    }
  }
}

/// can match by > op
unittest {
  auto matcher = new MathMatch!">"([ Json("zoom"), `["+",["get","min_zoom"], 0]`.parseJsonString ]);

  string[string] properties;
  properties["zoom"] = "5";

  matcher.match(properties).should.equal(true);

  properties["min_zoom"] = "6";
  matcher.match(properties).should.equal(false);

  properties["min_zoom"] = "5";
  matcher.match(properties).should.equal(false);

  properties["min_zoom"] = "4";
  matcher.match(properties).should.equal(true);
}


///
class InMatch(bool negate) : IMatcher {
  private const Json key;
  private Json[] values;

  this(Json[] params) {
    this.key = params[0];
    this.values = params[1..$];

    if(this.values.length == 1 && this.values[0].type == Json.Type.array) {
      this.values = cast(Json[]) params[1];
    }
  }

  bool match(const string[string] properties) const {
    return matchT!string(properties);
  }

  bool match(const Json[string] properties) const {
    return matchT!Json(properties);
  }

  bool matchT(T)(const T[string] properties) const {
    bool result;
    auto resolvedKey = key.resolveValue!string(properties);
    string[] resolvedValues;

    if(values.length >= 1 && values[0].isOperation) {
      resolvedValues = values.serializeToJson.resolveValue!(string[])(properties);
    } else {
      resolvedValues = values.map!(a => a.to!string).array.dup;
    }

    static if(negate) {
      return !resolvedValues.canFind(resolvedKey);
    } else {
      return resolvedValues.canFind(resolvedKey);
    }
  }
}

///
class HasMatch(bool negate) : IMatcher {
  private const string key;

  this(Json[] params) {
    this.key = params[0].to!string;
  }

  bool match(const string[string] properties) const {
    return matchT!string(properties);
  }

  bool match(const Json[string] properties) const {
    return matchT!Json(properties);
  }

  bool matchT(T)(const T[string] properties) const {
    static if(negate) {
      if(key !in properties) {
        return false;
      }

      return true;
    } else {
      if(key !in properties) {
        return true;
      }

      return false;
    }
  }
}


///
class Match : IMatcher {
  private const Json key;
  private const Json values;
  private const Json success;
  private const Json fail;

  this(Json[] params) {
    this.key = params[0];
    this.values = params[1];
    this.success = params[2];
    this.fail = params[3];
  }

  bool match(const string[string] properties) const {
    return matchT!string(properties);
  }

  bool match(const Json[string] properties) const {
    return matchT!Json(properties);
  }

  bool matchT(T)(const T[string] properties) const {
    auto resolvedKey = resolveValue!string(key, properties);

    foreach(value; this.values) {
      if(value == resolvedKey) {
        return success.to!bool;
      }
    }

    return fail.to!bool;
  }
}


///
class NoOpMatch : IMatcher {

  this(Json[] params) {
  }

  bool match(const string[string] properties) const {
    return matchT!string(properties);
  }

  bool match(const Json[string] properties) const {
    return matchT!Json(properties);
  }

  bool matchT(T)(const T[string] properties) const {
    return false;
  }
}


///
class NotMatch : IMatcher {
  private const string key;
  private IMatcher matcher;

  this(Json[] params) {
    this.key = params[0].to!string;
    this.matcher = matchFactory(params[0][0].to!string, cast(Json[]) params[0][1..$]);
  }

  bool match(const string[string] properties) const {
    return matchT!string(properties);
  }

  bool match(const Json[string] properties) const {
    return matchT!Json(properties);
  }

  bool matchT(T)(const T[string] properties) const {
    return !matcher.match(properties);
  }
}

///
IMatcher matchFactory(const string operation, Json[] params) @trusted {
  switch(operation) {
    case "==":
      return new EqualMatch!"=="(params);

    case "!=":
      return new EqualMatch!"!="(params);

    case "<=":
      return new MathMatch!("<=")(params);

    case ">=":
      return new MathMatch!(">=")(params);

    case ">":
      return new MathMatch!(">")(params);

    case "<":
      return new MathMatch!("<")(params);

    case "all":
      return new AllMatch(params);

    case "any":
      return new AnyMatch(params);

    case "in":
      return new InMatch!false(params);

    case "!in":
      return new InMatch!true(params);

    case "has":
      return new HasMatch!true(params);

    case "!has":
      return new HasMatch!false(params);

    case "!":
      return new NotMatch(params);

    case "match":
      return new Match(params);

    case "step":
      return new NoOpMatch(params);

    default:
      enforce(false, "unknown filter operation `" ~ operation ~"`");
  }


  return null;
}

///
struct Filter {

  private {
    IMatcher matcher;
  }

  ///
  bool match(T)(const T[string] properties) const {
    if(this.matcher is null) {
      return true;
    }

    return this.matcher.match(properties);
  }

  ///
  Json toJson() const @safe {
    return Json();
  }

  ///
  static Filter fromJson(Json src) @safe {
    Filter result;

    result.matcher = matchFactory(src[0].to!string, cast(Json[]) src[1..$]);

    return result;
  }
}

/// Create a matcher
unittest {
  auto matchFilter = Json.emptyArray;
  matchFilter ~= Json("match");
  matchFilter.appendArrayElement(`[ "get", "class" ]`.parseJsonString);
  matchFilter.appendArrayElement(`[ "canal", "river", "stream"]`.parseJsonString);
  matchFilter ~= Json(true);
  matchFilter ~= Json(false);

  auto filter = Filter.fromJson(matchFilter);

  filter.match(["class": "canal"]).should.equal(true);
  filter.match(["class": "other"]).should.equal(false);
}

/// Create an equality style matcher
unittest {
  auto filter = Filter.fromJson(["==", "class", "pier"].serializeToJson);

  filter.match(["class": "pier"]).should.equal(true);
  filter.match(["class": "other"]).should.equal(false);
}

/// Create an inequality style matcher
unittest {
  auto filter = Filter.fromJson(["!=", "class", "pier"].serializeToJson);

  filter.match(["class": "other"]).should.equal(true);
  filter.match(["class": "pier"]).should.equal(false);
}

/// Create a has style matcher
unittest {
  auto filter = Filter.fromJson(["has", "class"].serializeToJson);

  filter.match(["class": "other"]).should.equal(true);
  filter.match(["other": "pier"]).should.equal(false);
}

/// Create a !has style matcher
unittest {
  auto filter = Filter.fromJson(["!has", "class"].serializeToJson);

  filter.match(["other": "pier"]).should.equal(true);
  filter.match(["class": "other"]).should.equal(false);
}

/// Create a less than or equal style matcher
unittest {
  auto filter = Filter.fromJson(["<=", "key", "2"].serializeToJson);

  filter.match(["key": "2"]).should.equal(true);
  filter.match(["key": "3"]).should.equal(false);
  filter.match(["other": "2"]).should.equal(false);
  filter.match(["key": "string"]).should.equal(false);
}

/// Create a greater than or equal style matcher
unittest {
  auto filter = Filter.fromJson([">=", "key", "2"].serializeToJson);

  filter.match(["key": "2"]).should.equal(true);
  filter.match(["key": "1"]).should.equal(false);
  filter.match(["other": "3"]).should.equal(false);
  filter.match(["key": "string"]).should.equal(false);
}

/// Create a greater than style matcher
unittest {
  auto filter = Filter.fromJson([">", "key", "2"].serializeToJson);

  filter.match(["key": "3"]).should.equal(true);
  filter.match(["key": "2"]).should.equal(false);
  filter.match(["other": "3"]).should.equal(false);
  filter.match(["key": "string"]).should.equal(false);
}

/// Create a less than style matcher
unittest {
  auto filter = Filter.fromJson(["<", "key", "2"].serializeToJson);

  filter.match(["key": "1"]).should.equal(true);
  filter.match(["key": "2"]).should.equal(false);
  filter.match(["other": "1"]).should.equal(false);
  filter.match(["key": "string"]).should.equal(false);
}

/// Create an in style matcher
unittest {
  auto filter = Filter.fromJson(["in", "class", "pier", "value"].serializeToJson);

  filter.match(["class": "pier"]).should.equal(true);
  filter.match(["class": "value"]).should.equal(true);
  filter.match(["class": "other"]).should.equal(false);
}

/// Create an !in style matcher
unittest {
  auto filter = Filter.fromJson(["!in", "class", "pier", "value"].serializeToJson);

  filter.match(["class": "pier"]).should.equal(false);
  filter.match(["class": "value"]).should.equal(false);
  filter.match(["class": "other"]).should.equal(true);
}

/// Create an all style matcher
unittest {
  auto filter = Filter.fromJson([Json("all"), ["==", "class", "pier"].serializeToJson, ["==", "other", "value"].serializeToJson].serializeToJson);

  filter.match(["class": "pier", "other": "value"]).should.equal(true);
  filter.match(["class": "pier"]).should.equal(false);
}

/// Create an any style matcher
unittest {
  auto filter = Filter.fromJson([Json("any"), ["==", "class", "pier"].serializeToJson, ["==", "other", "value"].serializeToJson].serializeToJson);

  filter.match(["class": "pier", "other": "value"]).should.equal(true);
  filter.match(["class": "pier"]).should.equal(true);
  filter.match(["other class": "pier"]).should.equal(false);
}

/// Match a Feature against the Filter
unittest {
  auto feature = `{"type":"Feature","properties":{"id":"267403","site":"stop_area",
    "naptan:StopAreaType":"GPBS","source":"naptan_import","name":"Oaktree Close","naptan:StopAreaCode":"210G896",
    "type":"site","naptan:verified":"no"},"geometry":{"type":"GeometryCollection","geometries":[]}}`
    .parseJsonString.deserializeJson!Feature;

  auto filter = Filter.fromJson(["==", "site", "stop_area"].serializeToJson);

  filter.match(feature.properties).should.equal(true);
}
