/**
  Copyright: © 2019-2023 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module geo.algo.intersection;

import geo.algorithm;

version(unittest) {
  import vibe.data.json;
  import std.file;
  import fluent.asserts;
  import std.conv;
}


/// Get the index of a segment that intersects another segment
long intersectedEdge(Point)(Point[] line, Point[] pointList) @safe pure {
  foreach(i; 1..pointList.length) {
    auto p1 = pointList[i-1];
    auto p2 = pointList[i];

    if(doIntersect(line, [p1, p2])) {
      return i-1;
    }
  }

  return -1;
}

/// intersectedEdge returns -1 when there is no intersection
unittest {
  expect(intersectedEdge([[0,0], [1,1]], [[10,10], [11,11]])).to.equal(-1);
}

/// intersectedEdge returns 0 when the first segment is intersected
unittest {
  expect(intersectedEdge([[0,0], [5,5]], [[2, 0], [2, 5]])).to.equal(0);
}

/// intersectedEdge returns 1 when the second segment is intersected
unittest {
  expect(intersectedEdge([[0,0], [5,5]], [[-1, -1], [2, 0], [2, 5]])).to.equal(1);
}