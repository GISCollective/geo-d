module geo.algo.unkinkPolygon;

version(unittest) {
  import fluent.asserts;
}


/**
 * Takes a kinked polygon and returns a feature collection of polygons that have no kinks.
 * Uses [simplepolygon](https://github.com/mclaeysb/simplepolygon) internally.
 */
Point[][][] unkinkPolygon(Point)(Point[][] polygon) {
  Point[][][] polygons;


  // simplepolygon(feature), function (poly) {
  //   features.push(polygon(poly.geometry.coordinates, feature.properties));
  // }

  return polygons;
}

/// example usage
unittest {
  auto poly = [[[0, 0], [2, 0], [0, 2], [2, 2], [0, 0]]];

  unkinkPolygon(poly).should.equal([[[0, 0]]]);
}