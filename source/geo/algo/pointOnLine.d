module geo.algo.pointOnLine;

import geo.geometries;
import std.math;

version(unittest) {
  import fluent.asserts;
}

///
enum LineBoundary {
  None,
  Start,
  End,
  Both
}

/// Returns true if a point is on a line. Accepts a optional parameter to ignore the start and end vertices of the linestring.
bool pointOnLine(Position)(Position pt, LineString line, bool ignoreEndVertices = true, double epsilon = 0) {
  auto ptCoords = pt.coordinates;
  auto lineCoords = line.coordinates;

  foreach(i; 0..lineCoords.length - 1) {
    auto ignoreBoundary = LineBoundary.None;

    if (ignoreEndVertices) {
      if (i == 0) {
        ignoreBoundary = LineBoundary.Start;
      }

      if (i == lineCoords.length - 2) {
        ignoreBoundary = LineBoundary.End;
      }

      if (i == 0 && i + 1 == lineCoords.length - 1) {
        ignoreBoundary = LineBoundary.Both;
      }
    }

    if (
      isPointOnLineSegment(
        lineCoords[i],
        lineCoords[i + 1],
        ptCoords,
        ignoreBoundary,
        epsilon
      )
    ) {
      return true;
    }
  }

  return false;
}

///
bool isPointOnLineSegment(Position)(Position lineSegmentStart, Position lineSegmentEnd, Position pt, LineBoundary excludeBoundary = LineBoundary.None, double epsilon = 0) {
  const x = pt[0];
  const y = pt[1];
  const x1 = lineSegmentStart[0];
  const y1 = lineSegmentStart[1];
  const x2 = lineSegmentEnd[0];
  const y2 = lineSegmentEnd[1];
  const dxc = pt[0] - x1;
  const dyc = pt[1] - y1;
  const dxl = x2 - x1;
  const dyl = y2 - y1;
  const cross = dxc * dyl - dyc * dxl;

  if (!isNaN(epsilon)) {
    if (abs(cross) > epsilon) {
      return false;
    }
  } else if (cross != 0) {
    return false;
  }

  if (excludeBoundary == LineBoundary.None) {
    if (abs(dxl) >= abs(dyl)) {
      return dxl > 0 ? x1 <= x && x <= x2 : x2 <= x && x <= x1;
    }
    return dyl > 0 ? y1 <= y && y <= y2 : y2 <= y && y <= y1;
  } else if (excludeBoundary == LineBoundary.Start) {
    if (abs(dxl) >= abs(dyl)) {
      return dxl > 0 ? x1 < x && x <= x2 : x2 <= x && x < x1;
    }
    return dyl > 0 ? y1 < y && y <= y2 : y2 <= y && y < y1;
  } else if (excludeBoundary == LineBoundary.End) {
    if (abs(dxl) >= abs(dyl)) {
      return dxl > 0 ? x1 <= x && x < x2 : x2 < x && x <= x1;
    }
    return dyl > 0 ? y1 <= y && y < y2 : y2 < y && y <= y1;
  } else if (excludeBoundary == LineBoundary.Both) {
    if (abs(dxl) >= abs(dyl)) {
      return dxl > 0 ? x1 < x && x < x2 : x2 < x && x < x1;
    }
    return dyl > 0 ? y1 < y && y < y2 : y2 < y && y < y1;
  }

  return false;
}

/// The true cases for pointOnLine
unittest {
  import std.file;
  import std.path;
  import std.stdio;
  import vibe.data.json;
  import geo.json;
  import geo.feature;

  foreach (string name; buildPath("test", "pointOnLine", "true").dirEntries("*.geojson", SpanMode.shallow)) {
    name.writeln(" <---");

    auto collection = name.readText.parseJsonString;

    auto options = collection["properties"];
    auto point = collection["features"][0]["geometry"].toBasicGeometryVariant.get!Point;
    auto line = collection["features"][1]["geometry"].toBasicGeometryVariant.get!LineString;

    bool ignoreEndVertices;
    double epsilon = 0;

    if(options.type == Json.Type.object && "ignoreEndVertices" in options) {
      ignoreEndVertices = options["ignoreEndVertices"].to!bool;
    }

    if(options.type == Json.Type.object && "epsilon" in options) {
      epsilon = options["epsilon"].to!double;
    }

    pointOnLine(point, line, ignoreEndVertices, epsilon).should.equal(true);
  }
}

/// The false cases for pointOnLine
unittest {
  import std.file;
  import std.path;
  import std.stdio;
  import vibe.data.json;
  import geo.json;
  import geo.feature;

  foreach (string name; buildPath("test", "pointOnLine", "false").dirEntries("*.geojson", SpanMode.shallow)) {
    name.writeln(" <---");

    auto collection = name.readText.parseJsonString;

    auto options = collection["properties"];
    auto point = collection["features"][0]["geometry"].toBasicGeometryVariant.get!Point;
    auto line = collection["features"][1]["geometry"].toBasicGeometryVariant.get!LineString;

    bool ignoreEndVertices;
    double epsilon = 0;

    if(options.type == Json.Type.object && "ignoreEndVertices" in options) {
      ignoreEndVertices = options["ignoreEndVertices"].to!bool;
    }

    if(options.type == Json.Type.object && "epsilon" in options) {
      epsilon = options["epsilon"].to!double;
    }

    pointOnLine(point, line, ignoreEndVertices, epsilon).should.equal(false);
  }
}