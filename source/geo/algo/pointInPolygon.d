/**
  Copyright: © 2019-2023 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module geo.algo.pointInPolygon;

import geo.geometries;
import std.math;
import std.traits;

version(unittest) {
  import fluent.asserts;
}

///
enum PointLocation {
  Outside,
  Inside,
  Boundary
}

/// Takes a Point and a Polygon orMultiPolygon and determines if the point resides inside the polygon. The polygon can be convex or concave. The function accounts for holes.
PointLocation pointInPolygon(Point)(Point point, Point[][] polygon) {
  alias Coordinate = Unqual!(typeof(point[0]));

  size_t k = 0;
  double f = 0;
  double u1 = 0;
  double v1 = 0;
  double u2 = 0;
  double v2 = 0;

  Unqual!Point currentP;
  Unqual!Point nextP;

  auto x = point[0];
  auto y = point[1];

  const numRings = polygon.length;

  foreach(i; 0..numRings) {
    auto ringLen = polygon[i].length - 1;
    auto ring = polygon[i];

    currentP = ring[0];

    if (currentP[0] != ring[ringLen][0] && currentP[1] != ring[ringLen][1]) {
        throw new Exception("First and last coordinates in a ring must be the same");
    }

    u1 = currentP[0] - x;
    v1 = currentP[1] - y;

    foreach(ii; 0..ringLen) {
      nextP = ring[ii + 1];

      v2 = nextP[1] - y;

      if ((v1 < 0 && v2 < 0) || (v1 > 0 && v2 > 0)) {
          currentP = nextP;
          v1 = v2;
          u1 = currentP[0] - x;
          continue;
      }

      u2 = nextP[0] - point[0];

      if (v2 > 0 && v1 <= 0) {
        f = (u1 * v2) - (u2 * v1);
        if (f > 0) k = k + 1;
        else if (f == 0) return PointLocation.Boundary;
      } else if (v1 > 0 && v2 <= 0) {
        f = (u1 * v2) - (u2 * v1);
        if (f < 0) k = k + 1;
        else if (f == 0) return PointLocation.Boundary;
      } else if (v2 == 0 && v1 < 0) {
        f = (u1 * v2) - (u2 * v1);
        if (f == 0) return PointLocation.Boundary;
      } else if (v1 == 0 && v2 < 0) {
        f = u1 * v2 - u2 * v1;
        if (f == 0) return PointLocation.Boundary;
      } else if (v1 == 0 && v2 == 0) {
        if (u2 <= 0 && u1 >= 0) {
          return PointLocation.Boundary;
        } else if (u1 <= 0 && u2 >= 0) {
          return PointLocation.Boundary;
        }
      }

      currentP = nextP;
      v1 = v2;
      u1 = u2;
    }
  }

  if (k % 2 == 0) {
    return PointLocation.Outside;
  }

  return PointLocation.Inside;
}

/// ditto
PointLocation pointInPolygon(Point)(Point point, Point[][][] multiPolygon) {
  foreach (polygon; multiPolygon) {
    auto result = pointInPolygon(point, polygon);

    if(result != PointLocation.Outside) {
      return result;
    }
  }

  return PointLocation.Outside;
}

/// ditto
bool pointInPolygon(Point)(Point point, Point[][] polygon, bool isBoundaryIncluded) {
  auto result = pointInPolygon(point, polygon);

  return isBoundaryIncluded
    ? result != PointLocation.Outside
    : result == PointLocation.Inside;
}

/// ditto
bool pointInPolygon(Point)(Point point, Point[][][] multiPolygon, bool isBoundaryIncluded) {
  auto result = pointInPolygon(point, multiPolygon);

  return isBoundaryIncluded
    ? result != PointLocation.Outside
    : result == PointLocation.Inside;
}

/// ditto
PointLocation pointInPolygon(Point point, Polygon polygon) {
  return pointInPolygon(point.coordinates, polygon.coordinates);
}

/// ditto
bool pointInPolygon(Point point, Polygon polygon, bool isBoundaryIncluded) {
  return pointInPolygon(point.coordinates, polygon.coordinates, isBoundaryIncluded);
}

/// ditto
PointLocation pointInPolygon(Point point, MultiPolygon multiPolygon) {
  return pointInPolygon(point.coordinates, multiPolygon.coordinates);
}

/// ditto
bool pointInPolygon(Point point, MultiPolygon multiPolygon, bool isBoundaryIncluded) {
  return pointInPolygon(point.coordinates, multiPolygon.coordinates, isBoundaryIncluded);
}


/// example usage
unittest {
  auto pt = [-77., 44.];
  auto poly = [[
    [-81., 41.],
    [-81., 47.],
    [-72., 47.],
    [-72., 41.],
    [-81., 41.]
  ]];

  pointInPolygon(pt, poly, true).should.equal(true);
  pointInPolygon(pt, poly).should.equal(PointLocation.Inside);
}

/// tests for multipolygon with hole
unittest {
  import std.file;
  import std.path;
  import std.stdio;
  import vibe.data.json;
  import geo.json;

  auto ptInHole = Point(PositionT!double(-86.69208526611328, 36.20373274711739));
  auto ptInPoly = Point(PositionT!double(-86.72229766845702, 36.20258997094334));
  auto ptInPoly2 = Point(PositionT!double(-86.75079345703125, 36.18527313913089));
  auto ptOutsidePoly = Point(PositionT!double(-86.75302505493164, 36.23015046460186));

  auto multiPolygon = buildPath("test", "pointInPolygon", "multipoly-with-hole.geojson")
    .readText
    .parseJsonString
    .toBasicGeometryVariant
    .get!MultiPolygon;

  pointInPolygon(ptInHole, multiPolygon).should.equal(PointLocation.Outside);

//   t.true(booleanPointInPolygon(ptInPoly, multiPolyHole));
//   t.true(booleanPointInPolygon(ptInPoly2, multiPolyHole));
//   t.true(booleanPointInPolygon(ptInPoly, multiPolyHole));
//   t.false(booleanPointInPolygon(ptOutsidePoly, multiPolyHole));
}

/// boundary tests for polygons
unittest {
  auto poly = [
    [
      [10, 10],
      [30, 20],
      [50, 10],
      [30, 0],
      [10, 10],
    ],
  ];

  pointInPolygon([10, 10], poly).should.equal(PointLocation.Boundary);
  pointInPolygon([10, 10], poly, false).should.equal(false);
  pointInPolygon([10, 10], poly, true).should.equal(true);
  pointInPolygon([30, 20], poly).should.equal(PointLocation.Boundary);
  pointInPolygon([50, 10], poly).should.equal(PointLocation.Boundary);
  pointInPolygon([30, 10], poly).should.equal(PointLocation.Inside);
  pointInPolygon([0, 10], poly).should.equal(PointLocation.Outside);
  pointInPolygon([60, 10], poly).should.equal(PointLocation.Outside);
  pointInPolygon([30, -10], poly).should.equal(PointLocation.Outside);
  pointInPolygon([30, 30], poly).should.equal(PointLocation.Outside);
}

/// inside giving false positive
unittest {
  auto pt = Point(PositionT!double(-9.9964077, 53.8040989));
  auto poly = Polygon([
    [
      PositionT!double(5.080336744095521, 67.89398938540765),
      PositionT!double(0.35070899909145403, 69.32470003971179),
      PositionT!double(-24.453622256504122, 41.146696777884564),
      PositionT!double(-21.6445524714804, 40.43225902006474),
      PositionT!double(5.080336744095521, 67.89398938540765),
    ],
  ]);

  pointInPolygon(pt, poly, true).should.equal(true);
  pointInPolygon(pt, poly).should.equal(PointLocation.Inside);
}
