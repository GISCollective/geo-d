/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module geo.algo.cut;

import geo.conv;
import geo.epsg;
import geo.geometries;
import geo.algorithm;
import geo.algo.intersection;
import geo.xyz;
import geo.json;
import std.conv;
import std.math;
import std.algorithm.mutation;

version(unittest) {
  import vibe.data.json;
  import std.file;
  import fluent.asserts;
  import std.conv;
}

///
Point[] cutSide(Point, U)(Point[] points, ref const BBox!U box, Direction direction) {
  Point[] result;

  if(points.length == 0) {
    return [];
  }

  result.reserve(points.length);

  auto borders = box.line(direction);
  auto border = [Point(borders[0][0], borders[0][1]), Point(borders[1][0], borders[1][1])];
  auto lastPoint = points[points.length - 1];
  auto isClosed = points[0] == lastPoint;

  import std.stdio;

  foreach(i; 0..points.length - 1) {
    auto prevPoint = points[i];
    auto point = points[i+1];
    auto prevDir = box.relativeDirection(prevPoint);
    auto dir = box.relativeDirection(point);

    if(!(prevDir & direction)) {
      result ~= prevPoint;
    }

    if(prevDir & direction && !(dir & direction)) {
      result ~= intersectionNoCheck(border, [prevPoint, point], true);
    }

    if(!(prevDir & direction) && (dir & direction)) {
      result ~= intersectionNoCheck(border, [prevPoint, point], true);
    }
  }

  if(box.relativeDirection(lastPoint) != direction) {
    result ~= lastPoint;
  }

  if(isClosed && result.length > 0 && result[0] != result[result.length - 1]) {
    result ~= result[0];
  }

  return result;
}

/// returns an empty list when there is no point provided
unittest {
  EPSG3857[] triangle = [];
  auto box = BBox!long(3, 7, 10, 0);

  auto result = triangle.cutSide(box, Direction.Left);

  result.length.should.equal(0);
}

/// Clip the left side of triangle with a bbox
unittest {
  auto triangle = [EPSG3857(1,3), EPSG3857(6,9), EPSG3857(9,1), EPSG3857(1,3)];
  auto box = BBox!long(3, 7, 10, 0);

  auto result = triangle.cutSide(box, Direction.Left);

  result.toList.serializeToJson.should.equal([[3, 5], [6, 9], [9, 1], [3, 2], [3, 5]].serializeToJson);
}

/// Clip the right side of triangle with a bbox
unittest {
  auto triangle = [EPSG3857(1,3), EPSG3857(6,9), EPSG3857(9,1), EPSG3857(1,3)];
  auto box = BBox!long(3, 7, 10, 0);

  auto result = triangle.cutSide(box, Direction.Right);

  result.toList.serializeToJson.should.equal([[1, 3], [6, 9], [7, 6], [7, 1], [1, 3]].serializeToJson);
}

/// Clip the top side of triangle with a bbox
unittest {
  auto triangle = [EPSG3857(1,3), EPSG3857(6,9), EPSG3857(9,1), EPSG3857(1,3)];
  auto box = BBox!long(0, 10, 7, 2);

  auto result = triangle.cutSide(box, Direction.Top);

  result.toList.to!string.should.equal("[[1, 3], [4, 7], [6, 7], [9, 1], [1, 3]]");
}

/// Clip the bottom side of polygon with a bbox
unittest {
  auto triangle = [
    EPSG4326(-10.540145857658532, 51.77040940067584),
    EPSG4326(-10.5438, 51.7703),
    EPSG4326(-10.5435, 51.770),
    EPSG4326(-10.5435, 51.7708),
    EPSG4326(-10.5435, 51.7714),
    EPSG4326(-10.5435, 51.7714),
    EPSG4326(-10.5434, 51.772),
    EPSG4326(-10.5428, 51.7723),
    EPSG4326(-10.5419, 51.7721),
    EPSG4326(-10.5408, 51.7717),
    EPSG4326(-10.5407, 51.7716),
    EPSG4326(-10.540145857658532, 51.77040940067584)
  ];
  auto box = BBox!double(-9.5512, -11.5295, 51.775, 51.7709);

  auto result = triangle.cutSide(box, Direction.Bottom);

  result.toList.to!string.should.equal("[[-10.5435, 51.7709], [-10.5435, 51.7714], [-10.5435, 51.7714], [-10.5434, 51.772], [-10.5428, 51.7723], [-10.5419, 51.7721], [-10.5408, 51.7717], [-10.5407, 51.7716], [-10.5404, 51.7709], [-10.5435, 51.7709]]");
}

/// Clip the left and top side of polygon with a bbox
unittest {
  auto triangle = [
    EPSG4326(-10.543023895316935, 51.77224390315885),
    EPSG4326(-10.543623156197896, 51.76928115431804),
    EPSG4326(-10.540529825090829, 51.76863306894248),
    EPSG4326(-10.537810934806998, 51.770683256158975),
    EPSG4326(-10.536241032544012, 51.771712868264245),
    EPSG4326(-10.53633322044169, 51.77436088438776),
    EPSG4326(-10.540104351744446, 51.77346265268568),
    EPSG4326(-10.540465061387408, 51.771544906936924),
    EPSG4326(-10.541402322193903, 51.772019977874464),
    EPSG4326(-10.54201377913671, 51.7720915227421),
    EPSG4326(-10.54206402583776, 51.77220389591028),
    EPSG4326(-10.541847468002857, 51.7724374190436),
    EPSG4326(-10.541850279648116, 51.772620699909766),
    EPSG4326(-10.543023895316935, 51.7722439031588)
  ];

  auto box = toBBox(16, 30849, 21714, 0.1);

  auto result = triangle.cutSide(box, Direction.Top);
  result = result.cutSide(box, Direction.Left);

  result.toList.to!string.should.equal("[[-10.5419, 51.7689], [-10.5405, 51.7686], [-10.5378, 51.7707], [-10.5362, 51.7717], [-10.5363, 51.7744], [-10.5401, 51.7735], [-10.5405, 51.7715], [-10.5414, 51.772], [-10.5419, 51.7721], [-10.5419, 51.7723], [-10.5418, 51.7724], [-10.5419, 51.7726], [-10.5419, 51.7726]]");
}

///
Point[] addCorners(Point, U)(U from, U to, ref const BBox!U box, byte isClockWise) {
  Point[] result;

  if(from == -1 || to == -1) {
    return [];
  }

  auto start = from;
  const end = to;

  int i;

  while(start != end && start >= 0) {
    i++;

    assert(i < 8, "ups");

    if(start == 0 && isClockWise) {
      result ~= EPSG3857(box.left, box.top);
    }

    if(start == 0 && !isClockWise) {
      result ~= EPSG3857(box.left, box.bottom);
    }

    if(start == 1 && isClockWise) {
      result ~= EPSG3857(box.right, box.top);
    }

    if(start == 1 && !isClockWise) {
      result ~= EPSG3857(box.left, box.top);
    }

    if(start == 2 && isClockWise) {
      result ~= EPSG3857(box.right, box.bottom);
    }

    if(start == 2 && !isClockWise) {
      result ~= EPSG3857(box.right, box.top);
    }

    if(start == 3 && isClockWise) {
      result ~= EPSG3857(box.left, box.bottom);
    }

    if(start == 3 && !isClockWise) {
      result ~= EPSG3857(box.right, box.bottom);
    }

    if(isClockWise) {
      start++;
    } else {
      start--;
    }

    if(start >= 4) {
      start = 0;
    }
    if(start == -1) {
      start = 3;
    }
  }

  return result;
}

/// addCorners returns an empty list when when from and to are the same edge
unittest {
  auto box = BBox!long(1,2,4,3);

  addCorners!EPSG3857(1, 1, box, true).should.equal([]);
  addCorners!EPSG3857(1, 1, box, false).should.equal([]);
}

/// addCorners returns a corner when when from and to are 1 corner away without jumping over origin
unittest {
  auto box = BBox!long(1,2,4,3);

  addCorners!EPSG3857(1, 2, box, true).should.equal([EPSG3857(2, 4)]);
  addCorners!EPSG3857(2, 1, box, false).should.equal([EPSG3857(2, 4)]);
}

/// addCorners returns a corner when when from and to are 1 corner away and jumbing over origin
unittest {
  auto box = BBox!long(1,2,4,3);

  addCorners!EPSG3857(3, 0, box, true).should.equal([EPSG3857(1, 3)]);
  addCorners!EPSG3857(0, 3, box, false).should.equal([EPSG3857(1, 3)]);
}

/// addCorners returns two corners when when from and to are paralel
unittest {
  auto box = BBox!long(1,2,4,3);

  addCorners!EPSG3857(0, 2, box, true).should.equal([EPSG3857(1, 4), EPSG3857(2, 4)]);
  addCorners!EPSG3857(2, 0, box, false).should.equal([EPSG3857(2, 4), EPSG3857(1, 4)]);
}


/// addCorners returns 3 corners when when from and to are 3 corners away
unittest {
  auto box = BBox!long(1,2,4,3);

  addCorners!EPSG3857(0, 3, box, true).should.equal([ EPSG3857(1, 4), EPSG3857(2, 4), EPSG3857(2, 3) ]);
  addCorners!EPSG3857(3, 0, box, false).should.equal([ EPSG3857(2, 3), EPSG3857(2, 4), EPSG3857(1, 4) ]);
}

EPSG3857[] addIntersectionPoints(EPSG3857[] points, ref const BBox!long box) {
  EPSG3857[] result = [points[0]];
  auto borders = box.allLines.toEPSG3857;

  /// finding all intersections with the borders
  foreach(i; 1..points.length) {
    auto prevPoint = points[i-1];
    auto point = points[i];

    EPSG3857[] intersections;

    foreach(borderIndex; 0..borders.length) {
      auto border = borders[borderIndex];

      if(onSameLine(border[0], point, border[1])) {
        break;
      }

      auto currentLine = [prevPoint, point];
      auto tmp = intersection(border, currentLine, true);

      if(tmp.length != 1) {
        continue;
      }

      if(result[result.length -1] != tmp[0] && prevPoint != tmp[0] && point != tmp[0]) {
        intersections ~= tmp[0];
      }
    }

    if(intersections.length == 2 && prevPoint.distance(intersections[0]) > prevPoint.distance(intersections[1])) {
      auto tmp = intersections[0];
      intersections[0] = intersections[1];
      intersections[1] = tmp;
    }

    if(intersections.length > 0) {
      result ~= intersections;
    }

    result ~= point;
  }

  return result;
}

///
long firstPointInside(Point)(Point[] points, ref const BBox!long box) {
  long pointIndex = -1;
  auto borders = box.allLines.to!Point;

  auto prevPoint = points[points.length - 1];

  foreach(i; 0..points.length) {
    auto point = points[i];

    auto isInside = box.contains(point[0], point[1]);

    if(isInside) {
      foreach(borderIndex; 0..borders.length) {
        auto border = borders[borderIndex];

        if(onSameLine(border[0], point, border[1])) {
          if(pointIndex == -1) {
            pointIndex = i;
          }

          isInside = false;
          break;
        }
      }
    }

    if(isInside) {
      pointIndex = i;
      break;
    }

    prevPoint = point;
  }

  return pointIndex;
}

///
Point[][] cutAndSplitWith(Point)(Point[] points, ref const BBox!long box) {
  Point[][] result;

  points = addIntersectionPoints(points, box);
  Point[] line;

  foreach(point; points) {
    auto isInside = box.contains(point[0], point[1]);

    if(isInside) {
      line ~= point;
    }

    if(!isInside && line.length) {
      result ~= line;
      line = [];
    }
  }

  if(line.length) {
    result ~= line;
  }

  return result;
}

///
Point[] cutWith(Point, U)(Point[] points, ref const BBox!U box, bool isClockWise) {
  auto pointsBox = BBox!U(points);

  if(box.fullyOutside(points)) {
    return [];
  }

  if(box.fullyInside(points)) {
    return box.toRing!Point;
  }

  import std.stdio;

  auto verticalBox =   BBox!U(pointsBox.left - box.dx, pointsBox.right + box.dx, box.top,                box.bottom);
  auto horizontalBox = BBox!U(box.left,                box.right,                pointsBox.top + box.dy, pointsBox.bottom - box.dy);

  Point[] clip = points.cutSide(verticalBox, Direction.Top);
  clip = clip.cutSide(verticalBox, Direction.Bottom);
  clip = clip.cutSide(horizontalBox, Direction.Left);
  clip = clip.cutSide(horizontalBox, Direction.Right);

  if(clip.length == 0) {
    return [];
  }

  if(clip[0] != clip[clip.length - 1]) {
    clip ~= clip[0];
  }

  return clip;
}

/// cutWith a triangle
unittest {
  auto points = [EPSG3857(6901808, 2273030), EPSG3857(7013127, 2391878), EPSG3857(7124447, 2391878), EPSG3857(6901808, 2273030)];
  auto box = BBox!long(6958337, 7052263, 2355991, 2262065);
  auto result = cutWith(points, box, true);

  expect(result.toEPSG4326.toList).to.equal([
    [62.5078, 20.5086],
    [62.698, 20.6987],
    [63.3516, 20.6987],
    [63.3516, 20.6765],
    [62.5078, 20.2545],
    [62.5078, 20.5086]]);
}

/// cutWith a small polygon with botom right corner outside
unittest {
  auto points = [EPSG3857(0, 0), EPSG3857(0, 4915), EPSG3857(2449, 4915), EPSG3857(2449, 3656), EPSG3857(2577, 2088), EPSG3857(2081, 98), EPSG3857(0, 0)];
  auto box = BBox!long(-4626, 1245, 908, 6778);
  auto result = cutWith(points, box, true);

  expect(result.toEPSG4326.toList).to.equal([
    [0, 0.0081567], [0, 0.0441522], [0.011184, 0.0441522], [0.011184, 0.0081567], [0, 0.0081567]
  ]);
}

/// cutWith a polygon with top left corner outside
unittest {
  auto points = [EPSG3857(1482674, 6897246), EPSG3857(1481804, 6890579), EPSG3857(1481514, 6885071), EPSG3857(1487988, 6873090), EPSG3857(1508182, 6872220), EPSG3857(1516202, 6882656), EPSG3857(1516395, 6896666), EPSG3857(1509148, 6906811), EPSG3857(1491467, 6907005), EPSG3857(1482674, 6897246)];
  auto box = BBox!long(1483245, 1530208, 6844854, 6891817);
  auto result = cutWith(points, box, true);

  expect(result.toEPSG4326.toList).to.equal([
    [13.3242, 52.4498], [13.3668, 52.4017], [13.5482, 52.397], [13.6203, 52.4541], [13.6214, 52.5042], [13.3242, 52.5042], [13.3242, 52.4498]
  ]);
}

/// cutWith inner ring with top left corner
unittest {
  auto points = [EPSG3857(-11521764, 6302053), EPSG3857(-11521344, 6299483), EPSG3857(-11526465, 6299483), EPSG3857(-11527067, 6300394), EPSG3857(-11526515, 6302065), EPSG3857(-11525401, 6302584), EPSG3857(-11521764, 6302053)];
  auto box = BBox!long(-11525970, -11520099, 6295476, 6301346);
  auto result = cutWith(points, box, true);

  expect(result.toEPSG4326.toList).to.equal([
    [-103.501, 49.1558], [-103.498, 49.1449], [-103.54, 49.1449], [-103.54, 49.1558], [-103.501, 49.1558]
  ]);
}

/// cutWith a tile with itself
unittest {
  auto points = [EPSG3857(-11525970, 6295476), EPSG3857(-11520099, 6295476), EPSG3857(-11520099, 6301346), EPSG3857(-11525970, 6301346), EPSG3857(-11525970, 6295476)];
  auto box = BBox!long(-11525970, -11520099, 6295476, 6301346);
  auto result = cutWith(points, box, true);

  expect(result.toEPSG4326.toList).to.equal([[-103.54, 49.1213], [-103.487, 49.1213], [-103.487, 49.1558], [-103.54, 49.1558], [-103.54, 49.1213]]);
}

/// Compute the direction of a line string relative to a BBox exterior
struct LineDirection(T) {
  BBox!T box;

  byte direction;
  byte startDirection;
  byte lastDirection;

  T prevX;
  T prevY;

  bool isFirst = true;

  int inBorder = -1;
  int outBorder = -1;

  void point(T x, T y) {
    lastDirection = box.relativeDirection([x, y]);

    if(direction == 0) {
      startDirection = lastDirection;
    }

    if(lastDirection & startDirection) {
      direction = startDirection;
    }

    direction |= lastDirection;

    auto borderIndex = box.borderIndex(x, y);

    if(borderIndex >= 0 && outBorder == -1) {
      outBorder = borderIndex;
    } else if(borderIndex >= 0 && outBorder >= 0) {
      inBorder = borderIndex;
    }

    if(!isFirst) {
      auto line = [[prevX, prevY], [x, y]];

      auto borders = box.allLines;

      int intersections;

      foreach(border; borders) {
        if(doIntersect(border, line)) {
          intersections++;
        }
      }

      if(intersections > 1) {
        inBorder = -1;
        outBorder = borderIndex;
      }
    }

    prevX = x;
    prevY = y;
    isFirst = false;
  }

  void reset() {
    direction = 0;
    startDirection = 0;
    lastDirection = 0;
    inBorder = -1;
    outBorder = -1;
  }

  bool isClockWise() {
    if(startDirection == direction) {
      return false;
    }

    if(startDirection & Direction.Left && direction & Direction.Top) {
      return true;
    }

    if(startDirection & Direction.Right && direction & Direction.Bottom) {
      return true;
    }

    if(startDirection & Direction.Top && direction & Direction.Right) {
      return true;
    }

    if(startDirection & Direction.Bottom && direction & Direction.Left) {
      return true;
    }

    return false;
  }
}

/// LineDirection is not clockwise when the points are on the left side
unittest {
  auto box = BBox!long(0, 100, 100, 0);

  auto direction = LineDirection!long(box);

  direction.point(-10, 10);
  direction.point(-20, 20);

  direction.isClockWise.should.equal(false);
}

/// LineDirection is clockwise for a point on the left side and one on the top
unittest {
  auto box = BBox!long(0, 100, 100, 0);

  auto direction = LineDirection!long(box);

  direction.point(-10, 10);
  direction.point(-10, 110);

  direction.isClockWise.should.equal(true);
}

/// LineDirection is clockwise for a line on top to right
unittest {
  auto box = BBox!long(0, 100, 100, 0);

  auto direction = LineDirection!long(box);

  direction.point(10, 110);
  direction.point(110, 90);

  direction.isClockWise.should.equal(true);
}

/// LineDirection is not clockwise for a point on the left side and one on the bottom
unittest {
  auto box = BBox!long(0, 100, 100, 0);

  auto direction = LineDirection!long(box);

  direction.point(-10, 10);
  direction.point(-10, -10);

  direction.isClockWise.should.equal(false);
}

/// LineDirection is not clockwise for a point on the left side, one on the top and then one on the left again
unittest {
  auto box = BBox!long(0, 100, 100, 0);

  auto direction = LineDirection!long(box);

  direction.point(-10, 10);
  direction.point(10, 110);
  direction.point(-10, 10);

  direction.isClockWise.should.equal(false);
}

/// LineDirection is clockwise for a line from the right to bottom
unittest {
  auto box = BBox!long(0, 100, 100, 0);

  auto direction = LineDirection!long(box);

  direction.point(110, 10);
  direction.point(-10, -10);

  direction.isClockWise.should.equal(true);
}

/// LineDirection is clockwise for a line from the bottom to left
unittest {
  auto box = BBox!long(0, 100, 100, 0);

  auto direction = LineDirection!long(box);

  direction.point(10, -10);
  direction.point(-10, 10);

  direction.isClockWise.should.equal(true);
}

/// LineDirection sets the out border when starting from inside and movig outside
unittest {
  auto box = BBox!long(0, 100, 100, 0);

  auto direction = LineDirection!long(box);

  direction.point(50, 50);
  direction.point(100, 50);

  direction.outBorder.should.equal(2);
  direction.inBorder.should.equal(-1);
}

/// LineDirection sets the out border when the line goes back inside
unittest {
  auto box = BBox!long(0, 100, 100, 0);
  auto direction = LineDirection!long(box);

  direction.point(50, 50);
  direction.point(100, 50);
  direction.point(-50, -50);
  direction.point(50, 0);

  direction.outBorder.should.equal(3);
  direction.inBorder.should.equal(-1);
}

/// LineDirection sets the out direction when a line crosses from left to right
unittest {
  auto box = BBox!long(0, 100, 100, 0);
  auto direction = LineDirection!long(box);

  direction.point(0, 50);
  direction.point(100, 40);

  direction.inBorder.should.equal(-1);
  direction.outBorder.should.equal(2);
}

////
private double sign(Point)(Point p1, Point p2, Point p3) {
  return (p1[0] - p3[0]) * (p2[1] - p3[1]) - (p2[0] - p3[0]) * (p1[1] - p3[1]);
}

///
bool pointInTriangle(Point)(Point pt, Point v1, Point v2, Point v3) {
  float d1, d2, d3;
  bool has_neg, has_pos;

  d1 = sign(pt, v1, v2);
  d2 = sign(pt, v2, v3);
  d3 = sign(pt, v3, v1);

  has_neg = (d1 < 0) || (d2 < 0) || (d3 < 0);
  has_pos = (d1 > 0) || (d2 > 0) || (d3 > 0);

  return !(has_neg && has_pos);
}

/// returns true if at least 1 point from polygon1 is not in polygon 2
bool isOutside(Point)(Point[] polygon1, Point[] polygon2) {
  auto maxX = polygon2[0][0];

  foreach(point; polygon2) {
    if(point[0] > maxX) {
      maxX = point[0] + 10;
    }
  }

  foreach(i; 0..polygon1.length) {
    size_t intersections;
    auto line1 = [
      [ polygon1[i][0], polygon1[i][1]],
      [ maxX,           polygon1[i][1]]
    ];
    bool isOnBorder = false;

    foreach(j; 1..polygon2.length) {
      auto line2 = [polygon2[j-1], polygon2[j]].toList;

      if(distance(line2[0], line1[0]) + distance(line2[1], line1[0]) == distance(line2)) {
        isOnBorder = true;
        continue;
      }

      if(line1.doIntersect(line2)) {
        intersections++;
      }
    }

    if(!isOnBorder && intersections % 2 == 0) {
      return true;
    }
  }

  return false;
}

///
void fixTileBorders(uint z, uint x, uint y, double[][][] polygon) {
  auto exteriourTilePolygon = getTilePolygon(z, x, y, 0.1);
  auto interiourTilePolygon = getTilePolygon(z, x, y, 0.9);

  auto tileBox = BBox!double(exteriourTilePolygon);
  auto interiourTileBox = BBox!double(interiourTilePolygon);

  foreach(ringIndex; 0..polygon.length) {
    auto allBorders = tileBox.allLines;

    foreach(i; 0..polygon[ringIndex].length) {
      foreach(border; allBorders) {
        if(polygon[ringIndex][i][0] < tileBox.left) {
          polygon[ringIndex][i][0] = tileBox.left;
        }

        if(polygon[ringIndex][i][0] > tileBox.right) {
          polygon[ringIndex][i][0] = tileBox.right;
        }

        if(polygon[ringIndex][i][1] > tileBox.top) {
          polygon[ringIndex][i][1] = tileBox.top;
        }

        if(polygon[ringIndex][i][1] < tileBox.bottom) {
          polygon[ringIndex][i][1] = tileBox.bottom;
        }
      }
    }

    tileBox = interiourTileBox;
  }
}


/// it returns false when the eastBulgarianTile is outside a tile boundary
unittest {
  import std.algorithm;
  import std.array;

  auto exteriourTilePolygon = getTilePolygon(6, 37, 23, 0.1);
  auto tileBox = BBox!double(exteriourTilePolygon);

  auto invalidHole = readText("test/eastBulgarianTile.json").parseJsonString;
  auto polygon = GeoJsonGeometry.fromJson(invalidHole).to!MultiPolygon;

  auto coordinates = polygon.coordinates[0].toList;

  fixTileBorders(6, 37, 23, coordinates);

  coordinates[0].map!(a => a[0] < tileBox.left).filter!(a => a).array.length.should.equal(0);
}

///
void fixOverlayLines(T, U)(ref T[] polygon, U d) if(!IsSomePoint!T) {
  foreach(ringIndex; 0..polygon.length) {
    if(polygon[ringIndex].length <= 3) {
      continue;
    }

    fixOverlayLines(polygon[ringIndex], d);
  }
}

///
void fixOverlayLines(T, U)(ref T[] ring, U d) if(IsSomePoint!T) {
  if(ring.length < 3) {
    return;
  }

  foreach(i; 1..ring.length - 1) {
    foreach(j; 1..ring.length) {
      if(i == j || ring[i] == ring[j] || ring[i] == ring[j - 1]) {
        continue;
      }

      if(!onSameLine(ring[j-1], ring[i], ring[j])) {
        continue;
      }

      auto isNextPointOnLine = onSameLine(ring[j-1], ring[i+1], ring[j]);
      auto point = isNextPointOnLine ? ring[i-1] : ring[i+1];

      reduceEndInPlace(point, ring[i], d);

      // if there are intersections, then we try to fix them
      auto edge = intersectedEdge([point, ring[i]], ring);

      if(edge != -1) {
        auto tmp = intersectionNoCheck([ring[edge], ring[edge + 1]], [point, ring[i]]);

        if(tmp.length) {
          ring[i][0] = tmp[0][0];
          ring[i][1] = tmp[0][1];
        }
      }
    }
  }
}

/// It moves a point from a line
unittest {
  double[][] polygon = [
    [ -85.43847024045705, 37.22333369822982 ],
    [ -85.38322385047601, 37.22333369822982 ],
    [ -85.38437369403985, 37.22354463350841 ],
    [ -85.38759864591027, 37.22333369822982 ],
    [ -85.40775684088872, 37.22333369822982 ],
    [ -85.40764576905885, 37.22335571753149 ],
    [ -85.41095484330064, 37.2244673800262 ],
    [ -85.4063105532811, 37.23489577667866 ],
    [ -85.43847024045705, 37.22333369822982 ]
  ];

  fixOverlayLines(polygon, 0.0001);

  expect(polygon).to.equal([[-85.4385, 37.2233], [-85.3832, 37.2233],
  [-85.3844, 37.2235], [-85.3844, 37.2235], [-85.4077, 37.2234],
  [-85.4076, 37.2234], [-85.411, 37.2245], [-85.4063, 37.2349], [-85.4385, 37.2233]]);
}

PositionT!T[] fixOverlayPoints(T, U)(PositionT!T[] ring, U d) {
  if(ring.length == 0) {
    return [];
  }

  PositionT!T[] result;
  result.reserve(ring.length);

  result ~= ring[0];

  foreach (i; 1..ring.length) {
    if(distance(result[result.length - 1], ring[i]) > d) {
      result ~= ring[i];
    }
  }

  return result;
}

PositionT!T[][] cut(T)(PositionT!T[][] coordinates, uint z, uint x, uint y) {
  auto tileBox = toBBox(z, x, y, 0.1);
  auto interiourTileBox = toBBox(z, x, y, 0.09);

  PositionT!T[][] newCoordinates;
  newCoordinates.reserve(coordinates.length);

  double minArea = tileBox.area / 400_000;

  foreach(i; 0 .. coordinates.length) {
    auto ring = coordinates[i];
    auto newRing = ring.cutWith(tileBox, i == 0);

    fixOverlayLines(newRing, tileBox.dx / 3900);
    newRing = fixOverlayPoints(newRing, tileBox.dx / 3900);

    bool isTooSmall = abs(absoluteArea(newRing)) < minArea;

    if(newRing.length <= 3 && i == 0 || isTooSmall) {
      break;
    }

    if(newRing.length <= 3 || isTooSmall) {
      continue;
    }

    if(i != 0 && newRing.isOutside(coordinates[0])) {
      continue;
    }

    if(i == 0 && newRing.isClockWise) {
      newRing = newRing.reverse;
    }

    if(i != 0 && !newRing.isClockWise) {
      newRing = newRing.reverse;
    }

    newCoordinates ~= newRing;
    tileBox = interiourTileBox;
  }

  return newCoordinates;
}

///
Polygon cut(Polygon polygon, uint z, uint x, uint y) {
  Polygon newPolygon;

  newPolygon.coordinates = polygon.coordinates.cut(z, x, y);

  return newPolygon;
}

///
MultiPolygon cut(MultiPolygon multiPolygon, uint z, uint x, uint y) {
  MultiPolygon newPolygon;

  foreach(i; 0 .. multiPolygon.coordinates.length) {
   auto result = multiPolygon.coordinates[i].cut(z, x, y);

   if(result.length) {
    newPolygon.coordinates ~= result;
   }
  }

  return newPolygon;
}

/// it can cut the district of kenmare at level 16
unittest {
  import geo.algo.simplify;

  const tileBox = toBBox(16, 30848, 21714, 0.1);

  auto geometry = readText("test/kenmare.json").parseJsonString;
  auto multiPolygon = GeoJsonGeometry.fromJson(geometry).to!MultiPolygon;

  auto simpleMultiPolygon = multiPolygon.simplify(tileBox.dx / 12_000).cut(16, 30848, 21714);
  simpleMultiPolygon.coordinates[0][0].serializeToJson.toString.should.equal("[[-10.54083251953251,51.77089929486656],"~
  "[-10.54083251953251,51.77168178886643],[-10.54101254299997,51.77179941500003],[-10.54122587599994,51.77192774800005],"~
  "[-10.54139672799994,51.77201760800006],[-10.54162715999996,51.77206772200003],[-10.54183368899993,51.77207729900005],"~
  "[-10.54194996299998,51.77208984400005],[-10.54201350099993,51.77211097500003],[-10.54205637999996,51.77219026800003],"~
  "[-10.54204648199993,51.77224829700003],[-10.54198154199997,51.77230457600007],[-10.54190354299993,51.77235310000003],"~
  "[-10.54186638699997,51.77241817500004],[-10.54183855999997,51.77249835900005],[-10.54185131199995,51.77258778200007],"~
  "[-10.54187463199997,51.77261125700005],[-10.54191496799996,51.77261256100007],[-10.54201095599996,51.77257540800008],"~
  "[-10.54218847899995,51.77250853800007],[-10.54238548499995,51.77243658700007],[-10.54251646899996,51.77238800500004],"~
  "[-10.54262938599993,51.77235732800006],[-10.54275995699993,51.77232642200005],[-10.54285258899995,51.77230513100005],"~
  "[-10.54293913599997,51.77228026700004],[-10.54299027899998,51.77225159200003],[-10.54312483899997,51.77213287000006],"~
  "[-10.54319050199996,51.77209850400004],[-10.54325139899993,51.77209896600004],[-10.54333700399997,51.77207411300003],"~
  "[-10.54341278599998,51.77201892000005],[-10.54350224299998,51.77184043700004],[-10.54353703499993,51.77176381500004],"~
  "[-10.54353971699993,51.77166566000005],[-10.54357482099994,51.77156648900007],[-10.54358257999996,51.77150422700004],"~
  "[-10.54353994499996,51.77143163600005],[-10.54352732199993,51.77137572200007],[-10.54356263399995,51.77128386600003],"~
  "[-10.54361648299994,51.77112838100004],[-10.54361935899993,51.77100584600004],[-10.54360894299998,51.77095722200005],"~
  "[-10.5435693978353,51.77089929487066],[-10.54083251953251,51.77089929486656]]");
}