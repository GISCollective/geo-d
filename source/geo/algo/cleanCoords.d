module geo.algo.cleanCoords;

import geo.geometries;
import std.math;
import std.algorithm;
import std.array;
import std.meta;

version(unittest) {
  import fluent.asserts;
}

/// Removes redundant coordinates from any GeoJSON Geometry.
BasicGeometryVariant cleanCoords(BasicGeometryVariant value) {
  alias Types = AliasSeq!(Point, MultiPoint, LineString, MultiLineString, Polygon, MultiPolygon);
  BasicGeometryVariant result;

  static foreach(Type; Types) {
    if(value.peek!Type !is null) {
      result = value.get!Type.cleanCoords;
    }
  }

  return result;
}

/// ditto
Point cleanCoords(Point geometry) {
  return geometry;
}

/// ditto
LineString cleanCoords(LineString geometry) {
  return LineString(geometry.coordinates.cleanLine("LineString"));
}

/// ditto
MultiPoint cleanCoords(MultiPoint geometry) {
  return MultiPoint(geometry.coordinates.sort.uniq.array);
}

/// ditto
T cleanCoords(T)(T geometry) if(is(T == MultiLineString) || is(T == Polygon)) {
  T result;

  foreach(ring; geometry.coordinates) {
    result.coordinates ~= ring.cleanLine(T.stringof);
  }

  return result;
}

/// ditto
MultiPolygon cleanCoords(MultiPolygon geometry) {
  MultiPolygon result;

  foreach (polygon; geometry.coordinates) {
    PositionT!double[][] polyPoints;

    foreach(ring; polygon) {
      polyPoints ~= ring.cleanLine("Polygon");
    }

    result.coordinates ~= polyPoints;
  }

  return result;
}

/// example test with line and multi point
unittest {
  auto line = LineString([PositionT!double(0, 0), PositionT!double(0, 2), PositionT!double(0, 5), PositionT!double(0, 8), PositionT!double(0, 8), PositionT!double(0, 10)]);
  auto multiPoint = MultiPoint([PositionT!double(0, 0), PositionT!double(0, 0), PositionT!double(2, 2)]);

  cleanCoords(line).coordinates.toList.should.equal([[0., 0.], [0., 10.]]);
  cleanCoords(multiPoint).coordinates.toList.should.equal([[0., 0.], [2., 2.]]);
}

/// test geometries from files
unittest {
  import std.file;
  import std.path;
  import std.stdio;
  import vibe.data.json;
  import geo.json;

  foreach (string name; buildPath("test", "cleanCoords", "in").dirEntries("*.geojson", SpanMode.shallow)) {
    auto expected = buildPath("test", "cleanCoords", "out", name.baseName).readText.parseJsonString.serializeToPrettyJson;

    name.writeln(" <---");

    name.readText.parseJsonString.toBasicGeometryVariant.cleanCoords.toJson.serializeToPrettyJson
      .should.equal(expected);
  }
}

/// Clean Coords
Position[] cleanLine(Position)(Position[] points, string type) if(IsSomePoint!Position) {
  // handle "clean" segment
  if (points.length == 2 && points[0] != points[1]) {
    return points;
  }

  Position[] newPoints;
  auto secondToLast = points.length - 1;
  auto newPointsLength = newPoints.length;

  newPoints ~= points[0];
  for (size_t i = 1; i < secondToLast; i++) {
    auto prevAddedPoint = newPoints[newPoints.length - 1];
    if (
      points[i][0] == prevAddedPoint[0] &&
      points[i][1] == prevAddedPoint[1]
    )
      continue;
    else {
      newPoints ~= points[i];
      newPointsLength = newPoints.length;
      if (newPointsLength > 2) {
        if (
          isPointOnLineSegment(
            newPoints[newPointsLength - 3],
            newPoints[newPointsLength - 1],
            newPoints[newPointsLength - 2]
          )
        )
          newPoints = newPoints[0..$-2] ~ newPoints[$-1..$]; //?
      }
    }
  }

  newPoints ~= points[points.length - 1];
  newPointsLength = newPoints.length;

  // (Multi)Polygons must have at least 4 points, but a closed LineString with only 3 points is acceptable
  if (
    (type == "Polygon" || type == "MultiPolygon") &&
    points[0] == points[points.length - 1] &&
    newPointsLength < 4
  ) {
    throw new Error("invalid polygon");
  }

  if (
    isPointOnLineSegment(
      newPoints[newPointsLength - 3],
      newPoints[newPointsLength - 1],
      newPoints[newPointsLength - 2]
    )
  )

  newPoints = newPoints[0..$-2] ~ newPoints[$-1..$]; //?

  return newPoints;
}

/// Returns if `point` is on the segment between `start` and `end`.
private bool isPointOnLineSegment(Position)(Position start, Position end, Position point) if(IsSomePoint!Position) {
  auto x = point[0];
  auto y = point[1];

  auto startX = start[0];
  auto startY = start[1];
  auto endX = end[0];
  auto endY = end[1];

  auto dxc = x - startX;
  auto dyc = y - startY;
  auto dxl = endX - startX;
  auto dyl = endY - startY;
  auto cross = dxc * dyl - dyc * dxl;

  if (cross != 0) {
    return false;
  }

  if (abs(dxl) >= abs(dyl)) {
    return dxl > 0 ? startX <= x && x <= endX : endX <= x && x <= startX;
  }

  return dyl > 0 ? startY <= y && y <= endY : endY <= y && y <= startY;
}
