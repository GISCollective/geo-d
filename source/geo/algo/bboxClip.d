module geo.algo.bboxClip;


import geo.geometries;
import std.math;
import std.algorithm;
import std.array;
import std.meta;

version(unittest) {
  import fluent.asserts;
}

/// Takes a Feature and a bbox and clips the feature to the bbox. May result in degenerate edges when clipping Polygons.
BasicGeometryVariant bboxClip(T)(BasicGeometryVariant value, BBox!T bbox) {
  alias Types = AliasSeq!(Point, MultiPoint, LineString, MultiLineString, Polygon, MultiPolygon);
  BasicGeometryVariant result;

  static foreach(Type; Types) {
    if(value.peek!Type !is null) {
      result = value.get!Type.bboxClip(bbox);
    }
  }

  return result;
}

/// ditto
Point bboxClip(T)(Point geometry, BBox!T bbox) {
  assert(false, "not implemented");
}

/// ditto
MultiPoint bboxClip(T)(MultiPoint geometry, BBox!T bbox) {
  assert(false, "not implemented");
}

/// ditto
MultiLineString bboxClip(T)(LineString geometry, BBox!T bbox) {
  auto result = geometry.coordinates.clipLine(bbox);

  return MultiLineString(result);
}

/// ditto
MultiLineString bboxClip(T)(MultiLineString geometry, BBox!T bbox) {
  PositionT!double[][] points;

  foreach (line; geometry.coordinates) {
    points ~= line.clipLine(bbox);
  }

  return MultiLineString(points);
}

/// ditto
Polygon bboxClip(T)(Polygon geometry, BBox!T bbox) {
  return Polygon(geometry.coordinates.clipPolygon(bbox));
}

/// ditto
MultiPolygon bboxClip(T)(MultiPolygon geometry, BBox!T bbox) {
  PositionT!double[][][] points;

  foreach (PositionT!double[][] polygon; geometry.coordinates) {
    points ~= polygon.clipPolygon(bbox);
  }

  return MultiPolygon(points);
}

/// example test for bboxClip
unittest {
  auto bbox = BBox!double(0, 10, 0, 10);
  auto poly = Polygon([[[2, 2], [8, 4], [12, 8], [3, 7], [2, 2]]]);

  auto clipped = bboxClip(poly, bbox);

  expect(clipped.coordinates.toList).to.equal([[[2., 2.], [8., 4.], [10., 6.], [10., 7.77778], [3., 7.], [2., 2.]]]);
}

/// test geometries from files
unittest {
  import std.file;
  import std.path;
  import std.stdio;
  import vibe.data.json;
  import geo.json;

  foreach (string name; buildPath("test", "bboxClip", "in").dirEntries("*.geojson", SpanMode.shallow)) {
    auto expected = buildPath("test", "bboxClip", "out", name.baseName).readText.parseJsonString;

    name.writeln(" <---");

    auto collection = name.readText.parseJsonString;

    auto options = collection["properties"];
    auto feature = collection["features"][0]["geometry"].toBasicGeometryVariant;
    auto bbox = BBox!double(collection["features"][1]["geometry"].toBasicGeometryVariant.get!Polygon.coordinates[0]);

    auto clipped = bboxClip(feature, bbox);

    clipped.toJson["coordinates"].should.equal(expected["features"][1]["geometry"]["coordinates"]);
  }
}

///
auto clipPolygon(Point, T)(ref Point[][] rings, BBox!T bbox) if(IsSomePoint!Point) {
  Point[][] outRings = [];

  foreach(ring; rings) {
    auto clipped = ring.clipRing(bbox);

    if (clipped.length > 0) {
      if (
        clipped[0][0] != clipped[clipped.length - 1][0] ||
        clipped[0][1] != clipped[clipped.length - 1][1]
      ) {
        clipped ~= clipped[0];
      }
      if (clipped.length >= 4) {
        outRings ~= clipped;
      }
    }
  }

  return outRings;
}

///
Point[] clipRing(Point, T)(ref Point[] ring, BBox!T bbox) if(IsSomePoint!Point) {
  Point[] result;
  Point prev;
  bool prevIsInside;
  bool isInside;

  // clip against each side of the clip rectangle
  for(byte edge = 1; edge <= 8; edge *= 2) {
    result = [];
    prev = ring[ring.length - 1];
    prevIsInside = !(bbox.relativeDirection(prev) & edge);

    foreach(i; 0..ring.length) {
      auto p = ring[i];
      isInside = !(bbox.relativeDirection(p) & edge);

      // if segment goes through the clip window, add an intersection
      if (isInside != prevIsInside) {
        result ~= intersect(prev, p, cast(Direction) edge, bbox);
      }

      if (isInside) {
        result ~= p;
      }

      prev = p;
      prevIsInside = isInside;
    }

    ring = result;

    if (!ring.length) {
      break;
    }
  }

  return result;
}

/// intersect a segment against one of the 4 lines that make up the bbox
Point intersect(Point, T)(Point a, Point b, Direction edge, BBox!T bbox) {
  if(edge & Direction.Top) {
    return Point(a[0] + ((b[0] - a[0]) * (bbox.top - a[1])) / (b[1] - a[1]), bbox.top);
  }

  if(edge & Direction.Bottom) {
    return Point(a[0] + ((b[0] - a[0]) * (bbox.bottom - a[1])) / (b[1] - a[1]), bbox.bottom);
  }

  if(edge & Direction.Right) {
    return Point(bbox.right, a[1] + ((b[1] - a[1]) * (bbox.right - a[0])) / (b[0] - a[0]));
  }

  if(edge & Direction.Left) {
    return Point(bbox.left, a[1] + ((b[1] - a[1]) * (bbox.left - a[0])) / (b[0] - a[0]));
  }

  return Point();
}

///
Point[][] clipLine(Point)(Point[] points, BBox!double bbox) if(IsSomePoint!Point) {
  Point[][] result;

  auto len = points.length;
  auto codeA = bbox.relativeDirection(points[0]);
  Point[] part;
  byte codeB;
  byte lastCode;
  Point a;
  Point b;

  if (!result) result = [];

  foreach(i; 1..len) {
    a = points[i - 1];
    b = points[i];
    codeB = lastCode = bbox.relativeDirection(b);

    while (true) {
      if (!(codeA | codeB)) {
        // accept
        part ~= a;

        if (codeB != lastCode) {
          // segment went outside
          part ~= b;

          if (i < len - 1) {
            // start a new line
            result ~= part;
            part = [];
          }
        } else if (i == len - 1) {
          part ~= b;
        }
        break;
      } else if (codeA & codeB) {
        // trivial reject
        break;
      } else if (codeA) {
        // a outside, intersect with clip edge
        a = intersect(a, b, cast(Direction) codeA, bbox);
        codeA = bbox.relativeDirection(a);
      } else {
        // b outside
        b = intersect(a, b, cast(Direction) codeB, bbox);
        codeB = bbox.relativeDirection(b);
      }
    }

    codeA = lastCode;
  }

  if (part.length) {
    result ~= part;
  }

  return result;
}