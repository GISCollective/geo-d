/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module geo.algo.simplify;

import geo.algorithm;
import geo.conv;
import geo.epsg;
import geo.json;
import geo.xyz;
import std.algorithm.mutation;
import std.math;
import std.traits;
import geo.algo.cut;
import geo.algo.intersection;

version(unittest) {
  import vibe.data.json;
  import std.file;
  import fluent.asserts;
}

size_t pointsInsideTriangle(Point)(Point[] points, Point p1, Point p2, Point p3) {
  size_t result;

  foreach(point; points) {
    if(point.pointInTriangle(p1, p2, p3)) {
      result++;
    }
  }

  return result;
}

///
Point[] simplifyLineString(Point)(Point[] points, double minDistance, size_t minPoints = 2, bool closeGap = false) {
  if(points.length <= 2) {
    return points;
  }

  Point[] result;
  result.assumeSafeAppend();

  result = [];
  result.reserve(points.length);

  bool hasUpdates;

  size_t loopIndex = 0;
  do {
    size_t i = 0;
    hasUpdates = false;

    while(i < points.length - 3) {
      auto point1 = points[i];
      auto point2 = points[i+1];
      auto point3 = points[i+2];

      auto a = absoluteArea([point1, point2, point3, point1]);

      if(a < 0 || a > minDistance) {
        result ~= [point1, point2, point3];
      } else {
        result ~= [point1, point3];
        hasUpdates = true;
      }

      i += 3;
    }

    while(i < points.length) {
      result ~= points[i];
      i++;
    }

    if(hasUpdates) {
      points = result;
      result.length = 0;
    }

    loopIndex++;
  } while(hasUpdates && loopIndex < 50);

  if(result.length < 2) {
    return [];
  }


  return result;
}

///
Point[] simplifyLine(Point)(Point[] points, double minDistance, size_t minPoints = 2, bool closeGap = false) {
  if(points.length < minPoints) {
    return [];
  }

  if(points.length <= 2) {
    return points;
  }

  Point[] result;

  result = [];
  result.assumeSafeAppend();
  result.reserve(points.length);

  bool hasUpdates;

  import std.stdio;

  size_t loopIndex = 0;
  do {
    debug writeln("loopIndex ", loopIndex, " points=", points.length);

    if(loopIndex > 0) {
      points = result;
      result.length = 0;
    }

    size_t i = 0;
    hasUpdates = false;

    while(i < points.length - 3) {
      auto point1 = points[i];
      auto point2 = points[i+1];
      auto point3 = points[i+2];

      auto a = absoluteArea([point1, point2, point3, point1]);
      auto hasInvalidArea = a < 0 || a > minDistance;
      size_t pointsInside;

      if(!hasInvalidArea) {
        pointsInside = points.pointsInsideTriangle(point1, point2, point3);
      }

      if(hasInvalidArea || pointsInside > 0) {
        result ~= [point1, point2, point3];
      } else {
        result ~= [point1, point3];
        hasUpdates = true;
      }

      i += 3;
    }

    while(i < points.length) {
      result ~= points[i];
      i++;
    }

    loopIndex++;
  } while(hasUpdates && loopIndex < 50);

  if(result.length < minPoints) {
    return [];
  }

  if(closeGap) {
    points = result;
    result = [];
    result.reserve(points.length);

    result ~= points[0];
    foreach(j; 1..points.length) {
      auto prevPoint = points[j-1];
      auto newEdge = [prevPoint, points[j]];
      auto tmp = result[0..$-1];

      long intersectedEdge = -1;

      intersectedEdge = newEdge.intersectedEdge(tmp);

      if(intersectedEdge > -1) {
        result[intersectedEdge+1 .. $].reverse;
      }

      result ~= points[j];
    }

    if(result[0] != result[result.length - 1]) {
      result ~= result[0];
    }
  }

  return result;
}

///
Point[][] simplifyPolygon(Point)(uint z, uint x, uint y, PositionT!double[][] polygon) {
  auto exteriourTilePolygon = getTilePolygon(z, x, y, 0.1).projectTo!Point;
  auto interiourTilePolygon = getTilePolygon(z, x, y, 0.09).projectTo!Point;

  static if(is(Point == EPSG3857)) {
    alias U = long;
  } else {
    alias U = double;
  }

  auto exteriourTileBox = BBox!(Unqual!U)(exteriourTilePolygon);
  auto interiourTileBox = BBox!(Unqual!U)(interiourTilePolygon);

  Point[][] result;
  result.assumeSafeAppend();

  static if(is(Point == EPSG3857)) {
    auto maxDistance = exteriourTileBox.dx / 8_000;
  } else {
    auto maxDistance = exteriourTileBox.dx / 30_000;
  }

  import std.stdio;

  foreach(i; 0 .. polygon.length) {
    Point[] ring = polygon[i].projectTo!Point;
    debug writeln("ring i = ", ring.length, " points");

    auto tileBox = i == 0 ? exteriourTileBox : interiourTileBox;

    if(tileBox.fullyOutside(ring)) {
      if(i==0) {
        break;
      }

      return [];
    }

    if(tileBox.fullyInside(ring)) {
      result ~= exteriourTileBox.getRingCoordsAsPosition.projectTo!Point;
      continue;
    }

    auto newRing = simplifyLine(ring, maxDistance, 4);

    bool isTooSmall = abs(absoluteArea(newRing)) < maxDistance * 4;
    newRing = newRing.cutWith(tileBox, i == 0);
    fixOverlayLines(newRing, 2);

    if(newRing.length <= 3 && i == 0 || isTooSmall) {
      break;
    }

    if(newRing.length <= 3 || isTooSmall) {
      continue;
    }

    if(i != 0 && newRing.isOutside(result[0])) {
      continue;
    }

    if(i == 0 && newRing.isClockWise) {
      newRing = newRing.reverse;
    }

    if(i != 0 && !newRing.isClockWise) {
      newRing = newRing.reverse;
    }

    result ~= newRing;
  }

  return result;
}

/// it cuts the Pollok Country Park
unittest {
  auto invalidHole = readText("test/pollokCountryPark.json").parseJsonString;
  auto polygon = GeoJsonGeometry.fromJson(invalidHole).to!Polygon;

  auto result = simplifyPolygon!EPSG3857(12, 1998, 1278, polygon.coordinates);

  result[0].toEPSG4326.toList.should.equal([
    [-4.30575, 55.821],
    [-4.29785, 55.8284],
    [-4.29785, 55.8343],
    [-4.30907, 55.8399],
    [-4.32886, 55.8336],
    [-4.3318, 55.8309],
    [-4.3334, 55.8233],
    [-4.32874, 55.8226],
    [-4.32954, 55.821],
    [-4.30575, 55.821]
  ]);
}

/// it cuts the rings when they intersect with the bbox
unittest {
  auto invalidHole = readText("test/invalidHole.json").parseJsonString;
  auto polygon = GeoJsonGeometry.fromJson(invalidHole).to!Polygon;

  double[][][] result = simplifyPolygon!EPSG3857(13, 1740, 2808, polygon.coordinates).toEPSG4326.toList;

  double[][] exprctedRing1 = [[-103.54, 49.1213], [-103.487, 49.1213], [-103.487, 49.1558], [-103.54, 49.1558], [-103.54, 49.1213]];
  double[][] exprctedRing2 = [[-103.498, 49.1357], [-103.498, 49.1449], [-103.487, 49.1449], [-103.487, 49.1375], [-103.494, 49.1375], [-103.494, 49.1357], [-103.498, 49.1357]];

  result[0].should.equal(exprctedRing1);
  result[1].should.equal(exprctedRing2);
}

/// it cuts the rings when they intersect with the bottom right corner bbox
unittest {
  auto invalidHole = readText("test/invalidHoleBottomRight.json").parseJsonString;
  auto polygon = GeoJsonGeometry.fromJson(invalidHole).to!Polygon;

  auto result = simplifyPolygon!EPSG3857(13, 1740, 2808, polygon.coordinates);

  result[0].toEPSG4326.toList.should.equal([[-103.54, 49.1213], [-103.487, 49.1213], [-103.487, 49.1558], [-103.54, 49.1558], [-103.54, 49.1213]]);
  result[1].toEPSG4326.toList.should.equal([[-103.498, 49.1216], [-103.498, 49.1449], [-103.487, 49.1449], [-103.487, 49.1216], [-103.498, 49.1216]]);
}

/// it cuts the rings when they intersect with the bottom left corner bbox
unittest {
  auto invalidHole = readText("test/invalidHoleBottomLeft.json").parseJsonString;
  auto polygon = GeoJsonGeometry.fromJson(invalidHole).to!Polygon;

  auto result = simplifyPolygon!EPSG3857(13, 1740, 2808, polygon.coordinates);

  result[0].toEPSG4326.toList.should.equal([[-103.54, 49.1213], [-103.487, 49.1213], [-103.487, 49.1558], [-103.54, 49.1558], [-103.54, 49.1213]]);
  result[1].toEPSG4326.toList.should.equal([[-103.498, 49.1216], [-103.539, 49.1216], [-103.539, 49.1449], [-103.498, 49.1449], [-103.498, 49.1216]]);
}

/// it cuts the rings when they intersect with the top right corner bbox
unittest {
  auto invalidHole = readText("test/invalidHoleTopRight.json").parseJsonString;
  auto polygon = GeoJsonGeometry.fromJson(invalidHole).to!Polygon;

  auto result = simplifyPolygon!EPSG3857(13, 1740, 2808, polygon.coordinates);

  result[0].toEPSG4326.toList.should.equal([[-103.54, 49.1213], [-103.487, 49.1213], [-103.487, 49.1558], [-103.54, 49.1558], [-103.54, 49.1213]]);
  result[1].toEPSG4326.toList.should.equal([[-103.487, 49.1446], [-103.498, 49.1449], [-103.516, 49.1556], [-103.487, 49.1556], [-103.487, 49.1446]]);
}

/// it cuts the rings when they intersect with the top left corner bbox
unittest {
  auto invalidHole = readText("test/invalidHoleTopLeft.json").parseJsonString;
  auto polygon = GeoJsonGeometry.fromJson(invalidHole).to!Polygon;

  auto result = simplifyPolygon!EPSG3857(13, 1740, 2808, polygon.coordinates);

  result[0].toEPSG4326.toList.should.equal([[-103.54, 49.1213], [-103.487, 49.1213], [-103.487, 49.1558], [-103.54, 49.1558], [-103.54, 49.1213]]);
  result[1].toEPSG4326.toList.should.equal([[-103.501, 49.1556], [-103.498, 49.1449], [-103.539, 49.1449], [-103.539, 49.1556], [-103.501, 49.1556]]);
}

///
Polygon simplify(Polygon polygon, double maxDistance) {
  Polygon newPolygon;

  import std.stdio;

  size_t i;

  foreach(ring; polygon.coordinates) {
    debug writeln("simplifying ring ", i);
    newPolygon.coordinates ~= simplifyLine!(PositionT!double)(ring, maxDistance, 4);

    i++;
  }

  return newPolygon;
}

///
MultiPolygon simplify(MultiPolygon multiPolygon, double maxDistance) {
  MultiPolygon newPolygon;

  import std.stdio;

  size_t i;

  foreach(polygon; multiPolygon.coordinates) {
    debug writeln("simplifying polygon ", i);
    PositionT!double[][] list;

    size_t j;
    foreach(ring; polygon) {
      debug writeln("simplifying ring ", j);
      list ~= simplifyLine!(PositionT!double)(ring, maxDistance, 4);

      j++;
    }

    newPolygon.coordinates ~= list;
    i++;
  }

  return newPolygon;
}