/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module geo.mbtiles;

import std.file;
import std.zlib;
import std.array;
import std.traits;
import std.conv;
import std.datetime;
import std.path;
import std.experimental.allocator.mallocator;
import core.memory;
import d2sqlite3;

import geo.vector;
import geo.xyz;
import geo.stats;

import core.thread;

version(unittest) {
  import fluent.asserts;
}

/// Interface for reading MBTiles 1.3 files
interface ReadMBTiles {
  /// Get raw tile data
  ubyte[] get(int zoom_level, int tile_column, int tile_row);
  ///
  string name();
  ///
  string format();
  ///
  string bounds();
  ///
  int minzoom();
  ///
  int maxzoom();
  ///
  string attribution();
  ///
  string description();
  ///
  string type();
  ///
  string json();
}

/// Interface for writing MBTiles 1.3 files
interface WriteMBTiles {
  /// Set raw tile data
  void set(int zoom_level, int tile_column, int tile_row, const ubyte[] data);
  ///
  void name(const string);
  ///
  void format(const string);
  ///
  void bounds(const string);
  ///
  void minzoom(const int);
  ///
  void maxzoom(const int);
  ///
  void attribution(const string);
  ///
  void description(const string);
  ///
  void type(const string);
  ///
  void json(const string);
}

///
interface ReadWriteMBTiles : ReadMBTiles, WriteMBTiles { }

///
interface IVtTiles {
  ///
  Tile getVT(int zoom_level, int tile_column, int tile_row);

  ///
  void setVT(int zoom_level, int tile_column, int tile_row, Tile tile);

  void flush();

  string stringStats();
}


class PBFFileVtTiles : IVtTiles {

  private {
    string basePath;
    UpdateStats setStats;
    ubyte[] buffer;
  }

  this(string basePath) {
    this.basePath = basePath;
    this.buffer = cast(ubyte[]) Mallocator.instance.allocate(1024 * 1024 * 16);
  }

  ~this() {
    Mallocator.instance.deallocate(this.buffer);
  }

  string pathFromZXY(int zoom_level, int tile_column, int tile_row) {
    return buildPath(basePath, zoom_level.to!string ~ "." ~ tile_column.to!string ~ "." ~ tile_row.to!string ~ ".vt.pbf");
  }

  ///
  Tile getVT(int zoom_level, int tile_column, int tile_row) {
    return Tile();
  }

  ///
  void setVT(int zoom_level, int tile_column, int tile_row, Tile tile) {
    setStats.start;
    scope(exit) setStats.end;

    auto path = pathFromZXY(zoom_level, tile_column, tile_row);
    auto size = tile.toProtoBuf(buffer);

    path.write(buffer[0..size]);
  }

  void flush() {

  }

  string stringStats() {
    return "[set] " ~ setStats.toString;
  }
}

/// It writes a tile to a file
unittest {
  scope(exit) {
    if(tempDir.buildPath("vt").exists) {
      tempDir.buildPath("vt").rmdirRecurse;
    }
  }

  tempDir.buildPath("vt", "set").mkdirRecurse;

  auto path = tempDir.buildPath("vt", "set");
  auto tiles = new PBFFileVtTiles(path);

  Tile tile;

  tiles.setVT(1,2,3, tile);

//  path.buildPath("vt", "set", "1.2.3.vt.pbf").exists.should.equal(true);
}

///
private static string generateWriteMetadataField(Type, string name)() {
  string strType = Type.stringof;

  return `void ` ~ name ~ `(const ` ~ strType ~ ` value) {
    deleteMetadataStatement.bindAll("` ~ name ~ `");
    insertMetadataStatement.bindAll("` ~ name ~ `", value);

    scope(exit) {
      deleteMetadataStatement.reset();
      insertMetadataStatement.reset();
    }

    deleteMetadataStatement.execute;
    insertMetadataStatement.execute;
  }`;
}

///
private static string generateReadMetadataField(Type, string name)() {
  string strType = Type.stringof;

  return strType ~ ` ` ~ name ~ `() {
    getMetadataStatement.bind(":name", "` ~ name ~ `");
    scope(exit){
      getMetadataStatement.reset();
    }

    return getMetadataStatement.execute.oneValue!` ~ strType ~ `;
  }`;
}

///
class MBTilesReader {
  protected {
    Database db;
    Statement getDataStatement;
    Statement getMetadataStatement;
  }

  ///
  this(string path) {
    db = Database(path);

    getDataStatement = db.prepare("SELECT tile_data FROM tiles WHERE
      zoom_level=:zoom_level AND
      tile_column=:tile_column AND
      tile_row=:tile_row");

    getMetadataStatement = db.prepare("SELECT value FROM metadata WHERE name=:name");
  }

  mixin(generateReadMetadataField!(string, "name"));
  mixin(generateReadMetadataField!(string, "format"));
  mixin(generateReadMetadataField!(string, "bounds"));
  mixin(generateReadMetadataField!(int, "minzoom"));
  mixin(generateReadMetadataField!(int, "maxzoom"));
  mixin(generateReadMetadataField!(string, "attribution"));
  mixin(generateReadMetadataField!(string, "description"));
  mixin(generateReadMetadataField!(string, "type"));
  mixin(generateReadMetadataField!(string, "json"));

  /// Get raw tile data
  ubyte[] get(int zoom_level, int tile_column, int tile_row) {
    getDataStatement.bind(":zoom_level", zoom_level);
    getDataStatement.bind(":tile_column", tile_column);
    getDataStatement.bind(":tile_row", tile_row);
    scope(exit) getDataStatement.reset();

    auto result = getDataStatement.execute;

    if(result.empty) {
      return [];
    }

    return result.oneValue!(ubyte[]);
  }
}

/// Basic structure to access the tiles data
class MBTiles : MBTilesReader, ReadWriteMBTiles {
  protected {
    Statement insertDataStatement;
    Statement deleteDataStatement;

    Statement insertMetadataStatement;
    Statement deleteMetadataStatement;
  }

  ///
  this(string path) {
    bool initialize = !path.exists;

    if(initialize) {
      db = Database(path);

      db.run("CREATE TABLE metadata (name text, value text);");
      db.run("CREATE TABLE tiles (zoom_level integer, tile_column integer, tile_row integer, tile_data blob);");
      db.run("CREATE UNIQUE INDEX tile_index on tiles (zoom_level, tile_column, tile_row);");
    }

    super(path);

    insertDataStatement = db.prepare("INSERT INTO tiles
      (zoom_level, tile_column, tile_row, tile_data)
      values
      (:zoom_level, :tile_column, :tile_row, :tile_data)");

    deleteDataStatement = db.prepare("DELETE FROM tiles
      WHERE
      zoom_level=:zoom_level AND
      tile_column=:tile_column AND
      tile_row=:tile_row");

    insertMetadataStatement = db.prepare("INSERT INTO metadata (name, value) values (:name, :value)");
    deleteMetadataStatement = db.prepare("DELETE FROM metadata WHERE name=:name");
  }

  mixin(generateWriteMetadataField!(string, "name"));
  mixin(generateWriteMetadataField!(string, "format"));
  mixin(generateWriteMetadataField!(string, "bounds"));
  mixin(generateWriteMetadataField!(int, "minzoom"));
  mixin(generateWriteMetadataField!(int, "maxzoom"));
  mixin(generateWriteMetadataField!(string, "attribution"));
  mixin(generateWriteMetadataField!(string, "description"));
  mixin(generateWriteMetadataField!(string, "type"));
  mixin(generateWriteMetadataField!(string, "json"));

  /// Set raw tile data
  void set(int zoom_level, int tile_column, int tile_row, const ubyte[] data) {
    deleteDataStatement.bindAll(zoom_level, tile_column, tile_row);
    insertDataStatement.bindAll(zoom_level, tile_column, tile_row, data);

    scope(exit) {
      deleteDataStatement.reset();
      insertDataStatement.reset();
    }

    deleteDataStatement.execute;
    insertDataStatement.execute;
  }
}
/*
/// reading a tile
unittest {
  import geo.vector;

  scope tiles = new MBTiles("test/sample.mbtiles");
  auto decompressor = new UnCompress();
  auto data = tiles.get(14, 8749, 10_960);
  ubyte[] uncompressedData = cast(ubyte[]) decompressor.uncompress(data);

  uncompressedData.readTile.layers.length.should.equal(11);
}

/*
/// reading a missing tile
unittest {
  import geo.vector;

  scope tiles = new MBTiles("test/sample.mbtiles");
  auto data = tiles.get(1, 1, 1);

  data.length.should.equal(0);
}*/

/// reading a missing db file
unittest {
  import std.file;
  scope(exit) {
    if("test/new.mbtiles".exists) {
      "test/new.mbtiles".remove;
    }
  }

  scope tiles = new MBTiles("test/new.mbtiles");
  auto data = tiles.get(1, 1, 1);

  data.length.should.equal(0);
}

/// write data
unittest {
  import std.file;
  scope(exit) {
    if("test/new.mbtiles".exists) {
      "test/new.mbtiles".remove;
    }
  }

  scope tiles = new MBTiles("test/new.mbtiles");
  tiles.set(1, 1, 1, [1]);
  ubyte[] expected = [1];
  tiles.get(1, 1, 1).should.equal(expected);

  tiles.set(1, 1, 1, [2]);
  expected = [2];
  tiles.get(1, 1, 1).should.equal(expected);
}

/// structure to access the tiles data, by caching the uncompressed data
class CachedMBTiles : MBTiles, IVtTiles {
  private {
    Tile[ulong] cache;
    SysTime[ulong] times;

    SysTime lastFlush;
    UpdateStats flushStats;
    UpdateStats getStats;
    UpdateStats setStats;
  }

  ///
  this(string path) {
    super(path);

    db.run("PRAGMA synchronous = OFF");
    db.run("PRAGMA journal_mode = MEMORY");
    db.run("PRAGMA cache_size = 100000");

    lastFlush = Clock.currTime;
  }

  ///
  size_t cacheSize() {
    return cache.length;
  }

  ///
  Tile getVT(int zoom_level, int tile_column, int tile_row) {
    getStats.start;
    scope(exit) getStats.end;

    auto key = ZXY(zoom_level, tile_column, tile_row).hash;
    scope decompressor = new UnCompress;

    if(key !in cache) {
      ubyte[] uncompressedData = cast(ubyte[]) decompressor.uncompress(this.get(zoom_level, tile_column, tile_row).array);
      uncompressedData = cast(ubyte[]) decompressor.flush;

      cache[key] = readTile(uncompressedData);
      times[key] = SysTime.min;
    }

    return cache[key];
  }

  ///
  void setVT(int zoom_level, int tile_column, int tile_row, Tile tile) {
    setStats.start;
    scope(exit) setStats.end;

    auto key = ZXY(zoom_level, tile_column, tile_row).hash;

    cache[key] = tile;
    times[key] = Clock.currTime;
  }

  ///
  void flush() {
    flushStats.start;
    scope(exit) flushStats.end;

    auto now = Clock.currTime;
    auto lastFlushDiff = now - lastFlush;

    foreach(key, tile; cache) {
      if(now - times[key] > lastFlushDiff) {
        cache.remove(key);
        times.remove(key);
      }
    }

    auto buffer = cast(ubyte[]) Mallocator.instance.allocate(1024 * 1024 * 16);
    scope(exit )Mallocator.instance.deallocate(buffer);

    db.begin;
    size_t index;
    foreach(key, tile; cache) {
      auto pos = ZXY.fromHash(key);

      scope compressor = new Compress(HeaderFormat.gzip);

      auto size = tile.toProtoBuf(buffer);
      auto data = cast(ubyte[]) compressor.compress(buffer[0..size]);
      data ~= cast(ubyte[]) compressor.flush;

      this.set(pos.z, pos.x, pos.y, buffer[0..size]);
      index++;

      if(index >= 100) {
        db.commit;

        db.begin;
        index = 0;
      }
    }
    db.commit;

    lastFlush = Clock.currTime;
    cache.rehash;
    times.rehash;
  }

  string stringStats() {
    return "[flush] " ~ flushStats.toString ~ "\n[set] " ~ setStats.toString ~ "\n[get] " ~ getStats.toString ~ "\n";
  }
}


///
string membersToEnum(T)() {

  string members;

  static foreach (memberName; __traits(allMembers, T)) {{
    mixin("enum isVirtual = __traits(isVirtualMethod, T." ~ memberName ~ ");");

    static if(isVirtual) {
      static foreach (t; __traits(getOverloads, T, memberName)) {
        members ~= ", " ~ memberName ~ "_" ~ Parameters!t.length.to!string;
      }
    }
  }}

  return "enum Command { __unknown" ~ members ~ " }";
}

///
string membersToCommands(T)() {

  string members;

  static foreach (memberName; __traits(allMembers, T)) {{
    mixin("enum isVirtual = __traits(isVirtualMethod, T." ~ memberName ~ ");");

    static if(isVirtual) {
      members ~= "auto " ~ memberName ~ "(T...)(T params) { return this.call!(`" ~ memberName ~ "`)(params); };";
    }
  }}

  return members;
}

///
class ThreadAccess(T) : Thread {
  mixin(membersToEnum!T);
  mixin(membersToCommands!T);

  Command command;

  ///
  this(U...)(U params) {
    super({
      scope instance = T(params);
      this.run(instance);
    });
  }

  ///
  private void run(T instance)
  {
    static foreach (memberName; __traits(allMembers, T)) {{
      mixin("enum isVirtual = __traits(isVirtualMethod, T." ~ memberName ~ ");");

      static if(isVirtual) {
        static foreach (t; __traits(getOverloads, T, memberName)) {{
          mixin("enum c = Command." ~ memberName ~ "_" ~ Parameters!t.length.to!string ~ ";");

          if(this.command == c) {

          }
        }}
      }
    }}
  }

  private auto call(string memberName, U...)(U params) {

  }
}

/// structure to access the tiles data, by caching the uncompressed data
class ThreadedCachedMBTiles : ReadWriteMBTiles, IVtTiles {
  private {
    struct Pos { int z, x, y; }
    Tile[Pos] cache;
    ThreadAccess!CachedMBTiles threadedAccess;
  }

  ///
  this(string path) {
    //super(path);
  }

  ///
  Tile getVT(int zoom_level, int tile_column, int tile_row) {
    assert(false);
  }

  ///
  void setVT(int zoom_level, int tile_column, int tile_row, Tile tile) {
  }

  ///
  void flush() {
  }

  /// Get raw tile data
  ubyte[] get(int zoom_level, int tile_column, int tile_row) {
    assert(false, "not implemented");
  }

  ///
  string name() {
    assert(false, "not implemented");
  }

  ///
  string format() {
    assert(false, "not implemented");
  }

  ///
  string bounds() {
    assert(false, "not implemented");
  }

  ///
  int minzoom() {
    assert(false, "not implemented");
  }

  ///
  int maxzoom() {
    assert(false, "not implemented");
  }

  ///
  string attribution() {
    assert(false, "not implemented");
  }

  ///
  string description() {
    assert(false, "not implemented");
  }

  ///
  string type() {
    assert(false, "not implemented");
  }

  ///
  string json() {
    assert(false, "not implemented");
  }

  /// Set raw tile data
  void set(int zoom_level, int tile_column, int tile_row, const ubyte[] data) {
    assert(false, "not implemented");
  }
  ///
  void name(const string) {
    assert(false, "not implemented");
  }
  ///
  void format(const string) {
    assert(false, "not implemented");
  }
  ///
  void bounds(const string) {
    assert(false, "not implemented");
  }
  ///
  void minzoom(const int) {
    assert(false, "not implemented");
  }
  ///
  void maxzoom(const int) {
    assert(false, "not implemented");
  }
  ///
  void attribution(const string) {
    assert(false, "not implemented");
  }
  ///
  void description(const string) {
    assert(false, "not implemented");
  }
  ///
  void type(const string) {
    assert(false, "not implemented");
  }
  ///
  void json(const string) {
    assert(false, "not implemented");
  }

  string stringStats() {
    assert(false, "not implemented");
  }
}
