/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module geo.gpx;

import geo.xml;
import geo.epsg;

import std.conv;
import std.datetime;
import std.format;
import std.math;
import std.algorithm;
import std.traits;
import std.exception;
import std.array;

version(unittest) {
  import fluent.asserts;

  import std.string;
}

/// GPS Exchange Format v1.1
/// https://www.topografix.com/gpx.asp
struct GPX {
  /// The gpx version
  enum version_ = "1.1";

  ///
  enum mime = "application/gpx+xml";

  /// The name of the application which created the document
  string creator = "geo-d";

  /// Information about the GPX file, author, and copyright restrictions goes in the
  /// metadata section.
  Metadata metadata;

  /// Represents a waypoint, point of interest, or named feature on a map.
  WayPoint[] wpt;

  /// Represents route - an ordered list of waypoints representing a series of turn points leading to a destination.
  Route[] rte;

  /// Represents a track - an ordered list of points describing a path.
  Track[] trk;

  ///
  string xmlHeader() const {
    return `<?xml version="1.0" encoding="UTF-8" standalone="no" ?>`;
  }

  ///
  string openTag() const pure {
    return `<gpx xmlns="http://www.topografix.com/GPX/1/1" creator="` ~
      creator.escValue ~
      `" version="1.1" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">`;
  }

  ///
  string closeTag() const pure {
    return `</gpx>`;
  }

  /// Convert the struct to a file
  string toString() const {
    return xmlHeader ~ openTag ~
      metadata.toString ~
      wpt.map!(a => a.toString).join ~
      rte.map!(a => a.toString).join ~
      trk.map!(a => a.toString).join ~
      closeTag;
  }

  ///
  static GPX fromDom(D)(D dom) {
    if(dom.type != EntityType.elementStart || dom.name != "gpx") {
      foreach(node; dom.children) {
        if(node.name == "gpx") {
          return fromDom(node);
        }
      }
    }

    GPX gpx;

    foreach(attribute; dom.attributes) {
      if(attribute.name == "creator") {
        gpx.creator = attribute.value;
      }
    }

    foreach(node; dom.children) {
      if(node.name == "metadata") {
        gpx.metadata = Metadata.fromDom(node);
      }

      if(node.name == "wpt") {
        gpx.wpt ~= WayPoint.fromDom(node);
      }

      if(node.name == "rte") {
        gpx.rte ~= Route.fromDom(node);
      }

      if(node.name == "trk") {
        gpx.trk ~= Track.fromDom(node);
      }
    }

    return gpx;
  }
}

/// It should be able to create an empty gpx string
unittest {
  GPX gpx;

  gpx.toString().should.equal(`<?xml version="1.0" encoding="UTF-8" standalone="no" ?>` ~
  `<gpx xmlns="http://www.topografix.com/GPX/1/1" creator="geo-d" version="1.1" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">` ~
  `</gpx>`);

  gpx.creator = "some app";
  gpx.toString().should.equal(`<?xml version="1.0" encoding="UTF-8" standalone="no" ?>` ~
  `<gpx xmlns="http://www.topografix.com/GPX/1/1" creator="some app" version="1.1" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">` ~
  `</gpx>`);
}

/// When all fields have values it creates a gpx with all fields
unittest {
  GPX gpx;
  gpx.creator = "some creator";
  gpx.metadata.name = "some name";
  gpx.wpt ~= WayPoint(EPSG4326(2f, 1f));
  gpx.trk ~= Track("track name");
  gpx.rte ~= Route("route name");

  gpx.toString.should.equal(`<?xml version="1.0" encoding="UTF-8" standalone="no" ?>` ~
    `<gpx xmlns="http://www.topografix.com/GPX/1/1" creator="some creator" version="1.1" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">` ~
    `<metadata><name>some name</name></metadata>` ~
    `<wpt lat="1.0000000000000" lon="2.0000000000000"/>` ~
    `<rte><name>route name</name></rte>` ~
    `<trk><name>track name</name></trk>` ~
    `</gpx>`);
}

/// Convert dom nodes with gpx node to GPX struct
unittest {
  enum str = `<?xml version="1.0" encoding="UTF-8" standalone="no" ?>` ~
    `<gpx xmlns="http://www.topografix.com/GPX/1/1" creator="some creator" version="1.1" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">` ~
    `<metadata><name>some name</name></metadata>` ~
    `<wpt lat="1.0000000000000" lon="2.0000000000000"/>` ~
    `<rte><name>route name</name></rte>` ~
    `<trk><name>track name</name></trk>` ~
    `</gpx>`;

  auto dom = parseDOM(str);
  auto gpx = GPX.fromDom(dom.children[0]);

  gpx.toString.should.equal(str);
}

/// Information about the GPX file, author, and copyright restrictions goes in the metadata section.
/// Providing rich, meaningful information about your GPX files allows others to search for and use your GPS data.
struct Metadata {
  ///
  string name;

  ///
  string desc;

  /// A person or organization.
  Person author;

  /// Information about the copyright holder and any license governing use of this file.
  /// By linking to an appropriate license, you may place your data into the public domain or
  /// grant additional usage rights.
  Copyright copyright;

  /// Links to external resources (Web page, digital photo, video clip, etc) with additional information.
  Link[] link;

  ///
  SysTime time;

  ///
  string keywords;

  /// Two lat/lon pairs defining the extent of an element.
  Bounds bounds;

  /// You can add extend GPX by adding your own elements from another schema here.
  Extensions extensions;

  string toString() const {
    auto strAuthor = author.toString;
    auto strCopyright = copyright.toString;
    auto strLink = link.map!(a => a.toString).join;
    auto strTime = time.toXmlString;
    auto strBounds = bounds.toString;

    if(name == "" && desc == "" && strAuthor == "" && strCopyright == "" && strLink == "" &&
      strTime == "" && keywords == "" && strBounds == "") {
      return "";
    }

    string result = `<metadata>`;

    if(name != "") {
      result ~= format!(`<name>%s</name>`)(name.escValue);
    }

    if(desc != "") {
      result ~= format!(`<desc>%s</desc>`)(desc.escValue);
    }

    return result ~ strTime ~ strAuthor ~ strCopyright ~ strLink ~ strBounds ~ `</metadata>`;
  }

  ///
  static Metadata fromDom(D)(D dom) {
    Metadata metadata;

    metadata.fillDomProperties(dom);

    foreach(node; dom.children) {
      if(node.name == "person") {
        metadata.author = Person.fromDom(node);
      }

      if(node.name == "copyright") {
        metadata.copyright = Copyright.fromDom(node);
      }

      if(node.name == "bounds") {
        metadata.bounds = Bounds.fromDom(node);
      }
    }

    return metadata;
  }
}

/// Converted to a string will return an empty string when no value is set
unittest {
  Metadata metadata;
  metadata.toString.should.equal("");
}

/// Converted to a string when all fields are set it returns a tag with all fields
unittest {
  Metadata metadata;
  metadata.name = "some name";
  metadata.desc = "some desc";
  metadata.author.name = "some name";
  metadata.copyright.author = "some author";
  metadata.link ~= Link("some href");
  metadata.time = SysTime.fromISOExtString("2005-11-07T12:03:31Z");
  metadata.keywords = "a,b,c";
  metadata.bounds.minlat = 59.436;
  metadata.bounds.maxlat = 59.444;
  metadata.bounds.minlon = 24.743;
  metadata.bounds.maxlon = 24.797;

  metadata.to!string.should.equal(`<metadata>` ~
    `<name>some name</name>` ~
    `<desc>some desc</desc>` ~
    `<time>2005-11-07T12:03:31Z</time>` ~
    `<person><name>some name</name></person>` ~
    `<copyright author="some author"/>` ~
    `<link href="some href"/>` ~
    `<bounds minlat="59.4360000000000" maxlat="59.4440000000000" minlon="24.7430000000000" maxlon="24.7970000000000"/>` ~
    `</metadata>`);
}

/// Convert dom nodes with all attributes to a Metadata
unittest {
  enum str = `<metadata>` ~
    `<name>some name</name>` ~
    `<desc>some desc</desc>` ~
    `<time>2005-11-07T12:03:31Z</time>` ~
    `<person><name>some name</name></person>` ~
    `<copyright author="some author"/>` ~
    `<link href="some href"/>` ~
    `<bounds minlat="59.4360000000000" maxlat="59.4440000000000" minlon="24.7430000000000" maxlon="24.7970000000000"/>` ~
    `</metadata>`;

  auto dom = parseDOM(str);
  auto metadata = Metadata.fromDom(dom.children[0]);

  metadata.toString.should.equal(str);
}

/// A person or organization.
struct Person {
  ///
  string name;

  ///
  Email email;

  ///
  Link link;

  ///
  string toString() const {
    auto strEmail = email.toString;
    auto strLink = link.toString;

    if(name == "" && strEmail == "" && strLink == "") {
      return "";
    }

    string result = `<person>`;

    if(name) {
      result ~= format!(`<name>%s</name>`)(name.escValue);
    }

    return result ~ strEmail ~ strLink ~ `</person>`;
  }

  ///
  static Person fromDom(D)(D dom) {
    Person person;

    person.fillDomProperties(dom);

    foreach(node; dom.children) {
      if(node.name == "email") {
        person.email = Email.fromDom(node);
      }

      if(node.name == "link") {
        person.link = Link.fromDom(node);
      }
    }

    return person;
  }
}

/// Converted to a string will return an empty string when no value is set
unittest {
  Person person;
  person.to!string.should.equal("");
}

/// Converted to a string when the name is set it returns a tag with name
unittest {
  Person person;
  person.name = "some name";

  person.to!string.should.equal(`<person><name>some name</name></person>`);
}

/// Converted to a string when the email is set it returns a tag with email
unittest {
  Person person;
  person.email.id = "some id";
  person.email.domain = "some domain";

  person.to!string.should.equal(`<person><email id="some id" domain="some domain"/></person>`);
}

/// Converted to a string when the link is set it returns a tag with link
unittest {
  Person person;
  person.link.href = "some href";

  person.to!string.should.equal(`<person><link href="some href"/></person>`);
}

/// Convert dom nodes with all attributes to a Person
unittest {
  enum str = `<person><name>some name</name><email id="john" domain="doe.biz"/><link href="some href"/></person>`;

  auto dom = parseDOM(str);
  auto person = Person.fromDom(dom.children[0]);

  person.toString.should.equal(str);
}

/// An email address. Broken into two parts (id and domain) to help prevent email harvesting.
struct Email {
  ///
  string id;

  ///
  string domain;

  ///
  string toString() const {
    if(id == "" && domain == "") {
      return "";
    }

    return format!(`<email id="%s" domain="%s"/>`)(id.escValue, domain.escValue);
  }

  ///
  static Email fromDom(D)(D dom) {
    Email email;

    foreach(attribute; dom.attributes) {
      if(attribute.name == "id") {
        email.id = attribute.value;
      }

      if(attribute.name == "domain") {
        email.domain = attribute.value;
      }
    }

    return email;
  }
}

/// Converted to a string will return an empty string when no value is set
unittest {
  Email email;

  email.to!string.should.equal("");
}

/// Converted to a string when the id and domain are set it will return a tag with the attributes
unittest {
  Email email;
  email.id = "mikes";
  email.domain = "ayeltd.biz";

  email.to!string.should.equal(`<email id="mikes" domain="ayeltd.biz"/>`);
}

/// Convert dom nodes with all attributes to a Email
unittest {
  enum str = `<email id="mikes" domain="ayeltd.biz"/>`;

  auto dom = parseDOM(str);
  auto email = Email.fromDom(dom.children[0]);

  email.toString.should.equal(str);
}

/// A link to an external resource (Web page, digital photo, video clip, etc) with additional information.
struct Link {
  ///
  string href;

  ///
  string text;

  ///
  string type;

  string toString() const {
    if(href == "" && text == "" && type == "") {
      return "";
    }

    if(href != "" && text == "" && type == "") {
      return format!(`<link href="%s"/>`)(href.escValue);
    }

    string result = format!(`<link href="%s">`)(href.escValue);

    if(text != "") {
      result ~= format!(`<text>%s</text>`)(text.escValue);
    }

    if(type != "") {
      result ~= format!(`<type>%s</type>`)(type.escValue);
    }

    return result ~ `</link>`;
  }

  ///
  static Link fromDom(D)(D dom) {
    Link link;

    foreach(attribute; dom.attributes) {
      if(attribute.name == "href") {
        link.href = attribute.value;
      }
    }

    link.fillDomProperties(dom);

    return link;
  }
}

/// Converted to a string will return an empty string when no value is set
unittest {
  Link link;

  link.to!string.should.equal("");
}

/// Converted to a string when the href is set it will return a tag
unittest {
  Link link;
  link.href = "http://www.ayeltd.biz";

  link.to!string.should.equal(`<link href="http://www.ayeltd.biz"/>`);
}

/// Converted to a string when the href and text is set it will return a tag with text
unittest {
  Link link;
  link.href = "http://www.ayeltd.biz";
  link.text = "some text";

  link.to!string.should.equal(`<link href="http://www.ayeltd.biz"><text>some text</text></link>`);
}

/// Converted to a string when the href and type is set it will return a tag with type
unittest {
  Link link;
  link.href = "http://www.ayeltd.biz";
  link.type = "some type";

  link.to!string.should.equal(`<link href="http://www.ayeltd.biz"><type>some type</type></link>`);
}

/// Convert a simple dom node to a link
unittest {
  enum str = `<link href="http://www.ayeltd.biz"/>`;
  auto dom = parseDOM(str);
  auto link = Link.fromDom(dom.children[0]);

  link.toString.should.equal(str);
}

/// Convert a dom node with all fields to a link
unittest {
  enum str = `<link href="http://www.ayeltd.biz"><text>some text</text><type>some type</type></link>`;
  auto dom = parseDOM(str);
  auto link = Link.fromDom(dom.children[0]);

  link.toString.should.equal(str);
}

/// Information about the copyright holder and any license governing use of this file. By linking to
/// an appropriate license, you may place your data into the public domain or grant additional usage rights.
struct Copyright {
  ///
  string author;

  ///
  long year;

  ///
  string license;

  string toString() const {
    if(author == "" && year == 0 && license == "") {
      return "";
    }

    if(author != "" && year == 0 && license == "") {
      return format!(`<copyright author="%s"/>`)(author.escValue);
    }

    string result = format!(`<copyright author="%s">`)(author.escValue);

    if(year) {
      result ~= format!(`<year>%s</year>`)(year);
    }

    if(license) {
      result ~= format!(`<license>%s</license>`)(license.escValue);
    }

    return result ~ `</copyright>`;
  }

  ///
  static Copyright fromDom(T)(T dom) {
    Copyright copyright;

    foreach(attribute; dom.attributes) {
      if(attribute.name == "author") {
        copyright.author = attribute.value;
      }
    }

    copyright.fillDomProperties(dom);

    return copyright;
  }
}

/// Converted to a string will return an empty string when no value is set
unittest {
  Copyright copyright;

  copyright.to!string.should.equal("");
}

/// Converted to a string when the author is set will return a tag with author
unittest {
  Copyright copyright;
  copyright.author = "me";

  copyright.to!string.should.equal(`<copyright author="me"/>`);
}

/// Converted to a string when an author and year are set will return a tag with author and year
unittest {
  Copyright copyright;
  copyright.author = "me";
  copyright.year = 2020;

  copyright.to!string.should.equal(`<copyright author="me"><year>2020</year></copyright>`);
}

/// Converted to a string when an author and license are set will return a tag with author and license
unittest {
  Copyright copyright;
  copyright.author = "me";
  copyright.license = "some license";

  copyright.to!string.should.equal(`<copyright author="me"><license>some license</license></copyright>`);
}

/// Convert dom nodes with author to a Copyright
unittest {
  enum str = `<copyright author="me"/>`;

  auto dom = parseDOM(str);
  auto copyright = Copyright.fromDom(dom.children[0]);

  copyright.toString.should.equal(str);
}

/// Convert dom nodes with all fields to a Copyright
unittest {
  enum str = `<copyright author="me"><license>some license</license></copyright>`;

  auto dom = parseDOM(str);
  auto copyright = Copyright.fromDom(dom.children[0]);

  copyright.toString.should.equal(str);
}

/// Two lat/lon pairs defining the extent of an element.
struct Bounds {
  ///
  double minlat;

  ///
  double maxlat;

  ///
  double minlon;

  ///
  double maxlon;

  ///
  string toString() const {
    if(minlat.isNaN && maxlat.isNaN && minlon.isNaN && maxlon.isNaN) {
      return ``;
    }

    return format!(`<bounds minlat="%3.13f" maxlat="%3.13f" minlon="%3.13f" maxlon="%3.13f"/>`)(
      minlat, maxlat, minlon, maxlon
    );
  }

  ///
  static Bounds fromDom(T)(T dom) {
    Bounds bounds;

    foreach(attribute; dom.attributes) {
      if(attribute.name == "minlat") {
        bounds.minlat = attribute.value.to!double;
      }

      if(attribute.name == "maxlat") {
        bounds.maxlat = attribute.value.to!double;
      }

      if(attribute.name == "minlon") {
        bounds.minlon = attribute.value.to!double;
      }

      if(attribute.name == "maxlon") {
        bounds.maxlon = attribute.value.to!double;
      }
    }

    return bounds;
  }
}

/// Converted to a string will return an empty string when no value is set
unittest {
  Bounds bounds;

  bounds.toString.should.equal("");
}

/// Converted to a string will return a the xml tag when the values are set
unittest {
  auto bounds = Bounds(59.4367664166667, 59.4440920666666, 24.74394385, 24.7971432);

  bounds.toString.should.equal(`<bounds minlat="59.4367664166667" maxlat="59.4440920666666" minlon="24.7439438500000" maxlon="24.7971432000000"/>`);
}

/// Convert dom nodes with all fields to a waypoint
unittest {
  enum str = `<bounds minlat="59.4367664166667" maxlat="59.4440920666666" minlon="24.7439438500000" maxlon="24.7971432000000"/>`;

  auto dom = parseDOM(str);
  auto bounds = Bounds.fromDom(dom.children[0]);

  bounds.toString.should.equal(str);
}

/// You can add extend GPX by adding your own elements from another schema here.
struct Extensions {
  string toString() const {
    return "";
  }
}

/// Type of GPS fix. none means GPS had no fix. To signify "the fix info is unknown, leave out fixType entirely. pps = military signal used
enum Fix : string {
  ///
  unset = "",

  ///
  none = "none",

  ///
  _2d = "2d",

  ///
  _3d = "3d",

  /// Differential GPS
  dgps = "dgps",

  /// military signal used
  pps = "pps"
}

/// Represents a waypoint, point of interest, or named feature on a map.
struct WayPoint {

  ///
  EPSG4326 position;

  ///
  string name;

  /// GPS waypoint comment. Sent to GPS as comment.
  string cmt;

  /// A text description of the element. Holds additional information about the element intended for the user, not the GPS.
  string desc;

  /// Source of data. Included to give user some idea of reliability and accuracy of data. "Garmin eTrex", "USGS quad Boston North", e.g.
  string src;

  /// A link to an external resource (Web page, digital photo, video clip, etc) with additional information.
  Link[] link;

  /// Elevation (in meters) of the point.
  double ele;

  /// Creation/modification timestamp for element. Date and time in are in Univeral Coordinated Time (UTC), not local time!
  /// Conforms to ISO 8601 specification for date/time representation. Fractional seconds are allowed for millisecond timing in tracklogs.
  SysTime time;

  /// Magnetic variation (in degrees) at the point
  /// https://en.wikipedia.org/wiki/Magnetic_declination
  double magvar;

  /// Height (in meters) of geoid (mean sea level) above WGS84 earth ellipsoid. As defined in NMEA GGA message.
  double geoidheight;

  /// Text of GPS symbol name. For interchange with other programs, use the exact spelling of the symbol as displayed on
  /// the GPS. If the GPS abbreviates words, spell them out.
  string sym;

  /// Type (classification) of the waypoint.
  string type;

  /// Type of GPX fix.
  Fix fix;

  /// Number of satellites used to calculate the GPX fix.
  ulong sat;

  /// Horizontal dilution of precision.
  double hdop;

  /// Vertical dilution of precision.
  double vdop;

  /// Position dilution of precision.
  double pdop;

  /// Number of seconds since last DGPS update.
  double ageofdgpsdata;

  /// ID of DGPS station used in differential correction.
  ulong dgpsid;

  ///
  string toString() const {
    return toCustomString!"wpt";
  }

  ///
  string toCustomString(string tag)() const {
    if(position.latitude.isNaN && position.longitude.isNaN) {
      return "";
    }

    string result = format!(`<` ~ tag ~ ` lat="%3.13f" lon="%3.13f"`)(position.latitude, position.longitude);

    string content = this.toXmlString();

    if(fix != Fix.unset) {
      content ~= format!(`<fix>%s</fix>`)(cast(string) fix);
    }

    content ~= time.toXmlString;

    enum closeTag = `</` ~ tag ~ `>`;

    result ~= content.length ? (">" ~ content ~ closeTag) : `/>`;

    return result;
  }

  ///
  static WayPoint fromDom(T)(T dom) {
    WayPoint wayPoint;

    foreach(attribute; dom.attributes) {
      if(attribute.name == "lon") {
        wayPoint.position.longitude = attribute.value.to!double;
      }

      if(attribute.name == "lat") {
        wayPoint.position.latitude = attribute.value.to!double;
      }
    }

    wayPoint.fillDomProperties(dom);

    return wayPoint;
  }
}

/// Converted to a string will return an empty string when no value is set
unittest {
  WayPoint wayPoint;
  wayPoint.toString.should.equal("");
}

/// Converted to a string when the position is set, it will return a tag with lat and long
unittest {
  WayPoint wayPoint;
  wayPoint.position.latitude = 46.57638889;
  wayPoint.position.longitude = 8.89263889;

  wayPoint.toString.should.equal(`<wpt lat="46.5763888900000" lon="8.8926388900000"/>`);
}

/// Converted to a string when the position and name are set, it will return a tag with lat, long and name
unittest {
  WayPoint wayPoint;
  wayPoint.position.latitude = 46.57638889;
  wayPoint.position.longitude = 8.89263889;
  wayPoint.name = "some name";

  wayPoint.toString.should.equal(`<wpt lat="46.5763888900000" lon="8.8926388900000"><name>some name</name></wpt>`);
}

/// Converted to a string when all the fields are set, it will return a tag with all the fields
unittest {
  WayPoint wayPoint;
  wayPoint.position.latitude = 46.57638889;
  wayPoint.position.longitude = 8.89263889;
  wayPoint.name = "some name";
  wayPoint.cmt = "some cmt";
  wayPoint.desc = "some desc";
  wayPoint.src = "some src";
  wayPoint.sym = "some sym";
  wayPoint.type = "some type";

  wayPoint.ele = 0;
  wayPoint.magvar = 1;
  wayPoint.geoidheight = 2;
  wayPoint.hdop = 3;
  wayPoint.vdop = 4;
  wayPoint.pdop = 5;
  wayPoint.ageofdgpsdata = 6;

  wayPoint.sat = 7;
  wayPoint.dgpsid = 8;

  wayPoint.link ~= Link("some link");
  wayPoint.time = SysTime.fromISOExtString("2005-11-07T12:03:31Z");
  wayPoint.fix = Fix._2d;

  wayPoint.toString.should.equal(`<wpt lat="46.5763888900000" lon="8.8926388900000">` ~
    `<name>some name</name>` ~
    `<cmt>some cmt</cmt>` ~
    `<desc>some desc</desc>` ~
    `<src>some src</src>` ~
    `<link href="some link"/>` ~
    `<ele>0</ele>` ~
    `<magvar>1</magvar>` ~
    `<geoidheight>2</geoidheight>` ~
    `<sym>some sym</sym>` ~
    `<type>some type</type>` ~
    `<sat>7</sat>` ~
    `<hdop>3</hdop>` ~
    `<vdop>4</vdop>` ~
    `<pdop>5</pdop>` ~
    `<ageofdgpsdata>6</ageofdgpsdata>` ~
    `<dgpsid>8</dgpsid>` ~
    `<fix>2d</fix>` ~
    `<time>2005-11-07T12:03:31Z</time>` ~
    `</wpt>`);
}

/// Convert dom nodes with lon lat to a track point
unittest {
  enum str = `<trkpt lat="1.0000000000000" lon="2.0000000000000"/>`;
  auto dom = parseDOM(str);
  auto wayPoint = WayPoint.fromDom(dom.children[0]);

  wayPoint.toCustomString!"trkpt".should.equal(str);
}

/// Convert dom nodes with all fields to a waypoint
unittest {
  enum str = `<wpt lat="46.5763888900000" lon="8.8926388900000">` ~
    `<name>some name</name>` ~
    `<cmt>some cmt</cmt>` ~
    `<desc>some desc</desc>` ~
    `<src>some src</src>` ~
    `<link href="some link"/>` ~
    `<link href="some link"><text>some text</text></link>` ~
    `<ele>0</ele>` ~
    `<magvar>1</magvar>` ~
    `<geoidheight>2</geoidheight>` ~
    `<sym>some sym</sym>` ~
    `<type>some type</type>` ~
    `<sat>7</sat>` ~
    `<hdop>3</hdop>` ~
    `<vdop>4</vdop>` ~
    `<pdop>5</pdop>` ~
    `<ageofdgpsdata>6</ageofdgpsdata>` ~
    `<dgpsid>8</dgpsid>` ~
    `<fix>2d</fix>` ~
    `<time>2005-11-07T12:03:31Z</time>` ~
    `</wpt>`;

  auto dom = parseDOM(str);
  auto wayPoint = WayPoint.fromDom(dom.children[0]);

  wayPoint.toCustomString!"wpt".should.equal(str);
}

/// Represents route - an ordered list of waypoints representing a series of turn points leading to a destination.
struct Route {
  /// GPS name of route.
  string name;

  /// GPS comment for route.
  string cmt;

  /// Text description of route for user. Not sent to GPS.
  string desc;

  /// Text description of route for user. Not sent to GPS.
  string src;

  /// Links to external information about the route.
  Link[] link;

  /// GPS route number.
  ulong number;

  /// Type (classification) of route.
  string type;

  /// A list of route points.
  WayPoint[] rtept;

  ///
  string toString() const {
    string content;

    content ~= this.toXmlString();
    content ~= rtept.map!(a => a.toCustomString!"rtept").join;

    if(content == "") {
      return "";
    }

    return `<rte>` ~ content ~ `</rte>`;
  }

  ///
  static Route fromDom(T)(T dom) {
    Route route;

    enforce(dom.type == EntityType.elementStart, "You must send a `rte` node.");
    enforce(dom.name == "rte", "You must send a `rte` node.");

    route.fillDomProperties(dom);

    return route;
  }
}

/// Converted to a string will return an empty string when no value is set
unittest {
  Route route;
  route.toString.should.equal("");
}

/// Converted to a string when the name is set will return a route tag with the name
unittest {
  Route route;
  route.name = "some name";

  route.toString.should.equal(`<rte><name>some name</name></rte>`);
}

/// Converted to a string when all fields are set, it should return a route tag with all members
unittest {
  Route route;
  route.name = "some name";
  route.cmt = "some cmt";
  route.desc = "some desc";
  route.src = "some src";
  route.link ~= Link("some link");
  route.number = 1;
  route.type = "some type";
  route.rtept = [ WayPoint(EPSG4326(2., 1.)), WayPoint(EPSG4326(4., 3.)) ];

  route.toString.should.equal(`<rte>` ~
    `<name>some name</name>` ~
    `<cmt>some cmt</cmt>` ~
    `<desc>some desc</desc>` ~
    `<src>some src</src>` ~
    `<link href="some link"/>` ~
    `<number>1</number>` ~
    `<type>some type</type>` ~
    `<rtept lat="1.0000000000000" lon="2.0000000000000"/>` ~
    `<rtept lat="3.0000000000000" lon="4.0000000000000"/>` ~
    `</rte>`);
}

/// Convert dom nodes to a route
unittest {
  enum str = `<rte>` ~
    `<name>some name</name>` ~
    `<cmt>some cmt</cmt>` ~
    `<desc>some desc</desc>` ~
    `<src>some src</src>` ~
    `<link href="some link"/>` ~
    `<number>1</number>` ~
    `<type>some type</type>` ~
    `<rtept lat="1.0000000000000" lon="2.0000000000000"/>` ~
    `<rtept lat="3.0000000000000" lon="4.0000000000000"/>` ~
    `</rte>`;

  auto dom = parseDOM(str);
  auto route = Route.fromDom(dom.children[0]);

  route.toString.should.equal(str);
}

/// Represents a track - an ordered list of points describing a path.
struct Track {
  /// GPS name of track.
  string name;

  /// GPS comment for track.
  string cmt;

  /// User description of track.
  string desc;

  /// Source of data. Included to give user some idea of reliability and accuracy of data.
  string src;

  /// Type (classification) of track.
  string type;

  /// Links to external information about track.
  Link[] link;

  /// GPS track number.
  ulong number;

  /// A Track Segment holds a list of Track Points which are logically connected in order. To
  /// represent a single GPS track where GPS reception was lost, or the GPS receiver was turned
  /// off, start a new Track Segment for each continuous span of track data.
  TrackSegment[] trkseg;

  ///
  string toString() const {
    string content;

    content ~= this.toXmlString();

    if(content == "") {
      return "";
    }

    return `<trk>` ~ content ~ `</trk>`;
  }

  ///
  static Track fromDom(T)(T dom) {
    Track track;

    enforce(dom.type == EntityType.elementStart, "You must send a `trk` node.");
    enforce(dom.name == "trk", "You must send a `trk` node.");

    track.fillDomProperties(dom);

    return track;
  }
}

/// Converted to a string will return an empty string when no value is set
unittest {
  Track track;
  track.toString.should.equal("");
}

/// Converted to a string when the name is set it will return the track with a name tag
unittest {
  Track track;
  track.name = "Example gpx";
  track.toString.should.equal(`<trk><name>Example gpx</name></trk>`);
}

/// Converted to a string when all fields are set, it will return the track with all fields
unittest {
  Track track;
  track.name = "Example gpx";
  track.cmt = "some comment";
  track.desc = "some desc";
  track.src = "some src";
  track.type = "some type";
  track.number = 1;

  track.link ~= Link("this is not a link");
  track.trkseg ~= TrackSegment([ WayPoint(EPSG4326(22., 11.)), WayPoint(EPSG4326(44., 33.)) ]);

  track.toString.should.equal(`<trk>` ~
    `<name>Example gpx</name>` ~
    `<cmt>some comment</cmt>` ~
    `<desc>some desc</desc>` ~
    `<src>some src</src>` ~
    `<type>some type</type>` ~
    `<link href="this is not a link"/>` ~
    `<number>1</number>` ~
    `<trkseg>`~
    `<trkpt lat="11.0000000000000" lon="22.0000000000000"/>` ~
    `<trkpt lat="33.0000000000000" lon="44.0000000000000"/>` ~
    `</trkseg>`~
    `</trk>`);
}

/// Convert dom nodes to a track
unittest {
  enum str = `<trk>` ~
    `<name>Example gpx</name>` ~
    `<cmt>some comment</cmt>` ~
    `<desc>some desc</desc>` ~
    `<src>some src</src>` ~
    `<type>some type</type>` ~
    `<link href="this is not a link"/>` ~
    `<number>1</number>` ~
    `<trkseg>`~
    `<trkpt lat="11.0000000000000" lon="22.0000000000000"/>` ~
    `<trkpt lat="33.0000000000000" lon="44.0000000000000"/>` ~
    `</trkseg>`~
    `</trk>`;

  auto dom = parseDOM(str);
  auto track = Track.fromDom(dom.children[0]);

  track.toString.should.equal(str);
}

/// A Track Segment holds a list of Track Points which are logically connected in order.
struct TrackSegment {
  ///
  WayPoint[] trkpt;

  ///
  string toString() const {
    auto content = trkpt.map!(a => a.toCustomString!"trkpt").join;

    if(content == "") {
      return "";
    }

    return `<trkseg>` ~ content ~ `</trkseg>`;
  }

  ///
  static TrackSegment fromDom(T)(T dom) {
    TrackSegment segment;

    enforce(dom.type == EntityType.elementStart, "You must send a `trkseg` node.");
    enforce(dom.name == "trkseg", "You must send a `trkseg` node.");

    foreach(node; dom.children) {
      if(node.type == EntityType.elementStart || node.type == EntityType.elementEmpty) {
        segment.trkpt ~= WayPoint.fromDom(node);
      }
    }

    return segment;
  }
}

/// Converted to a string will return an empty string when no value is set
unittest {
  TrackSegment segment;
  segment.toString.should.equal("");
}

/// Converted to a string will return an empty string when no value is set
unittest {
  TrackSegment segment;
  segment.trkpt = [ WayPoint(EPSG4326(2., 1.)), WayPoint(EPSG4326(4., 3.)) ];
  segment.toString.should.equal(`<trkseg><trkpt lat="1.0000000000000" lon="2.0000000000000"/><trkpt lat="3.0000000000000" lon="4.0000000000000"/></trkseg>`);
}

/// Convert dom nodes to a track segment
unittest {
  enum str = `<trkseg><trkpt lat="1.0000000000000" lon="2.0000000000000"/><trkpt lat="3.0000000000000" lon="4.0000000000000"/></trkseg>`;
  auto dom = parseDOM(str);
  auto segment = TrackSegment.fromDom(dom.children[0]);

  segment.toString.should.equal(str);
}