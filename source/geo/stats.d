module geo.stats;

import std.datetime;
import std.stdio;
import std.conv;

struct UpdateStats {
  size_t updates;
  Duration avg;
  Duration total;
  SysTime startTime;

  void start() {
    updates++;
    startTime = Clock.currTime;
  }

  void end() {
    auto diff = Clock.currTime - startTime;
    total += diff;
    avg = (avg + diff) / 2;
  }

  string toString() {
    return "updates:" ~ updates.to!string ~ " avg:" ~ avg.to!string ~ " total:" ~ total.to!string;
  }
}