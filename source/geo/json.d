/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module geo.json;

public import geo.geometries;

import std.variant;
import std.meta;
import std.exception;
import std.algorithm;
import std.conv;

import geo.conv;
import geo.algorithm : centroid, areaMeters, metricDistance;

import std.math;
import vibe.data.json;
import vibe.data.bson;

version(unittest) import fluent.asserts;

alias GeoTypes = AliasSeq!(Point, MultiPoint, LineString, MultiLineString, Polygon, MultiPolygon);

///
Json toJson(GeometryVariant value) @trusted {
  alias Types = AliasSeq!(Point, MultiPoint, LineString, MultiLineString, Polygon, MultiPolygon);

  static foreach(Type; Types) {
    if(value.peek!Type !is null) {
      auto result = value.get!Type.serializeToJson;
      result["type"] = Type.stringof;

      return result;
    }
  }

  if(value.peek!GeometryCollection !is null) {
      auto v = value.get!GeometryCollection;

      auto result = Json.emptyObject;
      result["type"] = "GeometryCollection";
      result["geometries"] = Json.emptyArray;

      foreach (geometry; v.geometries) {
        result["geometries"].appendArrayElement(geometry.toJson);
      }

      return result;
  }

  return Json(null);
}

///
Json toJson(ref const(BasicGeometryVariant) value) @trusted {
  alias Types = AliasSeq!(Point, MultiPoint, LineString, MultiLineString, Polygon, MultiPolygon);

  if(!value.hasValue) {
    return Json(null);
  }

  static foreach(Type; Types) {
    if(value.peek!Type !is null) {
      auto result = value.get!Type.serializeToJson;
      result["type"] = Type.stringof;

      return result;
    }
  }

  return Json(null);
}

///
Json toJson(const(BasicGeometryVariant) value) @trusted {
  alias Types = AliasSeq!(Point, MultiPoint, LineString, MultiLineString, Polygon, MultiPolygon);

  if(!value.hasValue) {
    return Json(null);
  }

  static foreach(Type; Types) {
    if(value.peek!Type !is null) {
      auto result = value.get!Type.serializeToJson;
      result["type"] = Type.stringof;

      return result;
    }
  }

  return Json(null);
}

///
BasicGeometryVariant toBasicGeometryVariant(const ref Json value) @trusted {
  BasicGeometryVariant result;
  alias Types = AliasSeq!(Point, MultiPoint, LineString, MultiLineString, Polygon, MultiPolygon);

  static foreach(Type; Types) {
    if(value["type"].to!string == Type.stringof) {
     result = Type(value["coordinates"].deserializeJson!(typeof(Type.coordinates)));
    }
  }

  return result;
}

///
BasicGeometryVariant toBasicGeometryVariant(const Json value) @trusted {
  BasicGeometryVariant result;
  alias Types = AliasSeq!(Point, MultiPoint, LineString, MultiLineString, Polygon, MultiPolygon);

  static foreach(Type; Types) {
    if(value["type"].to!string == Type.stringof) {
     result = Type(value["coordinates"].deserializeJson!(typeof(Type.coordinates)));
    }
  }

  return result;
}


///
BasicGeometryVariant toBasicGeometryVariant(const ref Bson value) @trusted {
  BasicGeometryVariant result;

  static foreach(Type; GeoTypes) {
    if(value["type"].get!string == Type.stringof) {
     result = Type(value["coordinates"].deserializeBson!(typeof(Type.coordinates)));
    }
  }

  return result;
}

///
GeometryVariant toGeometryVariant(const Json value) @trusted {
  GeometryVariant result;

  if(value.type != Json.Type.object) {
    return result;
  }

  alias Types = AliasSeq!(Point, MultiPoint, LineString, MultiLineString, Polygon, MultiPolygon);

  static foreach(Type; Types) {
    if(value["type"].to!string == Type.stringof) {
      result = Type(value["coordinates"].deserializeJson!(typeof(Type.coordinates)));
    }
  }

  if(value["type"].to!string == "GeometryCollection") {
    BasicGeometryVariant[] geometries;

    foreach(item; value["geometries"]) {
      geometries ~= item.toBasicGeometryVariant;
    }

    result = GeometryCollection(geometries);
  }

  return result;
}

///
Json toJson(Variant value) {
  if(value.peek!bool !is null) {
    return Json(value.get!bool);
  }

  if(value.peek!double !is null) {
    return Json(value.get!double);
  }

  if(value.peek!long !is null) {
    return Json(value.get!long);
  }

  if(value.peek!ulong !is null) {
    return Json(value.get!ulong);
  }

  if(value.peek!string !is null) {
    return Json(value.get!string);
  }

  return Json();
}

///
Variant toVariant(const Json value) @trusted {
  switch (value.type) {
    case Json.Type.bool_:
      return Variant(value.to!bool);

    case Json.Type.bigInt:
    case Json.Type.int_:
      return Variant(value.to!long);

    case Json.Type.string:
      return Variant(value.to!string);

    case Json.Type.float_:
      return Variant(value.to!double);

    default:
      throw new Exception("Can not convert json to variant");
  }
}

/// Structure that facilitates interaction with GeoJson geometries
/// Point, LineString and Polygon
struct GeoJsonGeometry {
  private BasicGeometryVariant data;

  ///
  string type() inout @trusted {
    alias Types = AliasSeq!(Point, MultiPoint, LineString, MultiLineString, Polygon, MultiPolygon);

    static foreach(Type; Types) {
      if(data.peek!Type !is null) {
        return Type.stringof;
      }
    }

    return "Unknown";
  }

  ///
  Json coordinates() inout @trusted {
    alias Types = AliasSeq!(Point, MultiPoint, LineString, MultiLineString, Polygon, MultiPolygon);

    static foreach(Type; Types) {
      if(data.peek!Type !is null) {
        return data.get!Type.coordinates.serializeToJson;
      }
    }

    return Json.emptyArray;
  }

  ///
  T to(T)() @trusted {
    return data.get!T;
  }

  ///
  Json toJson() const @safe {
    return data.toJson;
  }

  ///
  void set(T)(T value) {
    data = value;
  }

  static foreach(Type; GeoTypes) {
    void opAssign(Type p) {
      data = p;
    }
  }

  ///
  static GeoJsonGeometry fromBson(const ref Bson src) @safe {
    return GeoJsonGeometry(src.toBasicGeometryVariant);
  }

  ///
  static GeoJsonGeometry fromJson(const ref Json src) @safe {
    return GeoJsonGeometry(src.toBasicGeometryVariant);
  }

  ///
  static GeoJsonGeometry fromJson(Json src) @safe {
    return GeoJsonGeometry(src.toBasicGeometryVariant);
  }

  static GeoJsonGeometry nullPoint() {
    return GeoJsonGeometry.fromJson(`{ "type": "Point", "coordinates": [0, 0] }`.parseJsonString);
  }
}

/// It should deserialize and serialize a Point
unittest {
  Json data = `{
    "type": "Point",
    "coordinates": [30.1, 10.1]
  }`.parseJsonString;

  auto geoJson = data.deserializeJson!GeoJsonGeometry;

  geoJson.serializeToJson.should.equal(data);

  geoJson.type.should.equal("Point");

  geoJson.coordinates[0].to!double.should.equal(30.1);
  geoJson.coordinates[1].to!double.should.equal(10.1);

  geoJson.to!Point.should.equal(Point(Position(30.1, 10.1)));
  geoJson.to!LineString.should.throwAnyException;
  geoJson.to!Polygon.should.throwAnyException;
}

/// It should deserialize and serialize a LineString
unittest {
  Json data = `{
    "type": "LineString",
    "coordinates": [[30.1, 10.1], [10.1, 30.1], [40.1, 40.1]]}`.parseJsonString;

  auto geoJson = data.deserializeJson!GeoJsonGeometry;

  geoJson.serializeToJson.should.equal(data);

  geoJson.type.should.equal("LineString");

  geoJson.coordinates[0][0].to!double.should.equal(30.1);
  geoJson.coordinates[0][1].to!double.should.equal(10.1);

  geoJson.to!Point.should.throwAnyException;
  geoJson.to!LineString.should.equal(LineString([[30.1, 10.1], [10.1, 30.1], [40.1, 40.1]].toPositions));
  geoJson.to!Polygon.should.throwAnyException;
}

/// It should deserialize and serialize a Polygon
unittest {
  Json data = `{
    "type": "Polygon",
    "coordinates": [
        [[30.1, 10.1], [40.1, 40.1], [20.1, 40.1], [10.1, 20.1], [30.1, 10.1]]
    ]}`.parseJsonString;

  auto geoJson = data.deserializeJson!GeoJsonGeometry;

  geoJson.serializeToJson.should.equal(data);

  geoJson.type.should.equal("Polygon");

  geoJson.coordinates[0][0][0].to!double.should.equal(30.1);
  geoJson.coordinates[0][0][1].to!double.should.equal(10.1);

  geoJson.to!Point.should.throwAnyException;
  geoJson.to!LineString.should.throwAnyException;
  geoJson.to!Polygon.coordinates.to!string.should.equal(`[[[30.1, 10.1], [40.1, 40.1], [20.1, 40.1], [10.1, 20.1], [30.1, 10.1]]]`);
}

///
void normalizeWgs84(ref GeoJsonGeometry source) {
  if(source.type == "Point") {
    auto point = source.to!Point;

    auto lon = point.coordinates[0];

    double distance;

    if(lon < -180) {
      distance = lon - 180;
    }
    else if (lon > 180) {
      distance = lon + 180;
    }

    if(!isNaN(distance)) {
      lon -= trunc(distance/360) * 360;
    }

    point.coordinates = PositionT!double(lon, point.coordinates[1]);

    source.set(point);
  }
}

/// it does not change coordinates when lon is 0
unittest {
  auto point1 = `{ "type": "Point", "coordinates": [ 0.0, 12.05680725475381 ] }`
    .parseJsonString
    .deserializeJson!GeoJsonGeometry;

  point1.normalizeWgs84;
  point1.serializeToJson["coordinates"].to!string
    .should
    .equal(`[0,12.05680725475381]`);
}

/// it does not change coordinates when lon is 180
unittest {
  auto point1 = `{ "type": "Point", "coordinates": [ 180.0, 12.05680725475381 ] }`
    .parseJsonString
    .deserializeJson!GeoJsonGeometry;

  point1.normalizeWgs84;
  point1.serializeToJson["coordinates"].to!string
    .should
    .equal(`[180,12.05680725475381]`);
}

/// it does not change coordinates when lon is -180
unittest {
  auto point1 = `{ "type": "Point", "coordinates": [ -180.0, 12.05680725475381 ] }`
    .parseJsonString
    .deserializeJson!GeoJsonGeometry;

  point1.normalizeWgs84;
  point1.serializeToJson["coordinates"].to!string
    .should
    .equal(`[-180,12.05680725475381]`);
}

/// it fixes point coordinates with positive overflows 243.8058646701426
unittest {
  auto point1 = `{ "type": "Point", "coordinates": [ 243.8058646701426, 12.05680725475381 ] }`
    .parseJsonString
    .deserializeJson!GeoJsonGeometry;

  point1.normalizeWgs84;
  point1.serializeToJson["coordinates"].to!string
    .should
    .equal(`[-116.1941353298574,12.05680725475381]`);
}


/// it fixes point coordinates with positive overflows 423.8058646701426
unittest {
  auto point1 = `{ "type": "Point", "coordinates": [ 423.8058646701426, 12.05680725475381 ] }`
    .parseJsonString
    .deserializeJson!GeoJsonGeometry;

  point1.normalizeWgs84;
  point1.serializeToJson["coordinates"].to!string
    .should
    .equal(`[63.80586467014263,12.05680725475381]`);
}


/// it fixes point coordinates with positive overflows 603.8058646701426
unittest {
  auto point1 = `{ "type": "Point", "coordinates": [ 603.8058646701426, 12.05680725475381 ] }`
    .parseJsonString
    .deserializeJson!GeoJsonGeometry;

  point1.normalizeWgs84;
  point1.serializeToJson["coordinates"].to!string
    .should
    .equal(`[-116.1941353298574,12.05680725475381]`);
}

/// it fixes point coordinates with negative overflows -783.8058646701426
unittest {
  auto point = `{ "type": "Point", "coordinates": [ -783.8058646701426, 20.05680725475381 ] }`
    .parseJsonString.deserializeJson!GeoJsonGeometry;

  point.normalizeWgs84;

  point.serializeToJson["coordinates"]
    .to!string.should
    .equal(`[-63.80586467014257,20.05680725475381]`);
}

/// it fixes point coordinates with negative overflows -603.8058646701426
unittest {
  auto point1 = `{ "type": "Point", "coordinates": [ -603.8058646701426, 20.05680725475381 ] }`
    .parseJsonString.deserializeJson!GeoJsonGeometry;

  point1.normalizeWgs84;

  point1.serializeToJson["coordinates"]
    .to!string.should
    .equal(`[116.1941353298574,20.05680725475381]`);
}

/// it fixes point coordinates with negative overflows -423.8058646701426
unittest {
  auto point = `{ "type": "Point", "coordinates": [ -423.8058646701426, -20.05680725475381 ] }`
    .parseJsonString.deserializeJson!GeoJsonGeometry;

  point.normalizeWgs84;

  point.serializeToJson["coordinates"]
    .to!string.should
    .equal(`[-63.80586467014263,-20.05680725475381]`);
}

///
GeoJsonGeometry centroid(GeoJsonGeometry geometry) {
  GeoJsonGeometry result;

  if(geometry.type == "Point") {
    result.set(geometry.to!Point);
  }

  if(geometry.type == "MultiPoint") {
    auto points = geometry.to!MultiPoint.coordinates;

    if(points.length > 0) {
      result.set(Point(points[0]));
    }
  }

  if(geometry.type == "LineString") {
    auto points = geometry.to!LineString.coordinates;

    if(points.length > 0) {
      result.set(Point(points[0]));
    }
  }

  if(geometry.type == "Polygon") {
    auto points = geometry.to!Polygon.coordinates;

    if(points.length > 0) {
      result.set(Point(points[0].centroid));
    }
  }

  if(geometry.type == "MultiPolygon") {
    auto points = geometry.to!MultiPolygon.coordinates;

    if(points.length > 0 && points[0].length > 0) {
      result.set(Point(points[0][0].centroid));
    }
  }

  return result;
}

/// it returns the point for a point
unittest {
  Json data = `{
    "type": "Point",
    "coordinates": [
      30.1, 10.1
    ]}`.parseJsonString;

  auto geoJson = data.deserializeJson!GeoJsonGeometry;
  geoJson.centroid.to!Point.coordinates.to!string.should.equal(`[30.1, 10.1]`);
}

/// it returns the first point for a line
unittest {
  Json data = `{
    "type": "LineString",
    "coordinates": [
      [1, 2], [30.1, 10.1]
    ]}`.parseJsonString;


  auto geoJson = data.deserializeJson!GeoJsonGeometry;
  geoJson.centroid.to!Point.coordinates.to!string.should.equal(`[1, 2]`);
}

/// it returns the first point for a multi point
unittest {
  Json data = `{
    "type": "MultiPoint",
    "coordinates": [
      [1, 2], [30.1, 10.1]
    ]}`.parseJsonString;

  auto geoJson = data.deserializeJson!GeoJsonGeometry;
  geoJson.centroid.to!Point.coordinates.to!string.should.equal(`[1, 2]`);
}

/// it returns the centroid for a polygon
unittest {
  Json data = `{
    "type": "Polygon",
    "coordinates": [[[0, 0], [0, 4], [4, 4], [4, 0], [0, 0]]]
  }`.parseJsonString;

  auto geoJson = data.deserializeJson!GeoJsonGeometry;
  geoJson.centroid.to!Point.coordinates.to!string.should.equal(`[2, 2]`);
}

/// it returns the centroid of the first multi polygon for a multi polygon
unittest {
  Json data = `{
    "type": "MultiPolygon",
    "coordinates": [[[[0, 0], [0, 4], [4, 4], [4, 0], [0, 0]]]]
  }`.parseJsonString;

  auto geoJson = data.deserializeJson!GeoJsonGeometry;
  geoJson.centroid.to!Point.coordinates.to!string.should.equal(`[2, 2]`);
}

/// get the area of a polygon or multi polygon in meters
double area(GeoJsonGeometry geometry) {
  if(geometry.type == "Polygon") {
    auto points = geometry.to!Polygon.coordinates;

    if(points.length > 0) {
      return points[0].areaMeters;
    }
  }

  if(geometry.type == "MultiPolygon") {
    auto points = geometry.to!MultiPolygon.coordinates;

    if(points.length > 0 && points[0].length > 0) {
      return points.filter!(a => a.length > 0).map!(a => a[0].areaMeters).sum;
    }
  }

  return 0;
}

/// it returns 0 for a point
unittest {
  Json data = `{
    "type": "Point",
    "coordinates": [
      30.1, 10.1
    ]}`.parseJsonString;

  auto geoJson = data.deserializeJson!GeoJsonGeometry;
  geoJson.area.should.equal(0);
}

/// it returns 0 for a line
unittest {
  Json data = `{
    "type": "LineString",
    "coordinates": [
      [1, 2], [30.1, 10.1]
    ]}`.parseJsonString;


  auto geoJson = data.deserializeJson!GeoJsonGeometry;
  geoJson.area.should.equal(0);
}

/// it returns 0 for a multi point
unittest {
  Json data = `{
    "type": "MultiPoint",
    "coordinates": [
      [1, 2], [30.1, 10.1]
    ]}`.parseJsonString;

  auto geoJson = data.deserializeJson!GeoJsonGeometry;
  geoJson.area.should.equal(0);
}

/// it returns the area for a polygon
unittest {
  Json data = `{
    "type": "Polygon",
    "coordinates": [[[0, 0], [0, 4], [4, 4], [4, 0], [0, 0]]]
  }`.parseJsonString;

  auto geoJson = data.deserializeJson!GeoJsonGeometry;
  geoJson.area.should.equal(1.98111e+11);
}

/// it returns the area for a multi polygon
unittest {
  Json data = `{
    "type": "MultiPolygon",
    "coordinates": [[[[0, 0], [0, 4], [4, 4], [4, 0], [0, 0]]]]
  }`.parseJsonString;

  auto geoJson = data.deserializeJson!GeoJsonGeometry;
  geoJson.area.should.equal(1.98111e+11);
}


/// get the area of a polygon or multi polygon in meters
double metricDistance(GeoJsonGeometry geometry) {
  if(geometry.type == "LineString") {
    auto points = geometry.to!LineString.coordinates;

    if(points.length > 0) {
      return points.metricDistance;
    }
  }

  if(geometry.type == "Polygon") {
    auto points = geometry.to!Polygon.coordinates;

    if(points.length > 0) {
      return points.map!(a => a.metricDistance).sum;
    }
  }

  if(geometry.type == "MultiPolygon") {
    auto points = geometry.to!MultiPolygon.coordinates;

    if(points.length > 0 && points[0].length > 0) {
      return points.filter!(a => a.length > 0).map!(a => a[0].metricDistance).sum;
    }
  }

  return 0;
}

/// it returns 0 for a point
unittest {
  Json data = `{
    "type": "Point",
    "coordinates": [
      30.1, 10.1
    ]}`.parseJsonString;

  auto geoJson = data.deserializeJson!GeoJsonGeometry;
  geoJson.metricDistance.should.equal(0);
}

/// it returns 0 for a line
unittest {
  Json data = `{
    "type": "LineString",
    "coordinates": [
      [1, 2], [30.1, 10.1]
    ]}`.parseJsonString;


  auto geoJson = data.deserializeJson!GeoJsonGeometry;
  geoJson.metricDistance.should.equal(3.33837e+06);
}

/// it returns 0 for a multi point
unittest {
  Json data = `{
    "type": "MultiPoint",
    "coordinates": [
      [1, 2], [30.1, 10.1]
    ]}`.parseJsonString;

  auto geoJson = data.deserializeJson!GeoJsonGeometry;
  geoJson.metricDistance.should.equal(0);
}

/// it returns the metricDistance for a polygon
unittest {
  Json data = `{
    "type": "Polygon",
    "coordinates": [[[0, 0], [0, 4], [4, 4], [4, 0], [0, 0]]]
  }`.parseJsonString;

  auto geoJson = data.deserializeJson!GeoJsonGeometry;
  geoJson.metricDistance.should.equal(1.77803e+06);
}

/// it returns the metricDistance for a multi polygon
unittest {
  Json data = `{
    "type": "MultiPolygon",
    "coordinates": [[[[0, 0], [0, 4], [4, 4], [4, 0], [0, 0]]]]
  }`.parseJsonString;

  auto geoJson = data.deserializeJson!GeoJsonGeometry;
  geoJson.metricDistance.should.equal(1.77803e+06);
}