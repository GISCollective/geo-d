/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module geo.pmtiles;

import geo.proto;
import geo.osm.file;
import geo.xyz;
import geo.vector;
import geo.svg.vectorTiles;

import std.bitmanip;
import std.array;
import std.conv;
import std.zlib;
import std.range;

import vibe.data.json;

version(unittest) {
  import fluent.asserts;
}

ubyte[] uncompress(ubyte[] data, PMTilesHeader.Compression compression) {
  if(compression == PMTilesHeader.Compression.none) {
    return data;
  }

  if(compression == PMTilesHeader.Compression.gzip) {
    UnCompress decmp = new UnCompress(HeaderFormat.gzip);

    return cast(ubyte[]) decmp.uncompress(data);
  }

  throw new Exception("Unknown compression algorithm: " ~ compression.to!string);
}

struct PMTilesLonLat {
  int lon;
  int lat;

  void read(FileRange data) {
    this.lon = data.read!int;
    this.lat = data.read!int;
  }
}

struct PMTilesLocation {
  ulong offset;
  ulong length;

  void read(FileRange data) {
    this.offset = data.read!ulong;
    this.length = data.read!ulong;
  }
}

/// PMTiles Header v3
struct PMTilesHeader {
  enum Type: ubyte {
    unknown = 0x0,
    mvt = 0x1,
    png = 0x2,
    jpeg = 0x3,
    webp = 0x4,
    avif = 0x5
  }

  enum Compression: ubyte {
    unknown = 0x0,
    none = 0x1,
    gzip = 0x2,
    brotli = 0x3,
    zstd = 0x4
  }

  PMTilesLocation rootDir;
  PMTilesLocation jsonMetadata;
  PMTilesLocation leafDirs;
  PMTilesLocation tileData;

  ulong addressedTilesCount;
  ulong tileEntriesCount;
  ulong tileContentsCount;

  bool clustered;

  PMTilesHeader.Compression internalCompression;
  PMTilesHeader.Compression tileCompression;

  PMTilesHeader.Type tileType;

  ubyte minZoom;
  ubyte maxZoom;

  PMTilesLonLat min;
  PMTilesLonLat max;

  ubyte centerZoom;
  PMTilesLonLat center;

  this(FileRange data) {
    this.read(data);
  }

  void read(FileRange data) {
    auto magicNumber = data.readString(7);

    if (magicNumber != "PMTiles") {
      return;
    }

    auto ver = data.readByte();

    if (ver != 3) {
      return;
    }

    this.rootDir.read(data);
    this.jsonMetadata.read(data);
    this.leafDirs.read(data);
    this.tileData.read(data);

    this.addressedTilesCount = data.read!ulong;
    this.tileEntriesCount = data.read!ulong;
    this.tileContentsCount = data.read!ulong;
    this.clustered = data.readByte == 1;

    this.internalCompression = cast(PMTilesHeader.Compression) data.readByte;
    this.tileCompression = cast(PMTilesHeader.Compression) data.readByte;
    this.tileType = cast(PMTilesHeader.Type) data.readByte;

    this.minZoom = data.readByte;
    this.maxZoom = data.readByte;

    this.min.read(data);
    this.max.read(data);

    this.centerZoom = data.readByte;
    this.center.read(data);
  }
}

/// can read the pmtiles header
unittest {
  auto data = new FileRange("test/test_fixture_1.pmtiles");
  auto header = PMTilesHeader(data);

  header.serializeToJson.should.equal(`{
    "tileType": 1,
    "tileContentsCount": 1,
    "centerZoom": 0,
    "addressedTilesCount": 1,
    "clustered": false,
    "min": {
      "lon": 0,
      "lat": 0
    },
    "max": {
      "lon": 9999999,
      "lat": 10000000
    },
    "leafDirs": {
      "length": 0,
      "offset": 0
    },
    "minZoom": 0,
    "maxZoom": 0,
    "rootDir": {
      "length": 25,
      "offset": 127
    },
    "tileData": {
      "length": 69,
      "offset": 399
    },
    "jsonMetadata": {
      "length": 247,
      "offset": 152
    },
    "internalCompression": 2,
    "center": {
      "lon": 0,
      "lat": 0
    },
    "tileCompression": 2,
    "tileEntriesCount": 1
  }`.parseJsonString);
}

///
struct Directory {
  long tileId;
  ulong offset;
  ulong length;
  ulong runLength;
}

///
struct DirectoryList {
  Directory[] entries;

  this(Directory[] entries) {
    this.entries = entries;
  }

  this(FileRange data, size_t length, PMTilesHeader.Compression compression) {
    this.read(data, length, compression);
  }

  void read(FileRange data, size_t length, PMTilesHeader.Compression compression) {
    if(length == 0) {
      return;
    }

    auto buffer = data.takeExactly(length).uncompress(compression);
    auto range = VarintInputRange(buffer);

    auto numEntries = range.moveFront.to!ulong;
    entries.reserve(numEntries);

    long lastId = 0;
    foreach(i; 0..numEntries) {
      auto value = range.moveFront.to!long;
      lastId = lastId + value;

      this.entries ~= Directory(lastId);
    }

    foreach(i; 0..numEntries) {
      this.entries[i].runLength = range.moveFront.to!ulong;
    }

    foreach(i; 0..numEntries) {
      this.entries[i].length = range.moveFront.to!ulong;
    }

    foreach(i; 0..numEntries) {
      auto value = range.moveFront.to!ulong;

      if(value == 0 && i > 0){
        auto prev = this.entries[i - 1];

        this.entries[i].offset = prev.offset + prev.length;
      } else {
        this.entries[i].offset = value - 1;
      }
    }
  }

  Directory findTile(long tileId) {
    long m = 0;
    long n = this.entries.length - 1;

    while (m <= n) {
      auto k = (n + m) >> 1;

      if (tileId > entries[k].tileId) {
        m = k + 1;
      } else if (tileId < entries[k].tileId) {
        n = k - 1;
      } else {
        return entries[k];
      }
    }

    if (n >= 0) {
      if (entries[n].runLength == 0) {
        return entries[n];
      }

      if (tileId - entries[n].tileId < entries[n].runLength) {
        return entries[n];
      }
    }

    return Directory();
  }
}

/// can find an entry with runlength 1
unittest {
  auto list = DirectoryList([Directory(5, 1337, 42, 1)]);

  list.findTile(5).serializeToJson.should.equal(`{"length":42,"tileId":5,"offset":1337,"runLength":1}`.parseJsonString);
}

/// can find an entry with runlength 3
unittest {
  auto list = DirectoryList([Directory(5, 1337, 42, 3)]);

  list.findTile(5).serializeToJson.should.equal(`{"length":42,"tileId":5,"offset":1337,"runLength":3}`.parseJsonString);
  list.findTile(6).serializeToJson.should.equal(`{"length":42,"tileId":5,"offset":1337,"runLength":3}`.parseJsonString);
  list.findTile(7).serializeToJson.should.equal(`{"length":42,"tileId":5,"offset":1337,"runLength":3}`.parseJsonString);
  list.findTile(8).serializeToJson.should.equal(`{"length":0,"tileId":0,"offset":0,"runLength":0}`.parseJsonString);
}

/// can find an entry with runlength 0
unittest {
  auto list = DirectoryList([Directory(5, 1337, 42, 0)]);

  list.findTile(4).serializeToJson.should.equal(`{"length":0,"tileId":0,"offset":0,"runLength":0}`.parseJsonString);
  list.findTile(5).serializeToJson.should.equal(`{"length":42,"tileId":5,"offset":1337,"runLength":0}`.parseJsonString);
  list.findTile(6).serializeToJson.should.equal(`{"length":42,"tileId":5,"offset":1337,"runLength":0}`.parseJsonString);
}

/// it returns a 0 Directory when it is not found
unittest {
  auto list = DirectoryList([Directory(5, 1337, 42, 1)]);

  list.findTile(6).serializeToJson.should.equal(`{"length":0,"tileId":0,"offset":0,"runLength":0}`.parseJsonString);
}

/// can read the pmtiles root directory
unittest {
  auto data = new FileRange("test/test_fixture_1.pmtiles");
  auto header = PMTilesHeader(data);

  auto list = DirectoryList(data, header.rootDir.length, header.internalCompression);

  list.serializeToJson.should.equal(`{"entries":[{"length":69,"tileId":0,"runLength":1,"offset":0}]}`.parseJsonString);
}

/// PMTiles v3
/// https://github.com/protomaps/PMTiles/blob/main/spec/v3/spec.md
struct PMTiles {
  PMTilesHeader header;
  DirectoryList rootDirectory;
  Json metadata;
  DirectoryList leafDirectories;
  string filePath;

  this(string filePath) {
    this.filePath = filePath;
    auto data = new FileRange(filePath);

    header.read(data);
    rootDirectory.read(data, header.rootDir.length, header.internalCompression);

    data.seek(header.jsonMetadata.offset);
    auto jsonData = data.takeExactly(header.jsonMetadata.length);
    string jsonString = cast(string) jsonData.uncompress(header.internalCompression);
    metadata = jsonString.parseJsonString;

    leafDirectories.read(data, header.leafDirs.length, header.internalCompression);
  }

  ubyte[] getTileData(ZXY zxy) {
    auto tileId = zxy.toId;
    auto entry = this.rootDirectory.findTile(tileId);

    if(entry.runLength == 0) {
      auto data = new FileRange(filePath);
      data.seek(header.leafDirs.offset + entry.offset);

      entry = DirectoryList(data, entry.length, header.internalCompression).findTile(tileId);
    }

    auto fileRange = new FileRange(filePath);
    fileRange.seek(this.header.tileData.offset + entry.offset);

    auto data = fileRange
      .takeExactly(entry.length)
      .uncompress(this.header.internalCompression);

    return data;
  }
}

/// it can read the pmtiles metadata
unittest {
  auto tiles = PMTiles("test/test_fixture_1.pmtiles");

  tiles.metadata.should.equal(`{
    "generator": "tippecanoe v2.5.0",
    "tilestats": {
      "layers": [{
        "attributeCount": 0,
        "attributes": [],
        "layer": "test_fixture_1pmtiles",
        "count": 1,
        "geometry": "Polygon"
      }],
      "layerCount": 1
    },
    "name": "test_fixture_1.pmtiles",
    "description": "test_fixture_1.pmtiles",
    "type": "overlay",
    "version": "2",
    "vector_layers": [{
      "description": "",
      "maxzoom": 0,
      "id": "test_fixture_1pmtiles",
      "minzoom": 0,
      "fields": {}
    }],
    "generator_options": "./tippecanoe -zg -o test_fixture_1.pmtiles --force"
  }`.parseJsonString);
}

/// it can read the pmtiles leaf directories when they have length 0
unittest {
  auto tiles = PMTiles("test/test_fixture_1.pmtiles");

  tiles.leafDirectories.serializeToJson.should.equal(`{"entries":[]}`.parseJsonString);
}

/// it can find a tile in the root directory list
unittest {
  auto tiles = PMTiles("test/test_fixture_1.pmtiles");

  auto tile = tiles.getTileData(ZXY(0, 0, 0)).readTile;

  tile.layers.length.should.equal(1);
  tile.layers[0].toJson.should.equal(`{
    "values": [],
    "keys": [],
    "version": 2,
    "features": [{
        "type": 3,
        "tags": [],
        "geometry": {
          "commands": [{
              "commandInteger": { "count": 1, "id": 1 },
              "parameters": [ 2059, 2036 ]
            },
            {
              "commandInteger": {
                "count": 3,
                "id": 2
              },
              "parameters": [ 0, 12, -11, 0, 0, -12 ]
            },
            {
              "commandInteger": { "count": 0, "id": 7 },
              "parameters": []
            }]
        },
        "id": 0
    }],
    "extent": 4096,
    "name": "test_fixture_1pmtiles"
  }`.parseJsonString);
}