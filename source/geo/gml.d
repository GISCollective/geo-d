/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module geo.gml;

import std.conv;

import geo.geometries;

version(unittest) import fluent.asserts;

///
string toGmlString(Point point) {
  return `<gml:Point srsDimension="2" srsName="http://www.opengis.net/def/crs/EPSG/0/4326"><gml:pos>` ~
    point.coordinates[0].to!string ~ " " ~ point.coordinates[1].to!string ~
    `</gml:pos></gml:Point>`;
}

/// Convert a point to a gml string
unittest {
  Point(Position(1, 2)).toGmlString.should.equal(`<gml:Point srsDimension="2" srsName="http://www.opengis.net/def/crs/EPSG/0/4326"><gml:pos>1 2</gml:pos></gml:Point>`);
}
