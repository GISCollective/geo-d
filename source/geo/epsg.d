/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module geo.epsg;

import geo.conv;
import geo.geometries;
import std.conv;

version(unittest) {
  import fluent.asserts;
}

/// World Geodetic System 1984, used in GPS
struct EPSG4326 {
  ///
  double latitude;
  ///
  double longitude;
  /*WGS84 bounds:
  -180.0 -90.0
  180.0 90.0 */


  alias lon = longitude;
  alias lat = latitude;

  this(const double longitude, const double latitude) @safe pure nothrow {
    this.longitude = longitude;
    this.latitude = latitude;
  }

  this(double[] pos) @safe pure nothrow {
    this.longitude = pos[0];
    this.latitude = pos[1];
  }

  this(const double[] pos) @safe pure nothrow {
    this.longitude = pos[0];
    this.latitude = pos[1];
  }

  this(const Position coordinates) @safe pure nothrow {
    this(coordinates[0], coordinates[1]);
  }

  bool isNullPoint() {
    return longitude == 0 && latitude == 0;
  }

  double opIndex(size_t index) inout nothrow @safe pure @nogc {
    switch(index) {
      case 0: return longitude;
      case 1: return latitude;
      default:
        assert(false, "You can't get the element. Only 0 and 1 are allowed.");
    }
  }

  double[] toArray() inout {
    return [longitude, latitude];
  }

  void opIndexAssign(T)(in T value, size_t index) {
    switch(index) {
      case 0: longitude = value.to!double; break;
      case 1: latitude = value.to!double; break;
      default:
        assert(false, "You can't get the element. Only 0 and 1 are allowed.");
    }
  }

  Position toPosition() inout {
    return Position(longitude, latitude);
  }

  string toString() inout {
    import std.format;

    return format("[%10.18f, %10.18f]", longitude, latitude);
  }
}

/// EPSG4326 equal comparison
unittest {
  EPSG4326(1, 2).should.equal(EPSG4326(1, 2));
  EPSG4326(1, 2).should.not.equal(EPSG4326(2, 2));
}

double[] toList(EPSG4326 point) {
  return [point.longitude, point.latitude];
}


alias WGS84 = EPSG4326;

/// Spherical Mercator, Google Maps, OpenStreetMap, Bing, ArcGIS, ESRI
struct EPSG3857 {
  long x;
  long y;

  alias lon = x;
  alias lat = y;

  this(const long x, const long y) @safe pure nothrow {
    this.x = x;
    this.y = y;
  }

  this(const int x, const int y) @safe pure nothrow {
    this.x = x;
    this.y = y;
  }

  this(const double longitude, const double latitude) @safe pure nothrow {
    auto coordinates = EPSG4326(longitude, latitude).toEPSG3857;

    this.x = coordinates.x;
    this.y = coordinates.y;
  }

  this(const Position coordinates) @safe pure {
    this(coordinates[0], coordinates[1]);
  }

  this(const long[] coordinates) @safe pure {
    this(coordinates[0], coordinates[1]);
  }

  long opIndex(size_t index) inout nothrow @safe pure @nogc {
    switch(index) {
      case 0: return x;
      case 1: return y;
      default:
        assert(false, "You can't get the element. Only 0 and 1 are allowed.");
    }
  }

  bool isNullPoint() {
    return x ==0 && y == 0;
  }

  void opIndexAssign(in long value, size_t index) {
    switch(index) {
      case 0: x = value; break;
      case 1: y = value; break;
      default:
        assert(false, "You can't get the element. Only 0 and 1 are allowed.");
    }
  }

  void opIndexAssign(in double value, size_t index) {
    switch(index) {
      case 0: x = value.to!long; break;
      case 1: y = value.to!long; break;
      default:
        assert(false, "You can't get the element. Only 0 and 1 are allowed.");
    }
  }

  long[] toArray() inout {
    return [x, y];
  }

  string toString() inout {
    import std.conv;
    return "[" ~ x.to!string ~ ", " ~ y.to!string ~ "]";
  }
}

/// EPSG3857 equal comparison
unittest {
  EPSG3857(1, 2).should.equal(EPSG3857(1, 2));
  EPSG3857(1, 2).should.not.equal(EPSG3857(2, 2));
}

long[][] toList(EPSG3857[] points) {
  long[][] result;
  result.reserve(points.length);

  foreach(point; points) {
    result ~= [[point.x, point.y]];
  }


  return result;
}

alias WebMercator = EPSG3857;
