/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module geo.svg.vectorTiles;

import geo.vector;
import geo.style;
import geo.xml;

import std.algorithm;
import std.array;
import std.conv;
import std.range;
import std.exception;
import std.string : join, toUpper, strip;
import std.traits;

enum double defaultSizeFactor = 8;

version(unittest) {
  import fluent.asserts;
}

///
string toSvgCommand(const Command command) pure @safe {
  string op;

  if(command.commandInteger.id == CommandId.MoveTo) {
    op = "m";
  }

  if(command.commandInteger.id == CommandId.LineTo) {
    op = "l";
  }

  if(command.commandInteger.id == CommandId.ClosePath) {
    op = "z";
  }

  return command.parameters
    .chunks(2)
    .map!(a => op ~ a[0].to!string ~ "," ~ a[1].to!string)
    .join(" ");
}

///
string formatMapText(string format, string[string] values) {
  string[] replaceVars(string[] parts) {
    if(parts.length < 2) {
      return parts;
    }

    parts[0] = parts[0] in values ? values[parts[0]] : "";
    return parts;
  }

  auto pieces = format.split("{")
    .map!(a => replaceVars(a.split("}")))
    .join;

  return pieces.join.to!string.strip;
}

/// it should replace the format params
unittest {
  string[string] values;
  values["a"] = "A";
  values["b"] = "B";

  formatMapText("", values).should.equal("");
  formatMapText("{a}", values).should.equal("A");
  formatMapText("{a} {b}", values).should.equal("A B");
  formatMapText("{a} {c}", values).should.equal("A");
}

string toSvgValue(T)(const(InterpolateValue!T) value, string[string] attributes, double sizeFactor = defaultSizeFactor) {
  auto result = value.render(attributes);

  static if(isNumeric!(T)) {
    result *= sizeFactor;
  }

  return result.to!string;
}

///
string toSvgFillStyle(const MapBoxLayer layer, VectorFeature feature, string[string] attributes) {
  return `stroke="` ~ layer.paint.fillOutlineColor.toSvgValue(attributes) ~
    `" fill="` ~ layer.paint.fillColor.toSvgValue(attributes) ~
    `" fill-opacity="`~ layer.paint.fillOpacity.toSvgValue(attributes) ~ `"`;
}

///
string toSvgLineStyle(const MapBoxLayer layer, VectorFeature feature, string[string] attributes) {

  string[] optionalStyles;
  auto lineDasharray = layer.paint.lineDasharray.render(attributes);


  if(lineDasharray.length) {
    optionalStyles ~= `stroke-dasharray="` ~ lineDasharray.map!(a => (a * defaultSizeFactor).to!string).join(" ") ~ `"`;
  }

  return `stroke="` ~ layer.paint.lineColor.toSvgValue(attributes) ~
    `" fill="none" ` ~
    optionalStyles.join(" ") ~
    ` stroke-opacity="` ~ layer.paint.lineOpacity.toSvgValue(attributes) ~ `"`~
    ` stroke-width="` ~ layer.paint.lineWidth.toSvgValue(attributes) ~ `"`;
}

///
string toBackgroundSvg(const MapBoxLayer layer, const string size, VectorFeature feature, string[string] attributes) {
  return `  <rect width="` ~ size ~ `" height="` ~ size ~ `"` ~
    ` fill="` ~ layer.paint.backgroundColor.toSvgValue(attributes) ~ `"` ~
    ` fill-opacity="`~ layer.paint.backgroundOpacity.toSvgValue(attributes) ~ `"` ~
  ` />`~ "\n";
}

string toSvgTextStyle(const MapBoxLayer layer, VectorFeature feature, string[string] attributes) {
  return `fill="` ~ layer.paint.textColor.toSvgValue(attributes) ~ `" ` ~
    `font-size="` ~ layer.layout.textSize.toSvgValue(attributes, defaultSizeFactor / 2) ~ `px" ` ~
    `stroke="` ~ layer.paint.textHaloColor.toSvgValue(attributes) ~ `" ` ~
    `stroke-width="` ~ layer.paint.textHaloWidth.toSvgValue(attributes, defaultSizeFactor / 4) ~ `px" ` ~
    `letter-spacing="` ~ layer.layout.textLetterSpacing.toSvgValue(attributes, defaultSizeFactor / 2) ~ `em" ` ~
    `paint-order="stroke" ` ~
    `dominant-baseline="middle" ` ~
    `text-rendering="optimizeLegibility" ` ~
    `font-family="` ~ layer.layout.textFont.render(attributes).join(", ") ~ `"`;
}

///
string[string] attributes(Layer layer, size_t featureIndex) {
  string[string] attributes;
  attributes["$type"] = layer.features[featureIndex].type.toString;
  attributes["geometry-type"] = layer.features[featureIndex].type.toString;

  foreach(a; layer.features[featureIndex].tags.chunks(2)) {
    if(a[0] >= layer.keys.length || a[1] >= layer.values.length) {
      continue;
    }

    const key = layer.keys[a[0]];
    const value = layer.values[a[1]].toString;

    attributes[key] = value;
  }

  return attributes;
}

///
string toDataAttributes(string[string] attributes) {
  return attributes.byKeyValue.map!(a => "data-" ~ a.key.replace(":", "-").replace("$", "") ~ `="` ~ a.value.escValue ~ `"`).join(" ");
}

///
string renderFill(const MapBoxLayer style, string[string] attributes, VectorFeature feature) {
  string result = `<path data-renderer="renderFill" d="`;

  import std.stdio;
  writeln(style.id, " ", style.filter);

  result ~= feature.geometry.commands.map!(toSvgCommand).join(" ");

  return result ~ `" ` ~ style.toSvgFillStyle(feature, attributes) ~ " " ~ attributes.toDataAttributes ~ "/>";
}

///
string renderLine(const MapBoxLayer style, string[string] attributes, VectorFeature feature) {
  string result = `<path data-renderer="renderLine" d="`;

  result ~= feature.geometry.commands.map!(toSvgCommand).join(" ");

  return result ~ `" ` ~ style.toSvgLineStyle(feature, attributes) ~ " " ~ attributes.toDataAttributes ~ "/>";
}

///
string renderPointSymbol(const MapBoxLayer style, string[string] attributes, VectorFeature feature) {
  enforce(style.type == MapBoxLayer.Type.symbol, "You must use a symbol style.");
  enforce(feature.type == VectorFeature.GeomType.point, "You must use a point geometry.");

  string result;
  string text = formatMapText(style.layout.textField.render(attributes), attributes);

  if(style.layout.textTransform == MapBoxLayer.TextTransform.uppercase) {
    text = text.toUpper;
  }

  auto point = feature.geometry.commands[0].parameters;

  if(text != "") {
    result ~= `<text data-renderer="renderPointSymbol" x="` ~ point[0].to!string ~ `" y="` ~ point[1].to!string ~ `" ` ~ style.toSvgTextStyle(feature, attributes) ~ `>` ~
      text ~ `</text>`;
  }

  /*
   ~ feature.geometry.commands[0].parameters
    .chunks(2)
    .map!(a => `<ellipse cx="` ~ a[0].to!string ~ `" cy="` ~ a[1].to!string ~ `" rx="5" ry="5" ` ~
      attributes.toDataAttributes ~ `/>`)
    .join(" ");*/
  return result;
}

/// Render a house number text
unittest {
  string[string] attributes = ["housenumber": "housenumber"];
  auto feature =  VectorFeature(1, [], VectorFeature.GeomType.point,
    Geometry([Command(CommandInteger(CommandId.MoveTo, 1), [0, 0])]));
  MapBoxLayer style;

  style.layout.textField = "{housenumber}";
  style.layout.textFont = ["Noto Sans Regular"];
  style.layout.textSize = InterpolateValue!double(10);
  style.paint.textColor = InterpolateValue!string("rgba(212, 177, 146, 1)");
  style.type = MapBoxLayer.Type.symbol;

  renderPointSymbol(style, attributes, feature).should.equal(
    `<text data-renderer="renderPointSymbol" x="0" y="0" fill="rgba(212, 177, 146, 1)" text-size="10px" ` ~
    `stroke="#000000" stroke-width="0px" letter-spacing="0em" font-family="Noto Sans Regular">housenumber</text>`);
}

///
string renderLineSymbol(const MapBoxLayer style, string[string] attributes, VectorFeature feature) {
  enforce(style.type == MapBoxLayer.Type.symbol, "You must use a symbol style.");
  enforce(feature.type == VectorFeature.GeomType.lineString, "You must use a point geometry.");

  string result;
  string text = formatMapText(style.layout.textField.render(attributes), attributes);

  auto point = feature.geometry.commands[0].parameters;

  result ~= `<path id="text-path-` ~ feature.id.to!string ~ `" fill="none" stroke="none" d="` ~
    feature.geometry.commands.map!(toSvgCommand).join(" ") ~ `" />`;


  if(text != "") {
    result ~= `<text ` ~ style.toSvgTextStyle(feature, attributes) ~ `>` ~
    `<textPath href="#text-path-` ~ feature.id.to!string ~ `">` ~ text ~ `</textPath></text>`;
  }

  return `<g data-renderer="renderLineSymbol">` ~ result ~ `</g>`;
}

/// Render a street name text
unittest {
  string[string] attributes = ["name:latin": "asd", "name:nonlatin": "qwe"];
  auto feature = VectorFeature(1, [], VectorFeature.GeomType.lineString,
    Geometry([ Command(CommandInteger(CommandId.MoveTo, 1), [0, 0]),
      Command(CommandInteger(CommandId.LineTo, 3), [10, 0, 0, 10, -10, 0])]));
  MapBoxLayer style;

  style.layout.textField = "{name:latin} {name:nonlatin}";
  style.layout.textFont = ["Noto Sans Regular"];
  style.layout.textLetterSpacing = InterpolateValue!double(0.1);
  style.layout.textTransform = MapBoxLayer.TextTransform.uppercase;

  style.paint.textHaloColor = InterpolateValue!string("hsl(0, 0%, 100%)");
  style.paint.textHaloWidth = InterpolateValue!double(2);

  style.type = MapBoxLayer.Type.symbol;

  renderLineSymbol(style, attributes, feature).should.equal(`<g data-renderer="renderLineSymbol">` ~
    `<path id="text-path-1" fill="none" stroke="none" d="m0,0 l10,0 l0,10 l-10,0" />` ~
    `<text fill="#000000" text-size="16px" stroke="hsl(0, 0%, 100%)" stroke-width="2px" letter-spacing="0.1em" font-family="Noto Sans Regular">` ~
    `<textPath href="#text-path-1">asd qwe</textPath></text></g>`);
}

///
string render(Layer layer, const MapBoxLayer style, string[string] params = null) {
  string result;

  import std.stdio;
  writeln;
  writeln(layer.name);
  writeln("==============");

  size_t count;

  foreach(i; 0..layer.features.length) {
    auto attributes = layer.attributes(i);

    foreach(string key, value; params) {
      attributes[key] = value;
    }

    if(!style.filter.match(attributes)) {
      continue;
    }
    writeln("==============");

    count++;
    if(style.type == MapBoxLayer.Type.symbol) {
      if(layer.features[i].type == VectorFeature.GeomType.point) {
        auto symbol = style.renderPointSymbol(attributes, layer.features[i]).strip;

        if(symbol != "") {
          result ~= "    " ~ symbol ~ "\n";
        }
      }

      if(layer.features[i].type == VectorFeature.GeomType.lineString) {
        auto symbol = style.renderLineSymbol(attributes, layer.features[i]).strip;

        if(symbol != "") {
          result ~= "    " ~ symbol ~ "\n";
        }
      }
    }

    if(style.type == MapBoxLayer.Type.fill) {
      auto fill = style.renderFill(attributes, layer.features[i]).strip;

      if(fill != "") {
        result ~= "    " ~ fill ~ "\n";
      }
    }

    if(style.type == MapBoxLayer.Type.line) {
      auto line = style.renderLine(attributes, layer.features[i]).strip;

      if(line != "") {
        result ~= "    " ~ line ~ "\n";
      }
    }
  }

  writeln(count);


  return result;
}

///
class TileRenderer {

  ///
  private MapboxStyle style;

  ///
  this(MapboxStyle style) {
    this.style = style;
  }

  ///
  string render(Tile tile, string[string] params = null) {
    string size = "512";

    if(tile.layers.length > 0) {
      size = tile.layers[0].extent.to!string;
    }

    auto result = `<svg viewBox="0 0 ` ~ size ~ ` ` ~ size ~ `" xmlns="http://www.w3.org/2000/svg">` ~ "\n";

    foreach(layerStyle; style.layers) {
      string[string] attributes;

      if(layerStyle.type == MapBoxLayer.Type.background) {
        result ~= layerStyle.toBackgroundSvg(size, VectorFeature(), attributes);
        continue;
      }

      auto featureLayers = tile.layers.filter!(a => a.name == layerStyle.sourceLayer || a.name == layerStyle.id).array;

      if(featureLayers.length == 0) {
        continue;
      }

      Layer selectedLayer;

      foreach(layer; featureLayers) {
        if(layer.name == layerStyle.sourceLayer) {
          selectedLayer = layer;
          break;
        }
      }

      import std.stdio;
      writeln(layerStyle.id, " from ", );
      writeln("================= > ===");

      foreach(layer; featureLayers) {
        if(layerStyle.sourceLayer != layer.name) {
          continue;
        }

        string tmp;

        try {
          tmp = layer.render(layerStyle, params);
        } catch(Exception e) {
          import std.stdio;
          writeln(e);
        }

        if(tmp != "") {
          result ~= `  <g data-layer-style-id="` ~ layerStyle.id.escValue ~ `">` ~ "\n" ~ tmp ~ `  </g>` ~ "\n";
        }

        break;
      }
    }

    return result ~ "</svg>";
  }
}
