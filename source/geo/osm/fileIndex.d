/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module geo.osm.fileIndex;

import geo.osm.storage.memory;
import geo.osm.storage.base;
import geo.osm.format;
import geo.osm.file;
import geo.osm.conv;
import geo.geometries;
import std.zlib;
import std.algorithm;
import std.array;
import containers.ttree;


version(unittest) {
    import fluent.asserts;
}

///
struct OsmFileNodeLocation {
  size_t blockIndex;
  size_t firstByteIndex;
  size_t firstNodeIndex;
  size_t lastNodeIndex;
}

///
class OsmFileIndex {
  OsmFileNodeLocation[] locations;

  private {
    MemoryOsmNodeStorage nodes;
    MemoryOsmWayStorage ways;
    size_t[] blockIndexList;
  }

  this() {
    nodes = new MemoryOsmNodeStorage();
    ways = new MemoryOsmWayStorage();
  }

  void add(size_t blockIndex, size_t firstByteIndex, PrimitiveBlock block) {
    size_t start;
    size_t end;
    bool isFirstNode = true;

    static foreach(propertyName; ["rawNodes", "primitiveGroup.ways", "primitiveGroup.relations"]) {{
      mixin("auto list = block." ~ propertyName ~ ";");

      foreach(node; list) {
        if(isFirstNode) {
          start = node.id;
          end = node.id;
          isFirstNode = false;
        }

        if(node.id < start) {
          start = node.id;
        }

        if(node.id > start) {
          end = node.id;
        }
      }
    }}

    if(!isFirstNode) {
      locations ~= OsmFileNodeLocation(blockIndex, firstByteIndex, start, end);
    }
  }

  PrimitiveBlock getBlockAt(size_t offset, FileRange data) {
    data.seek(offset);
    auto decompressor = new UnCompress();

    auto range = new OsmPbfBlockRange!FileRange(data);
    auto block = range.front;

    ubyte[] uncompressedData = cast(ubyte[]) decompressor.uncompress(block.blob.zlib_data);

    return readPrimitiveBlock(uncompressedData);
  }

  PrimitiveBlock[] getBlocksFor(size_t id, FileRange data) {
    PrimitiveBlock[] results;

    foreach(location; this.whereIs(id)) {
      if(blockIndexList.canFind(location.blockIndex)) {
        continue;
      }

      blockIndexList ~= location.blockIndex;

      results ~= getBlockAt(location.firstByteIndex, data);
    }

    return results;
  }

  Way getWayById(size_t id, FileRange data) {
    if(!ways.exists(id)) {
      this.cacheBlocksFor(id, data);
    }

    return ways.get(id);
  }

  void cacheBlocksFor(size_t id, FileRange data) {
    if(blockIndexList.length > 1000) {
      import std.stdio;
      writeln("Clearing index cache!");
      blockIndexList = [];
      nodes.clear();
      ways.clear();
    }

    auto blocks = getBlocksFor(id, data);

    foreach(block; blocks) {
      foreach(node; block.rawNodes) {
        nodes.add(node);
      }

      foreach(way; block.primitiveGroup.ways) {
        ways.add(way);
      }
    }
  }

  PositionT!double getNodeById(size_t id, FileRange data) {
    if(!nodes.exists(id)) {
      this.cacheBlocksFor(id, data);
    }

    return nodes.get(id);
  }

  OsmFileNodeLocation[] whereIs(size_t nodeId) {
    OsmFileNodeLocation[] results;

    foreach(location; locations) {
      if(nodeId >= location.firstNodeIndex && nodeId <= location.lastNodeIndex) {
        results ~= location;
      }
    }

    return results;
  }

  void indexFileRange(FileRange data) {
    auto range = new OsmPbfBlockRange!FileRange(data);
    auto decompressor = new UnCompress();
    size_t blockByteStart = 0;

    foreach(index, block; range) {
      if(block.header.type == "OSMHeader") {
        blockByteStart = range.currentByte;
        continue;
      }

      decompressor = new UnCompress();
      ubyte[] uncompressedData = cast(ubyte[]) decompressor.uncompress(block.blob.zlib_data);

      auto primitiveBlock = readPrimitiveBlock(uncompressedData);
      add(index, blockByteStart, primitiveBlock);
      blockByteStart = range.currentByte;
    }
  }
}

/// It creates an index for a given block
unittest {
  auto index = new OsmFileIndex();
  PrimitiveBlock[] blocks = getTestBlocks();

  foreach(i, block; blocks) {
    index.add(i, i * 2, block);
  }

  index.whereIs(3084924).should.contain(OsmFileNodeLocation(1, 2, 3084923, 157868707));
}

/// It creates an index for a given file range
unittest {
  auto data = new FileRange("test/sample.pbf");
  auto index = new OsmFileIndex();

  index.indexFileRange(data);
  index.whereIs(54932044).should.contain(OsmFileNodeLocation(3, 6120, 3084923, 157868707));
}

// It can get the block by offset
unittest {
  auto data = new FileRange("test/sample.pbf");
  auto index = new OsmFileIndex();
  auto block = index.getBlockAt(6120, data);

  auto ids = block.primitiveGroup.ways.map!(a => a.id).array;
  auto ways = block.primitiveGroup.ways.filter!(a => a.id == 54932044);

  ways.empty.should.equal(false);
}

// It can get the block for a node id
unittest {
  auto data = new FileRange("test/sample.pbf");
  auto index = new OsmFileIndex();
  index.indexFileRange(data);
  auto blocks = index.getBlocksFor(55071941, data);
  auto allWays = blocks.map!"a.primitiveGroup.ways".joiner;

  auto ways = allWays.filter!(a => a.id == 55071941);

  ways.empty.should.equal(false);
}

// It can get a node by id
unittest {
  auto data = new FileRange("test/sample.pbf");
  auto index = new OsmFileIndex();
  index.indexFileRange(data);
  auto way = index.getWayById(54932044, data);

  way.id.should.equal(54932044);
}

version(unittest):
auto getTestBlocks() {
  auto data = new FileRange("test/sample.pbf");
  auto range = new OsmPbfBlockRange!FileRange(data);

  auto decompressor = new UnCompress();

  PrimitiveBlock[] blocks;

  foreach(index, block; range) {
    if(block.header.type == "OSMHeader") {
      continue;
    }

    auto decompressor = new UnCompress();
    ubyte[] uncompressedData = cast(ubyte[]) decompressor.uncompress(block.blob.zlib_data);

    blocks ~= readPrimitiveBlock(uncompressedData);
  }

  return blocks;
}
