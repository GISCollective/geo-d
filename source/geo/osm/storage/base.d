/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module geo.osm.storage.base;

import geo.osm.format;
import geo.geometries;

import std.range;

version(unittest) {
  import fluent.asserts;
}

/// a group of coordinates and an id
struct RawNode {
  ///
  long id;
  ///
  double lat;
  ///
  double lon;
}

///
interface IOsmNodeStorage {


  /// add raw nodes
  void add(ref RawNode node);

  /// ditto
  void add(InputRange!RawNode rawNodes);

  /// ditto
  void add(RawNode[] rawNodes);

  /// Remove the stored items
  void clear();

  /// Return the number of the stored nodes
  size_t totalNodes();

  /// Get coordinates by node id
  Position get(ulong id);

  /// Check if a node is stored
  bool exists(long id);
}

///
interface IOsmWayStorage {
  /// Set a function that filters ways added to the store
  void filter(bool delegate(ref Way) newFilter);

  /// add ways
  void add(InputRange!Way ways);

  /// ditto
  void add(Way[] ways);

  /// ditto
  void add(ref Way way);

  /// Remove the stored items
  void clear();

  /// Return the number of the stored nodes
  size_t totalWays();

  /// Get coordinates by node id
  Way get(long id);

  /// Check if a way is stored
  bool exists(long id);
}

///
class OsmStorage {
  private {
    IOsmNodeStorage _nodeStorage;
    IOsmWayStorage _wayStorage;
  }

  ///
  this(IOsmNodeStorage nodeStorage, IOsmWayStorage wayStorage) {
    this._nodeStorage = nodeStorage;
    this._wayStorage = wayStorage;
  }

  ///
  IOsmNodeStorage nodeStorage() {
    return this._nodeStorage;
  }

  ///
  const(Position) getNode(ulong id) {
    return this._nodeStorage.get(id);
  }

  ///
  const(Way) getWay(ulong id) {
    return this._wayStorage.get(id);
  }
}
