/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module geo.osm.storage.mongo;

import geo.geometries;
import geo.osm.storage.base;
import std.range;
import std.algorithm;
import std.array;

import vibe.data.json;
import vibe.db.mongo.collection;
import vibe.core.log;

import std.container.rbtree;
import std.container.array;

/// Class that stores collected nodes in memory
class MongoOsmNodeStorage : IOsmNodeStorage {

  private {
    MongoCollection collection;
    Array!RawNode cache;

    RedBlackTree!long keys;
    Bson emptyQuery;
  }

  size_t overflowLimit = 500_000;

  this(MongoCollection collection) {
    this.collection = collection;
    bool hasIdIndex;

    try foreach (index; collection.listIndexes()) {
      auto jindex = index.toJson;

      if("id" in jindex["key"]) {
        hasIdIndex = true;
      }
    } catch(Exception e) {
      logError(e.message);
    }

    if(!hasIdIndex) {
      logInfo("creating index on the id field");
      this.collection.createIndex(["id": 1]);
    }

    emptyQuery = Json.emptyObject;
    keys = new RedBlackTree!(long);

    auto range = collection.find(emptyQuery, ["id": 1]).filter!`!a.isNull && !a["id"].isNull`.map!`a["id"].get!long`;
    keys.stableInsert(range);
    logInfo("there are %s stored nodes", keys.length);
  }

  /// add raw nodes
  void add(ref RawNode node) {
    if(exists(node.id)) {
      return;
    }

    keys.stableInsert(node.id);
    cache.insert(node);

    if(cache.length >= overflowLimit) {
      this.flush;
    }
  }

  void flush() {
    logInfo("flushing data to db...");
    size_t index;
    auto part = cache.length / 20;

    foreach(ref node; cache) {
      collection.insertOne(node);
      index++;

      if(index % part == 0) {
        logInfo(`stored %s/%s`, index, cache.length);
      }
    }

    cache.clear;
  }

  /// ditto
  void add(InputRange!RawNode rawNodes) {
    foreach(node; rawNodes) {
      add(node);
    }
  }

  /// ditto
  void add(RawNode[] rawNodes) {
    foreach(node; rawNodes) {
      add(node);
    }
  }

  /// Remove the stored items
  void clear() {
    assert(false, "not implemented");
  }

  /// Check if a node is stored
  bool exists(long id) {
    return !keys.equalRange(id).empty;
  }

  ///
  size_t totalNodes() {
    auto query = Bson.emptyObject;
    return this.collection.countDocuments!Bson(query);
  }

  ///
  Position get(ulong id) {
    if(!exists(id)) {
      return Position(0, 0);
    }

    foreach(node; cache) {
      if(node.id == id) {
        return Position(node.lon, node.lat);
      }
    }

    auto query = Bson.emptyObject;
    query["id"] = id;

    auto nodeRange = this.collection.find(query);

    if(nodeRange.empty) {
      this.flush;
      nodeRange = this.collection.find(query);
    }

    if(nodeRange.empty) {
      return Position(0, 0);
    }

    auto node = nodeRange.front;
    Position point = Position(node["lon"].get!double, node["lat"].get!double);

    return point;
  }
}
