/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module geo.osm.storage.file;

import geo.osm.format;
import geo.osm.storage.base;
import geo.geometries;
import geo.osm.fileIndex;
import geo.osm.file;

import std.range;
import containers.ttree;
import stdx.allocator.mallocator : Mallocator;

version(unittest) {
  import fluent.asserts;
}

/// Class that stores collected nodes in memory
class FileOsmNodeStorage : IOsmNodeStorage {
  private {
    OsmFileIndex index;
    FileRange fileRange;
  }

  this(OsmFileIndex index, FileRange fileRange) {
    this.index = index;
    this.fileRange = fileRange;
  }

  /// add raw nodes
  void add(InputRange!RawNode rawNodes) {
    assert(false, "you can't add nodes here.");
  }

  /// ditto
  void add(RawNode[] rawNodes) {
    assert(false, "you can't add nodes here.");
  }

  /// ditto
  void add(ref RawNode node) {
    assert(false, "you can't add nodes here.");
  }
  /// Remove the stored items
  void clear() {}

  /// Check if a node exists
  bool exists(long id) {
    return true;
  }

  ///
  size_t totalNodes() {
    return 0;
  }

  ///
  Position get(ulong id) {
    auto node = index.getNodeById(id, fileRange);
    return Position(node.lon, node.lat);
  }
}

///
class FileOsmWayStorage : IOsmWayStorage {

  private {
    bool delegate(ref Way) _filter;
  }

  private {
    OsmFileIndex index;
    FileRange fileRange;
  }

  this(OsmFileIndex index, FileRange fileRange) {
    this.index = index;
    this.fileRange = fileRange;
  }

  void filter(bool delegate(ref Way) newFilter) {
    this._filter = newFilter;
  }

  /// Remove the stored items
  void clear() {}

  void add(ref Way way) {
    assert(false, "you can't add ways here.");
  }

  /// add ways
  void add(InputRange!Way ways) {
    assert(false, "you can't add ways here.");
  }

  /// ditto
  void add(Way[] ways) {
    assert(false, "you can't add ways here.");
  }

  /// Return the number of the stored nodes
  size_t totalWays() {
    return 0;
  }

  /// Get coordinates by node id
  Way get(long id) {
    return index.getWayById(id, fileRange);
  }

  bool canAdd(ref Way way) const {
    assert(false, "you can't add ways here.");
  }

  /// Check if a node exists
  bool exists(long id) {
    return true;
  }
}
