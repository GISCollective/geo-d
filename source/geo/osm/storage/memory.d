/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module geo.osm.storage.memory;

import geo.osm.format;
import geo.osm.storage.base;
import geo.geometries;

import std.range;
import containers.ttree;
import stdx.allocator.mallocator : Mallocator;

version(unittest) {
  import fluent.asserts;
}

/// Class that stores collected nodes in memory
class MemoryOsmNodeStorage : IOsmNodeStorage {
  private {
    TTree!(RawNode, Mallocator, false, "a.id < b.id") nodes;
  }

  /// add raw nodes
  void add(InputRange!RawNode rawNodes) {
    foreach(node; rawNodes) {
      add(node);
    }
  }

  /// ditto
  void add(RawNode[] rawNodes) {
    foreach(ref node; rawNodes) {
      add(node);
    }
  }

  /// ditto
  void add(ref RawNode node) {
    nodes.insert(node);
  }

  /// Remove the stored items
  void clear() {
    nodes.clear();
  }

  /// Check if a node exists
  bool exists(long id) {
    return nodes.contains(RawNode(id));
  }

  ///
  size_t totalNodes() {
    return this.nodes.length;
  }

  ///
  Position get(ulong id) {
    auto range = nodes.equalRange(RawNode(id));

    if(range.empty) {
      return Position(0, 0);
    }

    auto node = range.front;
    return Position(node.lon, node.lat);
  }
}

///
class MemoryOsmWayStorage : IOsmWayStorage {
  private {
    TTree!(Way, Mallocator, false, "a.id < b.id", true, 128) ways;
    bool delegate(ref Way) _filter;
  }

  void filter(bool delegate(ref Way) newFilter) {
    this._filter = newFilter;
  }

  bool canAdd(ref Way way) const {
    if(this._filter is null) {
      return true;
    }

    return this._filter(way);
  }

  void add(ref Way way) {
    if(canAdd(way)) {
      this.ways.insert(way);
    }
  }

  /// add ways
  void add(InputRange!Way ways) {
    foreach(way; ways) {
      this.add(way);
    }
  }

  /// ditto
  void add(Way[] ways) {
    foreach(way; ways) {
      this.add(way);
    }
  }

  /// Remove the stored items
  void clear() {
    ways.clear();
  }

  /// Check if a node exists
  bool exists(long id) {
    return ways.contains(Way(id));
  }

  /// Return the number of the stored nodes
  size_t totalWays() {
    return this.ways.length;
  }

  /// Get coordinates by node id
  Way get(long id) {
    auto range = ways.equalRange(Way(id));

    if(range.empty) {
      return Way(id);
    }

    return range.front;
  }
}

/// It can not find a way before is added
unittest {
  auto storage = new MemoryOsmWayStorage;
  storage.exists(0).should.equal(false);
}

/// It can find a way after is added
unittest {
  auto storage = new MemoryOsmWayStorage;
  auto way = Way(1999, Info(1, 22), [1,2], [3,4], [5,6]);
  storage.add(way);

  storage.exists(1999).should.equal(true);
  storage.get(1999).id.should.equal(1999);
  storage.get(1999).info.version_.should.equal(1);
  storage.get(1999).info.timestamp.should.equal(22);
  storage.get(1999).keys.should.equal([1, 2]);
  storage.get(1999).vals.should.equal([3, 4]);

  long[] expected = [5, 6];
  storage.get(1999).refs.should.equal(expected);
}

/// Adding elements should not work when the filter does not accept them
unittest {
  bool filter(ref Way) {
    return false;
  }

  auto storage = new MemoryOsmWayStorage;
  storage.filter = &filter;

  storage.add([Way()]);
  storage.totalWays.should.equal(0);

  storage.add([Way()].inputRangeObject);
  storage.totalWays.should.equal(0);
}