/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module geo.osm.file;

/// https://wiki.openstreetmap.org/wiki/PBF_Format
/// https://github.com/scrosby/OSM-binary/blob/master/src/osmformat.proto

import std.algorithm;
import std.array : array;
import std.conv;
import std.range;
import std.utf;
import std.string;
import std.bitmanip;
import std.stdio;
import std.datetime;
import stdx.allocator;

import geo.proto;

version(unittest) {
  import fluent.asserts;
}

///
struct BlobHeader {
  /// contains the type of data in this block message.
  string type;

  /// is some arbitrary blob that may include metadata about the following blob,
  /// (e.g., for OSM data, it might contain a bounding box.) This is a stub
  /// intended to enable the future design of indexed *.osm.pbf files.
  ubyte[] indexdata;

  /// contains the serialized size of the subsequent Blob message.
  uint datasize;
}

///
struct Blob {

  // When compressed, the uncompressed size
  uint raw_size;

  /// Possible compressed versions of the data.
  ubyte[] zlib_data;

  /// No compression
  ubyte[] raw;
}

///
struct OsmPbfBlock {
  ///
  BlobHeader header;
  ///
  Blob blob;
}

///
struct PbfProcessStats {
  ///
  size_t processed_blocks;

  ///
  size_t processed_bytes;

  ///
  size_t bytes_per_second;

  ///
  size_t blocks_per_second;

  ///
  string toString() const {
    return "processed_blocks:" ~ processed_blocks.to!string ~ " " ~
      "processed_bytes:" ~ processed_bytes.to!string ~ " " ~
      "bytes_per_second:" ~ bytes_per_second.to!string ~ " " ~
      "blocks_per_second:" ~ blocks_per_second.to!string;
  }
}

///
private size_t fill(T)(ref ubyte[] buffer, T range) if(isInputRange!T && is(ElementType!T == ubyte)) {
  size_t index;

  foreach(value; range) {
    buffer[index] = value;
    index++;
  }

  return index;
}

///
class OsmPbfBlockRange(T) : InputRange!(const(OsmPbfBlock)) {

  private {
    T range;
    bool isEmpty;

    size_t index;
    size_t bytesProcessed;
    SysTime start;
    SysTime end;

    string headerType;
    uint dataSize;

    uint rawSize;
    const(ubyte)[] zlibData;
    ubyte[] buffer;
    IAllocator allocator;
  }

  ///
  this(T range, IAllocator allocator = null) {
    this.range = range;

    this.allocator = allocator;
    if(this.allocator !is null) {
      this.buffer = allocator.makeArray!ubyte(1000);
    }

    this.start = Clock.currTime;
    readBlock();
  }

  private void ensureBufferSize(size_t size) {
    if(buffer.length >= size) {
      return;
    }

    if(this.allocator is null) {
      buffer.length = size;
    }

    if(this.allocator !is null) {
      allocator.expandArray(buffer, size - buffer.length);
    }
  }

  private void readBlock() {
    if(range.empty) {
      isEmpty = true;
      return;
    }

    auto headerLength = bigEndianToNative!(uint, 4)(range.takeExactly(4).array[0..4]);
    bytesProcessed += 4;
    ensureBufferSize(headerLength);

    buffer.fill(range.takeExactly(headerLength));

    scope headerRange = VarintInputRange(buffer[0..headerLength].idup);
    bytesProcessed += headerLength;

    while(!headerRange.empty) {
      auto key = ProtoKey(headerRange.front);
      headerRange.popFront;

      if (key.index == 1) {
        auto len = headerRange.front.to!ulong;
        headerRange.popFront;
        headerType = headerRange.readRaw(len).idup.assumeUTF;
      }

      if (key.index == 3) {
        auto value = headerRange.front.to!uint;
        dataSize = value;
      }
    }

    if(buffer.length < dataSize) {
      buffer.length = dataSize;
    }
    buffer.fill(range.takeExactly(dataSize));

    scope blobRange = VarintInputRange(buffer[0..dataSize]);
    bytesProcessed += dataSize;

    while(!blobRange.empty) {
      auto key = ProtoKey(blobRange.front);
      blobRange.popFront;

      auto value = blobRange.front;
      blobRange.popFront;

      if (key.index == 2) {
        rawSize = value.to!uint;
      }

      if (key.index == 3) {
        zlibData = blobRange.readRaw(value.to!long);
      }
    }

    index++;
    end = Clock.currTime;
  }

  size_t currentByte() {
    return bytesProcessed;
  }

  ///
  const(OsmPbfBlock) front() {
    const header = const BlobHeader(headerType, [], dataSize);
    const blob = const Blob(rawSize, zlibData);

    return const OsmPbfBlock(header, blob);
  }

  const(OsmPbfBlock) moveFront() {
    assert(false);
  }

  ///
  const(PbfProcessStats) stats() {
    PbfProcessStats stats;

    auto totalSeconds = (end - start).total!"seconds";
    if(totalSeconds <= 0) {
      totalSeconds = 1;
    }

    stats.processed_blocks = index;
    stats.processed_bytes = bytesProcessed;
    stats.bytes_per_second = bytesProcessed / totalSeconds;
    stats.blocks_per_second = index / totalSeconds;

    return stats;
  }

  ///
  void popFront() {
    readBlock();
  }

  ///
  bool empty() {
    return isEmpty;
  }

  ///
  int opApply(scope int delegate(const OsmPbfBlock) dg) {
    int result;

    while(!isEmpty) {
      const header = const BlobHeader(headerType, [], dataSize);
      const blob = const Blob(rawSize, zlibData);

      result = dg(const OsmPbfBlock(header, blob));
      if (result) {
        break;
      }

      readBlock();
    }

    return result;
  }

  ///
  int opApply(scope int delegate(size_t, const OsmPbfBlock) dg) {
    int result;

    while(!isEmpty) {
      const header = const BlobHeader(headerType, [], dataSize);
      const blob = const Blob(rawSize, zlibData);

      result = dg(index, const OsmPbfBlock(header, blob));

      if (result) {
        break;
      }
      readBlock();
    }

    return result;
  }

  ~this() {
    if(this.allocator !is null) {
      allocator.dispose(buffer);
    }
  }
}

///
final class FileRange : InputRange!ubyte {

  private {
    File file;
    ubyte[] buffer;
    size_t bufferSize = 4096;
    size_t index;
  }

  /// open a file for reading
  this(string path, long offset = 0) {
    this.file = File(path, "r");
    buffer.length = bufferSize;

    if(offset != 0) {
      this.seek(offset);
      return;
    }

    this.readNextChunk;
  }

  void seek(long offset) {
    this.file.seek(offset, SEEK_SET);
    this.buffer.length = this.bufferSize;
    this.index = 0;
    this.readNextChunk;
  }

  ubyte front() {
    return buffer[index];
  }

  ubyte moveFront() {
    auto value = buffer[index];
    this.popFront;

    return value;
  }

  void popFront() {
    index++;
    if(index >= buffer.length) {
      this.readNextChunk;
    }
  }

  bool empty() {
    return this.buffer.length == 0;
  }

  void readNextChunk() {
    if(this.empty) {
      return;
    }

    this.index = 0;
    this.buffer = file.rawRead(this.buffer);
  }

  ///
  ubyte[] takeExactly(size_t size) {
    if(size + index < buffer.length) {
      auto slice = buffer[index .. size + index];

      index += size;

      return slice;
    }

    ubyte[] data;
    data.length = size;
    auto available = buffer.length - index;

    data[0..available] = buffer[index .. $];
    file.rawRead(data[available..$]);

    this.readNextChunk;

    return data;
  }

  int opApply(scope int delegate(ubyte)) {
    assert(false, "not implementeed");
  }

  int opApply(scope int delegate(ulong, ubyte)) {
    assert(false, "not implementeed");
  }

  string readString(size_t length) {
    return cast(string) this.takeExactly(length).array[0..length];
  }

  byte readByte() {
    auto value = this.front;
    this.popFront;

    return value;
  }

  T read(T)() {
    enum size = T.sizeof;
    auto value = this.takeExactly(size);

    return littleEndianToNative!(T, size)(value.array[0..size]);
  }
}

/// Parsing osm data from pbf file
unittest {
  auto data = new FileRange("test/sample.pbf");
  auto range = new OsmPbfBlockRange!FileRange(data);

  range.front.header.type.should.equal("OSMHeader");
  range.front.header.datasize.should.equal(146);

  range.front.blob.raw_size.should.equal(132);
  range.front.blob.zlib_data.length.should.equal(140);

  range.popFront;

  range.front.header.type.should.equal("OSMData");
  range.front.header.datasize.should.equal(5940);

  range.front.blob.raw_size.should.equal(7540);
  range.front.blob.zlib_data.length.should.equal(5934);

  range.popFront;
  range.front.header.type.should.equal("OSMData");

  range.popFront;
  range.front.header.type.should.equal("OSMData");

  range.popFront;
  range.front.header.type.should.equal("OSMData");

  range.empty.should.equal(true);
}

/// Parsing osm data from pbf file using opApply
unittest {
  auto data = new FileRange("test/sample.pbf");
  auto range = new OsmPbfBlockRange!FileRange(data);

  foreach(block; range) {
    block.header.type.should.equal("OSMHeader");
    block.header.datasize.should.equal(146);

    block.blob.raw_size.should.equal(132);
    block.blob.zlib_data.length.should.equal(140);

    break;
  }

  range.popFront;
}

/// Parsing osm data from pbf file using indexed opApply
unittest {
  auto data = new FileRange("test/sample.pbf");
  auto range = new OsmPbfBlockRange!FileRange(data);

  foreach(index, block; range) {
    block.header.type.should.equal("OSMHeader");
    block.header.datasize.should.equal(146);

    block.blob.raw_size.should.equal(132);
    block.blob.zlib_data.length.should.equal(140);

    break;
  }

  range.popFront;
}
