/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module geo.osm.conv;

import geo.osm.file;
import geo.osm.format;
import geo.osm.storage.base;
import geo.filter;
import geo.geometries;
import geo.conv;
import geo.json;
import geo.feature;
import geo.algorithm;
import geo.stats;

import std.algorithm;
import std.array;
import std.conv;
import std.range;
import std.string;
import std.exception;
import std.variant;
import std.traits;
import std.stdio;

import vibe.data.json;

version(unittest) {
  import fluent.asserts;
  import geo.osm.storage.memory;
}

///
auto features(const PrimitiveBlock block) {
  Feature convert(const Node node) {
    Feature feature;

    Point point;
    double lon = 0.000000001 * (block.lonOffset.to!double + (block.granularity.to!double * node.lon.to!double));
    double lat = 0.000000001 * (block.latOffset.to!double + (block.granularity.to!double * node.lat.to!double));

    point.coordinates = Position(lon, lat);
    feature.geometry = point;

    long id = node.id;
    feature.properties["id"] = id;

    foreach(i; 0..node.keys.length) {
      auto key = node.keys[i];
      auto value = node.vals[i];
      feature.properties[block.stringTable.s[key]] = block.stringTable.s[value];
    }

    return feature;
  }

  Feature convertByIndex(const ulong index) {
    Feature feature;

    Point point;
    double lon = 0.000000001 * (block.lonOffset.to!double +
      (block.granularity.to!double * block.primitiveGroup.dense.lon[index].to!double));
    double lat = 0.000000001 * (block.latOffset.to!double +
      (block.granularity.to!double * block.primitiveGroup.dense.lat[index].to!double));

    point.coordinates = Position(lon, lat);
    feature.geometry = point;

    long id = block.primitiveGroup.dense.id[index];
    feature.properties["id"] = id;

    return feature;
  }

  auto range1 = block.primitiveGroup.nodes.map!(a => convert(a));
  auto range2 = iota(0, block.primitiveGroup.dense.id.length).map!(a => convertByIndex(a));

  return range1.chain(range2);
}

/// extracting features from a PrimitiveBlock should result into a range
unittest {
  PrimitiveBlock block;

  block.features.array.length.should.equal(0);
}

/// extracting a feature from a PrimitiveBlock node
unittest {
  PrimitiveBlock block;
  Node node;

  node.id = 1;
  node.keys = [1, 2];
  node.vals = [3, 4];
  node.lat = 514_779_481;
  node.lon = -14_863;

  block.granularity = 100;
  block.latOffset = 3345;
  block.lonOffset = 8634;

  block.stringTable.s = ["", "a", "b", "c", "d"];
  block.primitiveGroup.nodes = [ node ];

  block.features.array.length.should.equal(1);
  auto feature = block.features.array[0];

  feature.geometry.get!Point.coordinates.to!string.should.equal("[-0.00147767, 51.478]");
  feature.properties["id"].get!long.should.equal(1);
  feature.properties["a"].get!string.should.equal("c");
  feature.properties["b"].get!string.should.equal("d");
}

/// extracting a feature from a PrimitiveBlock dense nodes
unittest {
  PrimitiveBlock block;
  DenseNodes nodes;

  nodes.id = [ 1 ];
  nodes.lat = [514_779_481];
  nodes.lon = [-14_863];

  block.granularity = 100;
  block.latOffset = 3345;
  block.lonOffset = 8634;

  block.primitiveGroup.dense = nodes;

  block.features.array.length.should.equal(1);
  auto feature = block.features.array[0];

  feature.geometry.get!Point.coordinates.to!string.should.equal("[-0.00147767, 51.478]");
  feature.properties["id"].get!long.should.equal(1);
}

/// count features in pbf file
unittest {
  import std.zlib;
  import std.stdio;

  auto data = new FileRange("test/sample.pbf");
  auto range = new OsmPbfBlockRange!FileRange(data);

  range
    .filter!(a => a.header.type == "OSMData")
    .map!(a => cast(ubyte[]) uncompress(a.blob.zlib_data, a.blob.raw_size))
    .map!(a => a.readPrimitiveBlock)
    .map!(a => a.features.length)
    .sum
    .should.equal(290);
}

///
T toGeometry(T)(const Position coordinates) {
  T value;

  Point point;
  point.coordinates = coordinates;
  value = point;

  return value;
}

///
auto rawNodes(const PrimitiveBlock block) {
  RawNode[] result;

  result.reserve(block.primitiveGroup.nodes.length + block.primitiveGroup.dense.id.length);

  foreach(node; block.primitiveGroup.nodes) {
    RawNode rawNode;

    rawNode.id = node.id;
    rawNode.lon = 0.000000001 * (block.lonOffset.to!double + (block.granularity.to!double * node.lon.to!double));
    rawNode.lat = 0.000000001 * (block.latOffset.to!double + (block.granularity.to!double * node.lat.to!double));

    result ~= rawNode;
  }

  foreach(index; 0..block.primitiveGroup.dense.id.length) {
    RawNode node;
    node.id = block.primitiveGroup.dense.id[index];

    node.lon = 0.000000001 * (block.lonOffset.to!double +
      (block.granularity.to!double * block.primitiveGroup.dense.lon[index].to!double));
    node.lat = 0.000000001 * (block.latOffset.to!double +
      (block.granularity.to!double * block.primitiveGroup.dense.lat[index].to!double));

    result ~= node;
  }

  return result.inputRangeObject;
}

/// extracting raw Nodes from a PrimitiveBlock should result into a range
unittest {
  PrimitiveBlock block;

  block.rawNodes.array.length.should.equal(0);
}

/// extracting a raw Nodes from a PrimitiveBlock node
unittest {
  PrimitiveBlock block;
  Node node;

  node.id = 1;
  node.lat = 514_779_481;
  node.lon = -14_863;

  block.granularity = 100;
  block.latOffset = 3345;
  block.lonOffset = 8634;

  block.primitiveGroup.nodes = [ node ];

  block.rawNodes.array.length.should.equal(1);
  auto rawNode = block.rawNodes.array[0];

  rawNode.id.should.equal(1);
  rawNode.lat.to!string.should.equal("51.478");
  rawNode.lon.to!string.should.equal("-0.00147767");
}

/// extracting a raw Nodes from a PrimitiveBlock dense nodes
unittest {
  PrimitiveBlock block;
  DenseNodes nodes;

  nodes.id = [ 1 ];
  nodes.lat = [514_779_481];
  nodes.lon = [-14_863];

  block.granularity = 100;
  block.latOffset = 3345;
  block.lonOffset = 8634;

  block.primitiveGroup.dense = nodes;

  block.features.array.length.should.equal(1);
  auto rawNode = block.rawNodes.array[0];

  rawNode.id.should.equal(1);
  rawNode.lat.to!string.should.equal("51.478");
  rawNode.lon.to!string.should.equal("-0.00147767");
}

/// store raw nodes from pbf file
unittest {
  import std.zlib;
  import core.memory;

  auto data = new FileRange("test/sample.pbf");

  auto range = new OsmPbfBlockRange!FileRange(data);
  auto storage = new MemoryOsmNodeStorage();

  auto rawNodes = range
    .filter!(a => a.header.type == "OSMData")
    .map!(a => cast(immutable ubyte[]) uncompress(a.blob.zlib_data, a.blob.raw_size).idup)
    .map!(a => a.readPrimitiveBlock)
    .map!(a => a.rawNodes)
    .joiner.array;

  storage.add(rawNodes.inputRangeObject);

  storage.totalNodes.should.equal(290);
  rawNodes[0].id.should.equal(653_970_877);
  rawNodes[1].id.should.equal(647_105_170);
  rawNodes[2].id.should.equal(672_663_476);

  rawNodes[0].lat.should.be.approximately(51.76360270000001, 0.0000001);
  rawNodes[1].lat.should.be.approximately(51.76359050000001, 0.0000001);
  rawNodes[2].lat.should.be.approximately(51.7657492, 0.0000001);

  rawNodes[0].lon.should.be.approximately(-0.22875700000000002, 0.0000001);
  rawNodes[1].lon.should.be.approximately(-0.23446450000000002, 0.0000001);
  rawNodes[2].lon.should.be.approximately(-0.22907030000000003, 0.0000001);
}

///
T toGeometry(T)(const Way way, IOsmNodeStorage nodeStorage) {
  T value;

  Position[] coordinates;
  coordinates.length = way.refs.length;

  foreach(ref const i, ref const id; way.refs) {
    coordinates[i] = nodeStorage.get(id);
  }

  if(coordinates.length <= 2 || coordinates[0] != coordinates[coordinates.length - 1]) {
    LineString line;
    line.coordinates = coordinates;
    value = line;
  } else {
    Polygon polygon;
    polygon.coordinates = [ coordinates ].rewind;
    value = polygon;
  }

  return value;
}

Feature convert(ref const PrimitiveBlock block, IOsmNodeStorage nodeStorage, ref const Way way) {
  Feature feature;

  feature.properties["id"] = way.id.to!string;

  foreach(ref const i, ref const keyId; way.keys) {
    const key = block.stringTable.s[keyId];
    const value = block.stringTable.s[way.vals[i]];

    feature.properties[key] = value;
  }

  feature.geometry = way.toGeometry!GeometryVariant(nodeStorage);

  return feature;
}

struct RefFeatureProperties(string type) {
  const(PrimitiveBlock)* block;
  size_t index;

  inout:
    Json[string] toJson() {
      Json[string] result;
      assert(block !is null, "the block is not set!");

      mixin("auto item = block.primitiveGroup." ~ type ~ "[index];");
      result["id"] = Json(item.id.to!string);

      foreach(ref const i, ref const keyId; item.keys) {
        const propertyKey = block.stringTable.s[keyId];
        const value = block.stringTable.s[item.vals[i]];

        result[propertyKey] = value;
      }

      return result;
    }

    Json opIndex(string key) {
      assert(block !is null, "the block is not set!");

      mixin("auto item = block.primitiveGroup." ~ type ~ "[index];");

      if(key == "id") {
        return Json(item.id.to!string);
      }

      foreach(ref const i, ref const keyId; item.keys) {
        const propertyKey = block.stringTable.s[keyId];
        const value = block.stringTable.s[item.vals[i]];

        if(propertyKey == key) {
          return Json(value);
        }
      }

      return Json();
    }

    bool opBinaryRight(string op)(string key) if(op == "in") {
      assert(block !is null, "the block is not set!");

      if(key == "id") {
        return true;
      }

      mixin("auto item = block.primitiveGroup." ~ type ~ "[index];");
      foreach(ref const i, ref const keyId; item.keys) {
        if(block.stringTable.s[keyId] == key) {
          return true;
        }
      }

      return false;
    }
}

///
struct RefWayFeature {
  const(PrimitiveBlock)* block;
  size_t wayIndex;

  IOsmNodeStorage nodeStorage;
  RefFeatureProperties!"ways" properties;

  this(const(PrimitiveBlock)* block, size_t wayIndex, IOsmNodeStorage nodeStorage) {
    this.block = block;
    this.wayIndex = wayIndex;
    this.nodeStorage = nodeStorage;

    this.properties = RefFeatureProperties!"ways"(block, wayIndex);
  }

  GeometryVariant geometry() {
    assert(nodeStorage !is null, "The node storage was not set.");
    return block.primitiveGroup.ways[wayIndex].toGeometry!GeometryVariant(nodeStorage);
  }

  Feature toFeature() {
    Feature feature;
    feature.geometry = this.geometry;
    feature.properties = properties.toJson;

    return feature;
  }
}

/// It can get the way properties
unittest {
  import std.zlib;
  import core.memory;
  import vibe.data.json;

  auto data = new FileRange("test/sample.pbf");
  ///auto data = new FileRange("/home/bogdan/Downloads/albania-latest.osm.pbf");

  auto storage = new MemoryOsmNodeStorage();

  auto blockRange = new OsmPbfBlockRange!FileRange(data);
  auto range = blockRange
    .filter!(a => a.header.type == "OSMData")
    .map!(a => cast(immutable ubyte[]) uncompress(a.blob.zlib_data, a.blob.raw_size).idup)
    .map!(a => a.readPrimitiveBlock)
    .array;

  storage.add(range[0].rawNodes);

  auto feature = RefWayFeature(&range[1], 0, storage);

  feature.properties["id"].should.equal("158788812");
  feature.properties["highway"].should.equal("footway");
  feature.geometry.serializeToJson.to!string.should.equal("{}");
}

struct WayRange(F) {
  const PrimitiveBlock* block;
  IOsmNodeStorage nodeStorage;

  size_t currentIndex;

  F getAt(size_t index) {
    auto refFeature = RefWayFeature(block, index, nodeStorage);

    static if(is(F == RefWayFeature)) {
      return refFeature;
    } else {
      return refFeature.toFeature;
    }
  }

  F[] array() {
    F[] result;
    result.reserve(block.primitiveGroup.ways.length);

    foreach(i; 0..block.primitiveGroup.ways.length) {
      result ~= getAt(i);
    }

    return result;
  }

  @property F front() {
    return getAt(currentIndex);
  }

  F moveFront() {
    currentIndex++;
    return getAt(currentIndex-1);
  }

  void popFront() {
    currentIndex++;
  }

  @property bool empty() {
    return currentIndex >= block.primitiveGroup.ways.length;
  }
}


///
auto ways(ref const PrimitiveBlock block, IOsmNodeStorage nodeStorage) {
  return WayRange!Feature(&block, nodeStorage);
}

///
auto refWays(const PrimitiveBlock* block, IOsmNodeStorage nodeStorage) {
  return WayRange!RefWayFeature(block, nodeStorage);
}

/// Get ways from the pbf file
unittest {
  import std.zlib;
  import core.memory;
  import vibe.data.json;

  auto data = new FileRange("test/sample.pbf");
  ///auto data = new FileRange("/home/bogdan/Downloads/albania-latest.osm.pbf");

  auto storage = new MemoryOsmNodeStorage();

  auto blockRange = new OsmPbfBlockRange!FileRange(data);
  auto range = blockRange
    .filter!(a => a.header.type == "OSMData")
    .map!(a => cast(immutable ubyte[]) uncompress(a.blob.zlib_data, a.blob.raw_size).idup)
    .map!(a => a.readPrimitiveBlock);

  const(Feature)[] ways;
  foreach(block; range) {
    storage.add(block.rawNodes);
    ways ~= block.ways(storage).array;
  }

  ways.length.should.equal(44);

  ways[0].toJson.toString.should.equal(`{"type":"Feature",
    "properties":{"highway":"footway","id":"158788812"},
    "geometry":{"type":"LineString",
      "coordinates":[[-0.2301727,51.7662309],[-0.2300577,51.766196],[-0.229886,51.7659603],[-0.2297466,51.7657707]]}}`
        .parseJsonString.toString);

  ways[1].toJson.toString.should.equal(`{"type":"Feature","properties":{"landuse":"garages","source":"survey",
    "id":"53588781"},"geometry":{"type":"Polygon","coordinates":
      [[[-0.2299369,51.765506],[-0.2297437,51.7653473],[-0.2295753,51.7652089],[-0.2293819,51.76529050000001],
      [-0.2297494,51.7655893],[-0.2299369,51.765506]]]}}`
        .parseJsonString.toString);

  ways[42].toJson.toString.should.equal(`{"type":"Feature","properties":{"foot":"yes","highway":"footway",
    "id":"55071941"},
    "geometry":{"type":"LineString","coordinates":[[-0.2289178,51.76618550000001],[-0.2284883,51.7662934]]}}`
        .parseJsonString.toString);
}

///
const(Feature) convert(const Relation relation, ref const PrimitiveBlock block, OsmStorage storage) {
  Feature feature;

  feature.properties["id"] = relation.id.to!string;

  foreach(ref const i, ref const keyId; relation.keys) {
    const key = block.stringTable.s[keyId];
    const value = block.stringTable.s[relation.vals[i]];

    feature.properties[key] = value;
  }

  if("type" in feature.properties && feature.properties["type"].to!string.toLower == "multipolygon") {
    MultiPolygon multiPolygon;
    multiPolygon.coordinates.length = 1;

    foreach(index, id; relation.memIds) {
      if(relation.types[index] == Relation.MemberType.way) {
        auto way = storage
          .getWay(id)
          .toGeometry!BasicGeometryVariant(storage.nodeStorage);

        if(way.peek!Polygon is null) {
          continue;
        }

        multiPolygon.coordinates[0] ~= way.get!Polygon.coordinates;
      }
    }
    multiPolygon.coordinates[0] = multiPolygon.coordinates[0].rewind;

    feature.geometry = multiPolygon;
    return feature;
  }

  GeometryCollection collection;

  foreach(index, id; relation.memIds) {
    if(relation.types[index] == Relation.MemberType.node) {
      collection.geometries ~= storage.getNode(id).toGeometry!BasicGeometryVariant;
    }

    if(relation.types[index] == Relation.MemberType.way) {
      auto way = storage.getWay(id);

      if(way.refs.length > 0) {
        collection.geometries ~= way.toGeometry!BasicGeometryVariant(storage.nodeStorage);
      }
    }
  }

  feature.geometry = collection;

  return feature;
}

///
struct RefRelationFeature {
  const(PrimitiveBlock)* block;
  size_t relationIndex;

  OsmStorage storage;
  RefFeatureProperties!"relations" properties;

  this(const(PrimitiveBlock)* block, size_t relationIndex, OsmStorage storage) {
    assert(block !is null, "the block is not set!");

    this.block = block;
    this.relationIndex = relationIndex;
    this.storage = storage;

    this.properties = RefFeatureProperties!"relations"(block, relationIndex);
  }

  GeometryVariant geometry() {
    assert(storage !is null, "The storage was not set.");

    auto relation = block.primitiveGroup.relations[relationIndex];


    if("type" in properties && properties["type"].to!string.toLower == "multipolygon") {
      MultiPolygon multiPolygon;
      multiPolygon.coordinates.length = 1;

      foreach(index, id; relation.memIds) {
        if(relation.types[index] == Relation.MemberType.way) {
          auto way = storage
            .getWay(id)
            .toGeometry!BasicGeometryVariant(storage.nodeStorage);

          if(way.peek!Polygon is null) {
            continue;
          }

          multiPolygon.coordinates[0] ~= way.get!Polygon.coordinates;
        }
      }
      multiPolygon.coordinates[0] = multiPolygon.coordinates[0].rewind;

      return GeometryVariant(multiPolygon);
    }

    GeometryCollection collection;

    foreach(index, id; relation.memIds) {
      if(relation.types[index] == Relation.MemberType.node) {
        collection.geometries ~= storage.getNode(id).toGeometry!BasicGeometryVariant;
      }

      if(relation.types[index] == Relation.MemberType.way) {
        auto way = storage.getWay(id);

        if(way.refs.length > 0) {
          collection.geometries ~= way.toGeometry!BasicGeometryVariant(storage.nodeStorage);
        }
      }
    }

    return GeometryVariant(collection);
  }

  const(Feature) toFeature() {
    Feature feature;
    feature.geometry = this.geometry;
    feature.properties = properties.toJson;

    return feature;
  }
}


/// A multipolgon feature should be converted to a multipolygon feature
unittest {
  auto nodeStorage = new MemoryOsmNodeStorage();
  auto wayStorage = new MemoryOsmWayStorage();
  auto storage = new OsmStorage(nodeStorage, wayStorage);

  nodeStorage.add([ RawNode(1, 1, 1), RawNode(2, 2, 2), RawNode(3, 1, 2) ]);
  nodeStorage.add([ RawNode(4, 10, 10), RawNode(5, 20, 20), RawNode(6, 10, 20) ]);

  wayStorage.add([ Way(2, Info(), [], [], [1,2,3,1]), Way(3, Info(), [], [], [4,5,6,4]) ]);

  Relation relation;
  relation.id = 1;
  relation.keys = [1];
  relation.vals = [2];
  relation.memIds = [2, 3];
  relation.types = [ Relation.MemberType.way, Relation.MemberType.way ];

  PrimitiveBlock block;
  block.stringTable.s = [ "", "type", "multipolygon" ];
  block.primitiveGroup.relations = [ relation ];

  const PrimitiveBlock constBlock = block;

  auto feature = RefRelationFeature(&block, 0, storage).toFeature.toJson;

  feature["geometry"]["type"].should.equal("MultiPolygon");
  feature["geometry"]["coordinates"].length.should.equal(1);
  feature["geometry"]["coordinates"][0].length.should.equal(2);
}

struct RelationRange(F) {
  const(PrimitiveBlock)* block;
  OsmStorage nodeStorage;

  size_t currentIndex;

  const(F) getAt(size_t index) {
    auto refFeature = RefRelationFeature(block, currentIndex, nodeStorage);

    static if(is(F == RefRelationFeature)) {
      return refFeature;
    } else {
      return refFeature.toFeature;
    }
  }

  const(F)[] array() {
    const(F)[] result;
    result.reserve(block.primitiveGroup.relations.length);

    foreach(i; 0..block.primitiveGroup.relations.length) {
      result ~= getAt(i);
    }

    return result;
  }

  const(F) front() {
    return getAt(currentIndex);
  }

  const(F) moveFront() {
    currentIndex++;
    return getAt(currentIndex-1);
  }

  void popFront() {
    currentIndex++;
  }

  bool empty() {
    return currentIndex >= block.primitiveGroup.relations.length;
  }
}

///
auto relations(ref const PrimitiveBlock block, OsmStorage storage) {
  return RelationRange!Feature(&block, storage);
}


///
auto refRelations(PrimitiveBlock* block, OsmStorage storage) {
  return RelationRange!RefRelationFeature(block, storage);
}

/// Get relations from the pbf file
unittest {
  import std.zlib;
  import core.memory;
  import vibe.data.json;
  import std.stdio;

  auto data = new FileRange("test/sample.pbf");

  auto nodeStorage = new MemoryOsmNodeStorage();
  auto wayStorage = new MemoryOsmWayStorage();
  auto storage = new OsmStorage(nodeStorage, wayStorage);

  auto blockRange = new OsmPbfBlockRange!FileRange(data);
  auto range = blockRange
    .filter!(a => a.header.type == "OSMData")
    .map!(a => cast(immutable ubyte[]) uncompress(a.blob.zlib_data, a.blob.raw_size).idup)
    .map!(a => a.readPrimitiveBlock);

  const(Feature)[] ways;
  const(Feature)[] relations;

  foreach(block; range) {
    nodeStorage.add(block.rawNodes);
    wayStorage.add(block.primitiveGroup.ways.inputRangeObject);
    ways ~= block.ways(nodeStorage).array;

    relations ~= block.relations(storage).array;
    //writeln(relations.map!(a => a.toJson.toPrettyString).joiner("\n\n").array);
  }

  ways.length.should.equal(44);

  relations[0].toJson.toString.should.equal(`{
    "type": "Feature",
    "properties": { "type": "tunnel", "id": "21855", "name": "Hatfield Tunnel" },
    "geometry": {
      "type": "GeometryCollection",
      "geometries": [
        {
          "type": "LineString",
          "coordinates": [ [-0.2334775, 51.76912960000001], [-0.233758, 51.76868260000001], [-0.2340633, 51.7681708], [-0.2343948, 51.7675723], [-0.2347828, 51.76692430000001], [-0.2351212, 51.7663492], [-0.2354077, 51.76590090000001], [-0.2358026, 51.7653887], [-0.2372349, 51.7637769], [-0.2376388, 51.7633111], [-0.2379885, 51.7629584], [-0.2384847, 51.762503], [-0.2389377, 51.7621347], [-0.2398273, 51.76150620000001], [-0.2402177, 51.7612128], [-0.2407377, 51.7607865], [-0.2411607, 51.76041050000001], [-0.2415577, 51.7600489]]
        }
      ]
    }
  }`.parseJsonString.toString);

  relations[1].toJson["properties"].toString.should.equal(`{"type":"tunnel","id":"21855","name":"Hatfield Tunnel"}`.parseJsonString.toString);

  relations[1].toJson["geometry"]["geometries"].length.should.not.equal(0);

  relations[1].toJson["geometry"]["geometries"][0].toString.should.equal(`{"type":"LineString","coordinates":
    [[-0.2334775,51.76912960000001],[-0.233758,51.76868260000001],[-0.2340633,51.7681708],[-0.2343948,51.7675723],[-0.2347828,51.76692430000001],
     [-0.2351212,51.7663492],[-0.2354077,51.76590090000001],[-0.2358026,51.7653887],[-0.2372349,51.7637769],[-0.2376388,51.7633111],[-0.2379885,51.7629584],
     [-0.2384847,51.762503],[-0.2389377,51.7621347],[-0.2398273,51.76150620000001],[-0.2402177,51.7612128],[-0.2407377,51.7607865],[-0.2411607,51.76041050000001],[-0.2415577,51.7600489]]}`
      .parseJsonString.toString);
  /*
  relations[1].toJson["geometry"]["geometries"][142].toString.should.equal(
      `{"type":"LineString","coordinates":[[-0.2323818,51.76440340000001],[-0.2301331,51.7655773],
      [-0.2300248,51.7656315],[-0.2297466,51.7657707],[-0.2289178,51.76618550000001],[-0.2288234,51.7662328]]}`
        .parseJsonString.toString);

  */
  relations[2].toJson.toString.should.equal(`{"type":"Feature","properties":
    {"type":"tunnel","id":"21855","name":"Hatfield Tunnel"},
    "geometry":
    {"type":"GeometryCollection","geometries":[{"type":"LineString","coordinates":[[-0.2334775,51.76912960000001],[-0.233758,51.76868260000001],[-0.2340633,51.7681708],
      [-0.2343948,51.7675723],[-0.2347828,51.76692430000001],[-0.2351212,51.7663492],[-0.2354077,51.76590090000001],[-0.2358026,51.7653887],[-0.2372349,51.7637769],[-0.2376388,51.7633111],
      [-0.2379885,51.7629584],[-0.2384847,51.762503],[-0.2389377,51.7621347],[-0.2398273,51.76150620000001],[-0.2402177,51.7612128],[-0.2407377,51.7607865],[-0.2411607,51.76041050000001],
      [-0.2415577,51.7600489]]}]}}`
        .parseJsonString.toString);
}


/// Struct that defines how an import is handeled
struct OsmImport {
  /// Layers that will be imported
  OsmImportLayer[string] layers;

  @optional UpdateStats[string] stats;

  /// Delegate called when a feature matches a layer
  void delegate(const string, const Feature) @safe onMatch;

  /// Add a features to the import. If matches a layer then it will be sent to the
  /// onMatch delegate
  void add(T)(T features) {
    foreach(key, layer; layers) {
      if(key !in stats) {
        stats[key] = UpdateStats();
      }

      foreach(item; features) {
        alias ItemType = Unqual!(typeof(item));

        static if(is(ItemType == Feature)) {
          auto feature = item;
        } else {
          const feature = (cast()item).toFeature;
        }

        if(layer.filter.match(feature.properties)) {
          if(layer.fields.length == 0) {
            this.onMatch(key, feature);
          } else {
            stats[key].start;
            auto updatedFeature = layer.update(feature);
            stats[key].end;

            this.onMatch(key, updatedFeature);
          }
        }
      }
    }
  }

  void printStats() {
    foreach(key, stat; stats) {
      writeln("[", key, "] ", stat.toString);
    }
  }
}

/// parse OsmImport structure
unittest {
  import std.file;
  import vibe.data.json;

  auto rules = readText("test/osmFilter.json").parseJsonString.deserializeJson!OsmImport;
  Json[string] properties;

  rules.layers["building"].filter.match(properties).should.equal(false);

  properties["building"] = "true";
  rules.layers["building"].filter.match(properties).should.equal(true);
}

/// check on match delegate
unittest {
  import std.file;
  import vibe.data.json;

  auto rules = readText("test/osmFilter.json").parseJsonString.deserializeJson!OsmImport;
  Json[string] properties;

  auto feature = `{"type":"Feature","properties":{"building":"no"},"geometry":{"type":"GeometryCollection","geometries":[]}}`
    .parseJsonString.deserializeJson!Feature;

  size_t count;
  void onMatch(const string layer, const Feature foundFeature) @safe {
    layer.should.equal("building");
    foundFeature.serializeToJson.should.equal(feature.serializeToJson);
    count++;
  }

  rules.onMatch = &onMatch;
  rules.add([ feature ]);

  count.should.equal(1);
}

/// Layer import definition
struct OsmImportLayer {
  /// Rule for which features will be added to this layer
  Filter filter;

  /// Rule defining how fields will be created
  @optional OsmFieldImport[string] fields;

  /// Update feature properties based on the fields
  Feature update(const Feature feature) const {
    Feature result;
    result.geometry = feature.geometry.toJson.toGeometryVariant;

    foreach(key, field; fields) {
      auto value = field.get(feature.properties);

      if(value != "") {
        result.properties[key] = value;
      }
    }

    return result;
  }
}

/// OsmImportLayer update feature properties
unittest {
  auto feature = `{"type":"Feature","properties":{"building":"no"},"geometry":{"type":"GeometryCollection","geometries":[]}}`
      .parseJsonString.deserializeJson!Feature;

  auto layer = OsmImportLayer();
  layer.fields["test"] = `{ "from": "building" }`.parseJsonString.deserializeJson!OsmFieldImport;
  layer.fields["missing"] = `{ "from": "missing" }`.parseJsonString.deserializeJson!OsmFieldImport;

  auto result = layer.update(feature);

  result.toJson.should.equal(`{"type":"Feature",
    "properties":{"test":"no"},
    "geometry":{"type":"GeometryCollection","geometries":[]}}`.parseJsonString);
}

/// Rule defining how a field will be created
struct OsmFieldImport {
  /// List of fields that will be used to create the value
  string[] from;

  /// Default value
  string default_;

  /// Custom function used to convert the value
  string transform;

  /// A list of possible values
  string[] enum_;

  ///
  string get(T)(const(T[string]) properties) const {
    string result = default_;

    foreach(key; from) {
      if(key in properties) {
        static if(is(T == string)) {
          result = properties[key];
        }

        static if(is(T == Json)) {
          result = properties[key].to!string;
        }

        static if(is(T == Variant)) {
          result = properties[key].toJson.to!string;
        }

        if(enum_.length > 0 && !enum_.canFind(result)) {
          result = default_;
        } else {
          break;
        }
      }
    }

    return result;
  }

  ///
  Json toJson() const @safe {
    assert(false, "not implemented");
  }

  ///
  static OsmFieldImport fromJson(Json src) @safe {
    OsmFieldImport result;

    if(src.type == Json.Type.string) {
      result.from ~= src.to!string;

      return result;
    }

    if(src.type == Json.Type.array) {
      result.from = src.deserializeJson!(string[]);

      return result;
    }

    enforce(src.type == Json.Type.object, "Expected field to be a string, string list or an object");

    if("from" in src) {
      if(src["from"].type == Json.Type.string) {
        result.from ~= src["from"].to!string;
      }

      if(src["from"].type == Json.Type.array) {
        result.from = src["from"].deserializeJson!(string[]);
      }
    }

    if("default" in src) {
      result.default_ = src["default"].to!string;
    }

    if("enum" in src) {
      result.enum_ ~= src["enum"].deserializeJson!(string[]);
    }

    return result;
  }
}

/// Convert a string Json to OsmFieldImport
unittest {
  auto result = `"test"`.parseJsonString.deserializeJson!OsmFieldImport;

  result.from.should.equal(["test"]);
}

/// Convert a string list Json to OsmFieldImport
unittest {
  auto result = `["test", "test2"]`.parseJsonString.deserializeJson!OsmFieldImport;

  result.from.should.equal(["test", "test2"]);
}

/// Convert an object Json to OsmFieldImport
unittest {
  auto result = `{ "from": "test", "default": "value" }`
    .parseJsonString.deserializeJson!OsmFieldImport;

  result.from.should.equal(["test"]);
}

/// Convert an object Json with a from list to OsmFieldImport
unittest {
  auto result = `{ "from": ["test", "test2"] }`
    .parseJsonString.deserializeJson!OsmFieldImport;

  result.from.should.equal(["test", "test2"]);
}

/// Convert an object Json with all properties to OsmFieldImport
unittest {
  auto result = `{
      "from": ["test", "test2"],
      "default": "other",
      "transform": "toFeet",
      "enum": [ "international", "public", "regional", "military", "private", "other" ]
    }`
    .parseJsonString.deserializeJson!OsmFieldImport;

  result.from.should.equal(["test", "test2"]);
  result.default_.should.equal("other");
  result.enum_.should.equal([ "international", "public", "regional", "military", "private", "other" ]);
}

/// handle OsmFieldImport enum
unittest {
  auto result = `{
      "from": ["test", "test2"],
      "default": "other",
      "enum": [ "international", "public", "regional", "military", "private", "other" ]
    }`
    .parseJsonString.deserializeJson!OsmFieldImport;

  result.get(["a": "a"]).should.equal("other");
  result.get(["test2": "public"]).should.equal("public");
  result.get(["test2": "a"]).should.equal("other");
  result.get(["test": "", "test2": "public"]).should.equal("public");
  result.get(["test": "public", "test2": ""]).should.equal("public");
}
