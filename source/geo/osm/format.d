/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module geo.osm.format;

/// https://wiki.openstreetmap.org/wiki/PBF_Format
/// https://github.com/scrosby/OSM-binary/blob/master/src/osmformat.proto

import std.algorithm;
import std.array : array;
import std.conv;
import std.range;
import std.utf;
import std.string;
import std.bitmanip;
import std.stdio;
import std.zlib;
import std.traits;
import std.exception;

import geo.proto;
import geo.osm.file;
import geo.geometries;

version(unittest) {
  import fluent.asserts;
}

/// The bounding box field in the OSM header. BBOX, as used in the OSM
/// header. Units are always in nanodegrees -- they do not obey
/// granularity rules.
alias HeaderBBox = BBox!long;

/// Convert protobuf to a readHeaderBBox
HeaderBBox readHeaderBBox(T)(T data) if(isInputRange!T && is(Unqual!(ElementType!T) == ubyte)) {
  scope range = VarintInputRange(data);
  return readHeaderBBox(range);
}

/// ditto
HeaderBBox readHeaderBBox(VarintInputRange range) {
  HeaderBBox headerBBox;

  while(!range.empty) {
    auto key = ProtoKey(range.front);
    range.popFront;

    auto value = range.front.to!long;
    range.popFront;

    if(key.index == 1) {
      headerBBox.left = value;
    }

    if(key.index == 2) {
      headerBBox.right = value;
    }

    if(key.index == 3) {
      headerBBox.top = value;
    }

    if(key.index == 4) {
      headerBBox.bottom = value;
    }
  }

  return headerBBox;
}

/// convert header protobuf to header bbox
unittest {
  ubyte[] data = [
    0x08, 0xd0, 0xda, 0xd6, 0x98, 0x3f,
    0x10, 0xd0, 0xbc, 0x8d, 0xfe, 0x42, 0x18, 0x80, 0xe1,
    0xad, 0xb7, 0x8f, 0x03, 0x20, 0x80, 0x9c, 0xa2, 0xfb,
    0x8a, 0x03];

  auto result = readHeaderBBox(data);

  result.top.should.equal(107221840000);
  result.left.should.equal(16963186000);
  result.right.should.equal(17981202000);
  result.bottom.should.equal(106022080000);
}

///
struct OsmHeader {
  ///
  HeaderBBox bbox;

  /// Additional tags to aid in parsing this dataset
  string[] requiredFeatures;

  /// ditto
  string[] optionalFeatures;

  ///
  string writingProgram;

  /// From the bbox field.
  string source;

  /// replication timestamp, expressed in seconds since the epoch,
  /// otherwise the same value as in the "timestamp=..." field
  /// in the state.txt file used by Osmosis
  ulong osmosisReplicationTimestamp;

  /// replication sequence number (sequenceNumber in state.txt)
  long osmosisReplicationSequenceNumber;

  /// replication base URL (from Osmosis' configuration.txt file)
  string osmosisReplicationBaseUrl;
}

/// Convert protobuf data to osm header
OsmHeader readOsmHeader(T)(T data) if(isInputRange!T && is(Unqual!(ElementType!T) == ubyte)) {
  scope range = VarintInputRange(data);
  return readOsmHeader(range);
}

/// ditto
OsmHeader readOsmHeader(VarintInputRange range) {
  OsmHeader header;

  while(!range.empty) {
    auto key = ProtoKey(range.front);
    range.popFront;

    auto value = range.front;
    range.popFront;

    if(key.index == 1) {
      header.bbox = readHeaderBBox(range.readRaw(value.to!ulong));
    }

    if(key.index == 4) {
      header.requiredFeatures ~= range.readRaw(value.to!ulong).idup.assumeUTF;
    }

    if(key.index == 5) {
      header.optionalFeatures ~= range.readRaw(value.to!ulong).idup.assumeUTF;
    }

    if(key.index == 6) {
      header.writingProgram ~= range.readRaw(value.to!ulong).idup.assumeUTF;
    }

    if(key.index == 16) {
      header.writingProgram ~= range.readRaw(value.to!ulong).idup.assumeUTF;
    }

    if(key.index == 17) {
      header.source ~= range.readRaw(value.to!ulong).idup.assumeUTF;
    }

    if(key.index == 32) {
      header.osmosisReplicationTimestamp = value.to!ulong;
    }

    if(key.index == 33) {
      header.osmosisReplicationSequenceNumber = value.to!long;
    }

    if(key.index == 34) {
      header.osmosisReplicationBaseUrl = range.readRaw(value.to!ulong).idup.assumeUTF;
    }
  }

  return header;
}

/// reads osm header using GC
unittest {
  auto data = new FileRange("test/sample.pbf");
  auto range = new OsmPbfBlockRange!FileRange(data);
  range.front.header.type.should.equal("OSMHeader");

  auto decompressor = new UnCompress();
  ubyte[] uncompressedData = cast(ubyte[]) decompressor.uncompress(range.front.blob.zlib_data);

  auto header = readOsmHeader(uncompressedData);

  header.bbox.left.should.equal(470_752_199L);
  header.bbox.right.should.equal(457_026_799L);
  header.bbox.top.should.equal(103_533_719_400L);
  header.bbox.bottom.should.equal(103_529_681_400L);

  header.requiredFeatures.should.containOnly(["OsmSchema-V0.6", "DenseNodes"]);
  header.optionalFeatures.should.containOnly(["Sort.Type_then_ID"]);
  header.writingProgram.should.equal("osmconvert 0.7G");
  header.source.should.equal("http://www.openstreetmap.org/api/0.6");
}

/// reads osm header using a custom allocator
// unittest {
//   import stdx.allocator, core.memory;
//   auto before = GC.stats.usedSize;

//   auto data = new FileRange("test/sample.pbf");
//   scope range = new OsmPbfBlockRange(data, theAllocator);
//   range.front.header.type.should.equal("OSMHeader");

//   auto decompressor = new UnCompress();
//   ubyte[] uncompressedData = cast(ubyte[]) decompressor.uncompress(range.front.blob.zlib_data);

//   auto header = readOsmHeader(uncompressedData);

//   header.bbox.left.should.equal(470_752_199L);
//   header.bbox.right.should.equal(457_026_799L);
//   header.bbox.top.should.equal(103_533_719_400L);
//   header.bbox.bottom.should.equal(103_529_681_400L);

//   header.requiredFeatures.should.containOnly(["OsmSchema-V0.6", "DenseNodes"]);
//   header.optionalFeatures.should.containOnly(["Sort.Type_then_ID"]);
//   header.writingProgram.should.equal("osmconvert 0.7G");
//   header.source.should.equal("http://www.openstreetmap.org/api/0.6");

//   auto used = GC.stats.usedSize - before;
//   used.should.equal(0);
// }

///
struct StringTable {
  string[] s;
}

///
StringTable readStringTable(T)(T data) if(isInputRange!T && is(Unqual!(ElementType!T) == ubyte)) {
  scope range = VarintInputRange(data);
  return readStringTable(range);
}

///
StringTable readStringTable(VarintInputRange range) {
  StringTable table;

  while(!range.empty) {
    auto key = ProtoKey(range.front);
    range.popFront;

    auto value = range.front;
    range.popFront;

    if(key.index == 1) {
      table.s ~= range.readRaw(value.to!ulong).idup.assumeUTF;
    }
  }

  return table;
}

///
struct Node {
  long id;

  uint[] keys;
  uint[] vals;

  Info info;

  long lat;
  long lon;
}

/// Convert prootobuf data to Node
Node readNode(T)(T data) if(isInputRange!T && is(Unqual!(ElementType!T) == ubyte)) {
  scope range = VarintInputRange(data);
  return readNode(range);
}

/// ditto
Node readNode(VarintInputRange range) {
  Node node;

  assert(false, "not implemented");
  ///return node;
}

///
struct Way {
  long id;
  Info info;

  uint[] keys;

  uint[] vals;

  long[] refs;
}

/// Convert prootobuf data to Way
Way readWay(T)(T data) if(isInputRange!T && is(Unqual!(ElementType!T) == ubyte)) {
  scope range = VarintInputRange(data);
  return readWay(range);
}

/// ditto
Way readWay(VarintInputRange range) {
  Way way;

  while(!range.empty) {
    mixin(genReadKey());

    mixin(genRead("way.id", "long", "1"));
    mixin(genReadPacked("way.keys", "uint", "2"));
    mixin(genReadPacked("way.vals", "uint", "3"));
    mixin(genReadStruct("way.info", "Info", "4"));
    mixin(genReadZigZagDeltaPacked("way.refs", "long", "8"));
  }

  return way;
}

///
struct Relation {
  ///
  enum MemberType : uint {
    node = 0,
    way = 1,
    relation = 2
  }

  ///
  long id;

  uint[] keys;
  uint[] vals;

  Info info;

  int[] rolesSid;

  long[] memIds;

  MemberType[] types;
}

/// Convert prootobuf data to Relation
Relation readRelation(T)(T data) if(isInputRange!T && is(Unqual!(ElementType!T) == ubyte)) {
  scope range = VarintInputRange(data);
  return readRelation(range);
}

/// ditto
Relation readRelation(VarintInputRange range) {
  Relation relation;

  while(!range.empty) {
    mixin(genReadKey());

    mixin(genRead("relation.id", "long", "1"));
    mixin(genReadPacked("relation.keys", "uint", "2"));
    mixin(genReadPacked("relation.vals", "uint", "3"));
    mixin(genReadStruct("relation.info", "Info", "4"));

    mixin(genReadPacked("relation.rolesSid", "int", "8"));
    mixin(genReadZigZagDeltaPacked("relation.memIds", "long", "9"));
    mixin(genReadPacked("relation.types", "Relation.MemberType", "10"));
  }

  return relation;
}

///
struct ChangeSet {
}

/// Convert prootobuf data to ChangeSet
ChangeSet readChangeSet(T)(T data) if(isInputRange!T && is(Unqual!(ElementType!T) == ubyte)) {
  scope range = VarintInputRange(data);
  return readChangeSet(range);
}

/// ditto
ChangeSet readChangeSet(VarintInputRange range) {
  ChangeSet set;

  assert(false, "not implemented");
  ///return set;
}

///
struct Info {
  int version_ = -1;
  long timestamp;
  long changeSet;
  int uid;
  uint user_sid;

  /// The visible flag is used to store history information. It indicates that
  /// the current object version has been created by a delete operation on the
  /// OSM API.
  /// When a writer sets this flag, it MUST add a required_features tag with
  /// value "HistoricalInformation" to the HeaderBlock.
  /// If this flag is not available for some object it MUST be assumed to be
  /// true if the file has the required_features tag "HistoricalInformation"
  /// set.
  bool visible;
}

/// Convert prootobuf data to Way
Info readInfo(T)(T data) if(isInputRange!T && is(Unqual!(ElementType!T) == ubyte)) {
  scope range = VarintInputRange(data);
  return readInfo(range);
}

/// ditto
Info readInfo(VarintInputRange range) {
  Info info;

  while(!range.empty) {
    mixin(genReadKey());

    mixin(genRead("info.version_", "int", "1"));
    mixin(genRead("info.timestamp", "long", "2"));
    mixin(genRead("info.changeSet", "long", "3"));
    mixin(genRead("info.uid", "int", "4"));
    mixin(genRead("info.user_sid", "uint", "5"));
    mixin(genRead("info.visible", "bool", "6"));
  }

  return info;
}

///
struct DenseInfo {
  int[] version_;
  long[] timestamp;
  long[] changeset;
  long[] uid;
  long[] user_sid;

  /// The visible flag is used to store history information. It indicates that
  /// the current object version has been created by a delete operation on the
  /// OSM API.
  /// When a writer sets this flag, it MUST add a required_features tag with
  /// value "HistoricalInformation" to the HeaderBlock.
  /// If this flag is not available for some object it MUST be assumed to be
  /// true if the file has the required_features tag "HistoricalInformation"
  /// set.
  bool[] visible;
}

/// Convert prootobuf data to osm header
DenseInfo readDenseInfo(T)(T data) if(isInputRange!T && is(Unqual!(ElementType!T) == ubyte)) {
  scope range = VarintInputRange(data);
  return readDenseInfo(range);
}

/// ditto
DenseInfo readDenseInfo(VarintInputRange range) {
  DenseInfo info;

  while(!range.empty) {
    mixin(genReadKey());

    mixin(genReadPacked("info.version_", "int", "1"));
    mixin(genReadZigZagDeltaPacked("info.timestamp", "long", "2"));
    mixin(genReadZigZagDeltaPacked("info.changeset", "long", "3"));
    mixin(genReadZigZagDeltaPacked("info.uid", "long", "4"));
    mixin(genReadZigZagDeltaPacked("info.user_sid", "long", "5"));
    mixin(genReadPacked("info.visible", "bool", "6"));
  }

  return info;
}

///
struct DenseNodes {
  ///
  long[] id;

  /// repeated Info info = 4;
  DenseInfo denseinfo;

  ///
  long[] lat;
  ///
  long[] lon;

  /// Special packing of keys and vals into one array. May be empty if all nodes in this block are tagless.
  int[] keysVals;
}

/// Convert prootobuf data to osm header
DenseNodes readDenseNodes(T)(T data) if(isInputRange!T && is(Unqual!(ElementType!T) == ubyte)) {
  scope range = VarintInputRange(data);
  return readDenseNodes(range);
}

/// ditto
DenseNodes readDenseNodes(VarintInputRange range) {
  DenseNodes nodes;

  while(!range.empty) {
    mixin(genReadKey());

    mixin(genReadZigZagDeltaPacked("nodes.id", "long", "1"));
    mixin(genReadStruct("nodes.denseinfo", "DenseInfo", "5"));

    mixin(genReadZigZagDeltaPacked("nodes.lat", "long", "8"));
    mixin(genReadZigZagDeltaPacked("nodes.lon", "long", "9"));
    mixin(genReadPacked("nodes.keysVals", "int", "10"));
  }

  return nodes;
}

///
struct PrimitiveGroup {
  ///
  Node[] nodes;

  ///
  DenseNodes dense;

  ///
  Way[] ways;

  ///
  Relation[] relations;

  ///
  ChangeSet[] changeSets;
}

/// Convert prootobuf data to osm header
PrimitiveGroup readPrimitiveGroup(T)(T data) if(isInputRange!T && is(Unqual!(ElementType!T) == ubyte)) {
  scope range = VarintInputRange(data);
  return readPrimitiveGroup(range);
}

/// ditto
PrimitiveGroup readPrimitiveGroup(VarintInputRange range) {
  PrimitiveGroup group;

  while(!range.empty) {
    mixin(genReadKey());

    mixin(genReadRepeatedStruct("group.nodes", "Node", "1"));

    mixin(genReadStruct("group.dense", "DenseNodes", "2"));

    mixin(genReadRepeatedStruct("group.ways", "Way", "3"));

    mixin(genReadRepeatedStruct("group.relations", "Relation", "4"));

    mixin(genReadRepeatedStruct("group.changeSets", "ChangeSet", "5"));

    //enforce(false, "unknown field index `" ~ key.index.to!string ~ "`");
  }

  return group;
}

///
struct PrimitiveBlock {
  ///
  StringTable stringTable;

  ///
  PrimitiveGroup primitiveGroup;

  /// Granularity, units of nanodegrees, used to store coordinates in this block
  int granularity = 100;

  /// Offset value between the output coordinates coordinates and the granularity grid, in units of nanodegrees.
  long latOffset;

  /// ditto
  long lonOffset;

  /// Granularity of dates, normally represented in units of milliseconds since the 1970 epoch.
  int date_granularity = 1000;
}

/// Convert prootobuf data to osm header
PrimitiveBlock readPrimitiveBlock(T)(T data) if(isInputRange!T && is(Unqual!(ElementType!T) == ubyte)) {
  scope range = VarintInputRange(data);
  return readPrimitiveBlock(range);
}

/// ditto
PrimitiveBlock readPrimitiveBlock(VarintInputRange range) {
  PrimitiveBlock block;

  while(!range.empty) {
    mixin(genReadKey());

    mixin(genReadStruct("block.stringTable", "StringTable", "1"));
    mixin(genReadStruct("block.primitiveGroup", "PrimitiveGroup", "2"));
    mixin(genRead("block.granularity", "int", "17"));
    mixin(genRead("block.date_granularity", "int", "18"));
    mixin(genRead("block.latOffset", "long", "19"));
    mixin(genRead("block.lonOffset", "long", "20"));
  }

  return block;
}

/// reading the first primitive block
unittest {
  auto data = new FileRange("test/sample.pbf");
  auto range = new OsmPbfBlockRange!FileRange(data);
  range.popFront;
  range.front.header.type.should.equal("OSMData");

  auto decompressor = new UnCompress();
  ubyte[] uncompressedData = cast(ubyte[]) decompressor.uncompress(range.front.blob.zlib_data);

  auto block = readPrimitiveBlock(uncompressedData);

  block.primitiveGroup.dense.id.length.should.equal(290);

  block.primitiveGroup.nodes.length.should.equal(0);
  block.primitiveGroup.ways.length.should.equal(0);
  block.primitiveGroup.relations.length.should.equal(0);
  block.primitiveGroup.changeSets.length.should.equal(0);
}

/// reading the second primitive block
unittest {
  auto data = new FileRange("test/sample.pbf");
  auto range = new OsmPbfBlockRange!FileRange(data);
  range.popFront;
  range.popFront;
  range.front.header.type.should.equal("OSMData");

  auto decompressor = new UnCompress();
  ubyte[] uncompressedData = cast(ubyte[]) decompressor.uncompress(range.front.blob.zlib_data);

  auto block = readPrimitiveBlock(uncompressedData);

  block.primitiveGroup.dense.id.length.should.equal(0);
  block.primitiveGroup.nodes.length.should.equal(0);
  block.primitiveGroup.ways.length.should.equal(44);
  block.primitiveGroup.relations.length.should.equal(0);
  block.primitiveGroup.changeSets.length.should.equal(0);
}

/// reading the 3rd primitive block
unittest {
  auto data = new FileRange("test/sample.pbf");
  auto range = new OsmPbfBlockRange!FileRange(data);
  range.popFront;
  range.popFront;
  range.popFront;
  range.front.header.type.should.equal("OSMData");

  auto decompressor = new UnCompress();
  ubyte[] uncompressedData = cast(ubyte[]) decompressor.uncompress(range.front.blob.zlib_data);

  auto block = readPrimitiveBlock(uncompressedData);

  block.primitiveGroup.dense.id.length.should.equal(0);

  block.primitiveGroup.nodes.length.should.equal(0);
  block.primitiveGroup.ways.length.should.equal(0);
  block.primitiveGroup.relations.length.should.equal(5);
  block.primitiveGroup.changeSets.length.should.equal(0);
}
