/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module geo.xyz;

import std.math;
import std.conv;
import std.string;
import std.algorithm;
import std.array;

import geo.conv;
import geo.json;
import geo.epsg;
import geo.geometries;
import geo.algorithm;
import geo.algo.intersection;

version(unittest) {
  import std.conv;
  import vibe.data.json;

  import fluent.asserts;
}

///
double longitudeFromRow(const uint x, const uint zoom) pure {
  double z = zoom;
  return x.to!double / pow(2, z) * 360. - 180.;
}

///
uint longitudeToRow(const double longitude, const uint zoom) pure {
  double z = zoom;
  auto res = ((longitude + 180.) / 360. * pow(2., z));

  if(res < 0) {
    return 0;
  }

  return cast(uint) res;
}

/// Get the row on zoom 1 for the longitude -100.3633
unittest {
  longitudeToRow(-100.3633, 1).should.equal(0);
}

/// longitude to row
unittest {
  longitudeFromRow(3, 5).longitudeToRow(5).should.equal(3);
  longitudeFromRow(4, 6).longitudeToRow(6).should.equal(4);

  (longitudeFromRow(4, 6) + 1).longitudeToRow(6).should.equal(4);
  (longitudeFromRow(4, 6) - 1).longitudeToRow(6).should.equal(3);
}

/// longitude to row for 89.9183 should return 0 instead of negative value
unittest {
  latitudeToCol(89.9183, 1).should.equal(0);
}

///
double latitudeFromCol(const uint y, const uint zoom) pure {
  double z = zoom;

  immutable n = PI - 2 * PI * y.to!double / pow(2, z);

  return 180 / PI * atan(0.5 * (exp(n) - exp(-n)));
}

///
uint latitudeToCol(const double latitude, const uint zoom) pure {
  double z = zoom;

  auto res = ((1. - log(tan(latitude * PI/180.) + 1/cos(latitude * PI/180.))/PI)/2. *pow(2., z));

  if(res < 0) {
    return 0;
  }

  return cast(uint) res;
}

/// latitude to col for 89.9183 should return 0 instead of negative value
unittest {
  latitudeToCol(89.9183, 1).should.equal(0);
}

/// latitude to col
unittest {
  latitudeFromCol(3, 5).latitudeToCol(5).should.equal(3);

  (latitudeFromCol(4, 6) - 0.01).latitudeToCol(6).should.equal(4);
  (latitudeFromCol(4, 6) + 0.01).latitudeToCol(6).should.equal(3);
}

///
uint[2] getTileCoordinates(EPSG3857 coordinates, const uint zoom) {
  auto pos = coordinates.toEPSG4326;

  return getTileCoordinates(Position(pos.longitude, pos.latitude), zoom);
}

///
uint[2] getTileCoordinates(const Position pos, const uint zoom) {
  auto x = longitudeToRow(pos[0], zoom);
  auto y = latitudeToCol(pos[1], zoom);

  return [x, y];
}

///
uint[][] tileIndexes(T)(const T geometry, uint zoom) if(is(T == GeometryVariant) || is(T == BasicGeometryVariant)) {
  if(geometry.peek!Point !is null) {
    return geometry.get!Point.tileIndexes(zoom);
  }

  if(geometry.peek!LineString !is null) {
    return geometry.get!LineString.tileIndexes(zoom);
  }

  if(geometry.peek!Polygon !is null) {
    return geometry.get!Polygon.tileIndexes(zoom);
  }

  if(geometry.peek!MultiPolygon !is null) {
    return geometry.get!MultiPolygon.tileIndexes(zoom);
  }

  return [];
}

uint[][] tileIndexes(ref GeoJsonGeometry geometry, uint zoom) {
  if(geometry.type == "Point") {
    return geometry.to!Point.tileIndexes(zoom);
  }

  if(geometry.type == "MultiPoint") {
    return geometry.to!MultiPoint.tileIndexes(zoom);
  }

  if(geometry.type == "LineString") {
    return geometry.to!LineString.tileIndexes(zoom);
  }

  if(geometry.type == "MultiLineString") {
    return geometry.to!MultiLineString.tileIndexes(zoom);
  }

  if(geometry.type == "Polygon") {
    return geometry.to!Polygon.tileIndexes(zoom);
  }

  if(geometry.type == "MultiPolygon") {
    return geometry.to!MultiPolygon.tileIndexes(zoom);
  }

  return [];
}

///
uint[][] tileIndexes(const Point point, uint zoom) {
  return [[ point.coordinates[0].longitudeToRow(zoom), point.coordinates[1].latitudeToCol(zoom) ]];
}

///
uint[][] tileIndexes(T)(const T geometry, uint zoom) if(is(T == MultiPoint) || is(T == LineString) || is(T == MultiLineString) || is(T == Polygon) || is(T == MultiPolygon)) {
  uint[][] result;

  uint minLon;
  uint maxLon;

  uint minLat;
  uint maxLat;

  static if(is(T == MultiPoint)) {
    if(geometry.coordinates.length == 0) {
      return [];
    }

    result = geometry.coordinates.map!(a => [ a[0].longitudeToRow(zoom), a[1].latitudeToCol(zoom) ]).array;

    return result.sort.uniq.array;
  }

  static if(is(T == LineString)) {
    if(geometry.coordinates.length == 0) {
      return [];
    }

    const list = geometry.coordinates.map!(a => [ a[0].longitudeToRow(zoom), a[1].latitudeToCol(zoom) ]).array;
    result = list.tileIndexesWithoutFilling(zoom);

    return result;
  }

  static if(is(T == MultiLineString)) {
    if(geometry.coordinates.length == 0 || geometry.coordinates[0].length == 0) {
      return [];
    }

    foreach(line; geometry.coordinates) {
      const list = line.map!(a => [ a[0].longitudeToRow(zoom), a[1].latitudeToCol(zoom) ]).array;

      result ~= list.tileIndexesWithFilling(zoom);
    }

    return result.sort.uniq.array;
  }

  static if(is(T == Polygon)) {
    if(geometry.coordinates.length == 0 || geometry.coordinates[0].length == 0) {
      return [];
    }

    const list = geometry.coordinates[0].map!(a => [ a[0].longitudeToRow(zoom), a[1].latitudeToCol(zoom) ]).array;

    return list.tileIndexesWithFilling(zoom);
  }

  static if(is(T == MultiPolygon)) {
    if(geometry.coordinates.length == 0 || geometry.coordinates[0].length == 0) {
      return [];
    }

    foreach(polygon; geometry.coordinates) {
      const list = polygon[0].map!(a => [ a[0].longitudeToRow(zoom), a[1].latitudeToCol(zoom) ]).array;

      result ~= list.tileIndexesWithFilling(zoom);
    }


    return result.sort.uniq.array;
  }
}

/// point tile indexes
unittest {
  Point point = Point(Position(50, 50));

  GeometryVariant(point).tileIndexes(10).serializeToJson.should.equal([[654, 347]].serializeToJson);
}

/// line tile indexes
unittest {
  LineString line = LineString([[50, 50], [51, 51], [52, 52]].toPositions!double);

  GeometryVariant(line).tileIndexes(10).serializeToJson.should.equal([[654, 347], [655, 345], [656, 343],
    [657, 342], [658, 340], [659, 338]].serializeToJson);
}

/// polygon tile indexes
unittest {
  Polygon polygon = Polygon([
    [ [ 50, 50 ], [ 51, 51 ], [ 52, 52 ], [ 48, 51 ], [ 50, 50 ] ]
  ].toPositions!double);

  GeometryVariant(polygon).tileIndexes(10).serializeToJson.should.equal([[648, 342], [649, 341], [649, 342],
  [650, 341], [650, 342], [650, 343], [651, 340], [651, 342], [651, 344], [652, 340], [652, 341], [652, 343],
  [652, 345], [653, 340], [653, 341], [653, 342], [653, 346], [654, 339], [654, 341], [654, 342], [654, 343],
  [654, 347], [655, 339], [655, 340], [655, 342], [655, 343], [655, 344], [655, 345], [656, 339], [656, 340],
  [656, 341], [656, 343], [656, 344], [657, 338], [657, 340], [657, 341], [657, 342], [657, 344], [657, 345],
  [658, 338], [658, 339], [658, 340], [658, 341], [658, 342], [658, 343], [658, 345], [659, 338], [659, 339],
  [659, 340], [659, 342], [659, 343], [659, 344], [659, 346], [660, 340], [660, 341], [660, 343], [660, 344],
  [660, 345], [661, 342], [661, 345], [661, 346], [662, 347]].serializeToJson);
}

/// MultiPolygon tile indexes
unittest {
  MultiPolygon polygon = MultiPolygon([
  [ [ [50, 50], [51, 51], [52, 52], [48, 51], [50, 50] ] ].toPositions!double,
  [ [ [47.91153294605442, 52.592876294931585], [47.91153294605442, 52.00275320395579], [49.65525085427237, 52.00275320395579],
      [49.65525085427237, 52.592876294931585], [47.91153294605442, 52.592876294931585] ] ].toPositions!double
  ]);

  GeometryVariant(polygon).tileIndexes(10).serializeToJson
    .should.equal([[648, 335], [648, 336], [648, 337], [648, 338], [648, 342], [649, 335], [649, 336], [649, 338], [649, 341],
      [649, 342], [650, 335], [650, 336], [650, 338], [650, 341], [650, 342], [650, 343], [651, 335], [651, 336], [651, 337],
      [651, 338], [651, 340], [651, 342], [651, 344], [652, 335], [652, 336], [652, 337], [652, 338], [652, 340], [652, 341],
      [652, 343], [652, 345], [653, 335], [653, 336], [653, 337], [653, 338], [653, 340], [653, 341], [653, 342], [653, 346],
      [654, 336], [654, 337], [654, 338], [654, 339], [654, 341], [654, 342], [654, 343], [654, 347], [655, 338], [655, 339],
      [655, 340], [655, 342], [655, 343], [655, 344], [655, 345], [656, 339], [656, 340], [656, 341], [656, 343], [656, 344],
      [657, 338], [657, 340], [657, 341], [657, 342], [657, 344], [657, 345], [658, 338], [658, 339], [658, 340], [658, 341],
      [658, 342], [658, 343], [658, 345], [659, 338], [659, 339], [659, 340], [659, 342], [659, 343], [659, 344], [659, 346],
      [660, 340], [660, 341], [660, 343], [660, 344], [660, 345], [661, 342], [661, 345], [661, 346], [662, 347]].serializeToJson);
}

/// building polygon tile index
unittest {
  Polygon polygon = Polygon([[[13.3027733,52.51963120000001],[13.3027744,52.51957160000001],
  [13.3033235,52.51957530000001],[13.3033224,52.5196349],[13.3027733,52.51963120000001]]].toPositions);

  GeometryVariant(polygon).tileIndexes(14).serializeToJson.should.equal([[8797, 5373]].serializeToJson);
}

/// Fills a buffer with the tile polygon. The function does not check if buffer is allocated. You must to ensure that is large enough.
void fillTilePolygon(ref double[][] polygonBuffer, const uint zoom, const uint x, const uint y, const double paddingPercentage = 0) {
  auto box = toBBox(zoom, x, y, paddingPercentage);

  polygonBuffer[0][0] = box.left;
  polygonBuffer[0][1] = box.top;

  polygonBuffer[1][0] = box.left;
  polygonBuffer[1][1] = box.bottom;

  polygonBuffer[2][0] = box.right;
  polygonBuffer[2][1] = box.bottom;

  polygonBuffer[3][0] = box.right;
  polygonBuffer[3][1] = box.top;

  polygonBuffer[4][0] = box.left;
  polygonBuffer[4][1] = box.top;
}

/// fill the tile bounds for 0,0,0
unittest {
  double[][] polygonBuffer = [
    [0, 0],
    [0, 0],
    [0, 0],
    [0, 0],
    [0, 0],
  ];

  polygonBuffer.fillTilePolygon(0, 0, 0);

  polygonBuffer.map!(a => a[0].to!string ~ "," ~ a[1].to!string)
    .array
    .should.equal([
      "-180,85.0511",
      "-180,-85.0511",
      "180,-85.0511",
      "180,85.0511",
      "-180,85.0511",
    ]);
}

/// Get a polygon coresponding to a zxy tile
EPSG4326[] getTilePolygon(uint zoom, uint x, uint y, double paddingPercentage = 0) {
  auto box = toBBox(zoom, x, y, paddingPercentage);

  return [
      EPSG4326(box.left, box.top),
      EPSG4326(box.right, box.top),
      EPSG4326(box.right, box.bottom),
      EPSG4326(box.left, box.bottom),
      EPSG4326(box.left, box.top) ];
}

/// ditto
EPSG4326[] getTilePolygon(ZXY zxy, double paddingPercentage = 0) {
  return getTilePolygon(zxy.z, zxy.x, zxy.y, paddingPercentage);
}

/// get the tile bounds for 0,0,0
unittest {
  getTilePolygon(0, 0, 0)
    .map!(a => a.latitude.to!string ~ "," ~ a.longitude.to!string)
    .array
    .should.equal([
      "85.0511,-180",
      "85.0511,180",
      "-85.0511,180",
      "-85.0511,-180",
      "85.0511,-180",
    ]);
}

/// get the tile bounds for 0,0,0 with 0.1 padding
unittest {
  getTilePolygon(0, 0, 0, 0.1)
    .map!(a => a.latitude.to!string ~ "," ~ a.longitude.to!string)
    .array
    .should.equal(["90,-180", "90,180", "-90,180", "-90,-180", "90,-180"]);
}

/// get the tile bounds for 1,0,0
unittest {
  getTilePolygon(1, 0, 0)
    .map!(a => a.latitude.to!string ~ "," ~ a.longitude.to!string)
    .array
    .should.equal([
      "85.0511,-180",
      "85.0511,0",
      "0,0",
      "0,-180",
      "85.0511,-180",
    ]);
}

void rotate(long n, ref long x, ref long y, long rx, long ry) {
  if (ry != 0) {
    return;
  }

  if (rx == 1) {
    x = n - 1 - x;
    y = n - 1 - y;
  }

  long t = x;
  x = y;
  y = t;
}

///
struct ZXY {
  uint z, x, y;

  ///
  ulong hash() {
    return (cast(ulong)z) << 59 | (cast(ulong)x) << 29 | cast(ulong)y;
  }

  ///
  string toDotNotation() {
    return z.to!string ~ "." ~ x.to!string ~ "." ~ y.to!string;
  }

  ///
  long[2] declutter(EPSG4326 point, uint extent, int areaSize) {
    auto group = point.toVectorTileCoordinates(x, y, z, extent);

    int diffX;
    int diffY;

    if(group[0] < 0) {
      diffX = group[0]/extent.to!int - 1;
    }

    auto posX = cast(long)((double(group[0]) / double(extent)) * areaSize);
    auto posY = cast(long)((double(group[1]) / double(extent)) * areaSize);

    if(group[1] < 0) {
      diffY = group[1]/extent.to!int - 1;
    }

    if(diffX != 0 || diffY != 0) {
      group = point.toVectorTileCoordinates(x + diffX, y + diffY, z, extent);

      if(diffX != 0) {
        posX = cast(long)((double(group[0]) / double(extent)) * areaSize) + diffX * areaSize;
      }

      if(diffY != 0) {
        posY = cast(long)((double(group[1]) / double(extent)) * areaSize) + diffY * areaSize;
      }
    }

    return [ posX, posY ];
  }

  long toId() {
    if (z > 31) {
      throw new Exception("tile zoom exceeds 64-bit limit");
    }

    if (x > (1U << z) - 1U || y > (1U << z) - 1U) {
      throw new Exception("tile x/y outside zoom level bounds");
    }

    long acc = 0;
    for (ubyte t_z = 0; t_z < z; t_z++) {
      acc += (1 << t_z) * (1 << t_z);
    }

    long n = 1 << z;
    long rx, ry, s, d = 0;
    long tx = x;
    long ty = y;

    for (s = n / 2; s > 0; s /= 2) {
      rx = (tx & s) > 0;
      ry = (ty & s) > 0;
      d += s * s * ((3 * rx) ^ ry);
      rotate(s, tx, ty, rx, ry);
    }

    return acc + d;
  }

  ///
  static ZXY fromHash(ulong key) {
    ZXY zxy;

    zxy.z = cast(int) (key >> 59);
    zxy.x = cast(int) (key >> 29 & 0xFFFF);
    zxy.y = cast(int) (key & 0xFFFF);

    return zxy;
  }

  static ZXY fromDotNotation(string key) {
    auto pieces = key.split(".");

    return ZXY(pieces[0].to!uint, pieces[1].to!uint, pieces[2].to!uint);
  }
}

/// convert ZXY to a hash and back
unittest {
  auto pos = ZXY.fromHash(ZXY(1, 1, 1).hash);

  (cast(long) pos.z).should.equal(1);
  (cast(long) pos.y).should.equal(1);
  (cast(long) pos.x).should.equal(1);

  pos = ZXY.fromHash(ZXY(21, 2 ^ 21, 2 ^21).hash);

  (cast(long) pos.z).should.equal(21);
  (cast(long) pos.y).should.equal(2 ^ 21);
  (cast(long) pos.x).should.equal(2 ^ 21);
}

/// create ZXY from dot notation
unittest {
  ZXY(1, 2, 3).toDotNotation.should.equal("1.2.3");
}

/// convert ZXY to dot notation
unittest {
  auto pos = ZXY.fromDotNotation("1.2.3");

  (cast(long) pos.z).should.equal(1);
  (cast(long) pos.x).should.equal(2);
  (cast(long) pos.y).should.equal(3);
}

/// Declutter a point in the center of a tile
unittest {
  auto zxy = ZXY(4, 3, 2);
  auto polygon = getTilePolygon(zxy.z, zxy.x, zxy.y);

  double lon = (polygon[0].longitude + polygon[1].longitude) / 2;
  double lat = (polygon[0].latitude + polygon[3].latitude) / 2;

  EPSG4326 point;
  point.latitude = lat;
  point.longitude = lon;

  auto group = zxy.declutter(point, 4096, 15);
  group.serializeToJson.should.equal([7, 8].serializeToJson);

  /// check nearby tiles
  zxy.y = 3;
  group = zxy.declutter(point, 4096, 15);
  group.serializeToJson.should.equal([7, -7].serializeToJson);

  zxy.y = 4;
  group = zxy.declutter(point, 4096, 15);
  group.serializeToJson.should.equal([7, -22].serializeToJson);
}

/// Declutter a weird point
unittest {
  auto zxy = ZXY(5, 15, 11);

  EPSG4326 point;
  point.latitude = 46.148411;
  point.longitude = -0.404359;

  auto group = zxy.declutter(point, 4096, 15);
  group.serializeToJson.should.equal([14, 5].serializeToJson);

  /// check nearby tiles
  zxy = ZXY(5, 16, 11);
  group = zxy.declutter(point, 4096, 15);
  group.serializeToJson.should.equal([-1, 5].serializeToJson);
}

/// convert zxy to tileId
unittest {
  ZXY(0, 0, 0).toId.should.equal(0);
  ZXY(1, 0, 0).toId.should.equal(1);
  ZXY(1, 0, 1).toId.should.equal(2);
  ZXY(1, 1, 1).toId.should.equal(3);
  ZXY(12, 3423, 1763).toId.should.equal(19078479);
}

///
string[] strTiles(const Position pos, const int minZoom, const int maxZoom) {
  string[] result;
  result.length = maxZoom - minZoom;

  foreach(zoom; minZoom..maxZoom) {
    result[zoom-minZoom] = zoom.to!string ~ "." ~
      longitudeToRow(pos[0], zoom).to!string ~ "." ~
      latitudeToCol(pos[1], zoom).to!string;
  }

  return result;
}

/// Get str zxy for point 1,1 for a range of zoom levels
unittest {
  string[] result1 = ["0.0.0", "1.1.0", "2.2.1", "3.4.3", "4.8.7", "5.16.15", "6.32.31",
    "7.64.63", "8.128.127", "9.257.254", "10.514.509", "11.1029.1018", "12.2059.2036",
    "13.4118.4073", "14.8237.8146", "15.16475.16292", "16.32950.32585", "17.65900.65171",
    "18.131800.130343", "19.263600.260687", "20.527200.521375", "21.1054401.1042750",
    "22.2108802.2085500", "23.4217605.4171001"];

  Position(1, 1).strTiles(0, 24).should.equal(result1);
  Position(1, 1).strTiles(10, 15).should.equal(result1[10..15]);
}

/// Get str zxy for point 87.3633, 89.9183 for a range of zoom levels
unittest {
  string[] result1 = ["0.0.0",
  "1.1.0", "2.2.0", "3.5.0", "4.11.0", "5.23.0", "6.47.0", "7.95.0", "8.190.0", "9.380.0",
  "10.760.0", "11.1521.0", "12.3042.0", "13.6084.0", "14.12168.0", "15.24336.0", "16.48672.0",
  "17.97344.0", "18.194688.0", "19.389376.0", "20.778752.0", "21.1557504.0", "22.3115008.0", "23.6230016.0"];

  Position(87.3633, 89.9183).strTiles(0, 24).should.equal(result1);
  Position(87.3633, 89.9183).strTiles(10, 15).should.equal(result1[10..15]);
}

/// Works like strTiles, but when there is a point on one of the edges,
/// the neighbour tile is also added
string[] strTilesWithEdgesCheck(const Position pos, const int minZoom, const int maxZoom) {
  string[] result;

  const dir = [
    [ -1, 0 ],
    [ +1, 0 ],
    [ 0, -1 ],
    [ 0, +1 ],
    [ +1, +1 ],
    [ +1, -1 ],
    [ -1, +1 ],
    [ -1, -1 ],
  ];

  foreach(zoom; minZoom..maxZoom) {
    auto x = longitudeToRow(pos[0], zoom);
    auto y = latitudeToCol(pos[1], zoom);
    auto polygon = getTilePolygon(zoom, x, y);
    immutable lonDiff = abs(polygon[0].longitude - polygon[2].longitude) * 0.1;
    immutable latDiff = abs(polygon[0].latitude - polygon[1].latitude) * 0.1;

    const operations = [
      [ -lonDiff, 0 ],
      [ +lonDiff, 0 ],
      [ 0, -latDiff ],
      [ 0, +latDiff ],
      [ +lonDiff, +latDiff ],
      [ +lonDiff, -latDiff ],
      [ -lonDiff, +latDiff ],
      [ -lonDiff, -latDiff ],
    ];

    string key = zoom.to!string ~ "." ~
      x.to!string ~ "." ~
      y.to!string;

    result ~= key;

    foreach(index, operation; operations) {
      if(x == 0 && dir[index][0] < 0 || y == 0 && dir[index][1] < 0) continue;

      Position tmpPos = Position(pos[0] + operation[0], pos[1] + operation[1]);

      if(longitudeToRow(tmpPos[0], zoom) != x || latitudeToCol(tmpPos[1], zoom) != y) {
        key = zoom.to!string ~ "." ~
          (x + dir[index][0]).to!string ~ "." ~
          (y + dir[index][1]).to!string;
        result ~= key;
      }
    }
  }

  return result.sort.uniq.array;
}

/// Get str zxy for point 0,25.039359 for a range of zoom levels
unittest {
  string[] result1 = ["0.0.0", "1.1.0", "2.2.1", "3.4.3"];
  string[] result2 = ["0.0.0", "1.0.0", "1.0.1", "1.1.0", "2.1.0", "2.1.1",
    "2.1.2", "2.2.1", "3.3.2", "3.3.3", "3.3.4", "3.4.3"];

  Position(0, 25.039359).strTiles(0, 4).should.equal(result1);
  Position(0, 25.039359).strTilesWithEdgesCheck(0, 4).should.equal(result2);
}

/// Get str zxy for point 0,0 for a range of zoom levels
unittest {
  string[] result1 = ["0.0.0", "1.1.1"];
  string[] result2 = ["0.0.0", "1.0.0", "1.0.1", "1.0.2", "1.1.1"];

  Position(0, 0).strTiles(0, 2).should.equal(result1);
  Position(0, 0).strTilesWithEdgesCheck(0, 2).should.equal(result2);
}

///
string[] strTiles(const Position[] pos, const int minZoom, const int maxZoom) {
  if(pos.length <= 1) {
    return [];
  }

  string[] result;
  foreach(zoom; minZoom..maxZoom) {
    string[] zoomTiles;

    foreach(index; 1..pos.length) {

      auto begin = pos[index - 1];
      auto end = pos[index];

      auto tiles = lineTiles(begin, end, zoom);

      foreach(position; tiles) {
        zoomTiles ~= zoom.to!string ~ "." ~ position[0].to!string ~ "." ~ position[1].to!string;
      }
    }

    result ~= zoomTiles.sort.uniq.array;
  }

  return result;
}

/// strTiles for a line with one or less points should return an empty list
unittest {
  Position[] pos;

  pos.strTiles(0, 24).should.equal([]);
  [[87, 89]].toPositions!double.strTiles(0, 24).should.equal([]);
}

/// strTiles for a line with two points should return the tiles for the line
unittest {
  string[] result = ["0.0.0", "1.1.0", "2.2.1", "3.5.3", "4.10.7", "5.21.14", "6.43.28", "7.86.56",
    "8.172.112", "8.172.113", "9.344.226", "9.345.225", "9.345.226", "10.688.453", "10.689.452", "10.689.453",
    "10.690.451", "10.690.452", "10.691.450", "10.691.451"];

  [[62, 20], [63, 21]].toPositions!double.strTiles(0, 11).should.equal(result);
}

/// strTiles for a line with three points should return the tiles for the line
unittest {
  string[] result = ["0.0.0", "1.1.0", "2.2.1", "3.5.3", "4.10.7", "5.21.14", "6.43.28", "7.86.56", "8.172.112",
    "8.172.113", "8.173.112", "9.344.226", "9.345.225", "9.345.226", "9.346.225", "9.347.225", "10.688.453",
    "10.689.452", "10.689.453", "10.690.451", "10.690.452", "10.691.450", "10.691.451", "10.692.450", "10.693.450",
    "10.694.450"];

  [[62, 20], [63, 21], [64, 21]].toPositions!double.strTiles(0, 11).should.equal(result);
}

/// strTiles for a line with 5 points should return the tiles for the line
unittest {
  string[] result = ["0.0.0", "1.1.0", "2.2.1", "3.4.2", "4.8.5", "5.17.10", "6.34.20", "7.68.41", "8.137.83", "9.274.167", "9.275.167", "10.549.335", "10.550.335", "11.1099.671", "11.1100.671", "12.2199.1343", "12.2200.1342", "12.2200.1343", "12.2201.1342", "13.4399.2686", "13.4400.2686", "13.4400.2687", "13.4401.2685", "13.4401.2686", "13.4401.2687", "13.4402.2685"];
  [[13.35541182101316, 52.52626258390336], [13.43732010412796, 52.49513767470791], [13.43565699178045, 52.51917917652966], [13.37661650344389, 52.53385068151067], [13.49095547733512, 52.5399202156365]].toPositions.strTiles(0,14).should.equal(result);
}

///
string[] strTiles(Point)(Point[][] polygon, const int minZoom, const int maxZoom) {
  string[] result;

  auto outerRing = polygon[0];

  foreach(zoom; minZoom..maxZoom) {
    string[] zoomTiles;

    auto points = outerRing.map!(a => getTileCoordinates(a, zoom)).array;

    auto box = BBox!uint(points);
    auto polygonSides = getPolygonSides(outerRing);

    foreach(x; box.left .. box.right+1) {
      foreach(y; box.bottom .. box.top+1) {
        auto tilePolygon = getTilePolygon(zoom, x, y, 0.1);
        auto tileBBox = BBox!double(tilePolygon);

        auto hasIntersection = tileBBox.fullyInside(outerRing);

        if(!hasIntersection) {
          foreach(i; 1..outerRing.length) {
            auto edge = [EPSG4326(outerRing[i-1][0], outerRing[i-1][1]), EPSG4326(outerRing[i][0], outerRing[i][1])];
            auto edgeIndex = edge.intersectedEdge(tilePolygon);

            if(edgeIndex >= 0) {
              hasIntersection = true;
              break;
            }
          }
        }

        if(hasIntersection) {
          result ~= zoom.to!string ~ "." ~ x.to!string ~ "." ~ y.to!string;
        }
      }
    }
  }

  return result;
}


/// strTiles for a polygon with three points should return the tiles for the line and its points for levels 0 - 10
unittest {
  string[] result = ["8.172.112", "8.172.113", "8.173.112", "8.173.113", "9.344.225", "9.344.226", "9.345.225", "9.345.226",
    "9.346.225", "9.346.226", "9.347.225", "10.688.453", "10.689.451", "10.689.452", "10.689.453", "10.690.450", "10.690.451",
    "10.690.452", "10.690.453", "10.691.450", "10.691.451", "10.691.452", "10.692.450", "10.692.451", "10.692.452", "10.693.450",
    "10.693.451", "10.694.450", "10.694.451"];

  [[[62, 20], [63, 21], [64, 21], [62, 20]]].toPositions!double.strTiles(0, 11).should.equal(result);
}

/// strTiles for a polygon should not generate interiour tiles when the border is on more than 9 tiles
unittest {
  string[] result = ["10.688.453", "10.689.451", "10.689.452", "10.689.453", "10.690.450", "10.690.451", "10.690.452", "10.690.453", "10.691.450", "10.691.451", "10.691.452",
    "10.692.450", "10.692.451", "10.692.452", "10.693.450", "10.693.451", "10.694.450", "10.694.451"];

  [[[62, 20], [63, 21], [64, 21], [62, 20]]].toPositions!double.strTiles(10, 11).should.equal(result);
}

/// strTiles for a polygon with three points should return the tiles for the line and its points for level 11
unittest {
  string[] result = ["11.1376.907", "11.1377.906", "11.1377.907", "11.1378.905", "11.1378.906", "11.1378.907",
    "11.1379.904", "11.1379.905", "11.1379.906", "11.1380.903", "11.1380.904", "11.1380.905", "11.1380.906",
    "11.1381.901", "11.1381.902", "11.1381.903", "11.1381.904", "11.1381.905", "11.1382.901", "11.1382.902",
    "11.1382.903", "11.1382.904", "11.1382.905", "11.1383.901", "11.1383.902", "11.1383.903", "11.1383.904",
    "11.1384.901", "11.1384.902", "11.1384.903", "11.1384.904", "11.1385.901", "11.1385.902", "11.1385.903",
    "11.1386.901", "11.1386.902", "11.1386.903", "11.1387.901", "11.1387.902", "11.1388.901"]
;

  auto tiles = [[[62, 20], [63, 21], [64, 21], [62, 20]]].toPositions!double.strTiles(11, 12);

  tiles.should.contain("11.1385.903");
  tiles.should.equal(result);
}

///
ulong[] hashTiles(const Position pos, const int minZoom, const int maxZoom) {
  ulong[] result;
  result.length = maxZoom - minZoom;

  foreach(zoom; minZoom..maxZoom) {
    result[zoom-minZoom] = ZXY(zoom,
      longitudeToRow(pos[0], zoom),
      latitudeToCol(pos[1], zoom)).hash;
  }

  return result;
}

/// Get point hash zxy for a range of zoom levels
unittest {
  ulong[] result1 = [0UL, 576460752840294400UL, 1152921505680588801UL,
    1729382259057754115UL, 2305843013508661255UL, 2882303770107052047UL, 3458764531000410143UL,
    4035225300483702847UL, 4611686087146864767UL, 5188146908706636030UL, 5764607798985884157UL,
    6341068827777827834UL, 6917530133058291700UL, 7493991990778925033UL, 8070454954453639122UL,
    8646920129499643812UL, 9223389726751358793UL, 9799868168951365267UL, 10376364301047954727UL,
    10952895812937710159UL, 11529498084413797535UL, 12106241875599419710UL, 12683268705130369660UL,
    13260861612425716985UL];

  Position(1,1).hashTiles(0, 24).should.equal(result1);
  Position(1,1).hashTiles(10, 15).should.equal(result1[10..15]);
}

///
uint[2][] lineTiles(const Position point1, const Position point2, const int zoom) {
  return lineTiles(EPSG3857(point1), EPSG3857(point2), zoom);
}

///
uint[2][] lineTiles(const EPSG3857 point1, const EPSG3857 point2, const int zoom) {

  auto tile = getTileCoordinates(point1, zoom);

  auto line = [point1.toArray, point2.toArray];
  bool hasValidIntersection = true;
  long[][] prevSide;

  enum direction = [[0, -1], [1, 0], [0, 1], [-1, 0]];

  uint[2][] result;
  result ~= tile;

  while(hasValidIntersection) {
    auto polygon = getTilePolygon(zoom, tile[0], tile[1]).map!(a => a.toEPSG3857.toArray).array;
    auto polygonSides = getPolygonSides(polygon);

    hasValidIntersection = false;
    foreach(index, side; polygonSides) {
      bool thereIsIntersection = side.doIntersect(line);

      if(!thereIsIntersection) continue;

      if(!prevSide.length || !doOverlap(side, prevSide)) {
        tile[0] += direction[index][0];
        tile[1] += direction[index][1];

        if(!canFind(result, tile)) {
          result ~= tile;
          prevSide = side;
          hasValidIntersection = true;
          break;
        }
      }
    }
  }

  return result;
}

/// Get tiles for line passing on 3 adjacent tiles
unittest {
  uint[2][] result = [ [8802, 5379], [ 8802, 5380 ], [ 8803, 5380 ] ];

  lineTiles(Position(13.40421132828836, 52.431803091416356), Position(13.42706485267503, 52.42400604472323), 14)
    .should.equal(result);
}

/// Get tiles for line that is on the diagonal of 2 tiles
unittest {
  uint[2][] result = [ [8802, 5379], [8803, 5379], [8803, 5380], [8802, 5380] ];

  lineTiles(EPSG3857(1493273, 6879332), EPSG3857(1495719, 6876886), 14).should.equal(result);
}

/// Get tiles for a line that corresponds to a tile side
unittest {
  uint[2][] result = [ [8801, 5379], [8801, 5378], [8802, 5378], [8803, 5378], [8803, 5379], [8802, 5379] ];

  lineTiles(EPSG3857(1492050, 6880555), EPSG3857(1494496, 6880555), 14).should.equal(result);
}

///
uint[][] tileIndexesWithFilling(const uint[][] list, uint zoom) {
  uint[][] result;

  uint minLon = list[0][0];
  uint maxLon = minLon;

  uint minLat = list[0][1];
  uint maxLat = minLat;

  foreach(pair; list) {
    immutable lon = pair[0];
    immutable lat = pair[1];

    if(lon > maxLon) {
      maxLon = lon;
    }

    if(lon < minLon) {
      minLon = lon;
    }

    if(lat > maxLat) {
      maxLat = lat;
    }

    if(lat < minLat) {
      minLat = lat;
    }
  }

  foreach(lon; minLon .. maxLon+1) {
    uint[] intersections;

    auto colLine = [[lon, minLat - 1], [lon, maxLat + 1]];

    foreach(i; 1..list.length) {
      auto prevPoint = list[i-1];
      auto point = list[i];

      foreach(p; colLine.intersection([prevPoint, point])) {
        intersections ~= p[1];
      }
    }

    intersections = intersections.sort.uniq.array;

    if(intersections.length == 0) {
      continue;
    }

    if(intersections.length % 2 == 1) {
      foreach(lat; intersections) {
        result ~= [lon, lat];
      }

      continue;
    }

    for(uint i=0; i<intersections.length; i+=2) {
      result ~= line([lon, intersections[i]], [lon, intersections[i+1]]);
    }
  }

  return result.sort.uniq.array;
}

///
uint[][] tileIndexesWithoutFilling(const uint[][] list, uint zoom) {
  uint[][] result;

  uint minLon = list[0][0];
  uint maxLon = minLon;

  uint minLat = list[0][1];
  uint maxLat = minLat;

  foreach(pair; list) {
    immutable lon = pair[0];
    immutable lat = pair[1];

    if(lon > maxLon) {
      maxLon = lon;
    }

    if(lon < minLon) {
      minLon = lon;
    }

    if(lat > maxLat) {
      maxLat = lat;
    }

    if(lat < minLat) {
      minLat = lat;
    }
  }

  foreach(lon; minLon .. maxLon+1) {
    uint[] intersections;

    auto colLine = [[lon, minLat - 1], [lon, maxLat + 1]];

    foreach(i; 1..list.length) {
      auto prevPoint = list[i-1];
      auto point = list[i];

      foreach(p; colLine.intersection([prevPoint, point])) {
        intersections ~= p[1];
      }
    }

    foreach(lat; intersections) {
      result ~= [lon, lat];
    }
  }

  return result.sort.uniq.array;
}