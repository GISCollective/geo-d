/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module geo.conv;

import geo.vector;
import geo.xyz;
import geo.geometries;
import geo.algorithm : distance;
import geo.epsg;

import std.array;
import std.algorithm;
import std.conv;
import std.math;
import std.exception;

version(unittest) {
  import fluent.asserts;
}

enum EPSG3857M_Precision = 1000000;

///
auto projectTo(T, U)(const U source) pure if(is(T == EPSG4326) || is(T == EPSG3857)) {
  static if(is(T == EPSG3857)) {
    static if(is(U == EPSG4326)) {
      return toEPSG3857(source);
    } else static if(is(U == PositionT!double[])) {
      return source.toEPSG4326.toEPSG3857;
    } else static if(is(U == PositionT!double)) {
      return toEPSG4326(source[0], source[1]).toEPSG3857;
    } else static if(is(U == double[][])) {
      return source.toEPSG3857;
    } else static if(is(U == EPSG4326[])) {
      return source.toEPSG3857;
    } else static if(is(U == long[][])) {
      return source.toEPSG3857;
    } else {
      static assert(false, "Can't convert from " ~ U.stringof ~ " to " ~ T.stringof ~ ".");
    }
  } else static if(is(T == EPSG4326)) {
    static if(is(U == EPSG4326)) {
      return EPSG4326(source[0], source[1]);
    } else static if(is(U == EPSG4326[])) {
      return source;
    } else static if(is(U == EPSG3857)) {
      return toEPSG4326(source);
    } else static if(is(U == double[][])) {
      return source.toEPSG4326;
    } else static if(is(U == PositionT!double[])) {
      return source.dup.toEPSG4326;
    } else {
      static assert(false, "Can't convert from " ~ U.stringof ~ " to " ~ T.stringof ~ ".");
    }
  } else {
    static assert(false, "Can't convert from " ~ U.stringof ~ " to " ~ T.stringof ~ ".");
  }
}

EPSG4326[] toEPSG4326(const PositionT!double[] list) pure {
  EPSG4326[] result;
  result.reserve(list.length);

  foreach(point; list) {
    result ~= EPSG4326(point[0], point[1]);
  }

  return result;
}

EPSG4326[] toEPSG4326(const double[][] list) pure {
  EPSG4326[] result;
  result.reserve(list.length);

  foreach(point; list) {
    result ~= EPSG4326(point[0], point[1]);
  }

  return result;
}


BBox!long toEPSG3857(BBox!double box) {
  auto points = [ toEPSG3857(EPSG4326(box.left, box.top)), toEPSG3857(EPSG4326(box.right, box.bottom)) ];
  return BBox!long(points);
}

///
auto toEPSG3857_impl(T, size_t Precision)(const EPSG4326 source) pure @safe nothrow {
  debug enforce(source.longitude >= -180 && source.longitude <= 180, "Invalid longitude: " ~ source.longitude.to!string);
  debug enforce(source.latitude >= -90 && source.latitude <= 90, "Invalid latitude: " ~ source.latitude.to!string);

  T result;

  immutable real R = 20_037_508.34;
  immutable real PI_180 = 0.017453292519943295769236907684886127134428718885417254560;
  immutable real PI_360 = 0.008726646259971647884618453842443063567214359442708627280;
  immutable real r_90 = 90;
  immutable real R_180 = 111_319.4907777777777777777777777777777777777777777777777777;

  real x = source.longitude * R / cast(real) 180.;

  real t = tan((r_90 + source.latitude) * PI_360);
  real y = log(t) / PI_180;

  y = y * R_180;

  if(isNaN(y) && source.latitude == 90) {
    y = 242528680.94374272;
  }

  if(isInfinity(y) && source.latitude == -90) {
    y = -242528680.94374272;
  }

  try {
    result.x = (x * Precision).to!long;
    result.y = (y * Precision).to!long;
  } catch(Exception e) {
    result.x = 0;
    result.y = 0;
  }

  return result;
}

///
EPSG3857 toEPSG3857(const EPSG4326 source) pure @safe nothrow {
  return toEPSG3857_impl!(EPSG3857, 1)(source);
}

/// ditto
EPSG3857[] toEPSG3857(const EPSG4326[] source) pure @safe nothrow {
  EPSG3857[] result;
  result.reserve(source.length);

  foreach(point; source) {
    result ~= toEPSG3857(point);
  }

  return result;
}

/// ditto
EPSG3857[] toEPSG3857(const PositionT!double[] source) pure @safe nothrow {
  EPSG3857[] result;
  result.reserve(source.length);

  foreach(point; source) {
    result ~= toEPSG3857(EPSG4326(point[0], point[1]));
  }

  return result;
}

/// ditto
EPSG3857[][] toEPSG3857(const PositionT!double[][] source) pure @safe nothrow {
  EPSG3857[][] result;
  result.reserve(source.length);

  foreach(list; source) {
    result ~= toEPSG3857(list);
  }

  return result;
}

/// ditto
EPSG3857 toEPSG3857(T)(const T[] source) pure @safe if(is(T == int) || is(T == long)) {
  return EPSG3857(source[0], source[1]);
}

/// ditto
EPSG3857[] toEPSG3857(T)(const T[][] source) pure @safe if(is(T == int) || is(T == long)) {
  EPSG3857[] result;
  result.reserve(source.length);

  foreach(list; source) {
    result ~= toEPSG3857(list);
  }

  return result;
}

/// ditto
EPSG3857[][] toEPSG3857(T)(const T[][][] source) pure @safe if(is(T == int) || is(T == long)) {
  EPSG3857[][] result;
  result.reserve(source.length);

  foreach(list; source) {
    result ~= toEPSG3857(list);
  }

  return result;
}


/// convert positive EPSG4326 to EPSG3857
unittest {
  EPSG4326 value;
  value.latitude = 4;
  value.longitude = 3;

  auto result = value.projectTo!EPSG3857;
  result.x.to!string.should.equal("333958");
  result.y.to!string.should.equal("445640");
}

/// convert positive 180 longitude and 90 latitude EPSG4326 to EPSG3857
unittest {
  EPSG4326 value;
  value.latitude = 90;
  value.longitude = -180;

  auto result = value.projectTo!EPSG3857;
  result.x.to!string.should.equal("-20037508");
  result.y.to!string.should.equal("242528680");
}

/// convert positive 180 longitude and -90 latitude EPSG4326 to EPSG3857
unittest {
  EPSG4326 value;
  value.latitude = -90;
  value.longitude = -180;

  auto result = value.projectTo!EPSG3857;
  result.x.to!string.should.equal("-20037508");
  result.y.to!string.should.equal("-242528680");
}

/// convert negative EPSG4326 to EPSG3857
unittest {
  EPSG4326 value;
  value.latitude = -4;
  value.longitude = -3;

  auto result = value.projectTo!EPSG3857;
  result.x.to!string.should.equal("-333958");
  result.y.to!string.should.equal("-445640");
}

///
EPSG4326 toEPSG4326(const EPSG3857 source) pure {
  EPSG4326 result;

  result.longitude = (source.x / 20037508.34) * 180;
  double lat = (source.y / 20037508.34) * 180;

  result.latitude = 180./PI * (2 * atan(exp(lat * PI / 180)) - PI_2);

  return result;
}

///
EPSG4326 toEPSG4326(const double[] source) pure {
  return EPSG4326(source);
}

///
EPSG4326[] toEPSG4326(const EPSG3857[] source) pure {
  EPSG4326[] result;
  result.reserve(source.length);

  foreach(value; source) {
    result ~= value.toEPSG4326;
  }

  return result;
}

///
EPSG4326[][] toEPSG4326(const EPSG3857[][] source) pure {
  EPSG4326[][] result;
  result.reserve(source.length);

  foreach(value; source) {
    result ~= value.toEPSG4326;
  }

  return result;
}

///
EPSG4326[][][] toEPSG4326(const EPSG3857[][][] source) pure {
  EPSG4326[][][] result;
  result.reserve(source.length);

  foreach(value; source) {
    result ~= value.toEPSG4326;
  }

  return result;
}

/// convert positive EPSG3857 to EPSG4326
unittest {
    EPSG3857 value;
    value.x = 333_958;
    value.y = 445_640;

    auto result = value.projectTo!EPSG4326;
    result.latitude.to!string.should.equal("4");
    result.longitude.to!string.should.equal("3");
}

/// convert negative EPSG3857 to EPSG4326
unittest {
  EPSG3857 value;
  value.x = -333_958;
  value.y = -445_640;

  auto result = value.projectTo!EPSG4326;
  result.latitude.to!string.should.equal("-4");
  result.longitude.to!string.should.equal("-3");
}

/// Convert a GeoJson point to vector feature
VectorFeature toVectorFeature(const Point point, uint x = 0, uint y = 0, uint z = 0, uint extent = 4096) pure {
  VectorFeature feature;
  feature.type = VectorFeature.GeomType.point;
  feature.geometry = Geometry.fromPoint(point.coordinates.toVectorTileCoordinates(x,y,z,extent));

  return feature;
}

/// Convert a GeoJson point to vector feature
unittest {
  import vibe.data.json;
  import std.array;

  auto point = `{
    "type": "Point",
    "coordinates": [ 1.0, 1.0 ]
    }`.parseJsonString.deserializeJson!Point;

  auto feature = point.toVectorFeature;
  feature.type.should.equal(VectorFeature.GeomType.point);
  feature.geometry.serialize.array.should.equal([9, 4118, 4072]);

  /// with tile coordinates and zoom
  feature = point.toVectorFeature(1000, 1000, 12);
  feature.type.should.equal(VectorFeature.GeomType.point);
  feature.geometry.serialize.array.should.equal([9, 8678422, 8492004]);
}

/// Convert a GeoJson line to vector feature
VectorFeature toVectorFeature(const LineString line, uint x = 0, uint y = 0, uint z = 0, uint extent = 4096, uint tolerance = 0) pure {
  VectorFeature feature;
  feature.type = VectorFeature.GeomType.lineString;
  feature.geometry = Geometry.fromLine(line.coordinates.toVectorTileCoordinates(x,y,z,extent,tolerance));

  return feature;
}

/// Convert a GeoJson line to vector feature
unittest {
  import vibe.data.json;

  LineString line = `{
    "type": "LineString",
    "coordinates": [[1.0, 1.0], [1.0, 2.0], [2.0, 3.0]]
    }`.parseJsonString.deserializeJson!LineString;

  auto feature = line.toVectorFeature;
  feature.type.should.equal(VectorFeature.GeomType.lineString);
  feature.geometry.serialize.array.should.equal([9, 4118, 4072, 18, 0, 21, 22, 23]);

  /// with tile coordinates and zoom
  feature = line.toVectorFeature(1000, 1000, 12);
  feature.type.should.equal(VectorFeature.GeomType.lineString);
  feature.geometry.serialize.array.should.equal([9, 8678422, 8492004, 18, 0, 93239, 93206, 93297]);
}

/// Convert a GeoJson line from osm to vector feature
unittest {
  import vibe.data.json;

  LineString line = `{
    "type": "LineString",
    "coordinates":[[13.358, 52.5362], [13.3592, 52.5368]]
  }`.parseJsonString.deserializeJson!LineString;

  auto feature = line.toVectorFeature(8799, 5371, 14);
  feature.type.should.equal(VectorFeature.GeomType.lineString);
  feature.geometry.serialize.array.should.equal(
    [9, 7678, 8236, 10, 448, 367]);
}

///
BBox!int toVectorTileCoordinates(BBox!double bbox, const uint tx = 0, const uint ty = 0, const uint z = 0, const uint extent = 4096) pure {
  auto point1 = toVectorTileCoordinates(Position(bbox.left, bbox.top));
  auto point2 = toVectorTileCoordinates(Position(bbox.right, bbox.bottom));

  return BBox!int(point1[0], point2[0], point1[1], point2[1]);
}

/// Convert a GeoJson line to vector feature
VectorFeature toVectorFeature(const Polygon polygon, uint x = 0, uint y = 0, uint z = 0, uint extent = 4096, uint tolerance = 0) pure {
  int[][] points;

  VectorFeature feature;
  double zoom = z;

  foreach(coordinates; polygon.coordinates) {
    feature.geometry.commands ~= Geometry.fromPolygon(coordinates.toVectorTileCoordinates(x,y,z,extent,tolerance)).commands;
  }

  feature.type = VectorFeature.GeomType.polygon;

  return feature;
}

/// Convert a GeoJson polygon to vector feature
unittest {
  import vibe.data.json;

  Polygon polygon = `{
    "type": "Polygon",
    "coordinates": [[[1.0, 1.0], [1.0, 2.0], [2.0, 3.0]], [[0.5, 0.5], [0.5, 1.5], [1.5, 2.5]]]
    }`.parseJsonString.deserializeJson!Polygon;

  auto feature = polygon.toVectorFeature;
  feature.type.should.equal(VectorFeature.GeomType.polygon);
  feature.geometry.serialize.array.should.equal(
    [9, 4118, 4072, 18, 0, 21, 22, 23, 15,
     9, 4106, 4084, 18, 0, 23, 24, 21, 15]);

  /// with tile coordinates and zoom
  feature = polygon.toVectorFeature(1000, 1000, 15);
  feature.type.should.equal(VectorFeature.GeomType.polygon);
  feature.geometry.serialize.array.should.equal(
    [9, 126771382, 125280036, 18, 0, 745919, 745654, 746373, 15,
     9, 126398554, 125652896, 18, 0, 745777, 745654, 746117, 15]);
}

/// Convert a GeoJson polygon from osm to vector feature
unittest {
  import vibe.data.json;

  Polygon polygon = `{
    "type": "Polygon",
    "coordinates": [
      [[13.1395802,52.5586492],
      [13.1395501,52.55860060000001],
      [13.1396354,52.558581],
      [13.1396655,52.5586296],
      [13.1395802,52.5586492]]
    ]
  }`.parseJsonString.deserializeJson!Polygon;

  auto feature = polygon.toVectorFeature(0, 0, 0);
  feature.type.should.equal(VectorFeature.GeomType.polygon);
  feature.geometry.serialize.array.should.equal(
    [9, 4394, 2684, 26, 0, 0, 0, 0, 0, 0, 15]);
}

///
int[][][] toVectorTileCoordinates(const Position[][] coordinates, const uint tx = 0, const uint ty = 0, const uint z = 0, const uint extent = 4096, const uint tolerance = 0) pure {
  int[][][] result;

  foreach (group; coordinates) {
    result ~= group.toVectorTileCoordinates(tx, ty, z, extent, tolerance);
  }

  return result;
}

/// convert polygon points
unittest {
  Position[][] points = [[[13.1395802,52.5586492],[13.1395501,52.55860060000001],[13.1396354,52.558581],
  [13.1396655,52.5586296],[13.1395802,52.5586492]]].toPositions;

  points.toVectorTileCoordinates.should.equal([[
    [ 2197, 1342 ],
    [ 2197, 1342 ],
    [ 2197, 1342 ],
    [ 2197, 1342 ],
    [ 2197, 1342 ]]]);

  points.toVectorTileCoordinates(1, 0, 1).should.equal([[
    [ 298, 2685 ],
    [ 298, 2685 ],
    [ 298, 2685 ],
    [ 299, 2685 ],
    [ 298, 2685 ]]]);
}

///
int[][] toVectorTileCoordinates(const Position[] coordinates, const uint tx = 0, const uint ty = 0, const uint z = 0, const uint extent = 4096, const uint tolerance = 0) pure {

  int[][] points;
  points.length = coordinates.length;

  immutable double z2 = 1 << z;

  size_t length;
  int oldX;
  int oldY;

  foreach(i; 0..coordinates.length) {
    auto point = coordinates[i].toVectorTileCoordinates(tx, ty, z, extent);

    auto diffX = abs(oldX - point[0]);
    auto diffY = abs(oldY - point[1]);
    auto line = [oldX, oldY];
    const hasAcceptedTolerance = point.distance(line) >= tolerance;
    const isLastPoint = coordinates.length == i + 1;

    if(hasAcceptedTolerance || isLastPoint) {
      points[length] = point;
      oldX = point[0];
      oldY = point[1];
      length++;
    }
  }

  return points[0..length];
}

/// mercator list toVectorTileCoordinates
unittest {
  Position[] points = [[13.1395802,52.5586492],[13.1395501,52.55860060000001],[13.1396354,52.558581],
  [13.1396655,52.5586296],[13.1395802,52.5586492]].toPositions;

  points.toVectorTileCoordinates.should.equal([
    [ 2197, 1342 ],
    [ 2197, 1342 ],
    [ 2197, 1342 ],
    [ 2197, 1342 ],
    [ 2197, 1342 ]]);

  points.toVectorTileCoordinates(1, 0, 1).should.equal([
    [ 298, 2685 ],
    [ 298, 2685 ],
    [ 298, 2685 ],
    [ 299, 2685 ],
    [ 298, 2685 ]]);
}

/// mercator list toVectorTileCoordinates with tolerance
unittest {
  Position[] points = [[13.1395802,52.5586492],[13.1395501,52.55860060000001],[13.1396354,52.558581],
  [13.1396655,52.5586296],[13.1395802,52.5586492]].toPositions;

  points.toVectorTileCoordinates(0, 0, 0, 4096, 3).should.equal([[2197, 1342], [2197, 1342]]);
}

/// It should ignore tolerance for short lines
unittest {
  Position[] points = [[ 13.5, 52.3978 ], [ 13.4977, 52.3976 ]].toPositions;

  points.toVectorTileCoordinates(274, 167, 9, 4096, 3).should.equal([[4915, 4908], [4901, 4910]]);
}

/// It should not remove points from a long line while using tolerance
unittest {
  import vibe.data.json;

  Position[] points = [[13.3857, 52.4701], [13.3905, 52.4701], [13.3934, 52.469]].toPositions;

  points.toVectorTileCoordinates(35_205, 21_507, 16, 4096, 3)
    .should.equal([[-851, 3235], [2728, 3235], [4890, 4581]]);
}

///
int[] toVectorTileCoordinates(const Position coordinates, const uint tx = 0, const uint ty = 0, const uint z = 0, const uint extent = 4096) pure {
  immutable double z2 = 1 << z;

  immutable double x = coordinates[0] / 360. + 0.5;

  immutable double sin = sin(coordinates[1] * PI / 180.);
  immutable double y2 = 0.5 - 0.25 * log((1. + sin) / (1. - sin)) / PI;
  immutable double y = y2 < 0. ? 0. : y2 > 1. ? 1. : y2;

  int intX = cast(int) (extent * (x * z2 - tx));
  int intY = cast(int) (extent * (y * z2 - ty));

  int dirX;
  int dirY;

  if(intX > extent) {
    dirX = 1;
  }

  if(intX < 0) {
    dirX = -1;
  }

  if(intY > extent) {
    dirY = 1;
  }

  if(intY < 0) {
    dirY = -1;
  }

  if(dirX != 0 || dirY != 0) {
    auto tmp = coordinates.toVectorTileCoordinates(tx + dirX, ty + dirY, z, extent);
    intX = tmp[0] + dirX * extent;
    intY = tmp[1] + dirY * extent;
  }

  return [intX, intY];
}

/// ditto
int[] toVectorTileCoordinates(const EPSG4326 point, const uint tx = 0, const uint ty = 0, const uint z = 0, const uint extent = 4096) pure {
  return toVectorTileCoordinates(Position(point.longitude, point.latitude), tx, ty, z, extent);
}

/// mercator point toVectorTileCoordinates
unittest {
  Position point = Position(13.1395802,52.5586492);

  point.toVectorTileCoordinates.should.equal([ 2197, 1342 ]);
  point.toVectorTileCoordinates(1, 0, 1).should.equal([ 298, 2685 ]);
}

/// Check if a point is generated in the right tile
unittest {
  Position point1 = Position(13.4931, 52.4954);
  Position point2 = Position(13.5112, 52.4877);

  Point(point1).tileIndexes(14).to!string.should.equal("[[8806, 5375]]");
  point1.toVectorTileCoordinates(8806, 5375, 14).to!string.should.equal("[352, 232]");

  Point(point2).tileIndexes(14).to!string.should.equal("[[8806, 5375]]");
  point2.toVectorTileCoordinates(8806, 5375, 14).to!string.should.equal("[3726, 2589]");
}

/// Check if a line is generated in the right tile
unittest {
  Position[] line = [[13.4931, 52.4954], [13.5112, 52.4877]].toPositions;

  LineString(line).tileIndexes(14).to!string.should.equal("[[8806, 5375]]");
  line.toVectorTileCoordinates(8806, 5375, 14).to!string.should.equal("[[352, 232], [3726, 2589]]");
}

///
BBox!double toBBox(uint zoom, uint x, uint y, double paddingPercentage = 0) {
  double paddingWidth = 0;
  double paddingHeight = 0;

  auto top = latitudeFromCol(y, zoom);
  auto left = longitudeFromRow(x, zoom);
  auto bottom = latitudeFromCol(y + 1, zoom);
  auto right = longitudeFromRow(x + 1, zoom);

  if(paddingPercentage != 0) {
    paddingWidth = abs(right - left) * abs(paddingPercentage);
    paddingHeight = abs(bottom - top) * abs(paddingPercentage);

    top += paddingHeight;
    bottom -= paddingHeight;

    left -= paddingWidth;
    right += paddingWidth;
  }

  if(left < -180) {
    left = -180;
  }

  if(left > 180) {
    left = 180;
  }

  if(right > 180) {
    right = 180;
  }

  if(right < -180) {
    right = -180;
  }

  if(top > 90) {
    top = 90;
  }

  if(top < -90) {
    top = -90;
  }

  if(bottom < -90) {
    bottom = -90;
  }

  if(bottom > 90) {
    bottom = 90;
  }

  return BBox!double(left, right, top, bottom);
}

BBox!double toBBox(ZXY zxy, double paddingPercentage = 0) {
  return toBBox(zxy.z, zxy.x, zxy.y, paddingPercentage);
}

Point[] toRing(Point, T)(BBox!T box) {
  return [
    Point(box.left, box.top),
    Point(box.right, box.top),
    Point(box.right, box.bottom),
    Point(box.left, box.bottom),
    Point(box.left, box.top)
  ];
}