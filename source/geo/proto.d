/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module geo.proto;

import std.algorithm;
import std.array : array;
import std.conv;
import std.range;
import std.utf;
import std.string;
import std.traits;

version(unittest) {
    import fluent.asserts;
}

///
struct Varint {
  /// The varint representation of a varint
  immutable(ubyte)[] raw;

  /// Create a varint value
  this(T)(T value) if(!is(Unqual!(T) == ubyte)) {
    enum size = T.sizeof * 8 / 7 + 1;
    bool hasValue;

    ubyte[] _raw = [ 0 ];

    foreach_reverse(index; 0..size) {
      ubyte result = (value >>> (index * 7)) & 0x7F;

      if(result > 0 && !hasValue) {
        _raw.length = index + 1;
        hasValue = true;
      }

      if(hasValue) {
        _raw[index] = result;

        if(index < _raw.length - 1) {
          _raw[index] = _raw[index] | 0x80;
        }
      }
    }

    raw = _raw.idup;
  }

  /// ditto
  this(const(ubyte)[] data) inout pure nothrow {
    this.raw = data.idup;
  }

  /// ditto
  this(immutable(ubyte)[] data) inout pure nothrow {
    this.raw = data;
  }

  ///
  T to(T)() const pure nothrow @nogc {
    static if(is(T == bool)) {
      return raw.ptr[0] == 0 ? false : true;
    }
    else {
      enum size = T.sizeof * 8 / 7 + 1;

      T value;

      static foreach(i; 0..size) {
        value |= cast(T) (raw.ptr[i] & 0x7f) << (i * 7);

        if((raw.ptr[i] & 0x80) == 0) {
          return value;
        }
      }

      return value;
    }
  }

  ///
  T to(T)() immutable pure nothrow @nogc {
    static if(is(T == bool)) {
      return raw.ptr[0] == 0 ? false : true;
    }
    else {
      enum size = T.sizeof * 8 / 7 + 1;

      T value;

      static foreach(i; 0..size) {
        value |= cast(T) (raw.ptr[i] & 0x7f) << (i * 7);

        if((raw.ptr[i] & 0x80) == 0) {
          return value;
        }
      }

      return value;
    }
  }

  /// get the last byte
  ubyte last() const @nogc nothrow {
    immutable index = raw.length - 1;

    return raw[index];
  }

  size_t write(ubyte[] data) inout pure nothrow @nogc {
    data[0..raw.length] = raw[0..$];
    return raw.length;
  }

  size_t length() inout pure nothrow @nogc {
    return raw.length;
  }
}

/// varint conversion of an int value
unittest {
  Varint(300).raw.should.equal(cast(ubyte[]) [172, 2]); /// [1010 1100], [0000 0010]
}

/// varint write data to buffer
unittest {
  ubyte[] data;
  data.length = 2;
  auto size = Varint(300).write(data);
  size.should.equal(2);
  data.should.equal(cast(ubyte[]) [172, 2]);
}

/// varint raw length
unittest {
  Varint(long.max).length.should.equal(9);
}

/// varint conversion of a long
unittest {
  Varint(long.max).raw.should.equal(cast(ubyte[]) [0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x7f]);
  Varint(long.min).raw.should.equal(cast(ubyte[]) [0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x01]);
  Varint(0L).raw.should.equal(cast(ubyte[]) [0]);
  Varint(1UL).raw.should.equal(cast(ubyte[]) [1]);
}

/// varint conversion to original value
unittest {
  Varint(long.max).to!long.should.equal(long.max);
  Varint(long.min).to!long.should.equal(long.min);
}

///
uint toZigZag(int value) {
  return (value << 1) ^ (value >> 31);
}

/// Test parameter serializing
unittest {
  toZigZag(25)
    .should.equal(50);

  toZigZag(17)
    .should.equal(34);

  toZigZag(-2)
    .should.equal(3);

  toZigZag(-5)
    .should.equal(9);
}

///
ulong toZigZag(long value) {
  return (value << 1) ^ (value >> 63);
}

/// Test parameter serializing
unittest {
  toZigZag(25L)
    .should.equal(50UL);

  toZigZag(17L)
    .should.equal(34UL);

  toZigZag(-2L)
    .should.equal(3UL);

  toZigZag(-5L)
    .should.equal(9UL);
}

///
int fromZigZag(uint value) {
  return ((value >> 1) ^ (-(value & 1)));
}

/// Test parameter deserializing
unittest {
  fromZigZag(50)
    .should.equal(25);

  fromZigZag(34)
    .should.equal(17);

  fromZigZag(3)
    .should.equal(-2);

  fromZigZag(9)
    .should.equal(-5);
}

///
long fromZigZag(ulong value) {
  return ((value >> 1) ^ (-(value & 1)));
}

/// Test parameter deserializing
unittest {
  fromZigZag(50UL)
    .should.equal(25L);

  fromZigZag(34UL)
    .should.equal(17L);

  fromZigZag(3UL)
    .should.equal(-2L);

  fromZigZag(9UL)
    .should.equal(-5L);
}

///
enum WireType : ubyte {
  ///
  varint = 0,
  ///
  fixed64 = 1,
  ///
  fixedLength = 2,
  ///
  fixed32 = 5
}

///
struct ProtoKey {
  /// The key value
  immutable(Varint) value;

  /// Create the key
  this(size_t index, WireType type) {
    this.value = Varint((index << 3) | type);
  }

  /// ditto
  this(immutable Varint value) {
    this.value = value;
  }

  /// ditto
  this(const(ubyte)[] raw) {
    this(immutable Varint(raw));
  }

  /// get the key index
  size_t index() {
    return value.to!long >> 3;
  }

  /// get the key type
  WireType type() {
    return (value.last & 0x07).to!WireType;
  }

  size_t write(ubyte[] data) {
    return value.write(data);
  }

  size_t length() {
    return value.length;
  }
}

/// serialize a proto buf key
unittest {
  ProtoKey(1, WireType.varint).value.raw.should.equal(cast(ubyte[]) [ 8 ]);
  ProtoKey(2, WireType.fixedLength).value.raw.should.equal(cast(ubyte[]) [ 0x12 ]);

  ubyte[] data;
  data.length = 3;

  auto size = ProtoKey(2, WireType.fixedLength).write(data);
  size.should.equal(1);
  data[0..size].should.equal(cast(ubyte[]) [ 0x12 ]);
}

/// deserialize a proto buf key
unittest {
  auto key1 = ProtoKey(cast(ubyte[]) [ 8 ]);
  key1.index.should.equal(1);
  key1.type.should.equal(WireType.varint);

  auto key2 = ProtoKey(cast(ubyte[]) [ 0x12 ]);
  key2.index.should.equal(2);
  key2.type.should.equal(WireType.fixedLength);
}

///
struct VarintInputRange {

  private {
    immutable ubyte val = 0x80;

    immutable(ubyte[]) data;
    size_t index;
    size_t length;
  }

  ///
  this(const ubyte[] data) {
    this.data = data.idup;

    if(data.length > 0) {
      this.popFront();
    }
  }

  ///
  this(immutable ubyte[] data) {
    this.data = data;

    if(data.length > 0) {
      this.popFront();
    }
  }

  ///
  this(T)(T data) if(isInputRange!T && is(Unqual!(ElementType!T) == ubyte)) {
    this(data.array);
  }

  /// Pop raw bytes
  immutable(ubyte)[] readRaw(size_t size) {
    auto result = data[index .. index+size];
    index += size;

    static foreach(i; 0..long.sizeof) {
      if(index+i >= data.length) {
        this.length = i + 1;
        return result;
      }

      if((data[index + i] & val) == false) {
        this.length = i + 1;

        return result;
      }
    }

    return result;
  }

  /// ditto
  immutable(ubyte)[Size] readRaw(size_t Size)() {
    immutable(ubyte)[Size] result = data[index .. index+Size];
    index += Size;

    static foreach(i; 0..long.sizeof) {
      if(index+i >= data.length) {
        this.length = i + 1;
        return result;
      }

      if((data[index + i] & val) == false) {
        this.length = i + 1;
        return result;
      }
    }

    return result;
  }

  ///
  immutable(Varint) front() {
    return Varint(data[index .. index+length]);
  }

  ///
  immutable(Varint) moveFront() {
    auto result = front;
    this.popFront;
    return result;
  }

  ///
  void popFront() {
    index += length;

    static foreach(i; 0..long.sizeof) {
      if(index+i >= data.length) {
        this.length = i + 1;
        return;
      }

      if((data[index + i] & val) == false) {
        this.length = i + 1;
        return;
      }
    }

    this.length = data.length - index;
  }

  ///
  @property bool empty() {
    return index >= data.length;
  }

  immutable(Varint)[] array() {
    immutable(Varint)[] result;
    result.reserve(data.length);

    size_t position;

    do {
      size_t _length;

      static foreach(i; 0..long.sizeof) {
        if(_length == 0 && position+i < data.length) {
          if((data[position + i] & val) == false) {
            _length = i + 1;
          }
        }
      }

      auto val = Varint(data[position..position + _length]);

      result ~= val;

      position += _length;
    } while(position < data.length);

    return result;
  }

  /// foreach iteration uses opApply, since one delegate call per loop
  /// iteration is faster than three virtual function calls.
  int opApply(scope int delegate(immutable(Varint)) dg) {
    int result;
    size_t position;

    do {
      size_t _length;

      static foreach(i; 0..long.sizeof) {
        if(_length == 0 && position+i < data.length) {
          if((data[position + i] & val) == false) {
            _length = i + 1;
          }
        }
      }

      auto val = Varint(data[position..position + _length]);

      result = dg(val);

      if (result)
          break;

      position += _length;
    } while(position < data.length);

    return result;
  }

  /// ditto
  int opApply(scope int delegate(size_t, immutable(Varint)) dg) {
    int result;
    size_t index;
    size_t position;

    do {
        size_t _length;

        static foreach(i; 0..long.sizeof) {
        if(_length == 0 && position+i < data.length) {
          if((data[position + i] & val) == false) {
            _length = i + 1;
          }
        }
      }

      result = dg(index, immutable Varint(data[position..position + _length]));
      if (result)
          break;

      position += _length;
      index++;
    } while(position < data.length);

    return result;
  }
}

/// Varint range
unittest {
  ubyte[] data = [0x06, 0x03, 0x8E, 0x02, 0x9E, 0xA7, 0x05];
  auto range = VarintInputRange(data);
  range.map!(a => a.to!ulong).array.should.equal([6UL, 3UL, 270UL, 86_942UL]);
}


/// Varint foreach
unittest {
  ulong[] results;

  ubyte[] data = [0x06, 0x03, 0x8E, 0x02, 0x9E, 0xA7, 0x05];
  auto range = VarintInputRange(data);

  foreach(value; range) {
    results ~= value.to!ulong;
  }

  results.should.equal([6UL, 3UL, 270UL, 86_942UL]);
}

/// Varint indexed foreach
unittest {
  ulong[] results;

  ubyte[] data = [0x06, 0x03, 0x8E, 0x02, 0x9E, 0xA7, 0x05];
  auto range = VarintInputRange(data);

  foreach(index, value; range) {
    results ~= value.to!ulong;
  }

  results.should.equal([6UL, 3UL, 270UL, 86_942UL]);
}

///
auto stringToProtoBuf(ulong index, const string value) {
  return chain(
    ProtoKey(index, WireType.fixedLength).value.raw,
    Varint(value.length).raw,
    value.byChar.map!(a => a.to!(immutable(ubyte))));
}

///
unittest {
  stringToProtoBuf(1, "testing").array.should.equal(cast(ubyte[]) [0x0A, 0x07, 0x74, 0x65, 0x73, 0x74, 0x69, 0x6e, 0x67]);
}

///
size_t stringToProtoBuf(ulong index, const string value, ubyte[] data) {
  size_t size = ProtoKey(index, WireType.fixedLength).write(data);
  size += Varint(value.length).write(data[size .. $]);
  data[size..size+value.length] = cast(ubyte[]) value[0..$];

  return size + value.length;
}

///
unittest {
  ubyte[] data;
  data.length = 1024;

  auto size = stringToProtoBuf(1, "testing", data);

  data[0..size].array.should.equal(cast(ubyte[]) [0x0A, 0x07, 0x74, 0x65, 0x73, 0x74, 0x69, 0x6e, 0x67]);
}

///
auto stringToProtoBufLength(ulong index, const string value) {
  return ProtoKey(index, WireType.fixedLength).length +
    Varint(value.length).length +
    value.length;
}

///
unittest {
  stringToProtoBufLength(1, "testing").should.equal(9);
}

///
private final class DeltaRange(T) : InputRange!T {

  private {
    InputRange!T data;
    T _front;
  }

  this(U)(U data) if(isInputRange!U && is(ElementType!U == T)) {
    this.data = data.inputRangeObject;

    if(!this.data.empty) {
      _front = data.front;
    }
  }

  T front() {
    return _front;
  }

  T moveFront() {
    auto result = front;
    this.popFront;
    return result;
  }

  void popFront() {

    data.popFront;

    if(data.empty) {
      return;
    }

    _front += data.front;
  }

  bool empty() {
    return data.empty;
  }

  int opApply(scope int delegate(T) dg) {
    int result;
    T prevValue;

    foreach(value; this.data) {
      prevValue += value;
      result = dg(prevValue);

      if (result)
          break;
    }

    return result;
  }

  int opApply(scope int delegate(size_t, T) dg) {
    int result;
    T prevValue;

    foreach(index, value; this.data) {
      prevValue += value;
      result = dg(index, prevValue);

      if (result)
          break;
    }

    return result;
  }
}

/// Delta decode a range. x[1], x[2]-x[1], x[3]-x[2], ...
auto deltaDecode(T)(T data) if(isInputRange!T) {
  return new DeltaRange!(ElementType!T)(data);
}

/// delta decode an array
unittest {
  auto data = [2, 2, 2, 3, -2];

  data.deltaDecode.array.should.equal([2, 4, 6, 9, 7]);
}

///
string genReadKey() {
  return `
    auto key = ProtoKey(range.front);
    range.popFront;

    auto value = range.front;
    range.popFront;`;
}

/// generate the code that reads a basic value
string genRead(string value, string type, string index) {
  return `
    if(key.index == ` ~ index ~ `) {
      ` ~ value ~ ` = value.to!(` ~ type ~ `);
      continue;
    }`;
}

///
string genReadStruct(string value, string type, string index) {
  return `
    if(key.index == ` ~ index ~ `) {
      ` ~ value ~ ` = read` ~ type ~ `(range.readRaw(value.to!ulong));
      continue;
    }`;
}

///
string genReadPacked(string value, string type, string index) {
  return `
    if(key.index == ` ~ index ~ `) {
      ` ~ value ~ ` = (VarintInputRange(range.readRaw(value.to!ulong))).map!(a => a.to!(` ~ type ~ `)).array;
      continue;
    }`;
}

__gshared long[] long_cache;

///
string genReadDeltaPacked(string value, string type, string index) {
  string fieldName = type ~ `_cache`;

  return `
    if(key.index == ` ~ index ~ `) {
      ` ~ type ~ ` val;
      auto len = value.to!ulong;

      if(` ~ fieldName ~ `.length < len) {
        ` ~ fieldName ~ `.length = len;
      }

      scope _range = VarintInputRange(range.readRaw(len));

      size_t size;
      foreach(_index, v; _range) {
        val += v.to!(` ~ type ~ `);
        ` ~ fieldName ~ `[_index] = val;
        size = _index;
      }

      ` ~ value ~ ` = ` ~ fieldName ~ `[0..size].dup;
      continue;
    }`;
}


///
string genReadZigZagDeltaPacked(string value, string type, string index) {
  string fieldName = type ~ `_cache`;

  return `
    if(key.index == ` ~ index ~ `) {
      ` ~ type ~ ` val;
      auto len = value.to!ulong;

      if(` ~ fieldName ~ `.length < len) {
        ` ~ fieldName ~ `.length = len;
      }

      scope _range = VarintInputRange(range.readRaw(len));

      size_t size;
      foreach(_index, v; _range) {

        val += v.to!(u` ~ type ~ `).fromZigZag;

        ` ~ fieldName ~ `[_index] = val;
        size = _index + 1;
      }

      ` ~ value ~ ` = ` ~ fieldName ~ `[0..size].dup;
      continue;
    }`;
}

///
string genReadRepeatedStruct(string value, string type, string index) {
  return `
    if(key.index == ` ~ index ~ `) {
      ` ~ value ~ ` ~= read` ~ type ~ `(range.readRaw(value.to!ulong));
      continue;
    }`;
}
