module geo.RBush;

import std.math;
import std.algorithm;
import std.conv;
import std.array;

version(unittest) {
  import fluent.asserts;
}

/// RBush is a high-performance JavaScript library for 2D spatial indexing of points and rectangles.
/// It's based on an optimized R-tree data structure with bulk insertion support.
///
/// Spatial index is a special data structure for points and rectangles that allows you to perform
/// queries like "all items within this bounding box" very efficiently (e.g. hundreds of times faster
/// than looping over all items). It's most commonly used in maps and data visualizations.
///
/// https://github.com/mourner/rbush/tree/master
class RBush {
  long maxEntries;
  long _minEntries;
  Node data;

  this(long maxEntries = 9) {
    // max entries in a node is 9 by default; min node fill is 40% for best performance
    this.maxEntries = max(4, maxEntries);
    this._minEntries = max(2, maxEntries / 4);
    this.clear();
  }

  size_t height() {
    return data.height;
  }

  Node[] all() {
    Node[] result;

    auto result2 = all(this.data, result);

    return result;
  }

  ref Node[] all(Node node, out Node[] result) {
    Node[] nodesToSearch = [];

    import std.stdio;

    do {

      if (node.leaf) {
        result ~= node.children;
      } else {
        nodesToSearch ~= node.children;
      }

      writeln(node.height, " -> ",nodesToSearch.length, " ", result.length, ": ", node.minX, " ", node.minY, " ", node.maxX, " ", node.maxY);

      node = nodesToSearch.pop;

    } while(node !is null);

    writeln(nodesToSearch.length, " ", result.length);

    return result;
  }

  Node[] search(Node bbox) {
    auto node = &this.data;
    Node[] result;

    if (!intersects(bbox, *node)) return result;

    Node[] nodesToSearch;

    while (node) {
      foreach(i; 0..(*node).children.length) {
        auto child = node.children[i];
        auto childBBox = child;

        if (intersects(bbox, childBBox)) {
          if ((*node).leaf) result ~= child;
          else if (contains(bbox, childBBox)) this.all(child, result);
          else nodesToSearch ~= child;
        }
      }

      node = &nodesToSearch[0];
      nodesToSearch = nodesToSearch[1..$];
    }

    return result;
  }

  bool collides(Node bbox) {
    auto node = &this.data;

    if (!intersects(bbox, *node)) return false;

    Node[] nodesToSearch;

    while (node) {
      foreach (i; 0..node.children.length) {
        auto child = node.children[i];
        auto childBBox = child;

        if (intersects(bbox, childBBox)) {
          if (node.leaf || contains(bbox, childBBox)) return true;
          nodesToSearch ~= child;
        }
      }

      node = &nodesToSearch[0];
      nodesToSearch = nodesToSearch[1..$];
    }

    return false;
  }

  RBush load(Node[] data) {
    import std.stdio;

    1.writeln;
    if (!data.length) {

      2.writeln;
      return this;
    }

    3.writeln;
    if (data.length < this._minEntries) {
      4.writeln;
      foreach (i; 0..data.length) {
        5.writeln;
        this.insert(data[i]);
      }

      6.writeln;
      return this;
    }

    7.writeln;
    // recursively build the tree with the given data from scratch using OMT algorithm
    auto node = this.build(data, 0, data.length - 1, 0);

    8.writeln(" ", node);
    if (!this.data.children.length) {
      9.writeln;
      // save as is if tree is empty
      this.data = node;
    } else if (this.data.height == node.height) {
      10.writeln;
      // split root if trees have the same height
      this.splitRoot(this.data, node);
    } else {
      11.writeln;
      if (this.data.height < node.height) {
          12.writeln;
          // swap trees if inserted one is bigger
          auto tmpNode = this.data;
          this.data = node;
          node = tmpNode;
      }

      13.writeln;
      // insert the small tree into the large tree at appropriate level
      this.insert(node, this.data.height - node.height - 1);
    }


    14.writeln;
    return this;
  }

  RBush insert(Node item) {
    this.insert(item, this.data.height - 1);
    return this;
  }

  void insert(Node item, long level) {
    auto bbox = item.toNode;
    Node[] insertPath = [];

    // find the best node for accommodating the item, saving all nodes along the path too
    auto node = this.chooseSubtree(bbox, this.data, level, insertPath);

    // put the item into the node
    node.children ~= item;
    extend(node, bbox);

    // split on node overflow; propagate upwards if necessary
    while (level >= 0) {
      if (insertPath[level].children.length > this.maxEntries) {
        this.split(insertPath, level);
        level--;
      } else break;
    }

    // adjust bboxes along the insertion path
    this.adjustParentBBoxes(bbox, insertPath, level);
  }

  RBush clear() {
    this.data = new Node();
    return this;
  }

  RBush remove(Node item) {
    auto node = &this.data;
    auto bbox = item;

    Node[] path;
    long[] indexes;
    long i;
    Node* parent;
    bool goingUp;

    // depth-first iterative tree traversal
    while (node !is null || path.length) {
      if (!node) { // go up
        node = &path[path.length - 1];
        path = path[1 .. $];
        parent = &path[path.length - 1];
        i = indexes[indexes.length - 1];
        indexes = indexes[0..$-1];
        goingUp = true;
      }

      if (node.leaf) { // check current node
        const index = node.children.countUntil(item);

        if (index != -1) {
          // item found, remove the item and condense tree upwards
          node.children = node.children[0..index-1] ~ node.children[index..$];
          path ~= *node;
          this.condense(path);
          return this;
        }
      }

      if (!goingUp && !node.leaf && contains(*node, bbox)) { // go down
          path ~= *node;
          indexes ~= i;
          i = 0;
          parent = node;
          node = &node.children[0];
      } else if (parent) { // go right
        i++;
        node = &parent.children[i];
        goingUp = false;

      } else node = null; // nothing found
    }

    return this;
  }

  Node build(Node[] items, long left, long right, long height) {
    import std.stdio;

    writeln("build");

    auto N = right - left + 1;
    auto M = this.maxEntries;
    Node node;

    writeln("m=", M, " n=", N);

    if (N <= M) {
      // reached leaf level; return leaf
      writeln("// reached leaf level; return leaf, height=", height);
      node = new Node(items[left..right + 1]);

      calcBBox(node);
      return node;
    }

    if (!height) {
        // target height of the bulk-loaded tree
      writeln("// target height of the bulk-loaded tree");

        height = ceil(log(N.to!double) / log(M.to!double)).to!long;

      writeln("// target number of root entries to maximize storage utilization, height=", height);
        // target number of root entries to maximize storage utilization
        M = N / pow(M, height - 1);
    }

    node = new Node([]);

    writeln("// split the items into M mostly square tiles");

    auto N2 = N / M;
    auto N1 = N2 * sqrt(M.to!real).to!long;

    multiSelect!compareNodeMinX(items, left, right, N1);

    for (auto i = left; i <= right; i += N1) {
      writeln("-> i=", i, " left=", left, " right=", right, "N1=", N1);
      auto right2 = min(i + N1 - 1, right);

      multiSelect!compareNodeMinY(items, i, right2, N2);

      for (auto j = i; j <= right2; j += N2) {
        writeln("=> j=", j, " i=", i, " right2=", right2, "N2=", N2);
        auto right3 = min(j + N2 - 1, right2);

        // pack each entry recursively
        node.children ~= this.build(items, j, right3, height - 1);
      }
    }

    calcBBox(node);

    return node;
  }

  Node chooseSubtree(Node bbox, Node node, long level, Node[] path) {
    while (true) {
      path ~= node;

      if (node.leaf || path.length - 1 == level) break;

      double minArea = double.infinity;
      double minEnlargement = double.infinity;
      Node targetNode;

      foreach(i; 0..node.children.length) {
        auto child = node.children[i];
        auto area = bboxArea(child);
        auto enlargement = enlargedArea(bbox, child) - area;

        // choose entry with the least area enlargement
        if (enlargement < minEnlargement) {
          minEnlargement = enlargement;
          minArea = area < minArea ? area : minArea;
          targetNode = child;

        } else if (enlargement == minEnlargement) {
          // otherwise choose one with the smallest area
          if (area < minArea) {
              minArea = area;
              targetNode = child;
          }
        }
      }

      node = targetNode;

      if(node is null) {
        node = node.children[0];
      }
    }

    return node;
  }

  // split overflowed node into two
  void split(Node[] insertPath, long level) {
    auto node = insertPath[level];
    auto M = node.children.length;
    auto m = this._minEntries;

    this.chooseSplitAxis(node, m, M);

    const splitIndex = this.chooseSplitIndex(node, m, M);

    auto newNode = new Node(node.children[splitIndex..$-splitIndex]);

    calcBBox(node);
    calcBBox(newNode);

    if (level) insertPath[level - 1].children ~= newNode;
    else this.splitRoot(node, newNode);
  }

  void splitRoot(Node node, Node newNode) {
    this.data = new Node([node, newNode]);

    calcBBox(this.data);
  }

  long chooseSplitIndex(Node node, long m, long M) {
    long index;
    double minOverlap = double.infinity;
    double minArea = double.infinity;

    foreach(i; m .. M - m) {
      auto bbox1 = distBBox(node, 0, i);
      auto bbox2 = distBBox(node, i, M);

      const overlap = intersectionArea(bbox1, bbox2);
      const area = bboxArea(bbox1) + bboxArea(bbox2);

      // choose distribution with minimum overlap
      if (overlap < minOverlap) {
        minOverlap = overlap;
        index = i;

        minArea = area < minArea ? area : minArea;
      } else if (overlap == minOverlap) {
        // otherwise choose distribution with minimum area
        if (area < minArea) {
          minArea = area;
          index = i;
        }
      }
    }

    return index || M - m;
  }

  /// sorts node children by the best axis for split
  void chooseSplitAxis(Node node, long m, long M) {
    auto xMargin = this.allDistMargin!compareNodeMinX(node, m, M);
    auto yMargin = this.allDistMargin!compareNodeMinY(node, m, M);

    // if total distributions margin value is minimal for x, sort by minX,
    // otherwise it's already sorted by minY
    if (xMargin < yMargin) node.children.sort!compareNodeMinX;
  }

  /// total margin of all possible split distributions where each node is at least m full
  double allDistMargin(alias compare)(Node node, long m, long M) {
    node.children.sort!compare;

    auto leftBBox = distBBox(node, 0, m);
    auto rightBBox = distBBox(node, M - m, M);
    auto margin = bboxMargin(leftBBox) + bboxMargin(rightBBox);

    foreach (i; m..(M - m)) {
      auto child = node.children[i];
      extend(leftBBox, child);
      margin += bboxMargin(leftBBox);
    }

    foreach_reverse(i; (M - m)..m) {
      auto child = node.children[i];
      extend(rightBBox, child);
      margin += bboxMargin(rightBBox);
    }

    return margin;
  }

  void adjustParentBBoxes(Node bbox, Node[] path, long level) {
    foreach_reverse(i; 0..level) {
      extend(path[i], bbox);
    }
  }

  void condense(Node[] path) {
    Node[] siblings;

    // go through the path, removing empty nodes and updating bboxes
    foreach_reverse(i ; (path.length - 1)..0) {
      if (path[i].children.length == 0) {
        if (i > 0) {
          siblings = path[i - 1].children;

          auto index = siblings.countUntil(path[i]);
          siblings = siblings[index..index+1];
        } else this.clear();
      } else calcBBox(path[i]);

      if(siblings.length == 0) {
        break;
      }
    }
  }
}

Node toNode(Node node) {
  return node;
}

long findItem(T, U)(U item, U[] items, T equalsFn) {
  if (!equalsFn) return items.indexOf(item);

  for (auto i = 0; i < items.length; i++) {
    if (equalsFn(item, items[i])) return i;
  }

  return -1;
}

/// calculate node's bbox from bboxes of its children
void calcBBox(Node node) {
  distBBox(node, 0, node.children.length, node);
}

/// min bounding rectangle of node children from k to p-1
Node distBBox(Node node, size_t k, size_t p, Node destNode) {
  destNode.minX = double.infinity;
  destNode.minY = double.infinity;
  destNode.maxX = -double.infinity;
  destNode.maxY = -double.infinity;

  foreach (i; k..p) {
      auto child = node.children[i];
      extend(destNode, child);
  }

  return destNode;
}

/// ditto
Node distBBox(Node node, size_t k, size_t p) {
  Node destNode;
  return distBBox(node, k, p);
}

Node extend(Node a, Node b) {
  a.minX = min(a.minX, b.minX);
  a.minY = min(a.minY, b.minY);
  a.maxX = max(a.maxX, b.maxX);
  a.maxY = max(a.maxY, b.maxY);

  return a;
}

bool compareNodeMinX(Node a, Node b) { return a.minX > b.minX; }
bool compareNodeMinY(Node a, Node b) { return a.minY > b.minY; }

double bboxArea(Node a)   { return (a.maxX - a.minX) * (a.maxY - a.minY); }
double bboxMargin(Node a) { return (a.maxX - a.minX) + (a.maxY - a.minY); }

double enlargedArea(Node a, Node b) {
  return (max(b.maxX, a.maxX) - min(b.minX, a.minX)) *
          (max(b.maxY, a.maxY) - min(b.minY, a.minY));
}

double intersectionArea(Node a, Node b) {
  const minX = max(a.minX, b.minX);
  const minY = max(a.minY, b.minY);
  const maxX = min(a.maxX, b.maxX);
  const maxY = min(a.maxY, b.maxY);

  return max(0, maxX - minX) * max(0, maxY - minY);
}

bool contains(Node a, Node b) {
    return a.minX <= b.minX &&
            a.minY <= b.minY &&
            b.maxX <= a.maxX &&
            b.maxY <= a.maxY;
}

bool intersects(Node a, Node b) {
    return b.minX <= a.maxX &&
            b.minY <= a.maxY &&
            b.maxX >= a.minX &&
            b.maxY >= a.minY;
}

class Node {
  Node[] children;
  double minX;
  double minY;
  double maxX;
  double maxY;

  this(int minX, int minY, int maxX, int maxY) {
    this.minX = minX;
    this.minY = minY;
    this.maxX = maxX;
    this.maxY = maxY;
  }

  this(Node[] children = [], double minX = double.infinity, double minY = double.infinity, double maxX = -double.infinity, double maxY = -double.infinity) {
    this.children = children;
    this.minX = minX;
    this.minY = minY;
    this.maxX = maxX;
    this.maxY = maxY;
  }

  bool leaf() {
    return this.height == 1;
  }

  long height() {
    long result = 0;

    foreach (node; children) {
      result = max(node.height + 1, result);
    }

    return result;
  }

  override string toString() {
    return `{ "children": ` ~ children.to!string ~
           `, "height": ` ~ height.to!string ~
           `, "leaf": ` ~ leaf.to!string ~
           `, "minX": ` ~ minX.to!string ~
           `, "minY": ` ~ minY.to!string ~
           `, "maxX": ` ~ maxX.to!string ~
           `, "maxY": ` ~ maxY.to!string ~ "}";
  }

  override bool opEquals(Object o) {
    Node node = cast(Node) o;

    if(node is null) {
      return false;
    }

    return minX == node.minX && minY == node.minY &&
      maxX == node.maxX && maxY == node.maxY;
  }
}

/// sort an array so that items come in groups of n unsorted items, with groups sorted between each other;
/// combines selection algorithm with binary divide & conquer approach
void multiSelect(alias compare)(ref Node[] arr, long left, long right, long n) {
  auto stack = [left, right];

  import std.stdio;

  while (stack.length) {
    writeln("--- ", stack.length);
    right = stack.pop;

    writeln("--- ", stack.length, " r=", right);

    left = stack.pop;

    writeln("--- ", stack.length, " l=", left);
    writeln("--- ", right ," - ", left ," <= ", n, " ", right - left <= n);

    if (right - left <= n) {
      continue;
    }

    const mid = left + ceil((right - left).to!double / n.to!double / 2.).to!long * n;

    writeln("1.?", mid, " ", left, " ", right);

    quickselect!compare(arr, mid, left, right);

    writeln("2.?", left, " ", mid, " ", mid, " ", right);

    stack ~= [left, mid, mid, right];
  }
}

///
void quickselect(alias compare)(ref Node[] arr, long k, long left, long right) {
  quickselectStep!compare(arr, k, left, right);
}

///
void quickselectStep(alias compare)(ref Node[] arr, long k, long left, long right) {
  import std.stdio;

  writeln(right ," > ",left);
  while (right > left) {
    if (right - left > 600) {
      auto n = right - left + 1;
      auto m = k - left + 1;
      auto z = log(n);
      auto s = 0.5 * exp(2 * z / 3);
      auto sd = 0.5 * sqrt(z * s * (n - s) / n) * (m - n / 2 < 0 ? -1 : 1);
      auto newLeft = max(left, k - m * s / n + sd).to!long;
      auto newRight = min(right, k + (n - m) * s / n + sd).to!long;
      quickselectStep!compare(arr, k, newLeft, newRight);
    }

    auto t = arr[k];
    auto i = left;
    auto j = right;

    swap(arr, left, k);
    if (compare(arr[right], t) > 0) swap(arr, left, right);

    while (i < j) {
      swap(arr, i, j);
      i++;
      j--;
      while (compare(arr[i], t) < 0) i++;
      while (compare(arr[j], t) > 0) j--;
    }

    if (compare(arr[left], t) == 0) swap(arr, left, j);
    else {
        j++;
        swap(arr, j, right);
    }

    if (j <= k) left = j + 1;
    if (k <= j) right = j - 1;
  }
}

private void swap(T)(ref T[] arr, long i, long j) {
  auto tmp = arr[i];
  arr[i] = arr[j];
  arr[j] = tmp;
}

/// can swap values in array
unittest {
  auto arr = [0, 1];
  swap(arr, 0, 1);

  arr.should.equal([1, 0]);
}

private T pop(T)(ref T[] arr) {

  static if(is(T == class)) {
    if(arr.length == 0) {
      return null;
    }
  }

  auto tmp = arr[arr.length - 1];

  arr.length = arr.length - 1;

  return tmp;
}

/// can pop values from array
unittest {
  auto arr = [0, 1];
  auto val = pop(arr);

  val.should.equal(1);
  arr.should.equal([0]);
}

version(unittest) {
  auto data = [
    new Node(0,0,0,0),new Node(10,10,10,10), new Node(20,20,20,20),new Node(25,0,25,0),new Node(35,10,35,10),new Node(45,20,45,20), new Node(0,25,0,25), new Node(10,35,10,35),
    new Node(20,45,20,45),new Node(25,25,25,25), new Node(35,35,35,35),new Node(45,45,45,45),new Node(50,0,50,0),new Node(60,10,60,10), new Node(70,20,70,20), new Node(75,0,75,0),
    new Node(85,10,85,10),new Node(95,20,95,20), new Node(50,25,50,25),new Node(60,35,60,35),new Node(70,45,70,45),new Node(75,25,75,25), new Node(85,35,85,35), new Node(95,45,95,45),
    new Node(0,50,0,50),new Node(10,60,10,60), new Node(20,70,20,70),new Node(25,50,25,50),new Node(35,60,35,60),new Node(45,70,45,70), new Node(0,75,0,75), new Node(10,85,10,85),
    new Node(20,95,20,95),new Node(25,75,25,75), new Node(35,85,35,85),new Node(45,95,45,95),new Node(50,50,50,50),new Node(60,60,60,60), new Node(70,70,70,70), new Node(75,50,75,50),
    new Node(85,60,85,60),new Node(95,70,95,70), new Node(50,75,50,75),new Node(60,85,60,85),new Node(70,95,70,95),new Node(75,75,75,75), new Node(85,85,85,85), new Node(95,95,95,95)
  ];

  auto smallData = [
    new Node(0,0,0,0),new Node(10,10,10,10), new Node(20,20,20,20),
    new Node(25,0,25,0),new Node(35,10,35,10),new Node(45,20,45,20),
    new Node(0,25,0,25), new Node(10,35,10,35)
  ];

  Node[] someData(long size) {
    Node[] data;

    foreach (i; 0..size) {
        data ~= new Node([], i, i, i, i);
    }

    return data;
  }
}

/// constructor uses 9 max entries by default
unittest {
  auto tree = new RBush().load(someData(9));
  tree.height.should.equal(1);

  auto tree2 = new RBush().load(someData(10));
  tree2.height.should.equal(2);

  import std.stdio;
  writeln(tree2.data);
}

/// load bulk-loads the given small data, given max node entries and forms a proper search tree
unittest {
  import vibe.data.json;

  auto tree = new RBush(4).load(smallData);

  auto all = tree.all;

  foreach(node; smallData) {
    auto exists = all.canFind(node);

    exists.should.equal(true);
  }

  all.length.should.equal(smallData.length);
}

/// load bulk-loads the given data, given max node entries and forms a proper search tree
unittest {
  import vibe.data.json;

  auto tree = new RBush(4).load(data);

  auto all = tree.all;

  foreach(node; data) {
    auto exists = all.canFind(node);

    exists.should.equal(true);
  }

  all.length.should.equal(data.length);
}
