/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module geo.xml;

import std.string;
import std.format;
import std.datetime;
import std.traits;
import std.algorithm;
import std.array;
import std.math;
import std.conv;

public import dxml.dom;
public import dxml.parser;

///
string escValue(const string val) pure {
  return val
    .replace(`"`, `&quot;`)
    .replace(`'`, `&apos;`)
    .replace(`<`, `&lt;`)
    .replace(`>`, `&gt;`)
    .replace(`&`, `&amp;`);
}

////
string toXmlString(SysTime time) {
  auto strTime = time.toISOExtString;

  if(strTime != SysTime.init.toISOExtString) {
    return format!(`<time>%s</time>`)(strTime);
  }

  return "";
}

///
string toXmlString(T)(T value) if(is(T == struct)) {
  string content;

  static foreach(member; __traits(allMembers, T)) static if(__traits(compiles, typeof(__traits(getMember, value, member))) && !is(typeof(__traits(getMember, value, member)) == void) && !isSomeFunction!(__traits(getMember, value, member))) {{
    auto val = __traits(getMember, value, member);
    alias M = Unqual!(typeof(val));

    static if(is(M == string)) {
      if(val != "") {
        content ~= format!(`<` ~ member ~ `>%s</` ~ member ~ `>`)(val.escValue);
      }
    }

    static if(is(M == double)) {
      if(!val.isNaN) {
        content ~= format!(`<` ~ member ~ `>%s</` ~ member ~ `>`)(val);
      }
    }

    static if(is(M == ulong)) {
      if(val) {
        content ~= format!(`<` ~ member ~ `>%s</` ~ member ~ `>`)(val);
      }
    }

    static if(isArray!(M) && __traits(hasMember, typeof(val[0]), "toString") && !__traits(hasMember, typeof(val[0]), "toCustomString")) {
      content ~= val.map!(a => a.toString).join;
    }
  }}

  return content;
}

///
auto fillDomProperties(T, D)(ref T value, D dom) if(is(T == struct)) {
  if(dom.type == EntityType.elementEmpty) {
    return value;
  }

  foreach (node; dom.children) {
    if(node.type != EntityType.elementStart && node.type != EntityType.elementEmpty) continue;

    static foreach(member; __traits(allMembers, T)) static if(__traits(compiles, typeof(__traits(getMember, value, member))) && !isCallable!(__traits(getMember, value, member))) {{
      alias M = Unqual!(typeof(__traits(getMember, value, member)));

      static if(is(M == string) || is(M == double) || is(M == ulong) || is(M == SysTime) || isArray!M || is(M == enum)) {
        if(member == node.name) {
          fillDomProperty!(member)(value, node);
        }
      }
    }}
  }

  return value;
}

///
void fillDomProperty(string member, T, D)(ref T value, D dom) if(is(T == struct)) {
  mixin(`alias M = typeof(value.` ~ member ~`);`);

  static if(isArray!M && !isSomeString!M && !is(M == enum)) {
    mixin(`alias MM = typeof(value.` ~ member ~`[0]);`);
    mixin(`value.` ~ member ~ ` ~= MM.fromDom(dom);`);
  } else {
    if(dom.type == EntityType.elementEmpty) {
      return;
    }

    foreach(node; dom.children) {
      if(node.type != EntityType.text) continue;
      string txtValue = node.text.strip;

      static if(is(M == SysTime)) {
        mixin(`value.` ~ member ~ ` = SysTime.fromISOExtString(txtValue);`);
      } else static if(is(M == enum)) {
        mixin(`value.` ~ member ~ ` = cast(M) txtValue;`);
      } else {
        mixin(`value.` ~ member ~ ` = txtValue.to!M;`);
      }
    }
  }
}