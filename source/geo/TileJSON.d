/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module geo.TileJSON;

import vibe.data.json;
import vibe.data.serialization : Name = name;

/// TileJson structure
/// https://github.com/mapbox/tilejson-spec/tree/master/2.2.0
/// https://github.com/mapbox/tilejson-spec/tree/3.0/3.0.0
struct TileJSON(string v = "3.0.0") if(v == "2.2.0" || v == "3.0.0") {
    /// A semver.org style version number. Describes the version of
    /// the TileJSON spec that is implemented by this JSON object.
    string tilejson = v;

    static if(v == "3.0.0") {
        /// An integer specifying the zoom level at which to generate
        /// overzoomed tiles from. Implementations may generate overzoomed
        /// tiles from parent tiles if the requested zoom level does not exist.
        /// In most cases, overzoomed tiles are generated from the maximum zoom
        /// level of the set of tiles. If fillzoom is specified, the overzoomed
        /// tile is generated from the fillzoom level.
        ///
        /// For example, in a set of tiles with maxzoom 10 and no fillzoom specified,
        /// if a request for a z11 tile comes through, the implementation will use the
        /// maximum z10 parent tiles to generate the new, overzoomed z11 tile. If the
        /// same TileJSON object had fillzoom specified at z7, a request for a z11
        /// tile would use the z7 tile instead of z10.
        @optional uint fillzoom = 16;

        /// An array of objects. Each object describes one layer of vector tile
        /// data. A vector_layer object MUST contain the id and fields keys,
        /// and MAY contain the description, minzoom, or maxzoom keys.
        struct VectorLayer {
            /// A string value representing the the layer id. For added context,
            /// this is referred to as the name of the layer in the Mapbox Vector Tile spec.
            /// https://github.com/mapbox/vector-tile-spec
            string id;

            /// An object whose keys and values are the names and descriptions of attributes
            /// available in this layer. Each value (description) MUST be a string that
            /// describes the underlying data. If no fields are present, the fields key MUST
            /// be an empty object.
            string[string] fields;

            /// A string representing a human-readable description of the entire layer's
            /// contents.
            @optional string description;

            /// An integer representing the lowest/highest zoom level whose tiles this layer
            /// appears in. minzoom MUST be greater than or equal to the set of tiles'
            /// minzoom. maxzoom MUST be less than or equal to the set of tiles' maxzoom.
            @optional uint minzoom;

            /// ditto
            @optional uint maxzoom;
        }

        /// ditto
        @optional VectorLayer[] vector_layers;
    }

    /// A name describing the tileset. The name can
    /// contain any legal character. Implementations SHOULD NOT interpret the
    /// name as HTML.
    @optional string name;

    /// A text description of the tileset. The
    /// description can contain any legal character. Implementations SHOULD NOT
    /// interpret the description as HTML.
    @optional string description;

    /// A semver.org style version number. When
    /// changes across tiles are introduced, the minor version MUST change.
    /// This may lead to cut off labels. Therefore, implementors can decide to
    /// clean their cache when the minor version changes. Changes to the patch
    /// level MUST only have changes to tiles that are contained within one tile.
    /// When tiles change significantly, the major version MUST be increased.
    /// Implementations MUST NOT use tiles with different major versions.
    @optional @Name("version") string version_ = "1.0.0";

    /// Contains an attribution to be displayed
    /// when the map is shown to a user. Implementations MAY decide to treat this
    /// as HTML or literal text. For security reasons, make absolutely sure that
    /// this field can't be abused as a vector for XSS or beacon tracking.
    @optional string attribution;

    /// Contains a mustache template to be used to
    /// format data from grids for interaction.
    /// See https://github.com/mapbox/utfgrid-spec/tree/master/1.2
    /// for the interactivity specification.
    @optional @Name("temlpate") string template_;

    /// Contains a legend to be displayed with the map.
    /// Implementations MAY decide to treat this as HTML or literal text.
    /// For security reasons, make absolutely sure that this field can't be
    /// abused as a vector for XSS or beacon tracking.
    @optional string legend;

    /// Either "xyz" or "tms". Influences the y
    /// direction of the tile coordinates.
    /// The global-mercator (aka Spherical Mercator) profile is assumed.
    @optional string scheme = "xyz";

    /// An array of tile endpoints. {z}, {x} and {y}, if present,
    /// are replaced with the corresponding integers. If multiple endpoints are specified, clients
    /// may use any combination of endpoints. All endpoints MUST return the same
    /// content for the same URL. The array MUST contain at least one endpoint.
    string[] tiles;

    /// An array of interactivity endpoints. {z}, {x}
    /// and {y}, if present, are replaced with the corresponding integers. If multiple
    /// endpoints are specified, clients may use any combination of endpoints.
    /// All endpoints MUST return the same content for the same URL.
    /// If the array doesn't contain any entries, interactivity is not supported
    /// for this tileset.
    /// See https://github.com/mapbox/utfgrid-spec/tree/master/1.2
    /// for the interactivity specification.
    @optional string[] grids;

    /// An array of data files in GeoJSON format.
    /// {z}, {x} and {y}, if present,
    /// are replaced with the corresponding integers. If multiple
    /// endpoints are specified, clients may use any combination of endpoints.
    /// All endpoints MUST return the same content for the same URL.
    /// If the array doesn't contain any entries, then no data is present in
    /// the map.
    @optional string[] data;

    /// An integer specifying the minimum zoom level.
    @optional uint minzoom = 0;

    /// An integer specifying the maximum zoom level. MUST be >= minzoom.
    @optional uint maxzoom = 16;

    /// The maximum extent of available map tiles. Bounds MUST define an area
    /// covered by all zoom levels. The bounds are represented in WGS:84
    /// latitude and longitude values, in the order left, bottom, right, top.
    /// Values may be integers or floating point numbers.
    @optional double[] bounds = [ -180, -85.05112877980659, 180, 85.0511287798066 ];

    /// The first value is the longitude, the second is latitude (both in
    /// WGS:84 values), the third value is the zoom level as an integer.
    /// Longitude and latitude MUST be within the specified bounds.
    /// The zoom level MUST be between minzoom and maxzoom.
    /// Implementations can use this value to set the default location. If the
    /// value is null, implementations may use their own algorithm for
    /// determining a default location.
    @optional double[] center = [ -76.275329586789, 39.153492567373, 8 ];
}