/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
/// Implementation for the well known binary representation
/// https://en.wikipedia.org/wiki/Well-known_text_representation_of_geometry
module geo.wkb;

import geo.geometries;
import geo.json;
import std.bitmanip;
import std.conv;

version(unittest) {
  import fluent.asserts;
  import vibe.data.json;
}

///
GeoJsonGeometry parseWKB(const ubyte[] value) @trusted {
  BasicGeometryVariant result;
  ubyte isLittleEndian = value[0];
  auto remainingData = value[1..$];
  uint coordinateCount = 2;

  T next(T)() {
    T result;
    if(isLittleEndian) {
      result = remainingData.read!(T, Endian.littleEndian);
    } else {
      result = remainingData.read!T;
    }

    return result;
  }

  Position nextPosition() {
    Position result;

    if(coordinateCount >= 1) result.lon = next!double;
    if(coordinateCount >= 2) result.lat = next!double;
    if(coordinateCount >= 3) result.alt = next!double;
    if(coordinateCount >= 4) result.m = next!double;

    return result;
  }

  Position[] nextRing() {
    Position[] result;

    result.length = next!uint;

    foreach (i; 0..result.length) {
      result[i] = nextPosition;
    }

    return result;
  }

  Position[][] nextRingList() {
    Position[][] result;

    result.length = next!uint;
    foreach (i; 0..result.length) {
      result[i] = nextRing;
    }

    return result;
  }

  Position[] nextPoints() {
    Position[] result;
    result.length = next!uint;

    foreach (i; 0..result.length) {
      isLittleEndian = next!ubyte;
      auto type = next!uint;

      result[i] = nextPosition;
    }

    return result;
  }

  Position[][] nextLines() {
    Position[][] result;
    result.length = next!uint;

    foreach (i; 0..result.length) {
      isLittleEndian = next!ubyte;
      auto type = next!uint;

      result[i] = nextRing();
    }

    return result;
  }

  Position[][][] nextPolygons() {
    Position[][][] result;

    result.length = next!uint;
    foreach (i; 0..result.length) {
      isLittleEndian = next!ubyte;
      auto type = next!uint;

      result[i] = nextRingList;
    }

    return result;
  }

  auto type = next!uint;

  if(type >= 1000 && type < 2000) {
    type -= 1000;
    coordinateCount++;
  }

  if(type >= 2000 && type < 3000) {
    type -= 2000;
    coordinateCount++;
  }

  if(type >= 3000 && type < 4000) {
    type -= 3000;
    coordinateCount += 2;
  }

  switch(type) {
    case 1:
      result = Point(nextPosition);
      break;

    case 2:
      result = LineString(nextRing);
      break;

    case 3:
      result = Polygon(nextRingList);
      break;

    case 4:
      result = MultiPoint(nextPoints);
      break;

    case 5:
      result = MultiLineString(nextLines);
      break;

    case 6:
      result = MultiPolygon(nextPolygons);
      break;

    default:
      throw new WKBException("Can not parse WKB type `" ~ type.to!string ~"`");
  }

  return GeoJsonGeometry(result);
}

/// A point representation in big endian WKB can be parsed
unittest {
  auto geometry = [ 0x0, /// big endian
    0x0, 0x0, 0x0, 0x1, /// geometry type
    0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, /// x coordinates
    0x40, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, /// y coordinates
  ].parseWKB;

  geometry.type.should.equal("Point");
  geometry.coordinates.should.equal("[2., 4.]".parseJsonString);
}

/// A multi point representation in big endian WKB can be parsed
unittest {
  auto geometry = [ 0x0, /// big endian
    0x0, 0x0, 0x0, 0x4,  /// geometry type
    0x0, 0x0, 0x0, 0x2,  /// the number of points
    0x0, /// big endian
    0x0, 0x0, 0x0, 0x4,  /// geometry type
    0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,  /// x coordinate
    0x40, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, /// y coordinate
    0x0, /// big endian
    0x0, 0x0, 0x0, 0x4,  /// geometry type
    0x40, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, /// x coordinate
    0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,  /// y coordinate
  ].parseWKB;

  geometry.type.should.equal("MultiPoint");
  geometry.coordinates.should.equal("[[2., 4.], [4., 2.]]".parseJsonString);
}

/// A pointZ representation in big endian WKB can be parsed
unittest {
  auto geometry = [ 0x0, /// big endian
    0x0,  0x0,  0x03, 0xe9, /// geometry type
    0x40, 0x0,  0x0, 0x0, 0x0, 0x0, 0x0, 0x0, /// x coordinate
    0x40, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, /// y coordinate
    0x40, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0  /// z coordinate
  ].parseWKB;

  geometry.type.should.equal("Point");
  geometry.coordinates.should.equal("[2., 4., 4.]".parseJsonString);
}

/// A pointM representation in big endian WKB can be parsed
unittest {
  auto geometry = [ 0x0, /// big endian
    0x0,  0x0,  0x07, 0xd1, /// geometry type
    0x40, 0x0,  0x0, 0x0, 0x0, 0x0, 0x0, 0x0, /// x coordinate
    0x40, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, /// y coordinate
    0x40, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0  /// m coordinate
  ].parseWKB;

  geometry.type.should.equal("Point");
  geometry.coordinates.should.equal("[2., 4., 4.]".parseJsonString);
}

/// A pointZM representation in big endian WKB can be parsed
unittest {
  auto geometry = [ 0x0, /// big endian
    0x0,  0x0,  0x0b, 0xb9, /// geometry type
    0x40, 0x0,  0x0, 0x0, 0x0, 0x0, 0x0, 0x0, /// x coordinate
    0x40, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, /// y coordinate
    0x40, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, /// z coordinate
    0x40, 0x10, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0  /// m coordinate
  ].parseWKB;

  geometry.type.should.equal("Point");
  geometry.coordinates.should.equal("[2., 4., 4., 4.]".parseJsonString);
}

/// A line representation in little endian WKB can be parsed
unittest {
  auto geometry = [ 0x01, /// little endian
    0x02, 0x0, 0x0, 0x0, /// geometry type
    0x05, 0x00, 0x00, 0x00, /// number of points
    0x92, 0x5c, 0x3d, 0x56, 0xf2, 0xd1, 0x2f, 0x40, /// point 1
    0x51, 0xd8, 0x99, 0x33, 0xfb, 0x61, 0x48, 0x40,
    0x6f, 0x2c, 0x83, 0x89, 0xac, 0x15, 0x35, 0x40, /// point 2
    0x94, 0xc1, 0x86, 0x6c, 0xbf, 0xb1, 0x47, 0x40,
    0xca, 0x9e, 0x1e, 0x02, 0x3c, 0x09, 0x2f, 0x40, /// point 3
    0x70, 0xf5, 0xd7, 0xea, 0x89, 0x23, 0x47, 0x40,
    0x64, 0xd6, 0x84, 0x0c, 0xd8, 0x50, 0x28, 0x40, /// point 4
    0x30, 0x76, 0x7e, 0x93, 0x4a, 0xc0, 0x47, 0x40,
    0x64, 0xfd, 0x84, 0xae, 0x80, 0xea, 0x2d, 0x40, /// point 5
    0xdc, 0xda, 0x96, 0x3f, 0x45, 0xf7, 0x47, 0x40
  ].parseWKB;

  geometry.type.should.equal("LineString");
  geometry.coordinates.to!string.should.equal("[
    [15.91005200861972,48.7654785634228],
    [21.08466395809631,47.38865429477588],
    [15.51803595184119,46.2776464037405],
    [12.15789832231093,47.50227588343512],
    [14.95801301358615,47.93180079332316]]".parseJsonString.to!string);
}

/// A multi line representation in little endian WKB can be parsed
unittest {
  auto geometry = [ 0x01, /// little endian
    0x05, 0x0, 0x0, 0x0, /// geometry type
    0x02, 0x00, 0x00, 0x00, /// number of segments
    0x01, /// little endian
    0x02, 0x0, 0x0, 0x0, /// geometry type
    0x02, 0x00, 0x00, 0x00, /// number of points
    0x92, 0x5c, 0x3d, 0x56, 0xf2, 0xd1, 0x2f, 0x40, /// point 1
    0x51, 0xd8, 0x99, 0x33, 0xfb, 0x61, 0x48, 0x40,
    0x6f, 0x2c, 0x83, 0x89, 0xac, 0x15, 0x35, 0x40, /// point 2
    0x94, 0xc1, 0x86, 0x6c, 0xbf, 0xb1, 0x47, 0x40,
    0x01, /// little endian
    0x02, 0x0, 0x0, 0x0, /// geometry type
    0x03, 0x00, 0x00, 0x00, /// number of points
    0xca, 0x9e, 0x1e, 0x02, 0x3c, 0x09, 0x2f, 0x40, /// point 1
    0x70, 0xf5, 0xd7, 0xea, 0x89, 0x23, 0x47, 0x40,
    0x64, 0xd6, 0x84, 0x0c, 0xd8, 0x50, 0x28, 0x40, /// point 2
    0x30, 0x76, 0x7e, 0x93, 0x4a, 0xc0, 0x47, 0x40,
    0x64, 0xfd, 0x84, 0xae, 0x80, 0xea, 0x2d, 0x40, /// point 3
    0xdc, 0xda, 0x96, 0x3f, 0x45, 0xf7, 0x47, 0x40
  ].parseWKB;

  geometry.type.should.equal("MultiLineString");
  geometry.coordinates.to!string.should.equal("[[
    [15.91005200861972,48.7654785634228],
    [21.08466395809631,47.38865429477588]],
    [[15.51803595184119,46.2776464037405],
    [12.15789832231093,47.50227588343512],
    [14.95801301358615,47.93180079332316]]]".parseJsonString.to!string);
}

/// A multi line generated with qfield
unittest {
  auto geometry = [ 0x01, 0x05, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00,
    0x00, 0x01, 0x02, 0x00, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00, 0xab, 0x92, 0x46, 0xb5, 0x29, 0xcd,
    0x2a, 0x40, 0xab, 0x80, 0x3b, 0x3d, 0xb2, 0x3c, 0x4a, 0x40, 0xa9, 0x1b, 0x4c, 0x91, 0xfb, 0xce,
    0x2a, 0x40, 0xdd, 0x3c, 0x74, 0x8c, 0x56, 0x3c, 0x4a, 0x40, 0xe7, 0x06, 0x53, 0xe4, 0x0a, 0xcf,
    0x2a, 0x40, 0x16, 0x4c, 0x91, 0x4f, 0x4b, 0x3c, 0x4a, 0x40, 0x47, 0x1a, 0xd5, 0x96, 0xf0, 0xd1,
    0x2a, 0x40, 0xdc, 0xc5, 0x79, 0xe8, 0x4f, 0x3c, 0x4a, 0x40, 0x47, 0x1a, 0xd5, 0x96, 0xf0, 0xd1,
    0x2a, 0x40, 0xdc, 0xc5, 0x79, 0xe8, 0x4f, 0x3c, 0x4a, 0x40
  ].parseWKB;

  geometry.type.should.equal("MultiLineString");
  geometry.coordinates.to!string.should.equal("[[
      [13.4007088326119,52.47418942838097],
      [13.40426305822423,52.47139125514671],
      [13.40437997354043,52.47104830355254],
      [13.41003867484427,52.47118860193197],
      [13.41003867484427,52.47118860193197]]]".parseJsonString.to!string);
}

/// A polygon representation in little endian WKB can be parsed
unittest {
  auto geometry = [ 0x01, /// little endian
    0x03, 0x0, 0x0, 0x0, /// geometry type
    0x01, 0x00, 0x00, 0x00, /// number of linear rings
    0x06, 0x00, 0x00, 0x00, /// number of points
    0x92, 0x5c, 0x3d, 0x56, 0xf2, 0xd1, 0x2f, 0x40, /// point 1
    0x51, 0xd8, 0x99, 0x33, 0xfb, 0x61, 0x48, 0x40,
    0x6f, 0x2c, 0x83, 0x89, 0xac, 0x15, 0x35, 0x40, /// point 2
    0x94, 0xc1, 0x86, 0x6c, 0xbf, 0xb1, 0x47, 0x40,
    0xca, 0x9e, 0x1e, 0x02, 0x3c, 0x09, 0x2f, 0x40, /// point 3
    0x70, 0xf5, 0xd7, 0xea, 0x89, 0x23, 0x47, 0x40,
    0x64, 0xd6, 0x84, 0x0c, 0xd8, 0x50, 0x28, 0x40, /// point 4
    0x30, 0x76, 0x7e, 0x93, 0x4a, 0xc0, 0x47, 0x40,
    0x64, 0xfd, 0x84, 0xae, 0x80, 0xea, 0x2d, 0x40, /// point 5
    0xdc, 0xda, 0x96, 0x3f, 0x45, 0xf7, 0x47, 0x40,
    0x92, 0x5c, 0x3d, 0x56, 0xf2, 0xd1, 0x2f, 0x40, /// point 1
    0x51, 0xd8, 0x99, 0x33, 0xfb, 0x61, 0x48, 0x40
  ].parseWKB;

  geometry.type.should.equal("Polygon");
  geometry.coordinates.to!string.should.equal("[[
    [15.91005200861972,48.7654785634228],
    [21.08466395809631,47.38865429477588],
    [15.51803595184119,46.2776464037405],
    [12.15789832231093,47.50227588343512],
    [14.95801301358615,47.93180079332316],
    [15.91005200861972,48.7654785634228]]]".parseJsonString.to!string);
}

/// A multi polygon representation in little endian WKB can be parsed
unittest {
  auto geometry = [ 0x01, /// little endian
    0x06, 0x0, 0x0, 0x0, /// geometry type
    0x02, 0x00, 0x00, 0x00, /// number of polygons
    0x01, /// little endian
    0x06, 0x0, 0x0, 0x0, /// geometry type
    0x01, 0x00, 0x00, 0x00, /// number of linear rings
    0x06, 0x00, 0x00, 0x00, /// number of points
    0x92, 0x5c, 0x3d, 0x56, 0xf2, 0xd1, 0x2f, 0x40, /// point 1
    0x51, 0xd8, 0x99, 0x33, 0xfb, 0x61, 0x48, 0x40,
    0x6f, 0x2c, 0x83, 0x89, 0xac, 0x15, 0x35, 0x40, /// point 2
    0x94, 0xc1, 0x86, 0x6c, 0xbf, 0xb1, 0x47, 0x40,
    0xca, 0x9e, 0x1e, 0x02, 0x3c, 0x09, 0x2f, 0x40, /// point 3
    0x70, 0xf5, 0xd7, 0xea, 0x89, 0x23, 0x47, 0x40,
    0x64, 0xd6, 0x84, 0x0c, 0xd8, 0x50, 0x28, 0x40, /// point 4
    0x30, 0x76, 0x7e, 0x93, 0x4a, 0xc0, 0x47, 0x40,
    0x64, 0xfd, 0x84, 0xae, 0x80, 0xea, 0x2d, 0x40, /// point 5
    0xdc, 0xda, 0x96, 0x3f, 0x45, 0xf7, 0x47, 0x40,
    0x92, 0x5c, 0x3d, 0x56, 0xf2, 0xd1, 0x2f, 0x40, /// point 1
    0x51, 0xd8, 0x99, 0x33, 0xfb, 0x61, 0x48, 0x40,
    0x01, /// little endian
    0x06, 0x0, 0x0, 0x0, /// geometry type
    0x01, 0x00, 0x00, 0x00, /// number of linear rings
    0x06, 0x00, 0x00, 0x00, /// number of points
    0x92, 0x5c, 0x3d, 0x56, 0xf2, 0xd1, 0x2f, 0x40, /// point 1
    0x51, 0xd8, 0x99, 0x33, 0xfb, 0x61, 0x48, 0x40,
    0x6f, 0x2c, 0x83, 0x89, 0xac, 0x15, 0x35, 0x40, /// point 2
    0x94, 0xc1, 0x86, 0x6c, 0xbf, 0xb1, 0x47, 0x40,
    0xca, 0x9e, 0x1e, 0x02, 0x3c, 0x09, 0x2f, 0x40, /// point 3
    0x70, 0xf5, 0xd7, 0xea, 0x89, 0x23, 0x47, 0x40,
    0x64, 0xd6, 0x84, 0x0c, 0xd8, 0x50, 0x28, 0x40, /// point 4
    0x30, 0x76, 0x7e, 0x93, 0x4a, 0xc0, 0x47, 0x40,
    0x64, 0xfd, 0x84, 0xae, 0x80, 0xea, 0x2d, 0x40, /// point 5
    0xdc, 0xda, 0x96, 0x3f, 0x45, 0xf7, 0x47, 0x40,
    0x92, 0x5c, 0x3d, 0x56, 0xf2, 0xd1, 0x2f, 0x40, /// point 1
    0x51, 0xd8, 0x99, 0x33, 0xfb, 0x61, 0x48, 0x40
  ].parseWKB;

  geometry.type.should.equal("MultiPolygon");
  geometry.coordinates.to!string.should.equal("[[[
    [15.91005200861972,48.7654785634228],
    [21.08466395809631,47.38865429477588],
    [15.51803595184119,46.2776464037405],
    [12.15789832231093,47.50227588343512],
    [14.95801301358615,47.93180079332316],
    [15.91005200861972,48.7654785634228]]],
    [[
    [15.91005200861972,48.7654785634228],
    [21.08466395809631,47.38865429477588],
    [15.51803595184119,46.2776464037405],
    [12.15789832231093,47.50227588343512],
    [14.95801301358615,47.93180079332316],
    [15.91005200861972,48.7654785634228]]]]".parseJsonString.to!string);
}

private @safe {

  size_t getSize(Position[] list) {
    return list.length * 2 * 8 + 4;
  }

  size_t getSize(Position[][] list) {
    size_t result = 4;

    foreach (item; list) {
      result += item.getSize;
    }

    return result;
  }

  size_t getSize(Position[][][] list) {
    size_t result = 4;

    foreach (item; list) {
      result += item.getSize;
    }

    return result;
  }
}

ubyte[] toWKB(GeoJsonGeometry geometry) @trusted {
  ubyte[] result;
  size_t index = 5;

  void add(T)(T value) {
    result.write!T(value, index);
    index += T.sizeof;
  }

  void addPosition(Position position) {
    add(position[0]);
    add(position[1]);
  }

  void addCoordinates(Position[] list) {
    add(list.length.to!uint);

    foreach (position; list) {
      addPosition(position);
    }
  }

  void addCoordinatesList(Position[][] list) {
    add(list.length.to!uint);

    foreach (item; list) {
      addCoordinates(item);
    }
  }

  switch(geometry.type) {
    case "Point":
      auto points = geometry.to!Point;
      result.length = 5 + 2 * 8;
      result.write!uint(1, 1);
      addPosition(points.coordinates);

      break;

    case "MultiPoint":
      auto multiPoints = geometry.to!MultiPoint;
      result.length = multiPoints.coordinates.getSize + 5 + multiPoints.coordinates.length * (ubyte.sizeof + uint.sizeof);
      result.write!uint(4, 1);

      add!uint(multiPoints.coordinates.length.to!uint);
      foreach(ref point; multiPoints.coordinates) {
        add!ubyte(0);
        add!uint(1);
        addPosition(point);
      }

      break;

    case "LineString":
      auto line = geometry.to!LineString;
      result.length = line.coordinates.getSize + 5;
      result.write!uint(2, 1);
      addCoordinates(line.coordinates);

      break;

    case "MultiLineString":
      auto multiLine = geometry.to!MultiLineString;
      result.length = multiLine.coordinates.getSize + 5 + multiLine.coordinates.length * (ubyte.sizeof + uint.sizeof);
      result.write!uint(5, 1);

      add!uint(multiLine.coordinates.length.to!uint);
      foreach(ref line; multiLine.coordinates) {
        add!ubyte(0);
        add!uint(2);
        addCoordinates(line);
      }

      break;

    case "Polygon":
      auto polygon = geometry.to!Polygon;
      result.length = polygon.coordinates.getSize + 5;
      result.write!uint(3, 1);
      addCoordinatesList(polygon.coordinates);

      break;

    case "MultiPolygon":
      auto multiPolygon = geometry.to!MultiPolygon;
      result.length = multiPolygon.coordinates.getSize + 5 + multiPolygon.coordinates.length * (ubyte.sizeof + uint.sizeof);
      result.write!uint(6, 1);

      add!uint(multiPolygon.coordinates.length.to!uint);
      foreach(ref polygon; multiPolygon.coordinates) {
        add!ubyte(0);
        add!uint(3);
        addCoordinatesList(polygon);
      }

      break;

    default:
  }

  return result;
}

/// It should convert a polygon to a WKB
unittest {
  auto geoJson = `{
    "type": "Polygon",
    "coordinates": [[[30, 10], [40, 40], [20, 40], [10, 20], [30, 10]]]
  }`.parseJsonString.deserializeJson!GeoJsonGeometry;

  ubyte[] expected = [0,
    0,   0, 0, 3,
    0,   0, 0, 1,
    0,   0, 0, 5,
    64, 62, 0, 0, 0, 0, 0, 0,
    64, 36, 0, 0, 0, 0, 0, 0,
    64, 68, 0, 0, 0, 0, 0, 0,
    64, 68, 0, 0, 0, 0, 0, 0,
    64, 52, 0, 0, 0, 0, 0, 0,
    64, 68, 0, 0, 0, 0, 0, 0,
    64, 36, 0, 0, 0, 0, 0, 0,
    64, 52, 0, 0, 0, 0, 0, 0,
    64, 62, 0, 0, 0, 0, 0, 0,
    64, 36, 0, 0, 0, 0, 0, 0 ];

  geoJson.toWKB.should.equal(expected);
}

/// It should convert a multi polygon to a WKB
unittest {
  auto geoJson = `{
    "type": "MultiPolygon",
    "coordinates": [[[[30, 10], [40, 40], [20, 40], [10, 20], [30, 10]]]]
  }`.parseJsonString.deserializeJson!GeoJsonGeometry;

  ubyte[] expected = [0,
    0, 0, 0, 6,
    0, 0, 0, 1,
    0,
    0, 0, 0, 3,
    0, 0, 0, 1,
    0, 0, 0, 5,
    64, 62, 0, 0, 0, 0, 0, 0,
    64, 36, 0, 0, 0, 0, 0, 0,
    64, 68, 0, 0, 0, 0, 0, 0,
    64, 68, 0, 0, 0, 0, 0, 0,
    64, 52, 0, 0, 0, 0, 0, 0,
    64, 68, 0, 0, 0, 0, 0, 0,
    64, 36, 0, 0, 0, 0, 0, 0,
    64, 52, 0, 0, 0, 0, 0, 0,
    64, 62, 0, 0, 0, 0, 0, 0,
    64, 36, 0, 0, 0, 0, 0, 0 ];

  geoJson.toWKB.should.equal(expected);
  geoJson.toWKB.parseWKB.toJson.to!string.should.equal(geoJson.toJson.to!string);
}

/// It should convert a multi line string to a WKB
unittest {
  auto geoJson = `{
    "type": "MultiLineString",
    "coordinates": [[[10, 10], [20, 20], [10, 40]], [[40, 40], [30, 30], [40, 20], [30, 10]]]
  }`.parseJsonString.deserializeJson!GeoJsonGeometry;

  ubyte[] expected = [0,
  0, 0, 0, 5,
  0, 0, 0, 2,
  0,
  0,   0, 0, 2,
  0,   0, 0, 3,
  64, 36, 0, 0, 0, 0, 0, 0,
  64, 36, 0, 0, 0, 0, 0, 0,
  64, 52, 0, 0, 0, 0, 0, 0,
  64, 52, 0, 0, 0, 0, 0, 0,
  64, 36, 0, 0, 0, 0, 0, 0,
  64, 68, 0, 0, 0, 0, 0, 0,
  0,
  0, 0, 0, 2,
  0, 0, 0, 4,
  64, 68, 0, 0, 0, 0, 0, 0,
  64, 68, 0, 0, 0, 0, 0, 0,
  64, 62, 0, 0, 0, 0, 0, 0,
  64, 62, 0, 0, 0, 0, 0, 0,
  64, 68, 0, 0, 0, 0, 0, 0,
  64, 52, 0, 0, 0, 0, 0, 0,
  64, 62, 0, 0, 0, 0, 0, 0,
  64, 36, 0, 0, 0, 0, 0, 0];

  geoJson.toWKB.should.equal(expected);
  geoJson.toWKB.parseWKB.toJson.to!string.should.equal(geoJson.toJson.to!string);
}

/// It should convert a line string to a WKB
unittest {
  auto geoJson = `{
    "type": "LineString",
    "coordinates": [[30.1, 10.1], [40.1, 50.1]]
  }`.parseJsonString.deserializeJson!GeoJsonGeometry;

  ubyte[] expected = [0,
    0,   0,  0, 2,
    0,   0,  0, 2,
    64, 62, 25, 153, 153, 153, 153, 154,
    64, 36, 51,  51,  51,  51,  51,  51,
    64, 68, 12, 204, 204, 204, 204, 205,
    64, 73, 12, 204, 204, 204, 204, 205];

  geoJson.toWKB.should.equal(expected);
}

/// It should convert a MultiPoint to a WKB
unittest {
  auto geoJson = `{
    "type": "MultiPoint",
    "coordinates": [[30.1, 10.1], [35.1, 15.1]]
  }`.parseJsonString.deserializeJson!GeoJsonGeometry;

  ubyte[] expected = [
    0,
    0, 0, 0, 4,
    0, 0, 0, 2,
    0,
    0, 0, 0, 1,
    64, 62, 25, 153, 153, 153, 153, 154,
    64, 36, 51, 51, 51, 51, 51, 51,
    0,
    0, 0, 0, 1,
    64, 65, 140, 204, 204, 204, 204, 205,
    64, 46, 51, 51, 51, 51, 51, 51];

  geoJson.toWKB.should.equal(expected);
  geoJson.toWKB.parseWKB.toJson.to!string.should.equal(geoJson.toJson.to!string);
}

/// It should convert a Point to a WKB
unittest {
  auto geoJson = `{
    "type": "Point",
    "coordinates": [30.1, 10.1]
  }`.parseJsonString.deserializeJson!GeoJsonGeometry;

  ubyte[] expected = [0,
    0, 0, 0, 1,
    64, 62, 25, 153, 153, 153, 153, 154,
    64, 36, 51, 51, 51, 51, 51, 51];

  geoJson.toWKB.should.equal(expected);
}


/// It should convert a Point to a WKB and back
unittest {
  auto geoJson = `{
    "type": "Point",
    "coordinates": [30.1, 10.1]
  }`.parseJsonString.deserializeJson!GeoJsonGeometry;

  ubyte[] expected = [0,
    0, 0, 0, 1,
    64, 62, 25, 153, 153, 153, 153, 154,
    64, 36, 51, 51, 51, 51, 51, 51];

  geoJson.toWKB.parseWKB.toJson.to!string
    .should.equal(geoJson.toJson.to!string);
}

///
class WKBException : Exception {
  this(string msg = null, Throwable next = null, string file = __FILE__, size_t line = __LINE__) @safe {
    super(msg, next, file, line);
  }

  this(string msg, string file, size_t line, Throwable next = null) @safe {
    super(msg, file, line, next);
  }
}
