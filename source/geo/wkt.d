/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
/// Implementation for the well known text representation
/// https://en.wikipedia.org/wiki/Well-known_text_representation_of_geometry
module geo.wkt;

import geo.geometries;
import geo.json;
import std.algorithm;
import std.string;
import std.array;
import std.exception;
import std.conv;
import std.format : format;

version(unittest) {
  import fluent.asserts;
  import vibe.data.json;
}

///
GeoJsonGeometry parseWKT(const string value) @trusted {
  BasicGeometryVariant result;

  auto begin = value.indexOf("(");
  auto end = value.lastIndexOf(")");

  enforce!WKTException(begin > -1 && end != -1, "Missing WKT paranthesis: " ~ value);
  enforce!WKTException(begin < end, "Got invalid WKT paranthesis: " ~ value);

  auto type = value[0..begin].strip.toLower;

  switch(type) {
    case "point":
      auto points = value[begin+1 .. end].parseCoordinates;
      enforce!WKTException(points.length > 0, "Got a point with no coordinates.");
      result = Point(points[0]);
      break;

    case "linestring":
      auto points = value[begin+1 .. end].parseCoordinates;
      enforce!WKTException(points.length > 0, "Got a linestring with no points. There should be at least 2.");
      enforce!WKTException(points.length > 1, "Got a linestring with one point. There should be at least 2.");

      result = LineString(points);
      break;

    case "multilinestring":
      auto points = value[begin+1 .. end].parseGroupCoordinates!1;

      result = MultiLineString(points);
      break;

    case "polygon":
      auto points = value[begin+1 .. end].parseGroupCoordinates!1;

      result = Polygon(points);
      break;

    case "multipolygon":
      auto points = value[begin+1 .. end].parseGroupCoordinates!2;

      result = MultiPolygon(points);
      break;

    default:
      throw new WKTException("Got invalid WKT type: " ~ type);
  }

  return GeoJsonGeometry(result);
}

/// It should parse a POINT geometry with int coordinates
unittest {
  "POINT (30 10)".parseWKT.type.should.equal("Point");
  "POINT (30 10)".parseWKT.coordinates.should.equal("[30., 10.]".parseJsonString);
}

/// It should throw an error for a point with no coordinates
unittest {
  should({
    "POINT()".parseWKT;
  }).throwException!WKTException.withMessage("Got a point with no coordinates.");
}

/// It should parse a POINT with 4 float values
unittest {
  "POINT(30.1234 10.1234 300.1234 50)".parseWKT.type.should.equal("Point");
  "POINT(30.1234 10.1234 300.1234 50)".parseWKT.coordinates.should.equal("[30.1234, 10.1234, 300.1234, 50.]".parseJsonString);
}

/// It should parse a LINESTRING with 2 point values
unittest {
  "LINESTRING(30.1234 10.1234, 30.2 50.4)".parseWKT.type.should.equal("LineString");
  "LINESTRING(30.1234 10.1234, 30.2 50.4)".parseWKT.coordinates.should.equal("[[30.1234,10.1234], [30.2,50.4]]".parseJsonString);
}

/// It should throw an error for a LINESTRING with no coordinates
unittest {
  should({
    "LINESTRING()".parseWKT;
  }).throwException!WKTException.withMessage("Got a linestring with no points. There should be at least 2.");
}

/// It should throw an error for a LINESTRING with one point
unittest {
  should({
    "LINESTRING(1 2)".parseWKT;
  }).throwException!WKTException.withMessage("Got a linestring with one point. There should be at least 2.");
}

/// It should parse a MULTILINESTRING with two lines with two points
unittest {
  "MULTILINESTRING((10 10, 20 20),(40 40, 30 30))".parseWKT.type.should.equal("MultiLineString");
  "MULTILINESTRING((10 10, 20 20),(40 40, 30 30))".parseWKT.coordinates.to!string
    .should.equal("[[[10,10],[20,20]],[[40,40],[30,30]]]");
}

/// It should parse a POLYGON with 5 points
unittest {
  "POLYGON((30 10, 40 40, 20 40, 10 20, 30 10))".parseWKT.type.should.equal("Polygon");
  "POLYGON((30 10, 40 40, 20 40, 10 20, 30 10))".parseWKT.coordinates.to!string
    .should.equal("[[[30,10],[40,40],[20,40],[10,20],[30,10]]]");
}

/// It should parse a POLYGON with a hole
unittest {
  "POLYGON((35 10, 45 45, 15 40, 10 20, 35 10),(20 30, 35 35, 30 20, 20 30))".parseWKT.type.should.equal("Polygon");
  "POLYGON((35 10, 45 45, 15 40, 10 20, 35 10),(20 30, 35 35, 30 20, 20 30))".parseWKT.coordinates.to!string
    .should.equal("[[[35,10],[45,45],[15,40],[10,20],[35,10]],[[20,30],[35,35],[30,20],[20,30]]]");
}

/// It should parse a MULTIPOLYGON
unittest {
  "MULTIPOLYGON(((30 20, 45 40, 10 40, 30 20)),((15 5, 40 10, 10 20, 5 10, 15 5)))".parseWKT.type.should.equal("MultiPolygon");
  "MULTIPOLYGON(((30 20, 45 40, 10 40, 30 20)),((15 5, 40 10, 10 20, 5 10, 15 5)))".parseWKT.coordinates.to!string
    .should.equal("[[[[30,20],[45,40],[10,40],[30,20]]],[[[15,5],[40,10],[10,20],[5,10],[15,5]]]]");
}

/// It should throw an exception when an invalid type is found
unittest {
  should({
    "INVALID(30.1234 10.1234 300.1234 50)".parseWKT;
  }).throwException!WKTException.withMessage("Got invalid WKT type: invalid");
}

/// It should throw an exception when the last paranthesis is missing
unittest {
  should({
    "POINT(30.1234 10.1234 300.1234 50".parseWKT;
  }).throwException!WKTException.withMessage("Missing WKT paranthesis: POINT(30.1234 10.1234 300.1234 50");
}

/// It should throw an exception when the paranthesis are not in the right order
unittest {
  should({
    "POINT)30.1234 10.1234 300.1234 50(".parseWKT;
  }).throwException!WKTException.withMessage("Got invalid WKT paranthesis: POINT)30.1234 10.1234 300.1234 50(");
}

/// parse the wkt coordinates
Position[] parseCoordinates(const string coordinates) @safe {
  try {
    return coordinates.split(",")
      .filter!(a => a != "")
      .map!(points => Position.fromRange(points.split(" ").filter!(a => a != "").map!(a => a.strip.to!double)))
      .array;
  } catch(Exception e) {
    throw new WKTException("Got invalid WKT coordinates: " ~ coordinates);
  }
}

/// It should parse two ints
unittest {
  "10 20".parseCoordinates.to!string.should.equal("[[10, 20]]");
}

/// It should parse 4 doubles
unittest {
  "10.3 20.5 2.4 4".parseCoordinates.to!string.should.equal("[[10.3, 20.5, 2.4, 4]]");
}

/// It should parse two points
unittest {
  "10 20, 30.4 40.9".parseCoordinates.to!string.should.equal("[[10, 20], [30.4, 40.9]]");
}

/// It should parse two points with white spaces
unittest {
  "  10    20  , 30.4   40.9   ".parseCoordinates.to!string.should.equal("[[10, 20], [30.4, 40.9]]");
}

/// It should throw an exception for invalid values
unittest {
  should({
    "abc".parseCoordinates;
  }).throwException!WKTException.withMessage("Got invalid WKT coordinates: abc");
}

///
auto parseGroupCoordinates(uint levels)(const string coordinates) @safe {
  static if(levels == 1) {
    Position[][] groups;
  }

  static if(levels == 2) {
    Position[][][] groups;
  }

  long index;
  size_t begin;

  foreach(i; 0..coordinates.length) {
    if(coordinates[i] == '(') {
      if(index == 0) {
        begin = i;
      }

      index++;
    }

    if(coordinates[i] == ')') {
      index--;

      if(index == 0) {
        static if(levels == 1) {
          groups ~= coordinates[begin+1..i].parseCoordinates;
        } else {
          groups ~= coordinates[begin+1..i].parseGroupCoordinates!(levels - 1);
        }
      }
    }
  }

  enforce!WKTException(index == 0, "Got invalid WKT group list: " ~ coordinates);

  return groups;
}

/// It should parse two groups
unittest {
  "(),()".parseGroupCoordinates!1.length.should.equal(2);
}

/// It should parse two groups with two points
unittest {
  "(1 2, 3 4),(5 6, 7 8)".parseGroupCoordinates!1.should.equal([[[1., 2.],[3., 4.]], [[5., 6.],[7., 8.]]].toPositions);
}

/// It should throw an exception when the end paranthesis for the secong group is missing
unittest {
  should({
    "(1 2, 3 4),(5 6, 7 8".parseGroupCoordinates!1;
  }).throwException!WKTException.withMessage("Got invalid WKT group list: (1 2, 3 4),(5 6, 7 8");
}

/// It should throw an exception when the end paranthesis for the first group is missing
unittest {
  should({
    "(1 2, 3 4,(5 6, 7 8)".parseGroupCoordinates!1;
  }).throwException!WKTException.withMessage("Got invalid WKT group list: (1 2, 3 4,(5 6, 7 8)");
}


/// It should throw an exception when the begin paranthesis for the first group is missing
unittest {
  should({
    "1 2, 3 4),(5 6, 7 8)".parseGroupCoordinates!1;
  }).throwException!WKTException.withMessage("Got invalid WKT group list: 1 2, 3 4),(5 6, 7 8)");
}

///
class WKTException : Exception {
  this(string msg = null, Throwable next = null, string file = __FILE__, size_t line = __LINE__) @safe {
    super(msg, next, file, line);
  }

  this(string msg, string file, size_t line, Throwable next = null) @safe {
    super(msg, file, line, next);
  }
}

/// Serialize to Well known string format
string toWKTString(const double[] point) @safe {
  return point.map!(a => format("%.15G", a)).joiner(" ").array.to!string;
}

/// ditto
string toWKTString(const Position position) @safe {
  return format("%.15G %.15G", position.lon, position.lat);
}

/// ditto
string toWKTString(const double[][] points) @safe {
  return points.map!(a => a.toWKTString).joiner(", ").array.to!string;
}

/// ditto
string toWKTString(const Position[] points) @safe {
  return points.map!(a => a.toWKTString).joiner(", ").array.to!string;
}

/// ditto
string toWKTString(const double[][][] points) @safe {
  return points.map!(a => "(" ~ a.toWKTString ~ ")").joiner(", ").array.to!string;
}

/// ditto
string toWKTString(const Position[][] points) @safe {
  return points.map!(a => "(" ~ a.toWKTString ~ ")").joiner(", ").array.to!string;
}

/// ditto
string toWKTString(const double[][][][] points) @safe {
  return points.map!(a => "(" ~ a.toWKTString ~ ")").joiner(", ").array.to!string;
}

/// ditto
string toWKTString(const Position[][][] points) @safe {
  return points.map!(a => "(" ~ a.toWKTString ~ ")").joiner(", ").array.to!string;
}

/// ditto
string toWKTString(GeoJsonGeometry geometry) @safe {
  switch(geometry.type) {
    case "Point":
      return "POINT(" ~ geometry.to!Point.coordinates.toWKTString ~ ")";

    case "MultiPoint":
      return "MULTIPOINT(" ~ geometry.to!MultiPoint.coordinates.toWKTString ~ ")";

    case "LineString":
      return "LINESTRING(" ~ geometry.to!LineString.coordinates.toWKTString ~ ")";

    case "MultiLineString":
      return "MULTILINESTRING(" ~ geometry.to!MultiLineString.coordinates.toWKTString ~ ")";

    case "Polygon":
      return "POLYGON(" ~ geometry.to!Polygon.coordinates.toWKTString ~ ")";

    case "MultiPolygon":
      return "MULTIPOLYGON(" ~ geometry.to!MultiPolygon.coordinates.toWKTString ~ ")";

    default:
  }

  return "";
}


/// It should convert a polygon to a WKT string
unittest {
  auto geoJson = `{
    "type": "Polygon",
    "coordinates": [[[30, 10], [40, 40], [20, 40], [10, 20], [30, 10]]]
  }`.parseJsonString.deserializeJson!GeoJsonGeometry;

  geoJson.toWKTString.should.equal("POLYGON((30 10, 40 40, 20 40, 10 20, 30 10))");
}

/// It should convert a multi polygon to a WKT string
unittest {
  auto geoJson = `{
    "type": "MultiPolygon",
    "coordinates": [[[[30, 10], [40, 40], [20, 40], [10, 20], [30, 10]]]]
  }`.parseJsonString.deserializeJson!GeoJsonGeometry;

  geoJson.toWKTString.should.equal("MULTIPOLYGON(((30 10, 40 40, 20 40, 10 20, 30 10)))");
}

/// It should convert a multi line string to a WKT string
unittest {
  auto geoJson = `{
    "type": "MultiLineString",
    "coordinates": [[[10, 10], [20, 20], [10, 40]], [[40, 40], [30, 30], [40, 20], [30, 10]]]
  }`.parseJsonString.deserializeJson!GeoJsonGeometry;

  geoJson.toWKTString.should.equal("MULTILINESTRING((10 10, 20 20, 10 40), (40 40, 30 30, 40 20, 30 10))");
}

/// It should convert a line string to a WKT string
unittest {
  auto geoJson = `{
    "type": "LineString",
    "coordinates": [[30.1, 10.1], [40.1, 50.1]]
  }`.parseJsonString.deserializeJson!GeoJsonGeometry;

  geoJson.toWKTString.should.equal("LINESTRING(30.1 10.1, 40.1 50.1)");
}

/// It should convert a Point to a WKT string
unittest {
  auto geoJson = `{
    "type": "Point",
    "coordinates": [30.1, 10.1]
  }`.parseJsonString.deserializeJson!GeoJsonGeometry;

  geoJson.toWKTString.should.equal("POINT(30.1 10.1)");
}

/// It should convert a MultiPoint to a WKT string
unittest {
  auto geoJson = `{
    "type": "MultiPoint",
    "coordinates": [[30.1, 10.1], [35.1, 15.1]]
  }`.parseJsonString.deserializeJson!GeoJsonGeometry;

  geoJson.toWKTString.should.equal("MULTIPOINT(30.1 10.1, 35.1 15.1)");
}
