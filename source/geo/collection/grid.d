/**
  Copyright: © 2019-2022 GISCollective
  License: Subject to the terms of the MIT LICENSE, as written in the included LICENSE file.
*/
module geo.collection.grid;

import geo.conv;
import geo.feature;

import std.algorithm;
import std.array;

version(unittest) {
  import fluent.asserts;
}

///
struct PointGrid(T) {
  alias PointList = T[];

  /// The square grid size
  public long distance = 1;

  private {
    PointList[long][long] storage;
  }

  ///
  void add(T)(T feature) {
    auto position = feature.longPosition;
    auto x = position[0] / distance;
    auto y = position[1] / distance;

    storage[x][y] ~= feature;
  }

  ///
  T[][] groups() {
    return storage.byValue.map!(a => a.byValue).joiner.array;
  }
}

/// Adding the same point it should be in the same cell
unittest {
  import vibe.data.json;

  PointGrid!Feature grid;

  Feature feature = `{
    "type": "Feature",
    "geometry": {
    "type": "Point",
    "coordinates": [ 0.0, 0.0 ] },
    "properties": {
    "name": "Dinagat Islands"
    }}`.parseJsonString.deserializeJson!Feature;

  grid.add(feature);
  grid.add(feature);

  grid.groups.length.should.equal(1);
  grid.groups.should.equal([ [ feature, feature ] ]);
}

/// Adding the two points in different cells
unittest {
  import vibe.data.json;

  PointGrid!Feature grid;
  grid.distance = 1;

  Feature feature1 = `{
    "type": "Feature",
    "geometry": {
    "type": "Point",
    "coordinates": [ 0.0, 0.0 ] },
    "properties": {
    "name": "Dinagat Islands"
    }}`.parseJsonString.deserializeJson!Feature;

  Feature feature2 = `{
    "type": "Feature",
    "geometry": {
    "type": "Point",
    "coordinates": [ 1.0, 1.0 ] },
    "properties": {
    "name": "Dinagat Islands"
    }}`.parseJsonString.deserializeJson!Feature;

  grid.add(feature1);
  grid.add(feature2);

  grid.groups.length.should.equal(2);
  grid.groups.should.equal([ [ feature1 ], [ feature2 ] ]);
}


/// Adding the two points in the same radius should be in the same cell
unittest {
  import vibe.data.json;

  PointGrid!Feature grid;
  grid.distance = 120_000;

  Feature feature1 = `{
    "type": "Feature",
    "geometry": {
    "type": "Point",
    "coordinates": [ 0.0, 0.0 ] },
    "properties": {
    "name": "Dinagat Islands"
    }}`.parseJsonString.deserializeJson!Feature;

  Feature feature2 = `{
    "type": "Feature",
    "geometry": {
    "type": "Point",
    "coordinates": [ 1.0, 1.0 ] },
    "properties": {
    "name": "Dinagat Islands"
    }}`.parseJsonString.deserializeJson!Feature;

  grid.add(feature1);
  grid.add(feature2);

  grid.groups.length.should.equal(1);
  grid.groups.should.equal([ [ feature1, feature2 ] ]);
}
